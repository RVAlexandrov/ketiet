//
//  AppGroups.swift
//  DietCore
//
//  Created by Kovalev Alexander on 23.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

enum AppGroups {
    static let extensions = "group.ketiet.ketietExtensions"
}
