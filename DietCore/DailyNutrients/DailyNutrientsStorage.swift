//
//  DailyNutrientsStorage.swift
//  DietCore
//
//  Created by Kovalev Alexander on 23.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

public final class DailyNutrientsStorage: WidgetDataStorage<NutritionMetrics> {
    
    public init() {
        super.init(widget: .dailyNutrients)
    }
}
