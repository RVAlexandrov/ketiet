//
//  Double+Convenience.swift
//  DietCore
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

public extension Double {
    func convert(from: Dimension, to: Dimension) -> Double {
        Measurement(value: self, unit: from).converted(to: to).value
    }
}
