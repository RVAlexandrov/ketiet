//
//  Float+Convenience.swift
//  DietCore
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation
import Darwin

public extension Float {
    
    var cleanValue: String {
        String(format: "%.0f", self)
    }
    
    func round(to places: Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
    
    func convert(from: Dimension, to: Dimension) -> Float {
        Float(Measurement(value: Double(self), unit: from).converted(to: to).value)
    }
    
    func measurementRound() -> Float {
        Darwin.round(self * 10) / 10
    }
}
