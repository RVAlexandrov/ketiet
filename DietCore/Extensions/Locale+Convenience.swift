//
//  Locale+Convenience.swift
//  DietCore
//
//  Created by Kovalev Alexander on 25.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

public extension Locale {
    func isRu() -> Bool {
        identifier.lowercased().contains("ru")
    }
}
