//
//  NumberFormatter+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 02.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

public extension NumberFormatter {
	
	static func formatterForSignedIntegerValues() -> NumberFormatter {
		let formatter = NumberFormatter.formatterForIntegerValues()
		formatter.addPrefixesForSignedValues()
		return formatter
	}
	
	static func formatterForIntegerValues() -> NumberFormatter {
		let formatter = NumberFormatter()
		formatter.minimumFractionDigits = 0
		formatter.maximumFractionDigits = 0
		formatter.minimumIntegerDigits = 1
		return formatter
	}
	
	static func formatterForSignedFloatingPointValues() -> NumberFormatter {
		let formatter = formatterForFloatingPointValues()
		formatter.addPrefixesForSignedValues()
		return formatter
	}
	
	static func formatterForFloatingPointValues() -> NumberFormatter {
		let formatter = NumberFormatter()
		formatter.maximumFractionDigits = 1
		formatter.minimumIntegerDigits = 1
		return formatter
	}
    
    static func formatPercent(value: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        return formatter.string(from: NSNumber(value: value)) ?? "Unknown"
    }
	
	private func addPrefixesForSignedValues() {
		positivePrefix = "+"
		negativePrefix = "-"
	}
}
