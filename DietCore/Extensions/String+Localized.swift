//
//  String+Localized.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

public extension String {
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
    func mealLocalized() -> String {
        NSLocalizedString(self,
                          tableName: "MealLocalizable",
                          bundle: Bundle.main,
                          value: "",
                          comment: "")
    }
}
