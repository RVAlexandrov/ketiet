//
//  MeasurementService.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 28.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

/// Assume, that storage uses metric system
public final class MeasurementService {

	private let measurementFormatter = MeasurementFormatter()
	private let measurementType: MeasurementType
	
	public init(
        type: MeasurementType,
        unitStyle: MeasurementFormatter.UnitStyle = .medium,
        unitOptions: MeasurementFormatter.UnitOptions = [.providedUnit, .naturalScale]
    ) {
		measurementFormatter.unitStyle = unitStyle
		measurementFormatter.unitOptions = unitOptions
		measurementType = type
	}
}

extension MeasurementService: MeasurementServiceProtocol {
    
	public func measurement() -> String {
		measurementFormatter.string(from: measurementType.currentLocaleUnit)
	}
	
    public func convertToStorage(value: Float) -> Float {
        value.convert(
            from: measurementType.currentLocaleUnit,
            to: measurementType.metricUnit
        )
    }
	
    public func convertToLocale(value: Float) -> Float {
        value.convert(
            from: measurementType.metricUnit,
            to: measurementType.currentLocaleUnit
        ).measurementRound()
    }
	
    public func format(value: Float) -> String {
        measurementFormatter.string(
            from: Measurement(
                value: Double(value),
                unit: measurementType.currentLocaleUnit
            )
        )
    }
}
