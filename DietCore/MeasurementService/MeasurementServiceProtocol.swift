//
//  MeasurementServiceProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 28.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

public protocol MeasurementServiceProtocol {
	func measurement() -> String
	func convertToStorage(value: Float) -> Float
	func convertToLocale(value: Float) -> Float
	func format(value: Float) -> String
}
