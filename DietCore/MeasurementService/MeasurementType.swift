//
//  MeasurementType.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 28.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

public enum MeasurementType {
	case tall
	case weight
	case volume
	case lenght
	case smallWeight
	case energy
	
	public var metricUnit: Dimension {
		switch self {
		case .lenght:
			return UnitLength.centimeters
		case .volume:
			return UnitVolume.milliliters
		case .weight:
			return UnitMass.kilograms
		case .smallWeight:
			return UnitMass.grams
		case .energy:
			return UnitEnergy.kilocalories
		case .tall:
			return UnitLength.centimeters
		}
	}
	
	public var nonMetricUnit: Dimension {
		switch self {
		case .lenght:
			return UnitLength.inches
		case .volume:
			return UnitVolume.fluidOunces
		case .weight:
			return UnitMass.pounds
		case .smallWeight:
			return UnitMass.grams
		case .energy:
			return UnitEnergy.kilocalories
		case .tall:
			return UnitLength.feet
		}
	}
    
    public var currentLocaleUnit: Dimension {
        Locale.current.usesMetricSystem ? metricUnit : nonMetricUnit
    }
}

