//
//  PhysicalActivity.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

public enum PhysicalActivity: String, CaseIterable {
	case no
	case oneTwoPerWeek
	case threeFivePerWeek
	case everyDay
	case professional
	
	public init?(description: String) {
		if description == PhysicalActivity.no.description {
			self = .no
		} else if description == PhysicalActivity.oneTwoPerWeek.description {
			self = .oneTwoPerWeek
		} else if description == PhysicalActivity.threeFivePerWeek.description {
			self = .threeFivePerWeek
		} else if description == PhysicalActivity.everyDay.description {
			self = .everyDay
		} else if description == PhysicalActivity.professional.description {
			self = .professional
		} else {
			return nil
		}
	}
	
	public func harrisBenedictActivityMultiplier() -> Float {
		switch self {
		case .no:
			return 1.2
		case .oneTwoPerWeek:
			return 1.375
		case .threeFivePerWeek:
			return 1.55
		case .everyDay:
			return 1.725
		case .professional:
			return 1.99
		}
	}
    
    public func mifflinActivityMultiplier() -> Float {
        switch self {
        case .no:
            return 1.2
        case .oneTwoPerWeek:
            return 1.375
        case .threeFivePerWeek:
            return 1.55
        case .everyDay:
            return 1.725
        case .professional:
            return 1.99
        }
    }
	
	public var imageName: String {
		switch self {
		case .no:
			return "activity0"
		case .oneTwoPerWeek:
			return "activity1"
		case .threeFivePerWeek:
			return "activity2"
		case .everyDay:
			return "activity3"
		case .professional:
		return "activity4"
		}
	}
    
    public var descriptionString: String {
        switch self {
        case .no:
            return "NO_ACTIVITY_DESCR".localized()
        case .oneTwoPerWeek:
            return "ONE_TWO_ACTIVITY_DESCR".localized()
        case .threeFivePerWeek:
            return "THREE_FIVE_ACTIVITY_DESCR".localized()
        case .everyDay:
            return "EVERY_DAY_ACTIVITY_DESCR".localized()
        case .professional:
            return "PROF_ACTIVITY_DESCR".localized()
        }
    }
	
	public var description: String {
		switch self {
		case .no:
			return "NO_ACTIVITY".localized()
		case .oneTwoPerWeek:
			return "ONE_TWO_TIMES_PER_WEEK".localized()
		case .threeFivePerWeek:
			return "THREE_FIVE_TIMES_PER_WEEK".localized()
		case .everyDay:
			return "EVERY_DAY".localized()
		case .professional:
			return "PROFESSIONAL".localized()
		}
	}
}
