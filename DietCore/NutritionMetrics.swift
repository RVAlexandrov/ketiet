//
//  NutritionMetrics.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

public struct NutritionMetrics: WidgetDataProtocol {
    
    public static func zero() -> NutritionMetrics {
        NutritionMetrics(
            currentCalories: 0,
            targetCalories: 0,
            currentFats: 0,
            targetFats: 0,
            currentProteins: 0,
            targetProteins: 0,
            currentCarbohydrates: 0,
            targetCarbohydrates: 0
        )
    }
    public let currentCalories: Float
    public let targetCalories: Float
    
    public let currentFats: Float
    public let targetFats: Float
    
    public let currentProteins: Float
    public let targetProteins: Float
    
    public let currentCarbohydrates: Float
    public let targetCarbohydrates: Float
    
    public init(
        currentCalories: Float,
        targetCalories: Float,
        currentFats: Float,
        targetFats: Float,
        currentProteins: Float,
        targetProteins: Float,
        currentCarbohydrates: Float,
        targetCarbohydrates: Float
    ) {
        self.currentCalories = currentCalories
        self.targetCalories = targetCalories
        self.currentFats = currentFats
        self.targetFats = targetFats
        self.currentProteins = currentProteins
        self.targetProteins = targetProteins
        self.currentCarbohydrates = currentCarbohydrates
        self.targetCarbohydrates = targetCarbohydrates
    }
    
    public func caloriesPercent() -> Float {
        targetCalories.isZero ? 0 : currentCalories/targetCalories
    }
    
    public func proteinsPercent() -> Float {
        targetProteins.isZero ? 0 : currentProteins/targetProteins
    }
    
    public func carbohydratesPercent() -> Float {
        targetCarbohydrates.isZero ? 0 : currentCarbohydrates/targetCarbohydrates
    }
    
    public func fatsPercent() -> Float {
        targetFats.isZero ? 0 : currentFats/targetFats
    }
}
