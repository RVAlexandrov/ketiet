//
//  DailyWaterConsumptionCalculator.swift
//  DietCore
//
//  Created by Kovalev Alexander on 30.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

public struct DailyWaterConsumptionCalculator {
    
    private enum Constants {
        static let defaultMetricWaterLevel = 2000.0
        static let maleWaterMultiplier = 40.0
        static let femaleWaterMuutiplier = 30.0
    }
    
    public init() {}
    
    public func targetWaterVolume(
        physicalActivity: PhysicalActivity?,
        isMale: Bool,
        weight: Double?
    ) -> Double {
        guard let physicalActivity = physicalActivity,
                let weight = weight else {
            return defaultWaterLevel()
        }
        let metricWeight = weight.convert(
            from: MeasurementType.weight.currentLocaleUnit,
            to: MeasurementType.weight.metricUnit
        )
        let metricWater = metricWeight * (isMale ? Constants.maleWaterMultiplier : Constants.femaleWaterMuutiplier)
        let resultMetricWater = metricWater + physicalActivity.additionalMilliliters
        return resultMetricWater.convert(
            from: MeasurementType.volume.metricUnit,
            to: MeasurementType.volume.currentLocaleUnit
        )
    }
    
    private func defaultWaterLevel() -> Double {
        Constants.defaultMetricWaterLevel.convert(
            from: MeasurementType.volume.metricUnit,
            to: MeasurementType.volume.currentLocaleUnit
        )
    }
}

private extension PhysicalActivity {
    var additionalMilliliters: Double {
        switch self {
        case .no:
            return 0
        case .oneTwoPerWeek:
            return 200
        case .threeFivePerWeek:
            return 400
        case .everyDay:
            return 800
        case .professional:
            return 1000
        }
    }
}
