//
//  WaterConsumptionStorage.swift
//  DietCore
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

public final class WaterConsumptionStorage: WidgetDataStorage<WaterConsumptionMetrics> {

    public init() {
        super.init(widget: .waterConsumption)
    }
}
