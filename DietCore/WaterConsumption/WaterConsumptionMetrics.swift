//
//  WaterConsumptionMetrics.swift
//  DietCore
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

public struct WaterConsumptionMetrics: WidgetDataProtocol {
    public static func zero() -> WaterConsumptionMetrics {
        WaterConsumptionMetrics(
            currentWater: 0,
            targetWater: 0
        )
    }
    
    public let currentWater: Double
    public let targetWater: Double
    
    public init(
        currentWater: Double,
        targetWater: Double
    ) {
        self.currentWater = currentWater
        self.targetWater = targetWater
    }
    
    public func percentComplete() -> Double {
        targetWater != .zero ? currentWater / targetWater : .zero
    }
}
