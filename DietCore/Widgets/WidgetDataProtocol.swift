//
//  WidgetDataProtocol.swift
//  DietCore
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

public protocol WidgetDataProtocol: Hashable, Codable {
    static func zero() -> Self
}
