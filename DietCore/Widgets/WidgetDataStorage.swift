//
//  WidgetDataStorage.swift
//  DietCore
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import WidgetKit

public class WidgetDataStorage<Data: WidgetDataProtocol> {
    
    public var widgetData: Data {
        didSet {
            guard widgetData != oldValue else { return }
            guard let data = try? JSONEncoder().encode(widgetData),
                  let dateData = try? JSONEncoder().encode(Date()) else {
                assertionFailure("Unable to encode data")
                return
            }
            userDefaults?.set(data, forKey: widget.storageKey)
            userDefaults?.set(dateData, forKey: widget.updateDateKey)
            userDefaults?.synchronize()
            WidgetCenter.shared.reloadTimelines(ofKind: widget.kind)
        }
    }
    
    private let userDefaults = UserDefaults(suiteName: AppGroups.extensions)
    private let widget: WidgetType
    
    init(widget: WidgetType) {
        self.widget = widget
        let calendar = Calendar.current
        guard let widgetData = userDefaults?.data(forKey: widget.storageKey),
              let dateData = userDefaults?.data(forKey: widget.updateDateKey),
              let result = try? JSONDecoder().decode(Data.self, from: widgetData),
              let date = try? JSONDecoder().decode(Date.self, from: dateData),
              calendar.component(.day, from: date) == calendar.component(.day, from: Date()) else {
                  self.widgetData = .zero()
                  return
              }
        self.widgetData = result
    }
}
