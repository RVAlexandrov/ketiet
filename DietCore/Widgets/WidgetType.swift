//
//  WidgetsConstants.swift
//  DietCore
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

public enum WidgetType {
    case waterConsumption
    case dailyNutrients
    
    public var kind: String {
        switch self {
        case .waterConsumption:
            return "com.ketiet.waterConsumption"
        case .dailyNutrients:
            return "com.ketiet.dailyNutrients"
        }
    }
    
    var storageKey: String {
        switch self {
        case .waterConsumption:
            return "waterConsumptionKey"
        case .dailyNutrients:
            return "dailyNutrientsKey"
        }
    }
    
    var updateDateKey: String {
        switch self {
        case .waterConsumption:
            return "waterConsumptionUpdateDateKey"
        case .dailyNutrients:
            return "dailyNutrientsUpdateDateKey"
        }
    }
}
