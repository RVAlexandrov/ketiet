//
//  AppDelegate.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private var dailyNutrientsUpdater: DailyNutrientsUpdater?
    private var waterConsumptionUpdater: WaterConsumptionUpdater?
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        RemindersExporter.shared.startListeningMealDatesUpdates()
        dailyNutrientsUpdater = DailyNutrientsUpdater()
        waterConsumptionUpdater = WaterConsumptionUpdater()
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}

