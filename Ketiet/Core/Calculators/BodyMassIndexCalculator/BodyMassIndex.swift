//
//  BodyMassIndex.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 13.12.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

enum BodyMassIndex: CaseIterable {
    case hardDeficit
    case deficit
    case normal
    case proficit
    case obesity1
    case obesity2
    case obesity3
    
    var fullString: String {
        switch self {
        case .hardDeficit:
            return "BMI_HARD_DEFICIT".localized()
        case .deficit:
            return "BMI_DEFICIT".localized()
        case .normal:
            return "BMI_NORMAL".localized()
        case .proficit:
            return "BMI_PROFICIT".localized()
        case .obesity1:
            return "BMI_OBESITY1".localized()
        case .obesity2:
            return "BMI_OBESITY2".localized()
        case .obesity3:
            return "BMI_OBESITY3".localized()
        }
    }
    
    var lowerBound: Float {
        switch self {
        case .hardDeficit:
            return 0
        case .deficit:
            return 16
        case .normal:
            return 18.5
        case .proficit:
            return 25
        case .obesity1:
            return 30
        case .obesity2:
            return 35
        case .obesity3:
            return 40
        }
    }
    
    var upperBound: Float {
        switch self {
        case .hardDeficit:
            return 16
        case .deficit:
            return 18.5
        case .normal:
            return 25
        case .proficit:
            return 30
        case .obesity1:
            return 35
        case .obesity2:
            return 40
        case .obesity3:
            return Float.greatestFiniteMagnitude
        }
    }
    
    var associatedColor: UIColor {
        switch self {
        case .hardDeficit:
            return #colorLiteral(red: 0.4392156863, green: 0.6, blue: 0.7843137255, alpha: 1)
        case .deficit:
            return #colorLiteral(red: 0.3215686275, green: 0.5882352941, blue: 0.4156862745, alpha: 1)
        case .normal:
            return #colorLiteral(red: 0.4, green: 0.7843137255, blue: 0.4431372549, alpha: 1)
        case .proficit:
            return #colorLiteral(red: 0.3607843137, green: 0.7803921569, blue: 0.231372549, alpha: 1)
        case .obesity1:
            return #colorLiteral(red: 0.9333333333, green: 0.4352941176, blue: 0.1764705882, alpha: 1)
        case .obesity2:
            return #colorLiteral(red: 0.9215686275, green: 0.2823529412, blue: 0.1490196078, alpha: 1)
        case .obesity3:
            return #colorLiteral(red: 0.9215686275, green: 0.2, blue: 0.137254902, alpha: 1)

        }
    }
}
