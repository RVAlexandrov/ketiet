//
//  BodyMassIndexCalculator.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.12.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

protocol BodyMassIndexCalculatorProtocol {
    typealias NearestPositiveIndexType = (type: BodyMassIndex?, value: Float?)

    func calculateBMI(withWeight weight: Float,
                      tall: Float) -> BodyMassIndex
    func calculateRawBMI(withWeight weight: Float,
                         tall: Float) -> Float
    func nearestPositiveTendetionIndex(withWeight weight: Float,
                                       tall: Float) -> NearestPositiveIndexType
}

class BodyMassIndexCalculator: BodyMassIndexCalculatorProtocol {
    func calculateBMI(withWeight weight: Float,
                      tall: Float) -> BodyMassIndex {
        let index = calculateRawBMI(withWeight: weight, tall: tall)
        return indexType(withRawValue: index)
    }
    
    func calculateRawBMI(withWeight weight: Float,
                         tall: Float) -> Float {
        weight / (tall * tall)
    }
    
    func nearestPositiveTendetionIndex(withWeight weight: Float,
                                       tall: Float) -> NearestPositiveIndexType {
        let currentIndex = calculateBMI(withWeight: weight, tall: tall)
        
        var nearestPositiveIndexType: BodyMassIndex?
        var diffToPositiveTendetion: Float?
        
        switch currentIndex {
        case .hardDeficit:
            nearestPositiveIndexType = .deficit
            diffToPositiveTendetion = (weightForBoundIndex(BodyMassIndex.deficit.lowerBound, tall: tall) - weight).round(to: 1)
        case .deficit:
            nearestPositiveIndexType = .normal
            diffToPositiveTendetion = (weightForBoundIndex(BodyMassIndex.normal.lowerBound, tall: tall) - weight).round(to: 1)
        case .normal:
            nearestPositiveIndexType = nil
            diffToPositiveTendetion = nil
        case .proficit:
            nearestPositiveIndexType = .normal
            diffToPositiveTendetion = (weightForBoundIndex(BodyMassIndex.normal.upperBound, tall: tall) - weight).round(to: 1)
        case .obesity1:
            nearestPositiveIndexType = .proficit
            diffToPositiveTendetion = (weightForBoundIndex(BodyMassIndex.proficit.upperBound, tall: tall) - weight).round(to: 1)
        case .obesity2:
            nearestPositiveIndexType = .obesity1
            diffToPositiveTendetion = (weightForBoundIndex(BodyMassIndex.obesity1.upperBound, tall: tall) - weight).round(to: 1)
        case .obesity3:
            nearestPositiveIndexType = .obesity2
            diffToPositiveTendetion = (weightForBoundIndex(BodyMassIndex.obesity2.upperBound, tall: tall) - weight).round(to: 1)
        }
        
        return (type: nearestPositiveIndexType, value: diffToPositiveTendetion)
    }
    
    private func weightForBoundIndex(_ boundIndex: Float, tall: Float) -> Float {
        boundIndex * tall * tall
    }
    
    private func indexType(withRawValue rawValue: Float) -> BodyMassIndex {
        switch rawValue {
        case 0 ..< 16:
            return .hardDeficit
        case 16 ..< 18.5:
            return .deficit
        case 18.5 ..< 25:
            return .normal
        case 25 ..< 30:
            return .proficit
        case 30 ..< 35:
            return .obesity1
        case 35 ..< 40:
            return .obesity2
        default:
            // 40 and more
            return .obesity3
        }
    }
}
