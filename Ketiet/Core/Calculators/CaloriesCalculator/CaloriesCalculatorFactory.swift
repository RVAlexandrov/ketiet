//
//  CaloriesCalculatorFactory.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 17.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct CaloriesCalculatorFactory {
    func buildCalculator(for profile: ProfileProtocol) -> CaloriesCalculatorProtocol {
        guard let type = profile.caloriesCalculatorType else {
            return ConstantCaloriesCalculator(
                customCalories: 2000
            )
        }
        switch type {
        case .harrisBenedict:
            return HarrisBenedictCaloriesCalculator(
                weight: Float(profile.weight ?? 1),
                tall: Float(profile.tall ?? 1),
                gender: profile.gender ?? .female,
                age: profile.age ?? 18,
                weightChangeMode: HarrisBenedictCaloriesCalculator.HarrisWeightChangeMode(from: profile.goal?.ketoWeightChangeMode ?? .reduce),
                physicalActivity: profile.physicalActivity ?? .no
            )
        case .custom:
            return ConstantCaloriesCalculator(
                customCalories: profile.dailyCalories ?? 2000
            )
        case .mifflinSanJeor:
            return MifflinSanJeorCalculator(
                weight: Float(profile.weight ?? 1),
                tall: Float(profile.tall ?? 1),
                gender: profile.gender ?? .female,
                age: profile.age ?? 18,
                weightChangeMode: MifflinSanJeorCalculator.MifflinWeightChangeMode(from: profile.goal?.ketoWeightChangeMode ?? .reduce),
                physicalActivity: profile.physicalActivity ?? .no
            )
        }
    }
}
