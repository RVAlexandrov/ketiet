//
//  CaloriesCalculatorProtocol.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 07.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

protocol CaloriesCalculatorProtocol {
    func calculateDailyCalories() -> Float
}
