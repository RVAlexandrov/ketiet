//
//  ConstantCaloriesCalculator.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 17.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct ConstantCaloriesCalculator {
    let customCalories: Float
    
    init(customCalories: Float) {
        self.customCalories = customCalories
    }
}

extension ConstantCaloriesCalculator: CaloriesCalculatorProtocol {
    func calculateDailyCalories() -> Float {
        customCalories
    }
}
