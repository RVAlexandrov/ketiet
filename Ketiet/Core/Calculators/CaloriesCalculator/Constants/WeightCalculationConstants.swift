//
//  WeightCalculationConstants.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 12.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

struct WeightCalculationConstants {
	
	static let mifflinSanJeorMale = WeightCalculationConstants(weightMultiplier: 9.99,
															   heightMultiplier: 6.25,
															   ageMultiplier: -4.92,
															   freeConstant: 5)
	static let mifflinSanJeorFemale = WeightCalculationConstants(weightMultiplier: 9.99,
																 heightMultiplier: 6.25,
																 ageMultiplier: -4.92,
																 freeConstant: -161)
	static let harrisBenedictMale = WeightCalculationConstants(weightMultiplier: 13.397,
															   heightMultiplier: 4.799,
															   ageMultiplier: -5.677,
															   freeConstant: 88.362)
	static let harrisBenedictFemale = WeightCalculationConstants(weightMultiplier: 9.247,
																 heightMultiplier: 3.098,
																 ageMultiplier: -4.33,
																 freeConstant: 447.593)
	
	let weightMultiplier: Float
	let heightMultiplier: Float
	let ageMultiplier: Float
	let freeConstant: Float
}
