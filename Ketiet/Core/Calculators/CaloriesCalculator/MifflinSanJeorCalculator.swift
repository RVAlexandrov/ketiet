//
//  MifflinSanJeonCalculator.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 17.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation
import DietCore

struct MifflinSanJeorCalculator {
    enum MifflinWeightChangeMode {
        case support
        case reduce
        case gain
        
        init(from defaultModel: WeightChangeMode) {
            switch defaultModel {
            case .support:
                self = .support
            case .reduce:
                self = .reduce
            case .gain:
                self = .gain
            }
        }
        
        var convertConstant: Float {
            switch self {
            case .support:
                return 0
            case .reduce:
                return -0.2
            case .gain:
                return 0.2
            }
        }
    }
    
    private let weight: Float
    private let tall: Float
    private let gender: Gender
    private let age: Int
    private let weightChangeMode: MifflinWeightChangeMode
    private let physicalActivity: PhysicalActivity
    
    init(weight: Float,
         tall: Float,
         gender: Gender,
         age: Int,
         weightChangeMode: MifflinWeightChangeMode,
         physicalActivity: PhysicalActivity) {
        let convertedWeight = weight.convert(
            from: Locale.current.usesMetricSystem ? UnitMass.kilograms : UnitMass.pounds,
            to: UnitMass.kilograms
        )
        let convertedTall = tall.convert(
            from: Locale.current.usesMetricSystem ? UnitLength.meters : UnitLength.inches,
            to: UnitLength.meters
        )
        self.weight = convertedWeight
        self.tall = convertedTall
        self.age = age
        self.gender = gender
        self.weightChangeMode = weightChangeMode
        self.physicalActivity = physicalActivity
    }
}

// MARK: - CaloriesCalculatorProtocol
extension MifflinSanJeorCalculator: CaloriesCalculatorProtocol {
    func calculateDailyCalories() -> Float {
        let cc = gender.mifflinConstants()
        let bmrCalories = cc.freeConstant + weight * cc.weightMultiplier + tall * cc.heightMultiplier + Float(age) * cc.ageMultiplier
        let activityBasedBmrCalories = bmrCalories * physicalActivity.mifflinActivityMultiplier()
        let finalCalories = activityBasedBmrCalories * (1 + weightChangeMode.convertConstant)
        return finalCalories
    }
}
