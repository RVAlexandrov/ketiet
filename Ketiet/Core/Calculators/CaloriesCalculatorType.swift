//
//  CaloriesCalculatorType.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 14.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

enum CaloriesCalculatorType: String {
    case harrisBenedict
    case custom
    case mifflinSanJeor
}


