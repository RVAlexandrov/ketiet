//
//  CaloriesPerMealProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct Meal {
	
	let name: String
    let date: Date
	let nutriens: FoodNutrients
}

protocol CaloriesPerMealProviderProtocol {
	
    func createDietDay()
	func numberOfMealsPerDay() -> Int
	func meal(at index: Int) -> Meal
	func dishes(forMealAt index: Int) -> [DishProtocol]
}

extension CaloriesPerMealProviderProtocol {
	
	func meals() -> [Meal] {
		let numberOfMeals = numberOfMealsPerDay()
		if numberOfMeals <= 0 {
			return []
		}
		return (0...numberOfMeals - 1).reduce(into: [Meal]()) { $0.append(meal(at: $1)) }
	}
}
