//
//  DishWeightCalculator.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 30.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct DishWeightCalculator {
    
    struct WeightStep {
        static let baseWeight: Float = 0.1
        static let classicStep: Float = 0.05
    }
    
    func getWeightForDish(withTargerCalories targetCalories: Float,
                          dishCaloriesPer100Gramm: Float,
                          inaccuracy: Float = 0.1) -> Float {
        let caloriesRange = Range(baseValue: targetCalories, inaccuracy: inaccuracy)
        
        // Если сразу попадает с учётом погрешности, то оставляем базовое значение
        if caloriesRange.contains(dishCaloriesPer100Gramm) {
            return WeightStep.baseWeight * 1000
        } else {
            // если не попадает, то высчитываем сколько нужно на целевое количество шагов по 50 гр.
            // целевое точное количество блюда
            let productWeightForTargetCallories: Float = targetCalories / dishCaloriesPer100Gramm * WeightStep.baseWeight
            
            // количество шагов по 50 гр. в этой величине (с плавающей точкой)
            let numberOfSteps: Float = productWeightForTargetCallories / WeightStep.classicStep
            
            // округление количества шагов
            let roundedNumberOfSteps = lroundf(numberOfSteps)
            
            // вычисление целевого веса кратного шагу
            return Float(roundedNumberOfSteps) * WeightStep.classicStep * 1000
        }
    }
}
