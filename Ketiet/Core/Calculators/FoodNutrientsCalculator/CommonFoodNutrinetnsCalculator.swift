//
//  DailyCaloriesCalculator.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

final class DailyCaloriesCalculator {
	
	private struct Constants {
		static let weekReduceCaloriesStep: Float = 0.05
		static let maxWeekNumberToReduceCalories: UInt = 5
	}
	
	private let constants: WeightCalculationConstants
	private let weightInKilograms: Float
	private let heightInCentimeters: Float
	private let ageInYears: Float
	private let physicalActivity: PhysicalActivity
	private let weekNumber: UInt
	
	init(constants: WeightCalculationConstants,
		 weightInKilograms: Float,
		 heightInCentimeters: Float,
		 ageInYears: Float,
		 physicalActivity: PhysicalActivity,
		 weekNumber: UInt) {
		self.constants = constants
		self.weightInKilograms = weightInKilograms
		self.heightInCentimeters = heightInCentimeters
		self.ageInYears = ageInYears
		self.physicalActivity = physicalActivity
		self.weekNumber = weekNumber
	}
	
	private func weekNumberMuliplier() -> Float {
		return 1 - Float(min(Constants.maxWeekNumberToReduceCalories, weekNumber)) * Constants.weekReduceCaloriesStep
	}

	private func calculateCalories() -> Float {
		let weight = constants.weightMultiplier * weightInKilograms
		let height = constants.heightMultiplier * heightInCentimeters
		let age = constants.ageMultiplier * ageInYears
		return (weight + height + age + constants.freeConstant) * physicalActivity.harrisBenedictActivityMultiplier() * weekNumberMuliplier()
	}
}

extension DailyCaloriesCalculator: FoodNutrientsCalculatorProtocol {
    func calculateFoodNutrients(withCalories calories: Float) -> FoodNutrients {
        FoodNutrients(proteins: 1, carbohydrates: 1, fat: 1)
    }
}
