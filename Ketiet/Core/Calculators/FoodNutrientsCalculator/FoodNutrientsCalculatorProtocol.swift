//
//  DailyCalloriesCalculatorProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol FoodNutrientsCalculatorProtocol {
    func calculateFoodNutrients(withCalories calories: Float) -> FoodNutrients
}
