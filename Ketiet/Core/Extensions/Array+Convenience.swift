//
//  Array+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

extension Array {
	subscript(safe index: Int) -> Element? {
		if index < count && index > -1 {
			return self[index]
		}
		return nil
	}
}
