//
//  CAEmitterCell+Confetti.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.06.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit
import QuartzCore

extension CAEmitterCell {
    
    convenience init(color: UIColor?, intensity: Float, type: ConfettiType) {
        self.init()
        birthRate = 6.0 * intensity
        lifetime = 14.0 * intensity
        lifetimeRange = 0
        if let color = color {
            self.color = color.cgColor
        }
        velocity = CGFloat(350.0 * intensity)
        velocityRange = CGFloat(80.0 * intensity)
        emissionLongitude = CGFloat(Double.pi)
        emissionRange = CGFloat(Double.pi)
        spin = CGFloat(3.5 * intensity)
        spinRange = CGFloat(4.0 * intensity)
        scaleRange = CGFloat(intensity)
        scaleSpeed = CGFloat(-0.1 * intensity)
        contents = type.image?.cgImage
    }
}
