//
//  CAGradientLayer+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension CAGradientLayer {
	
	func setColors() {
		colors = [UIColor.white.cgColor, #colorLiteral(red: 0.08086263388, green: 0.5869681239, blue: 0.9839325547, alpha: 1).cgColor]
	}
	
	func animateStandardGradient() {
		startPoint = .zero
		endPoint = CGPoint(x: 0, y: 1)
		setColors()
		locations = [0.5, 1.2]
		let animation = CABasicAnimation(keyPath: NSStringFromSelector(#selector(getter: CAGradientLayer.locations)))
		animation.fromValue = [1.0, 1.0]
		animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
		animation.duration = CATransaction.animationDuration()
		add(animation, forKey: nil)
	}
}
