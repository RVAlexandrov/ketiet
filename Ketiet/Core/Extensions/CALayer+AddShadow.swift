//
//  CALayer+AddShadow.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 07.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension CALayer {
	
	private static var colors = [String: UIColor]()
	private static var swizzled = false
	
    func addShadow(rect: CGRect,
                   rectRadius: CGFloat = 0,
                   color: UIColor = .blackShadowColor,
                   alpha: Float = 0.4,
                   x: CGFloat = 0,
                   y: CGFloat = 0,
                   shadowRadius: CGFloat = 5) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        self.shadowRadius = shadowRadius
        shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: rectRadius).cgPath
		addUserInterfaceStyleObserver(shadowColor: color)
    }
    
    func addShadow(_ rect: CGRect) {
        addShadow(rect: rect,
                  color: .primaryShadowColor,
                  alpha: 0.6,
                  shadowRadius: 9.0)
    }
	
	fileprivate func updateShadowColor() {
		if let name = name {
			shadowColor = CALayer.colors[name]?.cgColor
		}
	}
	
	private func addUserInterfaceStyleObserver(shadowColor: UIColor) {
		if !CALayer.swizzled {
			UIView.swizzlee()
			CALayer.swizzled = true
		}
		let date = Date()
		let nanoseconds = Calendar.current.dateComponents([.nanosecond], from: date).nanosecond ?? 0
		let name = date.description + String(nanoseconds)
		if let oldName = self.name {
			CALayer.colors[oldName] = nil
		}
		self.name = name
		CALayer.colors[name] = shadowColor
	}
}

extension UIView {
	static func swizzlee() {
		swizzle(klass: UIView.self,
				originalSelector: #selector(UIView.traitCollectionDidChange(_:)),
				swizzledSelector: #selector(ketoTraitCollectionDidChange))
	}
	
	@objc func ketoTraitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
		ketoTraitCollectionDidChange(previousTraitCollection: previousTraitCollection)
		layer.updateShadowColor()
	}
}
