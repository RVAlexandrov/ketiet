//
//  CGPoint+Maths.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import CoreGraphics

extension CGPoint {
	
	static func / (lhs: CGPoint, rhs: CGFloat) -> CGPoint {
		return CGPoint(x: lhs.x / rhs, y: lhs.y / rhs)
	}
	
	static func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
		return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
	}
	
	static func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
		return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
	}
}
