//
//  NSManagedObjectContext+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
	
	func saveIfNeeded(with completion: ((Result<Void, Error>) -> Void)? = nil) {
		if !hasChanges {
			return
		}
		do {
			try save()
			completion?(.success(()))
		} catch {
			completion?(.failure(error))
		}
	}
}

