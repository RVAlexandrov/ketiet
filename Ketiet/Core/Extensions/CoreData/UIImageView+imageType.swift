//
//  ImageView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

extension UIImageView {
    private static let imageLoader = ImageLoader()
    
    func setImageType(_ imageType: ImageStringType) {
        switch imageType {
        case .asset(let assetName):
            image = UIImage(named: assetName)
        case .url(let urlString):
            showShimmer()
            guard let imageURL = URL(string: urlString) else { return }
            UIImageView.imageLoader.loadImage(by: imageURL, completion: { [weak self] image in
                if let image = image {
                    self?.image = image
                }
                self?.hideShimmer()
            })
        case .system(let systemString):
            image = UIImage(systemName: systemString)
        }
    }
    
    convenience init(imageType: ImageStringType) {
        self.init(frame: .zero)
        self.setImageType(imageType)
    }
}
