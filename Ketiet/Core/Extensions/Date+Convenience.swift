//
//  Date+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

extension Date {
    
    var hour: Int? {
        Calendar.current.dateComponents([.hour], from: self).hour
    }
    
    var minute: Int? {
        Calendar.current.dateComponents([.minute], from: self).minute
    }
	
	func dateComponentsByAdding(_ components: DateComponents,
								resultUnints: Set<Calendar.Component>) -> DateComponents {
		let date = Calendar.current.date(byAdding: components, to: self) ?? self
		return Calendar.current.dateComponents(resultUnints, from: date)
	}
	
	func dateByAdding(_ components: DateComponents,
						resultUnints: Set<Calendar.Component>) -> Date? {
		let date = Calendar.current.date(byAdding: components, to: self) ?? self
		let components = Calendar.current.dateComponents(resultUnints, from: date)
		return Calendar.current.date(from: components)
	}
    
    static func daysBetweenTwoDates(_ firstDate: Date, secondDate: Date) -> Int? {
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: firstDate)
        let date2 = calendar.startOfDay(for: secondDate)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)

        return components.day
    }
    
    func string(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
        DateFormatter.localizedString(
            from: self,
            dateStyle: dateStyle,
            timeStyle: timeStyle
        )
    }
    
    func shortTimeString() -> String {
        string(
            dateStyle: .none,
            timeStyle: .short
        )
    }
}
