//
//  Double+CommaSeparatedValue.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension String {
    var commaSeparatedValue: Double? {
        let formatter = NumberFormatter()
        formatter.decimalSeparator = ","
        let grade = formatter.number(from: self)

        return grade?.doubleValue
    }
}
