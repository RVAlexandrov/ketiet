//
//  Range+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 08.06.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

extension Range where Bound: Comparable {
    
    func intersection(with range: Range<Bound>) -> Range<Bound>? {
        let lower = Swift.max(lowerBound, range.lowerBound)
        let upper = Swift.min(upperBound, range.upperBound)
        return lower < upper ? Range(uncheckedBounds: (lower: lower, upper: upper)) : nil
    }
}
