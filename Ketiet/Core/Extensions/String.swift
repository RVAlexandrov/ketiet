//
//  String+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 27.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension String {
    
    static var metricCache: [String: CGFloat] = [:]
    
    func image(font: UIFont, size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.set()
        let rect = CGRect(origin: .zero, size: size)
        UIRectFill(CGRect(origin: .zero, size: size))
        (self as AnyObject).draw(in: rect, withAttributes: [.font: font])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func height(withWidth width: CGFloat, font: UIFont) -> CGFloat {
        let key = self + "height" + "forWidth" + Float(width).cleanValue + font.description
        
        if let cachedHeight = String.metricCache[key] {
            return cachedHeight
        } else {
            let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
            let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
            let height = actualSize.integral.height
            String.metricCache[key] = height
            return actualSize.height
        }
    }
    
    func width(withHeight height: CGFloat, font: UIFont) -> CGFloat {
        let key = self + "width" + "forHeight" + Float(height).cleanValue + font.description
        
        if let cachedWidth = String.metricCache[key] {
            return cachedWidth
        } else {
            let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
            let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
            let width = actualSize.integral.width
            String.metricCache[key] = width
            return width
        }
    }
}
