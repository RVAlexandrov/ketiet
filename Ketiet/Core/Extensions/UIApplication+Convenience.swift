//
//  UIApplication+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 07.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIApplication {
    func getKeyWindow() -> UIWindow? {
        connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .compactMap { $0 as? UIWindowScene }
            .first?.windows
            .filter { $0.isKeyWindow } .first
    }
    
    func firstWindowScene() -> UIWindowScene? {
        connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .first { $0 is UIWindowScene } as? UIWindowScene
    }
    
    func lastWindowScene() -> UIWindowScene? {
        connectedScenes.reversed()
            .first {$0 is UIWindowScene } as? UIWindowScene
    }
	
	func goToSettings() {
		guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
		open(url)
	}
	
	@objc func endEditing() {
		sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
	}
	
    func showWelcomeViewController(window: UIWindow? = nil) {
		let controller = UINavigationController(rootViewController: WelcomeToAppAssembly().makeViewController())
		controller.navigationBar.prefersLargeTitles = true
		controller.navigationController?.setNavigationBarHidden(true, animated: false)
		controller.navigationItem.largeTitleDisplayMode = .automatic
        guard let targetWindow = window ?? getKeyWindow() else {
            assertionFailure("No window")
            return
        }
        targetWindow.rootViewController = controller
        UIView.transition(
            with: targetWindow,
            duration: CATransaction.animationDuration(),
            options: .transitionCrossDissolve,
            animations: nil
        )
	}
}
