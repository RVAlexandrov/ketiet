//
//  UIButton+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIButton {
	
	func makeSquare() {
		translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			heightAnchor.constraint(equalTo: widthAnchor),
			heightAnchor.constraint(equalToConstant: 30)
		])
	}
	
	func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
		setBackgroundImage(color.image(), for: state)
	}
    
    static func makeCloseButton() -> UIButton {
        let button = UIButton(frame: .zero)
        button.setImage(UIImage.init(systemName: "xmark.circle.fill"), for: .normal)
        button.tintColor = .systemGray
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        return button
    }
    
    func closeButtonConstrains() -> [NSLayoutConstraint] {
        [widthAnchor.constraint(equalToConstant: 31),
        heightAnchor.constraint(equalToConstant: 30)]
    }
}
