//
//  UIColor+lighterColor.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 31.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIColor {

    var lighterColor: UIColor {
        lighterColor(removeSaturation: 0.5, resultAlpha: -1)
    }
    
    var gradientLighter: UIColor {
        lighterColor(removeSaturation: 0.25, resultAlpha: -1)
    }

    func lighterColor(removeSaturation val: CGFloat, resultAlpha alpha: CGFloat) -> UIColor {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0

        guard getHue(&h, saturation: &s, brightness: &b, alpha: &a) else { return self }
        return UIColor(hue: h,
                       saturation: max(s - val, 0.0),
                       brightness: b,
                       alpha: alpha == -1 ? a : alpha)
    }
    
    func darkerColor(by percentage: CGFloat = 20.0) -> UIColor {
        return self.adjust(by: -1 * abs(percentage) ) ?? .black
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
}
