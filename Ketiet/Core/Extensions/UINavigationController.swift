//
//  UINavigationController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 01.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum NavigationBarItemType: String {
    case settings = "icon_cog"
    case circleHelp = "icon_circle_help"
    case signOut = "icon_sign_out"
    case share = "icon_share"
    case feedback = "comments"
    case filterAndSort = "icon_funnel"
    case filterAndSortRed = "icon_funnel_red_dot"
    case close = "icon_close"
    case arrowLeft = "arrow_left"
    case search = "icon_search"
    case favoriteRed = "favoriteRed"
    case favorite = "favorite"
    case chat = "icon_chat"
    case download = "download"
    case none
}

enum CloseButtonType {
    case defaultCloseButton
    case defaultBackButton
}

private let buttonSideLength: CGFloat = 25.0

extension UINavigationController {
    
    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        navigationBar.tintColor = .accentColor
    }
    
    func setCloseButton(_ buttonType: CloseButtonType,
                        color: UIColor = .systemGray2,
                        target: ((NavigationBarItemType) -> Void)? = nil) {
        switch buttonType {
        case .defaultCloseButton:
            return setCloseButtonWith(color: color, target: target)
        case .defaultBackButton:
            return setBackButton(target: target)
        }
    }
    
    func setBackButton(target: ((NavigationBarItemType) -> Void)? = nil) {
        let barItem = ImageButton(frame: CGRect(origin: .zero,
                                                size: CGSize(width: buttonSideLength,
                                                             height: buttonSideLength)),
                                  target: target,
                                  navigationBarItemType: .arrowLeft)
        
        barItem.addTarget(self, action: #selector(actionBackButton(sender:)), for: .touchUpInside)
        let backButton = UIBarButtonItem(customView: barItem)
        topViewController?.navigationController?.navigationBar.tintColor = .systemGray2
        topViewController?.navigationItem.leftBarButtonItem = backButton
    }
    
    func setNavigationBarWith(_ barItems: [NavigationBarItemType],
                              data: Any? = nil,
                              target: ((NavigationBarItemType) -> Void)? = nil) {
        guard let topViewController = self.topViewController else { return }
    
        var barButtonItem = [UIBarButtonItem]()
        barItems.forEach {
            let barItem = ImageButton(frame: CGRect(origin: .zero,
                                                    size: CGSize(width: buttonSideLength,
                                                                 height: buttonSideLength)),
                                      target: target,
                                      navigationBarItemType: $0)
            barItem.addTarget(self, action: #selector(barItemAction(sender:)), for: .touchUpInside)
            let separator = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            separator.width = 12
            barButtonItem.append(UIBarButtonItem(customView: barItem))
            barButtonItem.append(separator)
        }
        topViewController.navigationController?.navigationBar.tintColor = UIColor.black.withAlphaComponent(0.3)
        topViewController.navigationItem.rightBarButtonItems = barButtonItem
    }
    
    private func setCloseButtonWith(color: UIColor,
                                    target: ((NavigationBarItemType) -> Void)? = nil) {
        let barItem = ImageButton(frame: CGRect(origin: .zero,
                                                size: CGSize(width: buttonSideLength,
                                                             height: buttonSideLength)),
                                  target: target,
                                  navigationBarItemType: .close)
        
        barItem.addTarget(self, action: #selector(barItemCloseAction(sender:)), for: .touchUpInside)
        let barButtonClose = UIBarButtonItem(customView: barItem)
        topViewController?.navigationController?.navigationBar.tintColor = color
        topViewController?.navigationItem.leftBarButtonItem = barButtonClose
    }
    
    @objc private func barItemCloseAction(sender: ImageButton) {
        guard let target = sender.target else { return }
        target(sender.navigationBarItemType)
    }
    
    @objc private func actionBackButton(sender: ImageButton) {
        guard let target = sender.target else { return }
        target(sender.navigationBarItemType)
    }
    
    @objc private func barItemAction(sender: ImageButton) {
        guard let target = sender.target else { return }
        target(sender.navigationBarItemType)
    }
}
