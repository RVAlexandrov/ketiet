//
//  UINavigationItem+addItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 14.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UINavigationItem {
    func makeNavigationBarLittItems(barButton: LittCapsuleButton) -> [UIBarButtonItem] {
        let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spacer.width = 4
        
        let barButtonItem = UIBarButtonItem(customView: barButton)
        return [spacer, barButtonItem]
    }
    
    func configureNavigationBarLittItems(leftButton: LittCapsuleButton?,
                                         rightButton: LittCapsuleButton?) {
        let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spacer.width = 4
        
        if let leftButton = leftButton {
            let leftBarButton = UIBarButtonItem(customView: leftButton)
            leftBarButtonItems = [spacer, leftBarButton]
        }
        
        if let rightButton = rightButton {
            let rightBarButton = UIBarButtonItem(customView: rightButton)
            rightBarButtonItems = [spacer, rightBarButton]
        }
    }
}
