//
//  UIStackView+AddArrangedSubviews.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 02.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ views: [UIView]) {
        views.forEach { addArrangedSubview($0) }
    }
}

