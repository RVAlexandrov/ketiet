//
//  UITableView+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UITableView {
	func lastIndexPath() -> IndexPath? {
		guard numberOfSections > 0 else { return nil }
		var sectionIndex = numberOfSections - 1
		while sectionIndex > -1 {
			if numberOfRows(inSection: sectionIndex) > 0 {
				return IndexPath(row: numberOfRows(inSection: sectionIndex) - 1, section: sectionIndex)
			}
			sectionIndex -= 1
		}
		return nil
	}
}
