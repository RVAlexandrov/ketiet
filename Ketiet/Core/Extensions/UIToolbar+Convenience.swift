//
//  UIToolbar+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

extension UIToolbar {
    static let regularSize = CGSize(width: UIScreen.main.bounds.width, height: 44)
}
