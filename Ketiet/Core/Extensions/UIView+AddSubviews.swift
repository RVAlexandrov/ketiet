//
//  UIView+AddSubviews.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 02.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIView {
    func addSubviews(_ views: [UIView]) {
        views.forEach { addSubview($0) }
    }
	
	func pinToSuperview(insets: UIEdgeInsets = .zero, useSafeArea: Bool = false) {
		guard let superview = superview else {
			return
		}
		let top = useSafeArea ? superview.safeAreaLayoutGuide.topAnchor : superview.topAnchor
		let bottom = useSafeArea ? superview.safeAreaLayoutGuide.bottomAnchor : superview.bottomAnchor
		let leading = useSafeArea ? superview.safeAreaLayoutGuide.leadingAnchor : superview.leadingAnchor
		let trailing = useSafeArea ? superview.safeAreaLayoutGuide.trailingAnchor : superview.trailingAnchor
		NSLayoutConstraint.activate([
			topAnchor.constraint(equalTo: top, constant: insets.top),
			bottom.constraint(equalTo: bottomAnchor, constant: insets.bottom),
			leadingAnchor.constraint(equalTo: leading, constant: insets.left),
			trailing.constraint(equalTo: trailingAnchor, constant: insets.right)
		])
	}
    
    func pinToSuperviewConstraints(insets: UIEdgeInsets = .zero, useSafeArea: Bool = false) -> [NSLayoutConstraint] {
        guard let superview = superview else {
            return []
        }
        let top = useSafeArea ? superview.safeAreaLayoutGuide.topAnchor : superview.topAnchor
        let bottom = useSafeArea ? superview.safeAreaLayoutGuide.bottomAnchor : superview.bottomAnchor
        let leading = useSafeArea ? superview.safeAreaLayoutGuide.leadingAnchor : superview.leadingAnchor
        let trailing = useSafeArea ? superview.safeAreaLayoutGuide.trailingAnchor : superview.trailingAnchor
        return [
            topAnchor.constraint(equalTo: top, constant: insets.top),
            bottom.constraint(equalTo: bottomAnchor, constant: insets.bottom),
            leadingAnchor.constraint(equalTo: leading, constant: insets.left),
            trailing.constraint(equalTo: trailingAnchor, constant: insets.right)
        ]
    }
	
	func addEndEditingGestireRecognizer() {
		addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing(_:))))
	}
	
	static func flexibleView(for axis: NSLayoutConstraint.Axis) -> UIView {
		let view = UIView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.backgroundColor = .clear
		view.setContentCompressionResistancePriority(.defaultHigh, for: axis)
		view.setContentHuggingPriority(.defaultHigh, for: axis)
		return view
	}
}
