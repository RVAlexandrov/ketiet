//
//  UIView+BottomInset.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 28.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIView {
    static let bottomInset: CGFloat = UIScreen.main.bounds.height > 736.0 ? 90.0 : 85.0
}
