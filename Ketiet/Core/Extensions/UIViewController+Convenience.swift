//
//  UIViewController+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 01.09.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIViewController {
	
	@objc func dismissController() {
		dismiss(animated: true)
	}
	
    func addBottomButtonWithBlur(with title: String,
                                 blurStyle: UIBlurEffect.Style = .regular) -> (LittBasicButton, CGFloat) {
        let button = LittBasicButton(
            title: title,
            config: .filledLarge()
        )
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: blurStyle))
		visualEffectView.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(visualEffectView)
		visualEffectView.contentView.addSubview(button)
        visualEffectView.layer.cornerRadius = 12
        visualEffectView.clipsToBounds = true
        NSLayoutConstraint.activate([
                                        visualEffectView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                                                                                 constant: -.smallLittMargin),
                                        visualEffectView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .largeLittMargin),
                                        visualEffectView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.largeLittMargin),
                                        visualEffectView.topAnchor.constraint(equalTo: button.topAnchor,
                                                                              constant: -.largeLittMargin),
                                        button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                        button.widthAnchor.constraint(equalTo: view.widthAnchor,
                                                                      constant: -2 * .largeLittScreenEdgeMargin),
                                        button.bottomAnchor.constraint(equalTo: visualEffectView.contentView.bottomAnchor,
                                                                       constant: -.largeLittMargin)])
        return (button, ((UIApplication.shared.getKeyWindow()?.safeAreaInsets.bottom ?? 0) +
                         50.33 + .largeLittMargin + .largeLittMargin + .largeLittMargin))
	}
	
	var littTabBarController: LittTabBarController? {
		var controller = parent
		while controller != nil {
			if controller is LittTabBarController {
				return controller as? LittTabBarController
			}
			controller = controller?.parent
		}
		return nil
	}
    
    func setupStandardSheetPresentation() {
        guard let sheet = sheetPresentationController else { return }
        sheet.detents = [.medium(), .large()]
        sheet.prefersGrabberVisible = true
        sheet.preferredCornerRadius = 30
    }
}
