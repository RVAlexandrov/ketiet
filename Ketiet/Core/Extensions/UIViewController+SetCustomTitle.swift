//
//  UIViewController+SetCustomTitle.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 01.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIViewController {
    func setCustomTitle(_ title: String,
                        isVisible: Bool = true,
                        color: UIColor = .label) {
        let labelTitle = UILabel()
        labelTitle.textColor = color
        labelTitle.textAlignment = .center
        labelTitle.font = .systemFont(ofSize: 14, weight: .semibold)
        labelTitle.text = title
        
        navigationItem.titleView = labelTitle
        navigationItem.titleView?.sizeToFit()
        animateTitle(labelTitle: labelTitle, isVisible: isVisible)
    }
    
    private func animateTitle(labelTitle: UILabel, isVisible: Bool) {
        let alpha: CGFloat = isVisible ? 1.0: 0.0
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.navigationItem.titleView?.alpha = alpha
        },
                       completion: { _ in
                        guard !isVisible else { return }
                        labelTitle.text = " "
        })
    }
}
