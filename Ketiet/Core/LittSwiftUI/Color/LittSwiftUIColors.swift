//
//  LittSwiftUIColors.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 15.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

extension Color {
    static var littProteinColor: Color {
        .blue
    }
    
    static var littFatColor: Color {
        .orange
    }
    
    static var littCarbohydratesColor: Color {
        .green
    }
    
    static var littNutrientAccentColor: Color {
        .pink
    }
}
