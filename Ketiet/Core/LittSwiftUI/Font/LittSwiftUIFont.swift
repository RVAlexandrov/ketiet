//
//  LittSwiftUIFont.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

extension Font {
    static func littFont(font: LittFont) -> Font {
        .system(size: font.size, weight: font.weight.swiftUIWeight)
    }
}

extension LittFontWeight {
    
    var swiftUIWeight: Font.Weight {
        switch self {
        case .heavy:
            return .heavy
        case .bold:
            return .bold
        case .semibold:
            return .semibold
        case .medium:
            return .medium
        case .regular:
            return .regular
        }
    }
}
