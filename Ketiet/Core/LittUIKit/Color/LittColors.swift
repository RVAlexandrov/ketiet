//
//  LittColors.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 02.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

// MARK: - Convenience initializer
extension UIColor {
    
    convenience init(light: UIColor, dark: UIColor) {
        self.init { collection -> UIColor in
            switch collection.userInterfaceStyle {
            case .dark, .unspecified:
                return dark
            case .light:
                return light
            @unknown default:
                return light
            }
        }
    }
}

// MARK: - Core colors
extension UIColor {
    
    static var accentColor: UIColor {
        .systemGreen
    }
	
	static var littBackgroundColor: UIColor {
		.systemGroupedBackground
	}
	
	static var littSecondaryBackgroundColor: UIColor {
		.secondarySystemGroupedBackground
	}
    
    static var littTurnedOnColor: UIColor {
        .systemGreen
    }
    
    static var itemTurnedOffColor: UIColor {
        .systemRed
    }
    
    static var littSuccessColor: UIColor {
        .systemGreen
    }
    
    static var littFailureColor: UIColor {
        .systemRed
    }
}

// MARK: - Calendar colors
extension UIColor {
    static var incompletedDayColor: UIColor {
        #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1).lighterColor
    }
    
    static var selectedDayColor: UIColor {
        .blue.lighterColor
    }
    
    static var completedDayColor: UIColor {
        #colorLiteral(red: 0.06696059555, green: 0.7966138721, blue: 0, alpha: 1).lighterColor
    }
    
    static var notStartedDayColor: UIColor {
        .systemGray4
    }
}

// MARK: - Bussiness colors
extension UIColor {
    static var littProteinColor: UIColor {
        .systemBlue
    }
    
    static var littFatColor: UIColor {
        .systemOrange
    }
    
    static var littCarbohydratesColor: UIColor {
        .systemGreen
    }
    
    static var nutrientAccentColor: UIColor {
        .systemPink
    }
    
    static var weightAccentColor: UIColor {
        .systemOrange
    }
    
    static var waterAccentColor: UIColor {
        .systemBlue
    }
    
    static var bodyMetricAccentColor: UIColor {
        .systemTeal
    }
}

// MARK: - Shadow colors
extension UIColor {
    static var primaryShadowColor: UIColor {
        UIColor {
            switch $0.userInterfaceStyle {
            case .dark, .unspecified:
                return .clear
            case .light:
                return .gray
            @unknown default:
                return .clear
            }
        }
    }
    
    static var blackShadowColor: UIColor {
        UIColor {
            switch $0.userInterfaceStyle {
            case .dark, .unspecified:
                return .clear
            case .light:
                return .black
            @unknown default:
                return .clear
            }
        }
    }
}

// MARK: - Another colors
extension UIColor {
    static var littOnSurfacePrimary: UIColor {
        return UIColor { traitCollection -> UIColor in
            switch traitCollection.userInterfaceStyle {
            case .light:
                return .white
            case .dark, .unspecified:
                return UIColor(red: 28.0/255.0, green: 28.0/255.0, blue: 30.0/255.0, alpha: 1)
            @unknown default:
                return UIColor(red: 28.0/255.0, green: 28.0/255.0, blue: 30.0/255.0, alpha: 1)
            }
        }
    }
	
	static var littGradientColor: UIColor {
		return UIColor(light: UIColor(red: 243.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1),
					   dark: .secondarySystemBackground)
	}
    
    static var checkmarkViewSelectedBackgroundColor: UIColor {
        UIColor(light: UIColor(red: 223.0/255.0, green: 244.0/255.0, blue: 226.0/255.0, alpha: 1),
                dark: UIColor(red: 43.0/255.0, green: 72.0/255.0, blue: 47.0/255.0, alpha: 1))
    }
    
    static var littOnSurfaceSecondary: UIColor {
		return UIColor { traitCollection -> UIColor in
			switch traitCollection.userInterfaceStyle {
			case .dark, .unspecified:
				return UIColor(red: 44.0/255.0, green: 44.0/255.0, blue: 46.0/255.0, alpha: 1)
			case .light:
				return .white
			@unknown default:
				return UIColor(red: 44.0/255.0, green: 44.0/255.0, blue: 46.0/255.0, alpha: 1)
			}
		}
	}
	
	static var littSecondaryTextColor: UIColor {
		.systemGray2
	}
}

// MARK: - Image from color
extension UIColor {
    func image(size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        UIGraphicsImageRenderer(size: size).image { context in
            self.setFill()
            context.fill(CGRect(origin: .zero, size: size))
        }
	}
}
