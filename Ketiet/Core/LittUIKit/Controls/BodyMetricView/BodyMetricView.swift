//
//  BodyMetricView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class BodyMetricView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .natural
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.font = .subTitle
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var diffLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        return label
    }()
    
    private lazy var currentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodyBold
        return label
    }()
    
    private(set) lazy var inputButton = LittBasicButton(
        title: "INPUT".localized(),
        config: .capsule
    )
    
    private lazy var iconView: SquareIconView = {
        let view = SquareIconView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        animate(isHighlighted: true)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        animate(isHighlighted: false)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        animate(isHighlighted: false)
    }
    
    init(
        metricName: String,
        currentValue: String?,
        diffValue: String?,
        color: UIColor
    ) {
        super.init(frame: .zero)
        titleLabel.text = metricName
        currentLabel.textColor = color
        if currentValue != nil {
            currentLabel.text = currentValue
        }
        
        if diffValue != nil {
            diffLabel.text = diffValue
        }
        
        configureView()
        
        iconView.backgroundColor = color
        iconView.iconImageType = .system("figure.stand")
    }
    
    func setCurrentMetric(_ value: String?, diff: String?) {
        currentLabel.text = value
        diffLabel.text = diff ?? " "
    }
}

// MARK: - Private methods
extension BodyMetricView {
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = .littMediumCornerRadius
        clipsToBounds = true
        
        let backgroundView = UIView()
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.backgroundColor = .littSecondaryBackgroundColor
        
        let todayLabel = UILabel()
        todayLabel.translatesAutoresizingMaskIntoConstraints = false
        todayLabel.font = .body
        todayLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
		todayLabel.text = "TODAY".localized() + ":"
        todayLabel.textColor = .secondaryLabel
        
        let stackView = UIStackView(arrangedSubviews: [todayLabel, currentLabel, diffLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubviews([backgroundView, iconView, titleLabel, inputButton, stackView])
        
        NSLayoutConstraint.activate([
            
            backgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundView.topAnchor.constraint(equalTo: topAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            iconView.topAnchor.constraint(equalTo: topAnchor, constant: .largeLittMargin),
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .largeLittMargin),
            iconView.widthAnchor.constraint(equalToConstant: 24),
            iconView.heightAnchor.constraint(equalToConstant: 24),
            
            titleLabel.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: .mediumLittMargin),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            stackView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: .largeLittMargin),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .largeLittMargin),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: inputButton.topAnchor, constant: -(.mediumLittMargin)),
            
            inputButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.largeLittMargin),
            inputButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .largeLittMargin),
            inputButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.largeLittMargin),
            inputButton.heightAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    private func animate(isHighlighted: Bool) {
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 0,
            options: .allowUserInteraction,
            animations: {
                self.transform = isHighlighted ? .init(scaleX: 0.96, y: 0.96) : .identity
            })
    }
}
