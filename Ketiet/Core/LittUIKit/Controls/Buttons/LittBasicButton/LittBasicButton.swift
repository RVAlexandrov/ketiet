//
//  LittBasicButton.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 24.04.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class LittBasicButton: UIButton {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
    
    private var tapHandler: (() -> Void)?
    private let config: LittBasicButtonConfiguration
    
    init(
        title: String = "",
        config: LittBasicButtonConfiguration
    ) {
        self.config = config
        super.init(frame: .zero)
        var conf = config.configuration
        switch config {
        case .custom:
            break
        default:
            conf.attributedTitle = AttributedString(
                title,
                attributes: AttributeContainer([.font: config.font])
            )
        }
        configuration = conf
        translatesAutoresizingMaskIntoConstraints = false
        addAction(
            UIAction { [weak self] _ in
                self?.tapHandler?()
            },
            for: .touchUpInside
        )
    }
    
    func setTitle(_ title: String?) {
        switch config {
        case .custom:
            configuration?.title = title
            setNeedsUpdateConfiguration()
        default:
            configuration?.attributedTitle = AttributedString(
                title ?? "",
                attributes: AttributeContainer([.font: config.font])
            )
            setNeedsUpdateConfiguration()
        }
    }
    
    func setBackgroundColor(_ color: UIColor) {
        configuration?.baseBackgroundColor = color
        setNeedsUpdateConfiguration()
    }
    
    func setTapHandler(handler: @escaping() -> Void) {
        tapHandler = handler
    }
}
