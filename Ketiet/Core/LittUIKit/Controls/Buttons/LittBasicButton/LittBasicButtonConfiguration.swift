//
//  LIttBasicButtonConfiguration.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

enum LittBasicButtonConfiguration {
    case filledLarge(UIColor = .accentColor)
    case filledMedium
    case capsule
    case borderedTinted(UIColor = .accentColor)
    case custom(UIButton.Configuration)
    
    var configuration: UIButton.Configuration {
        switch self {
        case let .filledLarge(color):
            var configuration = UIButton.Configuration.filled()
            configuration.baseBackgroundColor = color
            configuration.cornerStyle = .large
            configuration.buttonSize = .large
            return configuration
        case .filledMedium:
            var configuration = UIButton.Configuration.filled()
            configuration.baseBackgroundColor = .accentColor
            configuration.cornerStyle = .medium
            configuration.buttonSize = .medium
            return configuration
        case .capsule:
            var configuration = UIButton.Configuration.filled()
            configuration.baseBackgroundColor = .accentColor
            configuration.cornerStyle = .capsule
            configuration.buttonSize = .medium
            return configuration
        case let .borderedTinted(backgroundColor):
            var configuration = UIButton.Configuration.borderedTinted()
            configuration.baseBackgroundColor = backgroundColor
            configuration.baseForegroundColor = backgroundColor
            configuration.cornerStyle = .medium
            configuration.buttonSize = .medium
            return configuration
        case let .custom(configuration):
            return configuration
        }
    }
    
    var font: UIFont {
        switch self {
        case .filledLarge, .filledMedium, .capsule, .borderedTinted, .custom:
            return .bodyBold
        }
    }
}


