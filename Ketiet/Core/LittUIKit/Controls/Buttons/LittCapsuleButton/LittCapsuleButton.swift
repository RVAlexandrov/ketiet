//
//  BarButtonButton.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 13.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

/// Маленькая кнопка на подобие кнопочек "скачать" в AppStore
class LittCapsuleButton: UIButton {
    
    init(title: String) {
        super.init(frame: .zero)
        var conf = UIButton.Configuration.gray()
        conf.attributedTitle = AttributedString(title.uppercased(),
                                                          attributes: AttributeContainer([.font: UIFont.barButtonTitle]))
        conf.baseForegroundColor = .systemBlue
        conf.cornerStyle = .capsule
        conf.buttonSize = .mini
        
        configuration = conf
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
