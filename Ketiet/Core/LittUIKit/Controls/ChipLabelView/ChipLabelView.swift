//
//  ChipLabel.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ChipLabelView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .systemBackground
        label.font = .smallBodyHeavy
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private var textColor: UIColor
    private let chipColor: UIColor
    private var text: String
    
    init(text: String,
         textColor: UIColor,
         chipColor: UIColor = .label) {
        self.text = text
        self.textColor = textColor
        self.chipColor = chipColor
        super.init(frame: .zero)
        configureView()
    }
    
    func setText(_ value: String) {
        text = value
        label.text = value
    }
    
    func setColor(_ color: UIColor) {
        textColor = color
        label.textColor = color
    }
}

// MARK: - Private methods
extension ChipLabelView {
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(label)
        backgroundColor = chipColor
        label.textColor = textColor
        label.text = text
        
        layer.cornerRadius = .littBasicCornerRadius
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor),
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.mediumLittMargin),
            heightAnchor.constraint(equalToConstant: 25.0)
        ])
    }
}

