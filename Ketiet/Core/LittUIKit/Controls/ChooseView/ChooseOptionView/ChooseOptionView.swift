//
//  GoalCardView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 08.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ChooseOptionView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    var viewModel: ViewModel? {
        didSet {
            guard let viewModel = viewModel,
                  viewModel != oldValue else { return }
            layer.borderColor = viewModel.isSelected ? UIColor.accentColor.cgColor : UIColor.clear.cgColor
            checkMarkView.isSelected = viewModel.isSelected
            descriptionLabel.isHidden = viewModel.description == nil
            descriptionLabel.text = viewModel.description
            titleLabel.text = viewModel.title
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let viewModel = viewModel else { return }

        let layout = Layout(viewModel: viewModel,
                            width: bounds.width)
        
        zip([checkMarkView, titleLabel, descriptionLabel], layout.itemRects).forEach { (view, frame) in
            view.frame = frame
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .bodySemibold
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .smallBody
        label.textColor = .systemGray
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var checkMarkView: CheckMarkView = CheckMarkView()
    
    init() {
        super.init(frame: .zero)
        configureView()
        checkMarkView.isUserInteractionEnabled = false
        layer.borderWidth = 2
    }
}

// MARK: - Private methods
extension ChooseOptionView {
    private func configureView() {
        layer.cornerRadius = 12
        layer.borderColor = UIColor.clear.cgColor
        backgroundColor = .littSecondaryBackgroundColor
        descriptionLabel.isHidden = true
        
        addSubviews([checkMarkView, titleLabel, descriptionLabel])
    }
}

// MARK: - ViewModel
extension ChooseOptionView {
    struct ViewModel: Equatable {
        let isSelected: Bool
        let title: String
        let description: String?
    }
}

// MARK: - Layout
extension ChooseOptionView {
    struct Layout {
        let viewModel: ViewModel
        let width: CGFloat
        
        var itemRects: [CGRect] {
            var rects: [CGRect] = []
            let checkMarkViewFrame = CGRect(x: CGFloat.largeLittMargin,
                                            y: CGFloat.largeLittMargin,
                                            width: 28,
                                            height: 28)
            rects.append(checkMarkViewFrame)
            
            let titleWidth = width - CGFloat.largeLittMargin - checkMarkViewFrame.maxX - CGFloat.largeLittMargin
            let titleHeight = viewModel.title.height(withWidth: titleWidth, font: .bodySemibold)
            let titleFrame = CGRect(x: checkMarkViewFrame.maxX + CGFloat.largeLittMargin,
                                    y: CGFloat.largeLittMargin,
                                    width: titleWidth,
                                    height: titleHeight)
            rects.append(titleFrame)

            if let description = viewModel.description {
                let descriptionHeight = description.height(withWidth: titleWidth, font: .smallBody)
                let descriptionFrame = CGRect(x: checkMarkViewFrame.maxX + CGFloat.largeLittMargin,
                                              y: titleFrame.maxY + CGFloat.smallLittMargin,
                                              width: titleWidth,
                                              height: descriptionHeight)
                rects.append(descriptionFrame)
            } else {
                if titleHeight < checkMarkViewFrame.height {
                    let centeredTitleFrame = CGRect(x: titleFrame.minX,
                                                    y: (2 * CGFloat.largeLittMargin + checkMarkViewFrame.height - titleHeight) / 2,
                                                    width: titleWidth,
                                                    height: titleHeight)
                    rects[1] = centeredTitleFrame
                }
                rects.append(CGRect.zero)
            }
            
            return rects
        }
        
        func height(itemRects: [CGRect]) -> CGFloat {
            if viewModel.description != nil {
                guard itemRects.count > 2 else { return 0 }
                let height = max(itemRects[0].maxY, itemRects[1].maxY, itemRects[2].maxY) + CGFloat.largeLittMargin
                return height
            } else {
                guard itemRects.count > 1 else { return 0 }
                let height = max(itemRects[0].maxY, itemRects[1].maxY) + CGFloat.largeLittMargin
                return height
            }
        }
    }
}
