//
//  ExtendedChooseOptionView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

final class ExtendedChooseOptionView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    var viewModel: ViewModel? {
        didSet {
            guard let viewModel = viewModel,
                  viewModel != oldValue else { return }
            layer.borderColor = viewModel.isSelected ? UIColor.accentColor.cgColor : UIColor.clear.cgColor
            checkMarkView.isSelected = viewModel.isSelected
            descriptionLabel.text = viewModel.description
            var conf = button.configuration
            conf?.title = viewModel.buttonTitle
            button.configuration = conf
            button.setNeedsUpdateConfiguration()
            titleLabel.text = viewModel.title
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let viewModel = viewModel else { return }

        let layout = Layout(viewModel: viewModel,
                            width: bounds.width)
        
        zip([checkMarkView, titleLabel, descriptionLabel, button], layout.itemRects).forEach { (view, frame) in
            view.frame = frame
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .bodySemibold
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .smallBody
        label.textColor = .systemGray
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var button: LittBasicButton = {
        var buttonConfiguration = UIButton.Configuration.gray()
        buttonConfiguration.cornerStyle = .large
        let button = LittBasicButton(title: "", config: .custom(buttonConfiguration))
        button.translatesAutoresizingMaskIntoConstraints = true
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var checkMarkView: CheckMarkView = CheckMarkView()
    
    init() {
        super.init(frame: .zero)
        configureView()
        checkMarkView.isUserInteractionEnabled = false
        layer.borderWidth = 2
    }
}

// MARK: - Private methods
extension ExtendedChooseOptionView {
    private func configureView() {
        layer.cornerRadius = 12
        layer.borderColor = UIColor.clear.cgColor
        backgroundColor = .littSecondaryBackgroundColor
        
        addSubviews([checkMarkView, titleLabel, descriptionLabel, button])
    }
    
    @objc
    private func didTapButton() {
        viewModel?.buttonCompletion?()
    }
}

// MARK: - ViewModel
extension ExtendedChooseOptionView {
    struct ViewModel: Equatable {
        static func == (lhs: ExtendedChooseOptionView.ViewModel, rhs: ExtendedChooseOptionView.ViewModel) -> Bool {
            lhs.isSelected == rhs.isSelected &&
            lhs.title == rhs.title &&
            lhs.description == rhs.description &&
            lhs.buttonTitle == rhs.buttonTitle
        }
        
        let isSelected: Bool
        let title: String
        let description: String
        let buttonTitle: String
        let buttonCompletion: (() -> Void)?
    }
}

// MARK: - Layout
extension ExtendedChooseOptionView {
    struct Layout {
        let viewModel: ViewModel
        let width: CGFloat
        
        var itemRects: [CGRect] {
            var rects: [CGRect] = []
            let checkMarkViewFrame = CGRect(x: CGFloat.largeLittMargin,
                                            y: CGFloat.largeLittMargin,
                                            width: 28,
                                            height: 28)
            rects.append(checkMarkViewFrame)
            
            let titleWidth = width - CGFloat.largeLittMargin - checkMarkViewFrame.maxX - CGFloat.largeLittMargin
            let titleHeight = viewModel.title.height(withWidth: titleWidth, font: .bodySemibold)
            let titleFrame = CGRect(x: checkMarkViewFrame.maxX + CGFloat.largeLittMargin,
                                    y: CGFloat.largeLittMargin,
                                    width: titleWidth,
                                    height: titleHeight)
            rects.append(titleFrame)
            
            let description = viewModel.description
            let descriptionHeight = description.height(withWidth: titleWidth, font: .smallBody)
            let descriptionFrame = CGRect(x: checkMarkViewFrame.maxX + CGFloat.largeLittMargin,
                                          y: titleFrame.maxY + CGFloat.smallLittMargin,
                                          width: titleWidth,
                                          height: descriptionHeight)
            rects.append(descriptionFrame)
            
            let buttonFrame = CGRect(x: CGFloat.largeLittMargin,
                                     y: descriptionFrame.maxY + CGFloat.largeLittMargin,
                                     width: width - 2 * CGFloat.largeLittMargin,
                                     height: 40)
            
            rects.append(buttonFrame)
            
            return rects
        }
        
        func height(itemRects: [CGRect]) -> CGFloat {
            guard itemRects.count > 3 else { return 0 }
            let height = itemRects[3].maxY + CGFloat.largeLittMargin
            return height
        }
    }
}
