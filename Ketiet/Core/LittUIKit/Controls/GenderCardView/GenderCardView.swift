//
//  GenderCard.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 05.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum SelectionType {
    case selected
    case notSelected
}

class GenderCardView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
        
    private lazy var genderImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: gender.rawValue + "_gender_image")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var genderTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = (gender.rawValue.uppercased() + "_GENDER_CARD_TITLE").localized()
        label.font = .bodySemibold
        label.textAlignment = .center
        return label
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .littSecondaryBackgroundColor
        view.layer.cornerRadius = 12
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let gender: Gender
    
    private var selection: SelectionType = .notSelected {
        didSet {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                switch self.selection {
                case .notSelected:
					self.transform = .identity
                case .selected:
                    self.transform = CGAffineTransform(scaleX: 1.07, y: 1.07)
                }
            })
        }
    }
    
    init(gender: Gender) {
        self.gender = gender
        super.init(frame: .zero)
        configureView()
    }
}

// MARK: - Public API
extension GenderCardView {
    func setSelection(_ selection: SelectionType) {
        self.selection = selection
    }
}

// MARK: - Private methods
extension GenderCardView {
    private func configureView() {
        layer.cornerRadius = 12
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        containerView.layer.addShadow(bounds)
        addSubviews([containerView, genderTitle])
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(genderImageView)
        
        NSLayoutConstraint.activate([
            genderImageView.topAnchor.constraint(equalTo: containerView.topAnchor),
            genderImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            genderImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            genderImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            genderImageView.widthAnchor.constraint(equalTo: containerView.widthAnchor),
            genderImageView.heightAnchor.constraint(equalTo: containerView.widthAnchor),
            
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            genderTitle.topAnchor.constraint(equalTo: genderImageView.bottomAnchor, constant: 6),
            genderTitle.leadingAnchor.constraint(equalTo: genderImageView.leadingAnchor),
            genderTitle.trailingAnchor.constraint(equalTo: genderImageView.trailingAnchor),
            genderTitle.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -6)
        ])
    }
}
