//
//  HorizontalPercentBarView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class HorizontalPercentBarView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
        
    private lazy var progressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.progress = 0.0
        progressView.layer.cornerRadius = 15 / 2
        progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 8
        progressView.subviews[1].clipsToBounds = true
        progressView.translatesAutoresizingMaskIntoConstraints = false
        return progressView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        label.textColor = .label
        return label
    }()
    
    private lazy var hoizontalStackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.spacing = 2
        return view
    }()
    
    private lazy var currentValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBodySemibold
        label.textColor = .secondaryLabel
        return label
    }()
    
    private lazy var slashLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBodySemibold
        label.textColor = .secondaryLabel
        label.text = "/"
        return label
    }()
    
    private lazy var targetValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBodySemibold
        label.textColor = .secondaryLabel
        return label
    }()
    
    init(title: String,
         color: UIColor,
         targetValue: Int = 0,
         currentValue: Int = 0) {
        super.init(frame: .zero)
        progressView.progressTintColor = color
        progressView.progress = Float(currentValue) / Float(targetValue)
        currentValueLabel.text = String(currentValue)
        targetValueLabel.text = String(targetValue)
        titleLabel.text = title
        configureView()
    }
    
    func setCurrentPercentValue(_ percentValue: Float, rawValue: Int, animate: Bool = true) {
        progressView.setProgress(percentValue, animated: animate)
        currentValueLabel.text = String(rawValue)
    }
    
    func setTargetValue(_ value: Int) {
        targetValueLabel.text = String(value)
    }
}

// MARK: - Private methods
extension HorizontalPercentBarView {
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        hoizontalStackView.addArrangedSubviews([currentValueLabel, slashLabel, targetValueLabel])
        addSubviews([titleLabel, hoizontalStackView, progressView])
        
        NSLayoutConstraint.activate([
            progressView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 3),
            progressView.bottomAnchor.constraint(equalTo: bottomAnchor),
            progressView.leadingAnchor.constraint(equalTo: leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: trailingAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 15),
            
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            
            hoizontalStackView.topAnchor.constraint(equalTo: topAnchor),
            hoizontalStackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
}
