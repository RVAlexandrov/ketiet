//
//  ImageButton.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 01.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class ImageButton: UIButton {
    
    var target: ((NavigationBarItemType) -> Void)?
    var navigationBarItemType = NavigationBarItemType.none
    
    init(frame: CGRect,
         target: ((NavigationBarItemType) -> Void)?,
         navigationBarItemType: NavigationBarItemType) {
        self.target = target
        self.navigationBarItemType = navigationBarItemType
        super.init(frame: frame)
        setImage(UIImage(named: navigationBarItemType.rawValue)?.withTintColor(.label), for: .normal)
        layer.cornerRadius = 22.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
