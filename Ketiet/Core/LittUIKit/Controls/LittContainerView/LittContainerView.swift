//
//  LittContainerView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 14.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class LittContainerView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
    
    private var isShadowed: Bool
    
    init(cornerRadius: CGFloat, isShadowed: Bool = true) {
        self.isShadowed = isShadowed
        super.init(frame: .zero)
        layer.cornerRadius = cornerRadius
        backgroundColor = .littSecondaryBackgroundColor
        translatesAutoresizingMaskIntoConstraints = false
    }
	
	override func layoutSubviews() {
		super.layoutSubviews()
        if isShadowed {
            layer.addShadow(bounds)
        }
	}
}
