//
//  LittTabBarController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 25.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import AudioToolbox

final class LittTabBarController: UIViewController {
	
	private struct Constants {
		static let tabBarBottomConstant: CGFloat = -15
		static let iphone8PlusHeight: CGFloat = 736.0
	}
    private var viewControllers: [UIViewController] = []
	private lazy var tabBarBottomConstraint = tabBarView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,
																				 constant: Constants.tabBarBottomConstant)
	private var tabBarHidden = false
	
    
    private var barItems: [LittTabItem] = [] {
        didSet {
            configureBarItems()
        }
    }
        
    private var selectedIndex: Int = 0 {
        didSet {
            barItems[oldValue].setInactive()
            barItems[selectedIndex].setActive()
            
            let previousVC = viewControllers[oldValue]
            previousVC.willMove(toParent: nil)
            previousVC.view.removeFromSuperview()
            previousVC.removeFromParent()
            let newVC = viewControllers[selectedIndex]
            addChild(newVC)
            newVC.view.frame = containerView.bounds
            containerView.addSubview(newVC.view)
        }
    }
    
    private var tabBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
		view.layer.cornerRadius = UIScreen.main.bounds.height > Constants.iphone8PlusHeight ? 20.0 : 16.0
        return view
    }()
    
    private lazy var blurView: UIView = {
        let blurEffect = UIBlurEffect(style: .systemChromeMaterial)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
		blurEffectView.layer.cornerRadius = UIScreen.main.bounds.height > Constants.iphone8PlusHeight ? 20.0 : 16.0
        blurEffectView.clipsToBounds = true
        return blurEffectView
    }()
    
    private var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .systemBackground
        return view
    }()
    
    private var stackView: UIStackView = {
        let view = UIStackView()
        view.alignment = .center
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTabView()
        selectedIndex = 0
    }
	
	func setTabBarHidden(_ hidden: Bool) {
		if hidden == tabBarHidden {
			return
		}
		tabBarHidden.toggle()
		UIView.animate(withDuration: CATransaction.animationDuration(),
					   delay: 0,
					   options: .curveEaseOut) {
			if hidden {
				self.tabBarBottomConstraint.constant += (UIScreen.main.bounds.height - self.tabBarView.frame.minY)
			} else {
				self.tabBarBottomConstraint.constant = Constants.tabBarBottomConstant
			}
			self.view.layoutIfNeeded()
		}
	}
}

// MARK: - Private methods
extension LittTabBarController {
    private func configureTabView() {
		let heightOfTabBar: CGFloat = UIScreen.main.bounds.height > Constants.iphone8PlusHeight ? 65.0 : 60.0
        
        let mainVC = MainAssembly().initialViewController()
        
		let secondVc = DayMenuDisplayingAssembly().initialViewController()
        
		let thirdVc = UINavigationController(rootViewController: ProfileAssembly().initialViewController())
        
		let fourthVc = UINavigationController(rootViewController: SettingsAssembly().initialViewController())
		fourthVc.navigationBar.prefersLargeTitles = true
        
        setTabObjects([
            LittTabObject(viewController: mainVC,
						  tabItem: LittTabItem(image: UIImage(systemName: "house"),
											   title: "MAIN_PAGE".localized(),
									height: heightOfTabBar - 6)),
            LittTabObject(viewController: secondVc,
						  tabItem: LittTabItem(image: UIImage(systemName: "calendar"),
											   title: "TODAY".localized(),
											   height: heightOfTabBar - 6)),
            LittTabObject(viewController: thirdVc,
						  tabItem: LittTabItem(image: UIImage(systemName: "person.crop.circle"),
											   title: "PROFILE".localized(),
											   height: heightOfTabBar - 6)),
            LittTabObject(viewController: fourthVc,
						  tabItem: LittTabItem(image: UIImage(systemName: "gear"),
											   title: "SETTINGS".localized(),
											   height: heightOfTabBar - 6))
        ])
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .systemBackground
        view.addSubview(containerView)
        view.addSubview(tabBarView)
        tabBarView.addSubviews([blurView, stackView])
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
			tabBarBottomConstraint,
            tabBarView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .littScreenEdgeMargin),
            tabBarView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.littScreenEdgeMargin),
            tabBarView.heightAnchor.constraint(equalToConstant: heightOfTabBar),
            
            blurView.topAnchor.constraint(equalTo: tabBarView.topAnchor),
            blurView.leadingAnchor.constraint(equalTo: tabBarView.leadingAnchor),
            blurView.trailingAnchor.constraint(equalTo: tabBarView.trailingAnchor),
            blurView.bottomAnchor.constraint(equalTo: tabBarView.bottomAnchor),
            
            stackView.topAnchor.constraint(equalTo: tabBarView.topAnchor, constant: 3),
            stackView.leadingAnchor.constraint(equalTo: tabBarView.leadingAnchor, constant: 3),
            stackView.bottomAnchor.constraint(equalTo: tabBarView.bottomAnchor, constant: -3),
            stackView.trailingAnchor.constraint(equalTo: tabBarView.trailingAnchor, constant: -3)
        ])
        
        tabBarView.layer.addShadow(CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 24, height: heightOfTabBar))
    }
    
    private func configureBarItems() {
        barItems.first?.setActive()
        barItems.forEach {
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBarItem(sender:))))
            stackView.addArrangedSubview($0)
        }
    }
    
    private func setTabObjects(_ tabObjects: [LittTabObject]) {
        var tabItems: [LittTabItem] = []
        var viewControllers: [UIViewController] = []
        
        tabObjects.forEach {
            tabItems.append($0.tabItem)
            viewControllers.append($0.viewController)
        }
        
        self.barItems = tabItems
        self.viewControllers = viewControllers
    }
    
    @objc
    private func didTapBarItem(sender: UITapGestureRecognizer) {
        
        let firstIndex = barItems.firstIndex { $0 == sender.view }
        guard let index = firstIndex else { return }
        if selectedIndex != index {
            selectedIndex = index
            let generator = UISelectionFeedbackGenerator()
            generator.selectionChanged()
        }
    }
}
