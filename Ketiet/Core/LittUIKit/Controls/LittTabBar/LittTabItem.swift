//
//  LittTabItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 25.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

private let iphone8PlusHeight: CGFloat = 736.0

class LittTabItem: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
    
    var accentColor: UIColor = .accentColor
    var inactiveColor: UIColor = .systemGray
    
    private let height: CGFloat
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 5
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .center
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .description
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    init(image: UIImage?, title: String?, height: CGFloat) {
        self.height = height
        super.init(frame: .zero)
        imageView.image = UIScreen.main.bounds.height < iphone8PlusHeight ?
            image?.withConfiguration(UIImage.SymbolConfiguration(pointSize: 16, weight: .semibold)) :
            image?.withConfiguration(UIImage.SymbolConfiguration(pointSize: 20, weight: .semibold))
        if let title = title {
            titleLabel.text = title
        } else {
            titleLabel.isHidden = true
        }
        configureView()
        setInactive()
    }
    
    func setActive() {
        titleLabel.textColor = accentColor
        imageView.tintColor = accentColor
    }
    
    func setInactive() {
        titleLabel.textColor = inactiveColor
        imageView.tintColor = inactiveColor
    }
}

// MARK: - Private methods
extension LittTabItem {
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        layer.cornerRadius = UIScreen.main.bounds.height < iphone8PlusHeight ? 20.0 : 16.0
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubviews([imageView, titleLabel])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
}
