//
//  LittTabObject.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 25.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

struct LittTabObject {
    let viewController: UIViewController
    let tabItem: LittTabItem
}
