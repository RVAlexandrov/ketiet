//
//  Е.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = LittButton(title: "ЖМИ!", color: .accentColor)
        view.addSubview(button)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            button.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.widthAnchor.constraint(equalToConstant: 100),
            button.heightAnchor.constraint(equalToConstant: 100)
        ])
    }
    
    @objc
    func didTapButton() {
        let vc = UIViewController()
        vc.view.backgroundColor = .systemBackground
//        present(vc, animated: true, completion: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
}
