//
//  LittContaineredTextFieldView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 24.04.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

protocol LittContaineredTextFieldViewDelegate: AnyObject {
    func textFieldDidEndEditing(view: LittContaineredTextFieldView)
    func textFieldDidChange(view: LittContaineredTextFieldView)
    func didUpdateErrorState(view: LittContaineredTextFieldView)
}

extension LittContaineredTextFieldViewDelegate {
    func textFieldDidEndEditing(view: LittContaineredTextFieldView) {}
    func textFieldDidChange(view: LittContaineredTextFieldView) {}
    func didUpdateErrorState(view: LittContaineredTextFieldView) {}
}

final class LittContaineredTextFieldView: UIView {
    
    private weak var delegate: LittContaineredTextFieldViewDelegate?
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 3
        return stackView
    }()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .subTitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private(set) lazy var textView: TextViewWithPaceholder = {
        let view = TextViewWithPaceholder()
        view.isScrollEnabled = false
        view.delegate = self
        view.tintColor = .accentColor
        view.returnKeyType = .done
        view.autocorrectionType = .no
        view.font = .body
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 0
        view.backgroundColor = nil
        view.textAlignment = .center
        return view
    }()
    private lazy var separatorView = UIView()
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .tertiarySystemGroupedBackground
        view.layer.cornerRadius = 12
        return view
    }()
    private lazy var validationLabel: UILabel = {
        let label = UILabel()
        label.textColor = .littFailureColor
        label.font = .description
        label.numberOfLines = 0
        label.isHidden = true
        return label
    }()
    private var validator: Validator
    private(set) var isValid = false
        
    init(
        title: String? = nil,
        validator: Validator,
        keyboardType: UIKeyboardType = .default,
        placehodler: String? = nil,
        delegate: LittContaineredTextFieldViewDelegate? = nil
    ) {
        self.validator = validator
        super.init(frame: .zero)
        self.delegate = delegate
        textView.keyboardType = keyboardType
        textView.setPlacehodler(placehodler)
        titleLabel.text = placehodler
        configureViews()
        configureReturnButton()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

// MARK: - Public API
extension LittContaineredTextFieldView {
    func forceValidate(animated: Bool) {
        let validation = validator.checkIntermediateValidation(ofString: textView.text)
        validation.isValid ? setSuccessStyle(animated: animated) : setFailureStyle(withText: validation.invalidMessage, animated: animated)
    }
    
    func setPlaceholder(_ placehodler: String?) {
        textView.setPlacehodler(placehodler)
    }
}

// MARK: - Private methods
extension LittContaineredTextFieldView {
    private func configureReturnButton() {
        let type = textView.keyboardType
        
        if type == .decimalPad || type == .numberPad {
            textView.inputAccessoryView = LittTextFieldView.configureDoneAccessoryView()
        }
    }
    
    private func configureViews() {
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubviews([titleLabel, stackView])
        containerView.addSubview(textView)
        stackView.addArrangedSubviews([containerView, separatorView, validationLabel])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.bottomAnchor.constraint(
                equalTo: stackView.topAnchor,
                constant: -.smallLittMargin
            ),
            
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            textView.topAnchor.constraint(
                equalTo: containerView.topAnchor,
                constant: .mediumLittMargin
            ),
            textView.leadingAnchor.constraint(
                equalTo: containerView.leadingAnchor,
                constant: .mediumLittMargin
            ),
            textView.bottomAnchor.constraint(
                equalTo: containerView.bottomAnchor,
                constant: -.mediumLittMargin
            ),
            textView.trailingAnchor.constraint(
                equalTo: containerView.trailingAnchor,
                constant: -.mediumLittMargin
            )
        ])
    }
    
    private func setFailureStyle(withText text: String, animated: Bool) {
        guard validationLabel.text != text || isValid else { return }
        UINotificationFeedbackGenerator().notificationOccurred(.error)
        self.validationLabel.text = text
        isValid = false
        validationLabel.text = text
        separatorView.backgroundColor = .systemRed
        if animated {
            UIView.animate(withDuration: 0.3) {
                self.validationLabel.isHidden = false
            }
        } else {
            validationLabel.isHidden = false
        }
        delegate?.didUpdateErrorState(view: self)
    }
    
    private func setSuccessStyle(animated: Bool) {
        guard !isValid else { return }
        separatorView.backgroundColor = textView.isFirstResponder ? .accentColor : .systemGray2
        isValid = true
        if animated {
            UIView.animate(withDuration: 0.3) {
                self.validationLabel.isHidden = true
            }
        } else {
            validationLabel.isHidden = true
        }
        delegate?.didUpdateErrorState(view: self)
    }
}

// MARK: - UITextViewDelegate
extension LittContaineredTextFieldView: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        let result = validator.checkIntermediateValidation(ofString: textView.text)
        result.isValid ? setSuccessStyle(animated: true) : setFailureStyle(withText: result.invalidMessage, animated: true)
        delegate?.textFieldDidChange(view: self)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.separatorView.backgroundColor = self.isValid ? .systemGray2 : .systemRed
        }
        let result = validator.checkFullValidation(ofString: textView.text)
        result.isValid ? setSuccessStyle(animated: true) : setFailureStyle(withText: result.invalidMessage, animated: true)
        delegate?.textFieldDidEndEditing(view: self)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.separatorView.backgroundColor = .accentColor
        }
    }
    
    func textView(
        _ textView: UITextView,
        shouldChangeTextIn range: NSRange,
        replacementText text: String
    ) -> Bool {
        let string = NSMutableString(string: textView.text)
        string.replaceCharacters(in: range, with: text)
        return validator.checkFullValidation(ofString: String(string)).isValid
    }
}

extension LittContaineredTextFieldView {
    static func configureDoneAccessoryView() -> UIView {
        let toolBar = UIToolbar(frame: CGRect(origin: .zero,
                                              size: UIToolbar.regularSize))
        let flexButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                         target: nil,
                                         action: nil)
        let rightButton = LittCapsuleButton(title: "DONE".localized().uppercased())
        rightButton.addTarget(UIApplication.shared,
                              action: #selector(UIApplication.endEditing),
                              for: .touchUpInside)
        let button = UIBarButtonItem(customView: rightButton)
        toolBar.setItems([flexButton, button], animated: true)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
}
