//
//  KTTextField.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 01.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol LittTextFieldViewDelegate: AnyObject {
	func textFieldDidEndEditing(view: LittTextFieldView)
	func textFieldDidChange(view: LittTextFieldView)
	func willUpdateErrorState(view: LittTextFieldView)
	func didUpdateErrorState(view: LittTextFieldView)
}

extension LittTextFieldViewDelegate {
	func textFieldDidEndEditing(view: LittTextFieldView) {}
	func textFieldDidChange(view: LittTextFieldView) {}
	func willUpdateErrorState(view: LittTextFieldView) {}
	func didUpdateErrorState(view: LittTextFieldView) {}
}

final class LittTextFieldView: UIView {
	
	private struct Constants {
		static let separatorHighlightDuration: TimeInterval = 0.4
        static let separatorStandardHeight: CGFloat = 0.5
		static let separatorAccentHeight: CGFloat = 4
	}
	
    weak var delegate: LittTextFieldViewDelegate?
    private lazy var stackView = UIStackView()
    private lazy var titleLabel = UILabel()
    private(set) lazy var textField = UITextField()
    private lazy var separatorView = UIView()
    private lazy var validationLabel = UILabel()
    private var validator: Validator?
	private lazy var separatorHeightConstraint = separatorView.heightAnchor.constraint(equalToConstant: Constants.separatorStandardHeight)
    
    lazy var isValid = false
    lazy var isEmpty = true
        
    convenience init(title: String? = nil,
                     validator: Validator?,
                     keyboardType: UIKeyboardType = .default,
                     textFieldBackgroundColor: UIColor? = nil,
                     delegate: LittTextFieldViewDelegate? = nil) {
        self.init(frame: .zero)
		set(title: title,
			validator: validator,
			keyboardType: keyboardType,
			textFieldBackgroundColor: textFieldBackgroundColor,
			delegate: delegate)
    }
	
	func set(title: String? = nil,
			 validator: Validator?,
			 keyboardType: UIKeyboardType = .default,
			 textFieldBackgroundColor: UIColor? = nil,
			 delegate: LittTextFieldViewDelegate? = nil) {
		self.validator = validator
		textField.keyboardType = keyboardType
		self.delegate = delegate
		titleLabel.text = title
        
        configureReturnButton()
	}
	
	func highlightSeparator(with color: UIColor) {
		let oldColor = separatorView.backgroundColor
		UIView.animate(withDuration: CATransaction.animationDuration()) {
			self.separatorView.backgroundColor = color
			self.separatorHeightConstraint.constant = Constants.separatorAccentHeight
		} completion: { _ in
			UIView.animate(withDuration: CATransaction.animationDuration(),
						   delay: Constants.separatorHighlightDuration,
						   options: []) {
				self.separatorView.backgroundColor = oldColor
				self.separatorHeightConstraint.constant = Constants.separatorStandardHeight
			}
		}
	}
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    public override init(frame: CGRect) {
		super.init(frame: .zero)
		configureViews()
	}
}

// MARK: - Public API
extension LittTextFieldView {
	func forceValidate(animated: Bool) {
        guard let text = textField.text, let validator = validator else { return }
        let validation = validator.checkIntermediateValidation(ofString: text)
        validation.isValid ? setSuccessStyle(animated: animated) : setFailureStyle(withText: validation.invalidMessage, animated: animated)
    }
}

// MARK: - Private methods
extension LittTextFieldView {
    private func configureReturnButton() {
        let type = textField.keyboardType
        
        if type == .decimalPad || type == .numberPad {
            textField.inputAccessoryView = LittTextFieldView.configureDoneAccessoryView()
        }
    }
    
    private func configureViews() {
        translatesAutoresizingMaskIntoConstraints = false
        
        backgroundColor = .littSecondaryBackgroundColor
        
        configureTitleLabel()
        configureTextField()
        configureStackView()
        configureSeparatorView()
        configureValidationLabel()
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: .largeLittMargin),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .largeLittMargin),
            titleLabel.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -.smallLittMargin),
            
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .largeLittMargin),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.largeLittMargin),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.largeLittMargin),
            textField.heightAnchor.constraint(equalToConstant: 31),

            separatorHeightConstraint
        ])
    }
    
    private func configureTitleLabel() {
        titleLabel.font = .bodyBold
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func configureStackView() {
        addSubviews([titleLabel, stackView])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.addArrangedSubviews([textField, separatorView, validationLabel])
        stackView.spacing = 3
    }
    
    private func configureSeparatorView() {
        separatorView.backgroundColor = .systemGray2
		separatorView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func configureValidationLabel() {
        validationLabel.textColor = .littFailureColor
        validationLabel.font = .description
        validationLabel.numberOfLines = 0
        validationLabel.isHidden = true
    }
    
    private func configureTextField() {
        textField.delegate = self
        textField.tintColor = .accentColor
        textField.returnKeyType = .done
        textField.autocorrectionType = .no
        textField.borderStyle = .none
        textField.font = .body
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.borderWidth = 0
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
	private func setFailureStyle(withText text: String, animated: Bool) {
        guard validationLabel.text != text || isValid else { return }
		delegate?.willUpdateErrorState(view: self)
        UINotificationFeedbackGenerator().notificationOccurred(.error)
        self.validationLabel.text = text
        isValid = false
        validationLabel.text = text
        separatorView.backgroundColor = .systemRed
		if animated {
			UIView.animate(withDuration: 0.3) {
				self.validationLabel.isHidden = false
			}
		} else {
			validationLabel.isHidden = false
		}
		delegate?.didUpdateErrorState(view: self)
    }
    
	private func setSuccessStyle(animated: Bool) {
        guard !isValid else { return }
        delegate?.willUpdateErrorState(view: self)
        separatorView.backgroundColor = textField.isFirstResponder ? .accentColor : .systemGray2
        isValid = true
		if animated {
			UIView.animate(withDuration: 0.3) {
				self.validationLabel.isHidden = true
			}
		} else {
			validationLabel.isHidden = true
		}
		delegate?.didUpdateErrorState(view: self)
    }
    
    @objc private func textFieldDidChange() {
        guard let text = textField.text, let validator = validator else {
			delegate?.textFieldDidChange(view: self)
			return
		}
        let result = validator.checkIntermediateValidation(ofString: text)
        result.isValid ? setSuccessStyle(animated: true) : setFailureStyle(withText: result.invalidMessage, animated: true)
        isEmpty = text.isEmpty
        delegate?.textFieldDidChange(view: self)
    }
}

// MARK: - UITextFieldDelegate
extension LittTextFieldView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
		UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.separatorView.backgroundColor = self.isValid ? .systemGray2 : .systemRed
		}
        guard let text = textField.text, let validator = validator else {
			delegate?.textFieldDidEndEditing(view: self)
			return
		}
        let result = validator.checkFullValidation(ofString: text)
        result.isValid ? setSuccessStyle(animated: true) : setFailureStyle(withText: result.invalidMessage, animated: true)
        delegate?.textFieldDidEndEditing(view: self)
    }
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		UIView.animate(withDuration: CATransaction.animationDuration()) {
			self.separatorView.backgroundColor = .accentColor
		}
	}
}

extension LittTextFieldView {
    static func configureDoneAccessoryView() -> UIView {
        let toolBar = UIToolbar(frame: CGRect(origin: .zero,
                                              size: UIToolbar.regularSize))
        let flexButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                         target: nil,
                                         action: nil)
        let rightButton = LittCapsuleButton(title: "DONE".localized().uppercased())
        rightButton.addTarget(UIApplication.shared,
                              action: #selector(UIApplication.endEditing),
                              for: .touchUpInside)
        let button = UIBarButtonItem(customView: rightButton)
        toolBar.setItems([flexButton, button], animated: true)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
}
