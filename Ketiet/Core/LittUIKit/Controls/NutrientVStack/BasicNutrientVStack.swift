//
//  NutrientVStack.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 14.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class BasicNutrientVStack: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
    
	private lazy var valueLabel = UILabel.makeCenterAlignedLabel()
    private lazy var titleLabel = UILabel.makeCenterAlignedLabel()
    
    private let containerView = LittContainerView(cornerRadius: 12, isShadowed: false)
	
	var nutrient: Nutrient {
		didSet {
			valueLabel.text = nutrient.stringValue
			titleLabel.text = nutrient.type.rawValue.uppercased().localized()
		}
	}
	
	init(nutrient: Nutrient, font: UIFont = .body, color: UIColor) {
        self.nutrient = nutrient
        super.init(frame: .zero)
        configureView()
		updateView(font: font, color: color)
    }
}

extension BasicNutrientVStack {

	private func updateView(font: UIFont, color: UIColor) {
        valueLabel.text = nutrient.stringValue
        valueLabel.font = .bodyBold
        valueLabel.textColor = color
        
        titleLabel.text = nutrient.type.rawValue.uppercased().localized()
        titleLabel.font = font
    }
    
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.addArrangedSubviews([valueLabel, titleLabel])
        
        addSubview(containerView)
        containerView.addSubview(stackView)
        
        NSLayoutConstraint.activate(
            containerView.pinToSuperviewConstraints()
            +
            stackView.pinToSuperviewConstraints(insets: UIEdgeInsets(top: 12, left: 0, bottom: 12, right: 0), useSafeArea: false)
        )
    }
}

extension UILabel {
	
	static func makeCenterAlignedLabel() -> UILabel {
		let label = Self()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
	}
}
