//
//  VerticalPercentBarView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class VerticalPercentBarView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
        
    private lazy var progressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.progress = 0.0
        progressView.layer.cornerRadius = 8
        progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 8
        progressView.subviews[1].clipsToBounds = true
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.transform = CGAffineTransform(rotationAngle: .pi / -2)
        return progressView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        return label
    }()
    
    private lazy var hoizontalStackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.spacing = 2
        return view
    }()
    
    private lazy var currentValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .secondaryLabel
        return label
    }()
    
    private lazy var slashLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .secondaryLabel
        label.text = "/"
        return label
    }()
    
    private lazy var targetValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .secondaryLabel
        return label
    }()
    
    init(title: String,
         color: UIColor,
         targetValue: Int,
         currentValue: Int) {
        super.init(frame: .zero)
        progressView.progressTintColor = color
        progressView.progress = Float(currentValue) / Float(targetValue)
        currentValueLabel.text = String(currentValue)
        targetValueLabel.text = String(targetValue)
        titleLabel.text = title
        configureView()
    }
    
    func setCurrentPercentValue(_ percentValue: Float, rawValue: Int, animate: Bool = true) {
        progressView.setProgress(percentValue, animated: animate)
        currentValueLabel.text = String(rawValue)
    }
    
    func setTargetValue(_ value: Int) {
        targetValueLabel.text = String(value)
    }
}

// MARK: - Private methods
extension VerticalPercentBarView {
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        hoizontalStackView.addArrangedSubviews([currentValueLabel, slashLabel, targetValueLabel])
        addSubviews([titleLabel, hoizontalStackView, progressView])
        
        NSLayoutConstraint.activate([
            progressView.centerXAnchor.constraint(equalTo: centerXAnchor),
            progressView.centerYAnchor.constraint(equalTo: centerYAnchor),
            progressView.heightAnchor.constraint(equalToConstant: 20),
            progressView.widthAnchor.constraint(equalToConstant: 60),
            
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12),
            
            hoizontalStackView.topAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 30),
            hoizontalStackView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
}
