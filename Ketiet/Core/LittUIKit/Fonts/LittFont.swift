//
//  LittFont.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import CoreGraphics

enum LittFont {
    case titleDigits
    case primaryTitle
    case largeTitle
    case title
    case subTitle
    case bodyPrimary
    case bodySecondary
    case body
    case smallBodyPrimary
    case smallBodySecondary
    case smallBody
    case description
    case barButtonTitle
    
    var size: CGFloat {
        switch self {
        case .titleDigits:
            return 40
        case .primaryTitle:
            return 28
        case .largeTitle:
            return 28
        case .title:
            return 24
        case .subTitle:
            return 20
        case .bodyPrimary,
             .bodySecondary,
             .body:
            return 17
        case .smallBodyPrimary,
             .smallBodySecondary,
             .smallBody:
            return 15
        case .description:
            return 13
        case .barButtonTitle:
            return 13
        }
    }
    
    var weight: LittFontWeight {
        switch self {
        case .titleDigits:
            return .bold
        case .primaryTitle:
            return .heavy
        case .largeTitle:
            return .bold
        case .title:
            return .bold
        case .subTitle:
            return .semibold
        case .bodyPrimary:
            return .bold
        case .bodySecondary:
            return .semibold
        case .body:
            return .regular
        case .smallBodyPrimary:
            return .heavy
        case .smallBodySecondary:
            return .medium
        case .smallBody:
            return .regular
        case .description:
            return .regular
        case .barButtonTitle:
            return .bold
        }
    }
}
