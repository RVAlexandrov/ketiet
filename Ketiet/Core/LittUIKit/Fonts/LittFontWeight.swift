//
//  LittFontWeight.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

enum LittFontWeight {
    case heavy
    case bold
    case semibold
    case medium
    case regular
}
