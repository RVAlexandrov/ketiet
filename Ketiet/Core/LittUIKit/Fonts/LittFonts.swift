//
//  LittFonts.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 02.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIFont {
    static var giantDigits: UIFont {
        littFont(font: .titleDigits)
    }
    
    static var heavyLargeTitle: UIFont {
        littFont(font: .primaryTitle)
    }
    
    static var largeTitle: UIFont {
        littFont(font: .largeTitle)
    }
    
    static var title: UIFont {
        littFont(font: .title)
    }
    
    static var subTitle: UIFont {
        littFont(font: .subTitle)
    }
    
    static var bodyBold: UIFont {
        littFont(font: .bodyPrimary)
    }
    
    static var bodySemibold: UIFont {
        littFont(font: .bodySecondary)
    }
    
    static var body: UIFont {
        littFont(font: .body)
    }
    
    static var smallBodyHeavy: UIFont {
        littFont(font: .smallBodyPrimary)
    }
    
    static var smallBodySemibold: UIFont {
        littFont(font: .smallBodySecondary)
    }
    
    static var smallBody: UIFont {
        littFont(font: .smallBody)
    }
    
    static var description: UIFont {
        littFont(font: .description)
    }
    
    static var barButtonTitle: UIFont {
        littFont(font: .barButtonTitle)
    }
    
    static func littFont(font: LittFont) -> UIFont {
        .systemFont(ofSize: font.size, weight: font.weight.uikitWeight)
    }
}

extension LittFontWeight {
    
    var uikitWeight: UIFont.Weight {
        switch self {
        case .heavy:
            return .heavy
        case .bold:
            return .bold
        case .semibold:
            return .semibold
        case .medium:
            return .medium
        case .regular:
            return .regular
        }
    }
}
