//
//  ImageWrapper.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

enum ImageStringType: Equatable {
    case url(String)
    case asset(String)
    case system(String)
}
