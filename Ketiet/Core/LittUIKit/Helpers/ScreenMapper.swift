//
//  ScreenMapper.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 01.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ScreenMapper {
	func map<T>(iphoneSE: T, iphone8: T, iphone8Plus: T, iphoneX: T, iphoneXR: T) -> T {
		switch UIScreen.main.bounds.height {
		case 568:
			return iphoneSE
		case 667:
			return iphone8
		case 736:
			return iphone8Plus
		case 812:
			return iphoneX
		case 896:
			return iphoneXR
		default:
			return iphoneXR
		}
	}
}
