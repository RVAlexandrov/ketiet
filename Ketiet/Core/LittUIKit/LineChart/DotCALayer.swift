//
//  DotCALayer.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

/**
 * DotCALayer
 */
class DotCALayer: CALayer {

	var innerRadius: CGFloat = 8
	var dotInnerColor = UIColor.black

	override func layoutSublayers() {
		super.layoutSublayers()
		let inset = bounds.size.width - innerRadius
		let innerDotLayer = CALayer()
		innerDotLayer.frame = bounds.insetBy(dx: inset/2, dy: inset/2)
		innerDotLayer.backgroundColor = dotInnerColor.cgColor
		innerDotLayer.cornerRadius = innerRadius / 2
		addSublayer(innerDotLayer)
	}
}
