//
//  LineChart.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

struct PointEntry {
	let value: Float?
	let label: String
}

extension PointEntry: Comparable {
	static func <(lhs: PointEntry, rhs: PointEntry) -> Bool {
        if let l = lhs.value,
           let r = rhs.value {
            return l < r
        } else if lhs.value != nil {
            return false
        } else if rhs.value != nil {
            return true
        } else {
            return true
        }
	}
    
	static func ==(lhs: PointEntry, rhs: PointEntry) -> Bool {
		return lhs.value == rhs.value
	}
}

final class LineChart: UIView {
	
	/// gap between each point
	let lineGap: CGFloat = 70.0
	
	/// preseved space at top of the chart
	let topSpace: CGFloat = 40.0
	
	/// preserved space at bottom of the chart to show labels along the Y axis
	let bottomSpace: CGFloat = 40.0
	
	/// The top most horizontal line in the chart will be 10% higher than the highest value in the chart
	let topHorizontalLine: CGFloat = 1
	
	var isCurved = false

	/// Active or desactive animation on dots
	var animateDots: Bool = true
	
	var lineWidth: CGFloat = 3

	/// Active or desactive dots
	var showDots = true

	/// Dot inner Radius
	var innerRadius: CGFloat = 8

	/// Dot outer Radius
	var outerRadius: CGFloat = 12
	
	var dataEntries: [PointEntry]? {
		didSet {
			self.setNeedsLayout()
		}
	}
	
	var lineColor = UIColor.accentColor {
		didSet {
			self.setNeedsLayout()
		}
	}
	
	/// Contains the main line which represents the data
	private let dataLayer = CALayer()
	
	/// To show the gradient below the main line
	private let gradientLayer = CAGradientLayer()
	
	/// Contains dataLayer and gradientLayer
	private let mainLayer = CALayer()
	
	/// Contains mainLayer and label for each data entry
	private let scrollView = UIScrollView()
	private let valuesLeftView = UIView()
	
	/// Contains horizontal lines
	private let gridLayer = CALayer()
	
	/// An array of CGPoint on dataLayer coordinate system that the main line will go through. These points will be calculated from dataEntries array
	private var dataPoints: [CGPoint?]?

	override init(frame: CGRect) {
		super.init(frame: frame)
		setupView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setupView()
	}
	
	func setup(with progressValues: [Float], lineColor: UIColor) {
		self.lineColor = lineColor
		dataEntries = progressValues.enumerated().map {
            let value = $0.element == 0 && $0.offset >= 1 ? nil : $0.element
            return PointEntry(value: value,
                              label: String($0.offset + 1) + " " + "DAY_SHORT".localized())
		}
	}
	
	private func setupView() {
		mainLayer.addSublayer(dataLayer)
		scrollView.layer.addSublayer(mainLayer)
		scrollView.layer.addSublayer(gradientLayer)
		self.layer.addSublayer(gridLayer)
		self.addSubview(scrollView)
		self.backgroundColor = .clear
		addSubview(valuesLeftView)
		valuesLeftView.backgroundColor = .clear
		scrollView.showsHorizontalScrollIndicator = false
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		scrollView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
		valuesLeftView.frame = CGRect(x: 0, y: topSpace, width: 35, height: frame.size.height - topSpace - bottomSpace)
		if let dataEntries = dataEntries {
			scrollView.contentSize = CGSize(width: CGFloat(dataEntries.count) * lineGap, height: self.frame.size.height)
			mainLayer.frame = CGRect(x: 0, y: 0, width: CGFloat(dataEntries.count) * lineGap, height: self.frame.size.height)
			dataLayer.frame = CGRect(x: 0, y: topSpace, width: mainLayer.frame.width, height: mainLayer.frame.height - topSpace - bottomSpace)
			gradientLayer.frame = dataLayer.frame
			dataPoints = convertDataEntriesToPoints(entries: dataEntries)
			gridLayer.frame = CGRect(x: 0, y: topSpace, width: self.frame.width, height: mainLayer.frame.height - topSpace - bottomSpace)
			if showDots { drawDots() }
			clean()
			drawHorizontalLines()
			if isCurved {
				drawCurvedChart()
			} else {
				drawChart()
			}
			drawLables()
		}
	}
	
	/**
	 Convert an array of PointEntry to an array of CGPoint on dataLayer coordinate system
	 */
	private func convertDataEntriesToPoints(entries: [PointEntry]) -> [CGPoint?] {
        let nonZeroValuesEntries = entries.filter { $0.value != nil }
		if let max = nonZeroValuesEntries.max()?.value,
			let min = nonZeroValuesEntries.min()?.value {
			
			var result: [CGPoint?] = []
			let minMaxRange: CGFloat = CGFloat(max - min) * topHorizontalLine
			
			for i in 0..<entries.count {
                if let value = entries[i].value {
                    let height = minMaxRange == 0 ?
                    0 :
                    dataLayer.frame.height * (1 - ((CGFloat(value) - CGFloat(min)) / minMaxRange))
                    let point = CGPoint(x: CGFloat(i)*lineGap + 40, y: height)
                    result.append(point)
                } else {
                    result.append(nil)
                }
			}
			return result
		}
		return []
	}
	
	/**
	 Draw a zigzag line connecting all points in dataPoints
	 */
    private func drawChart() {
        if let dataPoints = dataPoints,
           dataPoints.count > 0,
           let layers = createLayersPath() {
            layers.forEach {
                dataLayer.addSublayer($0)
            }
        }
    }

	/**
	 Create a zigzag bezier path that connects all points in dataPoints
	 */
	private func createLayersPath() -> [CAShapeLayer]? {
		guard let dataPoints = dataPoints, dataPoints.count > 0 else {
			return nil
		}
        var layers = [CAShapeLayer]()
        
        var lastNonNilPoint: CGPoint?
		
		for i in 1..<dataPoints.count {
            let oldDataPoint = dataPoints[i-1]
            let newDataPoint = dataPoints[i]
            
            // первый кейс: первая nil, вторая nil, то не рисуем совсем
            // второй кейс: первый не nil, второй nil, то ставим точку на позицию (только для первой точки)
            // третий кейс: когда оба не nil, рисуем линию
            // четвёртый кейс: когда первая nil, а вторая не nil, то совмещаем с последним ненулевым
            if newDataPoint == nil, oldDataPoint == nil {
                continue
            } else if let oldDataPoint = oldDataPoint, newDataPoint == nil, i == 1 {
                lastNonNilPoint = oldDataPoint
                let dotPath = UIBezierPath()
                dotPath.move(to: oldDataPoint)
                dotPath.addLine(to: oldDataPoint)
                
                let dotLayer = CAShapeLayer()
                dotLayer.path = dotPath.cgPath
                dotLayer.strokeColor = lineColor.cgColor
                dotLayer.fillColor = UIColor.clear.cgColor
                dotLayer.lineWidth = lineWidth
                dotLayer.lineJoin = .round
                dotLayer.lineCap = .round
                layers.append(dotLayer)
            } else if let newDataPoint = newDataPoint,
                      let oldDataPoint = oldDataPoint {
                lastNonNilPoint = newDataPoint
                let localPath = UIBezierPath()
                localPath.move(to: oldDataPoint)
                localPath.addLine(to: newDataPoint)
                
                let lineLayer = CAShapeLayer()
                lineLayer.path = localPath.cgPath
                lineLayer.strokeColor = lineColor.cgColor
                lineLayer.fillColor = UIColor.clear.cgColor
                lineLayer.lineWidth = lineWidth
                lineLayer.lineJoin = .round
                lineLayer.lineCap = .round
                layers.append(lineLayer)
            } else if oldDataPoint == nil,
                      let newDataPoint = newDataPoint,
                      let lastNonNil = lastNonNilPoint {
                lastNonNilPoint = newDataPoint
                let dotPath = UIBezierPath()
                dotPath.move(to: lastNonNil)
                dotPath.addLine(to: newDataPoint)
                
                let dotLayer = CAShapeLayer()
                dotLayer.path = dotPath.cgPath
                dotLayer.strokeColor = lineColor.cgColor
                dotLayer.fillColor = UIColor.clear.cgColor
                dotLayer.lineWidth = lineWidth
                dotLayer.lineJoin = .round
                dotLayer.lineCap = .round
                layers.append(dotLayer)
            }
		}
		return layers
	}
	
	/**
	 Draw a curved line connecting all points in dataPoints
	 */
	private func drawCurvedChart() {
		guard let dataPoints = dataPoints, dataPoints.count > 0 else {
			return
		}
        if let path = CurveAlgorithm.shared.createCurvedPath(dataPoints.compactMap { $0 }) {
			let lineLayer = CAShapeLayer()
			lineLayer.path = path.cgPath
			lineLayer.strokeColor = lineColor.cgColor
			lineLayer.fillColor = UIColor.clear.cgColor
			lineLayer.lineWidth = lineWidth
			dataLayer.addSublayer(lineLayer)
		}
	}
	
	/**
	 Create titles at the bottom for all entries showed in the chart
	 */
	private func drawLables() {
		if let dataEntries = dataEntries,
			dataEntries.count > 0 {
			for i in 0..<dataEntries.count {
				let textLayer = CATextLayer()
				textLayer.frame = CGRect(x: lineGap*CGFloat(i) - lineGap/2 + 40, y: mainLayer.frame.size.height - bottomSpace/2 - 8, width: lineGap, height: 16)
                textLayer.foregroundColor = UIColor.label.cgColor
				textLayer.backgroundColor = UIColor.clear.cgColor
				textLayer.alignmentMode = CATextLayerAlignmentMode.center
				textLayer.contentsScale = UIScreen.main.scale
				textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
				textLayer.fontSize = 11
				textLayer.string = dataEntries[i].label
				mainLayer.addSublayer(textLayer)
			}
		}
	}
	
	/**
	 Create horizontal lines (grid lines) and show the value of each line
	 */
	private func drawHorizontalLines() {
		guard let dataEntries = dataEntries else {
			return
		}
		
		var gridValues: [CGFloat]? = nil
        gridValues = [0, 0.25, 0.5, 0.75, 1]
//		if dataEntries.count < 4 && dataEntries.count > 0 {
//			gridValues = [0, 1]
//		} else if dataEntries.count >= 4 {
//			gridValues = [0, 0.25, 0.5, 0.75, 1]
//		}
		if let gridValues = gridValues {
			for value in gridValues {
				let height = value * gridLayer.frame.size.height
				
				let path = UIBezierPath()
				path.move(to: CGPoint(x: 0, y: height))
				path.addLine(to: CGPoint(x: gridLayer.frame.size.width, y: height))
				
				let lineLayer = CAShapeLayer()
				lineLayer.path = path.cgPath
				lineLayer.fillColor = UIColor.clear.cgColor
				lineLayer.strokeColor = UIColor.lightGray.cgColor
				lineLayer.lineWidth = 0.5
				if (value > 0.0 && value < 1.0) {
					lineLayer.lineDashPattern = [4, 4]
				}
				
				gridLayer.addSublayer(lineLayer)
				
				var minMaxGap: CGFloat = 0
				var lineValue: String = ""
                
                let nonZeroValuesEntries = dataEntries.filter { $0.value != nil }
				if let max = nonZeroValuesEntries.max()?.value,
					let min = nonZeroValuesEntries.min()?.value {
					minMaxGap = CGFloat(max - min) * topHorizontalLine
                    let floatValue: CGFloat = (1-value) * minMaxGap + CGFloat(min)
                    lineValue = String(Float(floatValue).round(to: 1))
				}
				
				let textLayer = CATextLayer()
				textLayer.frame = CGRect(x: 4, y: height, width: 50, height: 16)
                textLayer.foregroundColor = UIColor.label.cgColor
				textLayer.backgroundColor = UIColor.clear.cgColor
				textLayer.contentsScale = UIScreen.main.scale
				textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
				textLayer.fontSize = 12
				textLayer.string = lineValue
				valuesLeftView.layer.addSublayer(textLayer)
			}
		}
	}
	
	private func clean() {
		mainLayer.sublayers?.forEach({
			if $0 is CATextLayer {
				$0.removeFromSuperlayer()
			}
		})
		dataLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
		gridLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
		valuesLeftView.layer.sublayers?.forEach{$0.removeFromSuperlayer()}
	}
	/**
	 Create Dots on line points
	 */
	private func drawDots() {
		var dotLayers: [CALayer] = []
		if let dataPoints = dataPoints {
			for dataPoint in dataPoints {
                if let dataPoint = dataPoint {
                    let xValue = dataPoint.x - outerRadius / 2
                    let yValue = dataPoint.y + topSpace - outerRadius / 2
                    let dotLayer = CALayer()
                    dotLayer.backgroundColor = UIColor.clear.cgColor
                    dotLayer.borderWidth = 1
                    dotLayer.borderColor = lineColor.cgColor
                    dotLayer.cornerRadius = outerRadius / 2
                    dotLayer.frame = CGRect(x: xValue, y: yValue, width: outerRadius, height: outerRadius)
                    dotLayers.append(dotLayer)
                    
                    mainLayer.addSublayer(dotLayer)
                    
                    if animateDots {
                        let anim = CABasicAnimation(keyPath: "opacity")
                        anim.duration = 1.0
                        anim.fromValue = 0
                        anim.toValue = 1
                        dotLayer.add(anim, forKey: "opacity")
                    }
                }
            }
		}
	}
}

