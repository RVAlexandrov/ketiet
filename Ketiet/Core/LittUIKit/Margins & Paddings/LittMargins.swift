//
//  LittMargins.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension CGFloat {
	/// Увеличенное расстояние до краёв экрана (32.0)
	static var largeLittScreenEdgeMargin: CGFloat {
		32
	}
    
    /// Классическле расстояние от границ экрана и между элементами (16.0)
    static var littScreenEdgeMargin: CGFloat {
        16.0
    }
    
    /// Уменьшеное расстояние между вертикальными кусками текста (4.0)
    static var smallLittMargin: CGFloat {
        4.0
    }
	
    /// Среднее расстояние между элементами (8.0)
	static var mediumLittMargin: CGFloat {
		8
	}
	
    /// Большое расстояние между элементами (16.0)
	static var largeLittMargin: CGFloat {
		16
	}
    
    /// Расстояние между логическами кусками UI (20.0)
    static var logicBlockLittMargin: CGFloat {
        20.0
    }
    
    /// 12 points
    static var littMediumWellMargin: CGFloat {
        12.0
    }

    /// Системное расстоение в 20 points как в системных ячейках
    static var systemCellSpacing: CGFloat {
        20.0
    }
}
