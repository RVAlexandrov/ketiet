//
//  LittUIConstants.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 31.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import CoreGraphics

extension CGFloat {
	
    /// 20.0
	static var littMediumCornerRadius: CGFloat {
		20
	}
    
    /// 12.0
    static var littBasicCornerRadius: CGFloat {
        12.0
    }
}
