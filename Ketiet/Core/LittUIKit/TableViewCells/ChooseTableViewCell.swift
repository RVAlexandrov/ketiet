//
//  GoalChooseTableViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 10.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class ChooseTableViewCell: UITableViewCell {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private let cardView = ChooseOptionView()
    
    var viewModel: ChooseOptionView.ViewModel? {
        didSet {
            cardView.viewModel = viewModel
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cardView.frame = CGRect(x: 12,
                                y: 12,
                                width: bounds.width - 24,
                                height: bounds.height - 24)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(cardView)
        contentView.backgroundColor = .littBackgroundColor
        selectionStyle = .none
    }
}
