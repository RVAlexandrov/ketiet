//
//  DimmingBackgroundPresentationController.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 14/07/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import Foundation
import UIKit

final class DimmingBackgroundPresentationController : UIPresentationController {
	
	var dismissingByTappingOutsideAllowed = true
	var finalAlpha = CGFloat(0.4)
	private let dimmingViewTag = 42
	var finalFrameOfPresentedVC : CGRect?
	override var frameOfPresentedViewInContainerView: CGRect {
		if let f = finalFrameOfPresentedVC {
			return f
		} else {
			return super.frameOfPresentedViewInContainerView
		}
	}
	
	override func presentationTransitionWillBegin() {
		let dimmingView = UIView(frame: UIScreen.main.bounds)
		if dismissingByTappingOutsideAllowed {
			dimmingView.addGestureRecognizer(UITapGestureRecognizer(target: presentedViewController, action: #selector(UIViewController.dismissController)))
		}
		dimmingView.backgroundColor = .black
		dimmingView.tag = dimmingViewTag
		dimmingView.alpha = 0
		containerView?.addSubview(dimmingView)
		presentingViewController.transitionCoordinator?.animate(alongsideTransition: { (_) in
			dimmingView.alpha = self.finalAlpha
		}, completion: {(context) in
			if context.isCancelled {
				dimmingView.removeFromSuperview()
			}
		})
	}
	
	override func dismissalTransitionWillBegin() {
		guard let dimmingView = containerView?.viewWithTag(dimmingViewTag) else {return}
		presentingViewController.transitionCoordinator?.animate(alongsideTransition: { (_) in
			dimmingView.alpha = 0
		}, completion: {(context) in
			if !context.isCancelled {
				dimmingView.removeFromSuperview()
			}
		})
	}
}
