//
//  PresentationFromCenterAnimator.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 28/07/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class PresentationFromCenterAnimator: NSObject, UIViewControllerAnimatedTransitioning {
	
	private struct Constants {
		static let transitionFromScale: CGFloat = 0.8
	}
	
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return CATransaction.animationDuration()
	}
	
	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		guard let toVC = transitionContext.viewController(forKey: .to), let toView = toVC.view else {return}
		transitionContext.containerView.addSubview(toView)
		toView.frame = transitionContext.finalFrame(for: toVC)
		toView.alpha = 0
		toView.transform = CGAffineTransform(scaleX: Constants.transitionFromScale, y: Constants.transitionFromScale)
		UIView.animate(withDuration: transitionDuration(using: nil), animations: {
			toView.alpha = 1
			toView.transform = .identity
		}) { _ in
			transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
		}
	}
}
