//
//  ToCenterDismissingAnimator.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 28/07/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class ToCenterDismissingAnimator: NSObject, UIViewControllerAnimatedTransitioning {
	
	private struct Constants {
		static let transitionFinalScale: CGFloat = 0.8
	}
	
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return CATransaction.animationDuration()
	}
	
	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		guard let fromView = transitionContext.view(forKey: .from) else { return }
		UIView.animate(withDuration: transitionDuration(using: nil)) {
			fromView.alpha = 0
			fromView.transform = CGAffineTransform(scaleX: Constants.transitionFinalScale, y: Constants.transitionFinalScale)
		} completion: { (_) in
			if !transitionContext.transitionWasCancelled {
				fromView.removeFromSuperview()
			}
			transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
		}
	}
}
