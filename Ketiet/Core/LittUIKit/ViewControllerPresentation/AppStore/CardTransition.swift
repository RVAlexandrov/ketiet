//
//  ViewControllerPresentation.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class CardTransition: NSObject, UIViewControllerTransitioningDelegate {
    struct Params {
        let fromCardFrame: CGRect
        let fromCardFrameWithoutTransform: CGRect
        let fromView: UIView
    }

    let params: Params

    init(params: Params) {
        self.params = params
        super.init()
    }

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentCardAnimator(params: PresentCardAnimator.Params(fromCardFrame: params.fromCardFrame,
																	  fromView: params.fromView))
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissCardAnimator(params: DismissCardAnimator.Params(fromCardFrame: params.fromCardFrame,
																	  fromCardFrameWithoutTransform: params.fromCardFrameWithoutTransform,
																	  fromView: params.fromView))
    }

    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return CardPresentationController(presentedViewController: presented, presenting: presenting)
    }
}
