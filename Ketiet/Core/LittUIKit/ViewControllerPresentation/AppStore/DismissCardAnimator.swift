//
//  ViewControllerPresentation.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DismissCardAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    struct Params {
        let fromCardFrame: CGRect
        let fromCardFrameWithoutTransform: CGRect
        let fromView: UIView
    }

    struct Constants {
        static let relativeDurationBeforeNonInteractive: TimeInterval = 0.5
        static let minimumScaleBeforeNonInteractive: CGFloat = 0.8
    }

    private let params: Params

    init(params: Params) {
        self.params = params
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return 0.6
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let ctx = transitionContext
        let container = ctx.containerView

        let cardDetailView = ctx.view(forKey: .from)!

        let animatedContainerView = UIView()
        animatedContainerView.translatesAutoresizingMaskIntoConstraints = false
        cardDetailView.translatesAutoresizingMaskIntoConstraints = false

        container.removeConstraints(container.constraints)
        
        container.addSubview(animatedContainerView)
        animatedContainerView.addSubview(cardDetailView)

		cardDetailView.pinToSuperview()

        let animatedContainerViewCenterX = animatedContainerView.centerXAnchor.constraint(equalTo: container.centerXAnchor)
		animatedContainerViewCenterX.isActive = true
        let animatedContainerTopConstraint = animatedContainerView.topAnchor.constraint(equalTo: container.topAnchor, constant: 0)
        let animatedContainerWidthConstraint = animatedContainerView.widthAnchor.constraint(equalToConstant: cardDetailView.frame.width)
        let animatedContainerHeightConstraint = animatedContainerView.heightAnchor.constraint(equalToConstant: cardDetailView.frame.height)

        NSLayoutConstraint.activate([animatedContainerTopConstraint, animatedContainerWidthConstraint, animatedContainerHeightConstraint])


        container.layoutIfNeeded()

        func animateCardViewBackToPlace() {
            cardDetailView.transform = .identity
			if params.fromView.layer.cornerRadius != 0 {
				cardDetailView.layer.cornerRadius = params.fromView.layer.cornerRadius
			} else {
				cardDetailView.layer.cornerRadius = .littBasicCornerRadius
			}
			animatedContainerViewCenterX.isActive = false
			animatedContainerView.centerXAnchor.constraint(equalTo: params.fromView.centerXAnchor).isActive = true
            animatedContainerTopConstraint.constant = params.fromCardFrameWithoutTransform.minY
            animatedContainerWidthConstraint.constant = params.fromCardFrameWithoutTransform.width
            animatedContainerHeightConstraint.constant = params.fromCardFrameWithoutTransform.height
            container.layoutIfNeeded()
            
            let colorAnimation = CABasicAnimation(keyPath: NSStringFromSelector(#selector(getter: CALayer.backgroundColor)))
            colorAnimation.fromValue = cardDetailView.backgroundColor?.cgColor
            colorAnimation.toValue = UIColor.littSecondaryBackgroundColor.cgColor
            colorAnimation.duration = 0.4
            colorAnimation.isRemovedOnCompletion = false
            colorAnimation.fillMode = .forwards
            cardDetailView.layer.add(colorAnimation, forKey: nil)
        }

        func completeEverything() {
            let success = !ctx.transitionWasCancelled
            animatedContainerView.removeConstraints(animatedContainerView.constraints)
            animatedContainerView.removeFromSuperview()
            if success {
				params.fromView.alpha = 1
				cardDetailView.removeFromSuperview()
				UIView.animate(withDuration: 0.1, animations: {
					if let cell = self.params.fromView as? UICollectionViewCell {
						cell.contentView.subviews.forEach { $0.alpha = 1 }
					} else {
						self.params.fromView.subviews.forEach { $0.alpha = 1 }
					}
				})
            } else {

                container.removeConstraints(container.constraints)

                container.addSubview(cardDetailView)
				cardDetailView.pinToSuperview()
            }
            ctx.completeTransition(success)
        }

        UIView.animate(withDuration: transitionDuration(using: ctx), delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: [], animations: {
            animateCardViewBackToPlace()
        }) { _ in
			completeEverything()
        }
    }
}
