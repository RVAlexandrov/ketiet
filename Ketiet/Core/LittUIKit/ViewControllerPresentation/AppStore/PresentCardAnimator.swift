//
//  ViewControllerPresentation.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class PresentCardAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    private let params: Params
    
    struct Params {
        let fromCardFrame: CGRect
        let fromView: UIView
    }
    
    private let presentAnimationDuration: TimeInterval
    private let springAnimator: UIViewPropertyAnimator
    private var transitionDriver: PresentCardTransitionDriver?
    
    init(params: Params) {
        self.params = params
        self.springAnimator = PresentCardAnimator.createBaseSpringAnimator(params: params)
        self.presentAnimationDuration = springAnimator.duration
        super.init()
    }
    
    private static func createBaseSpringAnimator(params: PresentCardAnimator.Params) -> UIViewPropertyAnimator {
        let cardPositionY = params.fromCardFrame.minY
        let distanceToBounce = abs(params.fromCardFrame.minY)
        let extentToBounce = cardPositionY < 0 ? params.fromCardFrame.height : UIScreen.main.bounds.height
        let dampFactorInterval: CGFloat = 0.3
        let damping: CGFloat = 1.0 - dampFactorInterval * (distanceToBounce / extentToBounce)
        
        let baselineDuration: TimeInterval = 0.5
        let maxDuration: TimeInterval = 0.5
        let duration: TimeInterval = baselineDuration + (maxDuration - baselineDuration) * TimeInterval(max(0, distanceToBounce)/UIScreen.main.bounds.height)
        
        let springTiming = UISpringTimingParameters(dampingRatio: damping, initialVelocity: .init(dx: 0, dy: 0))
        return UIViewPropertyAnimator(duration: duration, timingParameters: springTiming)
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return presentAnimationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        transitionDriver = PresentCardTransitionDriver(params: params,
                                                       transitionContext: transitionContext,
                                                       baseAnimator: springAnimator)
        interruptibleAnimator(using: transitionContext).startAnimation()
    }
    
    func animationEnded(_ transitionCompleted: Bool) {
        transitionDriver = nil
    }
    
    func interruptibleAnimator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        return transitionDriver!.animator
    }
}

final class PresentCardTransitionDriver {
    
    let animator: UIViewPropertyAnimator
    
    init(params: PresentCardAnimator.Params, transitionContext: UIViewControllerContextTransitioning, baseAnimator: UIViewPropertyAnimator) {
        let ctx = transitionContext
        let container = ctx.containerView
        
        let cardDetailView = ctx.view(forKey: .to)!
        let fromCardFrame = params.fromCardFrame
        
        let animatedContainerView = UIView()
        animatedContainerView.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(animatedContainerView)
        
        NSLayoutConstraint.activate([animatedContainerView.widthAnchor.constraint(equalToConstant: container.bounds.width),
                                     animatedContainerView.centerXAnchor.constraint(equalTo: container.centerXAnchor)])
        
        let animatedContainerVerticalConstraint = animatedContainerView.topAnchor.constraint(equalTo: container.topAnchor,
                                                                                             constant: fromCardFrame.minY)
        animatedContainerVerticalConstraint.isActive = true
        let animatedContainerVerticalBottomConstraint = container.bottomAnchor.constraint(equalTo: animatedContainerView.bottomAnchor,
                                                                                          constant: UIScreen.main.bounds.height - fromCardFrame.maxY)
        animatedContainerVerticalBottomConstraint.isActive = true
        
        animatedContainerView.addSubview(cardDetailView)
        cardDetailView.translatesAutoresizingMaskIntoConstraints = false
        
        let cardCenterConstraint = cardDetailView.centerXAnchor.constraint(equalTo: params.fromView.centerXAnchor)
        let cardConstraints = [cardDetailView.topAnchor.constraint(equalTo: animatedContainerView.topAnchor, constant: -1),
                               cardDetailView.bottomAnchor.constraint(equalTo: animatedContainerView.bottomAnchor),
                               cardCenterConstraint,
        ]
        NSLayoutConstraint.activate(cardConstraints)
        let cardWidthConstraint = cardDetailView.widthAnchor.constraint(equalToConstant: fromCardFrame.width)
        let cardHeightConstraint = cardDetailView.heightAnchor.constraint(equalToConstant: fromCardFrame.height)
        NSLayoutConstraint.activate([cardWidthConstraint, cardHeightConstraint])
        
        if params.fromView.layer.cornerRadius != 0 {
            cardDetailView.layer.cornerRadius = params.fromView.layer.cornerRadius
        } else {
            cardDetailView.layer.cornerRadius = .littBasicCornerRadius
        }
        cardDetailView.layer.addShadow(cardDetailView.bounds)
        
        params.fromView.transform = .identity
        
        container.layoutIfNeeded()
        
        func animateContainerBouncingUp() {
            if let cell = params.fromView as? UICollectionViewCell {
                cell.contentView.subviews.forEach { $0.alpha = 0 }
            } else {
                params.fromView.subviews.forEach { $0.alpha = 0 }
            }
            params.fromView.alpha = 0
            animatedContainerVerticalConstraint.constant = 0
            animatedContainerVerticalBottomConstraint.constant = 0
            container.layoutIfNeeded()
        }
        
        func animateCardDetailViewSizing() {
            cardCenterConstraint.isActive = false
            cardWidthConstraint.constant = animatedContainerView.bounds.width
            cardDetailView.centerXAnchor.constraint(equalTo: animatedContainerView.centerXAnchor).isActive = true
            cardDetailView.layer.cornerRadius = 0
            
            let animation = CABasicAnimation(keyPath: NSStringFromSelector(#selector(getter: CALayer.shadowOpacity)))
            animation.toValue = 0
            animation.duration = 0.4
            animation.isRemovedOnCompletion = false
            animation.fillMode = .forwards
            cardDetailView.layer.add(animation, forKey: nil)
            
            let colorAnimation = CABasicAnimation(keyPath: NSStringFromSelector(#selector(getter: CALayer.backgroundColor)))
            colorAnimation.fromValue = cardDetailView.backgroundColor?.cgColor
            colorAnimation.toValue = UIColor.littBackgroundColor.cgColor
            colorAnimation.duration = 0.4
            colorAnimation.isRemovedOnCompletion = false
            colorAnimation.fillMode = .forwards
            cardDetailView.layer.add(colorAnimation, forKey: nil)
            
            container.layoutIfNeeded()
        }
        
        func completeEverything() {
            animatedContainerView.removeConstraints(animatedContainerView.constraints)
            animatedContainerView.removeFromSuperview()
            
            container.addSubview(cardDetailView)
            
            cardDetailView.removeConstraints([cardWidthConstraint, cardHeightConstraint])
            cardDetailView.layer.removeAllAnimations()
            
            cardDetailView.pinToSuperview(insets: UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0))
            
            let success = !ctx.transitionWasCancelled
            ctx.completeTransition(success)
        }
        
        baseAnimator.addAnimations {
            
            animateContainerBouncingUp()
            
            let cardExpanding = UIViewPropertyAnimator(duration: baseAnimator.duration * 0.6, curve: .linear) {
                animateCardDetailViewSizing()
            }
            cardExpanding.startAnimation()
        }
        
        baseAnimator.addCompletion { (_) in
            completeEverything()
        }
        
        self.animator = baseAnimator
    }
}
