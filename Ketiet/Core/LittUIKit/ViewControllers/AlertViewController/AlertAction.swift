//
//  AlertAction.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 14.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class AlertAction {
	
	enum Style {
		case approve
		case normal
		case delete
	}
	
	let title: String
	let color: UIColor
    let style: AlertAction.Style
	let handler: (() -> Void)?
	
	init(title: String,
		 style: AlertAction.Style = .approve,
		 handler: (() -> Void)? = nil) {
		self.title = title
        self.style = style
		self.color = style.color()
		self.handler = handler
	}
}

extension AlertAction.Style {
	func color() -> UIColor {
		switch self {
		case .delete:
			return .littFailureColor
		case .approve:
			return .accentColor
		case .normal:
			return .systemGray
		}
	}
    
    func uiAlertStyle() -> UIAlertAction.Style {
        switch self {
        case .delete:
            return .destructive
        case .approve:
            return .cancel
        case .normal:
            return .default
        }
    }
}
