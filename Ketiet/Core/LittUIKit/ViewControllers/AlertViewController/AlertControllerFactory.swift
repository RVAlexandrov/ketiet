//
//  AlertControllerFactory.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

struct AlertControllerFactory: AlertControllerFactoryProtocol {
    func makeAlert(
        title: String?,
        description: String? = nil,
        icon: UIImage? = nil,
        actions: [AlertAction]
    ) -> UIViewController {
        AlertViewController(
            title: title,
            description: description,
            icon: icon,
            actions: actions
        )
    }
    
    func makeActionSheet(title: String?,
                         description: String? = nil,
                         icon: UIImage? = nil,
                         actions: [AlertAction]) -> UIViewController {
        let controller = UIAlertController(title: title, message:
                                            description,
                                           preferredStyle: .actionSheet)
        actions.forEach { action in
            controller.addAction(UIAlertAction(title: action.title,
                                               style: action.style.uiAlertStyle(),
                                               handler: { _ in
                action.handler?()
            }))
        }
        return controller
    }
}
