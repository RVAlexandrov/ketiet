//
//  File.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

protocol AlertControllerFactoryProtocol {
    func makeAlert(title: String?,
                   description: String?,
                   icon: UIImage?,
                   actions: [AlertAction]) -> UIViewController
    
    func makeActionSheet(title: String?,
                         description: String?,
                         icon: UIImage?,
                         actions: [AlertAction]) -> UIViewController
}

extension AlertControllerFactoryProtocol {
    func makeAlert(title: String?,
                   description: String? = nil,
                   icon: UIImage? = nil,
                   actions: [AlertAction]) -> UIViewController {
        makeAlert(title: title,
                  description: description,
                  icon: icon,
                  actions: actions)
    }
    
    func makeActionSheet(title: String?,
                         description: String? = nil,
                         icon: UIImage? = nil,
                         actions: [AlertAction]) -> UIViewController {
        makeActionSheet(title: title,
                        description: description,
                        icon: icon,
                        actions: actions)
    }
}
