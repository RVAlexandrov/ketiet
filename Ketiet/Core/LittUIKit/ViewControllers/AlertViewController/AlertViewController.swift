//
//  AlertViewController.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 27/07/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class AlertViewController: UIViewController {
	
	private struct Constants {
		static let imageViewSide: CGFloat = 30
		static let cornerRadius: CGFloat = 15
		static let borderWidth: CGFloat = 0.5
		static let visualEffectViewWidthMultiplier: CGFloat = 0.8
		static let minAlertHeight: CGFloat = 200
	}
	
	private let actions: [AlertAction]?
	private let titleString: String?
	private let descriptionString: String?
	private let icon: UIImage?
	
	init(
        title: String?,
        description: String? = nil,
        icon: UIImage? = nil,
        actions: [AlertAction]
    ) {
		self.actions = actions
		titleString = title
		descriptionString = description
		self.icon = icon
		super.init(nibName: nil, bundle: nil)
		modalPresentationStyle = .custom
		transitioningDelegate = self
	}
    	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		view = UIView()
        view?.backgroundColor = .clear
        let recognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissController)
        )
        view?.addGestureRecognizer(recognizer)
		
		let visuallEffectViewResult = addVisualEffectView()
		let visualEffectView = visuallEffectViewResult.0
		var constraints = visuallEffectViewResult.1
        
        var topAnchor = visualEffectView.topAnchor
        
        if let imageConstraints = addImageView(
            topAnchor: &topAnchor,
            rootView: visualEffectView.contentView
        ) {
            constraints.append(contentsOf: imageConstraints)
        }
        constraints.append(contentsOf: addLabels(
            topAnchor: &topAnchor,
            rootView: visualEffectView.contentView
        ))
        constraints.append(contentsOf: addButtons(
            topAnchor: topAnchor,
            rootView: visualEffectView.contentView
        ))
		NSLayoutConstraint.activate(constraints)
	}
	
	private func addVisualEffectView() -> (UIVisualEffectView, [NSLayoutConstraint]) {
		let visuallEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .systemThinMaterial))
		visuallEffectView.translatesAutoresizingMaskIntoConstraints = false
		visuallEffectView.layer.cornerRadius = Constants.cornerRadius
		visuallEffectView.layer.borderWidth = Constants.borderWidth
		visuallEffectView.layer.borderColor = UIColor.systemGray.cgColor
		visuallEffectView.clipsToBounds = true
		view?.addSubview(visuallEffectView)
		return (visuallEffectView, [
            visuallEffectView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            visuallEffectView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            visuallEffectView.widthAnchor.constraint(
                equalTo: view.widthAnchor,
                multiplier: Constants.visualEffectViewWidthMultiplier
            ),
            visuallEffectView.heightAnchor.constraint(greaterThanOrEqualToConstant: Constants.minAlertHeight)
        ])
	}
	
	private func addImageView(topAnchor: inout NSLayoutYAxisAnchor, rootView: UIView) -> [NSLayoutConstraint]? {
		guard let icon = icon else { return nil }
		let imageView = UIImageView(image: icon)
		imageView.translatesAutoresizingMaskIntoConstraints = false
		rootView.addSubview(imageView)
		let constraints = [
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.topAnchor.constraint(
                equalTo: topAnchor,
                constant: .logicBlockLittMargin
            ),
            imageView.heightAnchor.constraint(equalToConstant: Constants.imageViewSide),
            imageView.widthAnchor.constraint(equalToConstant: Constants.imageViewSide)
        ]
		topAnchor = imageView.bottomAnchor
		return constraints
	}
	
	private func addLabels(topAnchor: inout NSLayoutYAxisAnchor,
						   rootView: UIView) -> [NSLayoutConstraint] {
		let labelsStack = UIStackView()
		labelsStack.translatesAutoresizingMaskIntoConstraints = false
		labelsStack.spacing = .logicBlockLittMargin
		labelsStack.axis = .vertical
		
		if let title = titleString {
			let label = UILabel()
			label.translatesAutoresizingMaskIntoConstraints = false
			label.font = .subTitle
			label.text = title
			label.textAlignment = .center
			label.lineBreakMode = .byWordWrapping
			label.numberOfLines = 3
			label.setContentHuggingPriority(.required, for: .vertical)
			labelsStack.addArrangedSubview(label)
		}
		
		if let description = descriptionString {
			let label = UILabel()
			label.translatesAutoresizingMaskIntoConstraints = false
			label.font = .smallBody
			label.text = description
			label.textAlignment = .center
			label.lineBreakMode = .byWordWrapping
			label.numberOfLines = 4
			label.setContentHuggingPriority(.defaultLow, for: .vertical)
			labelsStack.addArrangedSubview(label)
		}
		rootView.addSubview(labelsStack)
		let constraints = [
            labelsStack.topAnchor.constraint(
                equalTo: topAnchor,
                constant: .logicBlockLittMargin
            ),
            labelsStack.leadingAnchor.constraint(
                equalTo: rootView.leadingAnchor,
                constant: .logicBlockLittMargin
            ),
            labelsStack.trailingAnchor.constraint(
                equalTo: rootView.trailingAnchor,
                constant: -.logicBlockLittMargin
            )
        ]
		topAnchor = labelsStack.bottomAnchor
		return constraints
	}
	
	private func addButtons(topAnchor: NSLayoutYAxisAnchor,
							rootView: UIView) -> [NSLayoutConstraint] {
        guard let actions = actions else { return [] }
		let buttons = makeButtons(actions: actions)
		let buttonsStack = UIStackView(arrangedSubviews: buttons)
		buttonsStack.spacing = .mediumLittMargin
		buttonsStack.axis = .vertical
		buttonsStack.translatesAutoresizingMaskIntoConstraints = false
		rootView.addSubview(buttonsStack)
		return [
            buttonsStack.topAnchor.constraint(
                equalTo: topAnchor,
                constant: .largeLittMargin
            ),
            buttonsStack.leadingAnchor.constraint(
                equalTo: rootView.leadingAnchor,
                constant: .logicBlockLittMargin
            ),
            buttonsStack.trailingAnchor.constraint(
                equalTo: rootView.trailingAnchor,
                constant: -.logicBlockLittMargin
            ),
            buttonsStack.bottomAnchor.constraint(
                equalTo: rootView.bottomAnchor,
                constant: -.largeLittMargin
            )
        ]
	}
    
    private func makeButtons(actions: [AlertAction]) -> [LittBasicButton] {
        actions.map { action -> LittBasicButton in
            let button = LittBasicButton(
                title: action.title,
                config: .filledLarge(action.color)
            )
            button.setTapHandler { [weak self] in
                self?.dismiss(animated: true) {
                    action.handler?()
                }
            }
            return button
        }
    }
}

extension AlertViewController: UIViewControllerTransitioningDelegate {
	func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
		DimmingBackgroundPresentationController(presentedViewController: presented, presenting: presenting)
	}
	
	func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        PresentationFromCenterAnimator()
	}

	func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		ToCenterDismissingAnimator()
	}
}
