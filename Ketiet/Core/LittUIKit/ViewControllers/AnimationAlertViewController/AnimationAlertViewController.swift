//
//  AnimationAlertViewController.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit
import Lottie

final class AnimationAlertViewController: UIViewController {
    
    private struct Constants {
        static let cornerRadius: CGFloat = 15
        static let borderWidth: CGFloat = 0.5
        static let visualEffectViewWidthMultiplier: CGFloat = 0.8
        static let minAlertHeight: CGFloat = 200
    }
    private let animationView: AnimationView
    private let animationTitleString: String?
    private let didDismissAction: (() -> Void)?
    private var isNeedToHide = false
    
    init(
        animationPath: String,
        title: String,
        didDismissAction: (() -> Void)?
    ) {
        self.animationTitleString = title
        self.animationView = AnimationView(name: animationPath)
        self.didDismissAction = didDismissAction
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func dismissController() {
        dismiss(animated: true) { [weak self] in
            self?.didDismissAction?()
        }
    }
    
    override func loadView() {
        view = UIView()
        view?.backgroundColor = .clear
        
        let visuallEffectViewResult = addVisualEffectView()
        let visualEffectView = visuallEffectViewResult.0
        var constraints = visuallEffectViewResult.1
        
        var topAnchor = visualEffectView.topAnchor

        constraints.append(contentsOf: animationTitleConstraints(
            topAnchor: &topAnchor,
            rootView: visualEffectView.contentView
        ))
        constraints.append(contentsOf: animationViewConstraints(
            topAnchor: topAnchor,
            rootView: visualEffectView.contentView
        ))
        NSLayoutConstraint.activate(constraints)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playAnimation()
    }
    
    func dismissAfterAnimationEnds() {
        isNeedToHide = true
    }
    
    private func playAnimation() {
        animationView.loopMode = .repeat(0.25)
        animationView.play { [weak self] _ in
            guard let self = self else { return }
            self.isNeedToHide ? self.dismissController() : self.playAnimation()
        }
    }
    
    private func addVisualEffectView() -> (UIVisualEffectView, [NSLayoutConstraint]) {
        let visuallEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .systemThinMaterial))
        visuallEffectView.translatesAutoresizingMaskIntoConstraints = false
        visuallEffectView.layer.cornerRadius = Constants.cornerRadius
        visuallEffectView.layer.borderWidth = Constants.borderWidth
        visuallEffectView.layer.borderColor = UIColor.systemGray.cgColor
        visuallEffectView.clipsToBounds = true
        view?.addSubview(visuallEffectView)
        return (visuallEffectView, [visuallEffectView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                    visuallEffectView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                                    visuallEffectView.widthAnchor.constraint(equalTo: view.widthAnchor,
                                                                             multiplier: Constants.visualEffectViewWidthMultiplier),
                                    visuallEffectView.heightAnchor.constraint(greaterThanOrEqualToConstant: Constants.minAlertHeight)])
    }
    
    private func animationTitleConstraints(topAnchor: inout NSLayoutYAxisAnchor,
                                           rootView: UIView) -> [NSLayoutConstraint] {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .title
        label.text = animationTitleString
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 4
        rootView.addSubview(label)
        
        let constraints = [
            label.topAnchor.constraint(equalTo: rootView.topAnchor, constant: .largeLittMargin),
            label.leadingAnchor.constraint(equalTo: rootView.leadingAnchor, constant: .largeLittMargin),
            label.trailingAnchor.constraint(equalTo: rootView.trailingAnchor, constant: -.largeLittMargin)
        ]
        
        topAnchor = label.bottomAnchor
        
        return constraints
    }
    
    private func animationViewConstraints(topAnchor: NSLayoutYAxisAnchor,
                                          rootView: UIView) -> [NSLayoutConstraint] {
        rootView.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        return [
            animationView.topAnchor.constraint(equalTo: topAnchor, constant: .mediumLittMargin),
            animationView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor, constant: .largeLittMargin),
            animationView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor, constant: -.mediumLittMargin),
            animationView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor, constant: -.largeLittMargin),
            animationView.heightAnchor.constraint(equalTo: animationView.widthAnchor)
        ]
    }
}

extension AnimationAlertViewController: UIViewControllerTransitioningDelegate {
    func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
        DimmingBackgroundPresentationController(
            presentedViewController: presented,
            presenting: presenting
        )
    }
    
    func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        PresentationFromCenterAnimator()
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        ToCenterDismissingAnimator()
    }
}
