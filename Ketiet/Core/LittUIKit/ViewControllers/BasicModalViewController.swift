//
//  BasicModalViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

class BasicModalViewController: UIViewController {
    
    private lazy var closeButton: UIButton = {
        let button = UIButton.makeCloseButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(closeButton)
        let topInset: CGFloat = .largeLittMargin
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topInset),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.littScreenEdgeMargin)
        ] + closeButton.closeButtonConstrains())
    }
}
