//
//  BasicModalWithTableViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

class BasicModalWithTableViewController: UIViewController {
    
    private(set) lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .littOnSurfaceSecondary
        tableView.separatorStyle = .none
        tableView.contentInset.top += .largeLittScreenEdgeMargin
        tableView.showsVerticalScrollIndicator = false
        return tableView
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "xmark.circle.fill"), for: .normal)
        button.tintColor = .systemGray
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
        return button
    }()
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .littOnSurfaceSecondary
        view.addSubview(tableView)
        tableView.pinToSuperview(insets: .zero)
        view.addSubview(closeButton)
        let topInset: CGFloat = .largeLittMargin
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topInset),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.littScreenEdgeMargin),
            closeButton.widthAnchor.constraint(equalToConstant: .largeLittScreenEdgeMargin),
            closeButton.heightAnchor.constraint(equalToConstant: .largeLittScreenEdgeMargin)
        ])
    }
}
