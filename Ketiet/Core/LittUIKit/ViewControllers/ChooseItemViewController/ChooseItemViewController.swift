//
//  ChooseItemViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ChooseItemViewController: UIViewController {
	private let chooseTableViewCellId = "chooseTableViewCellId"
	private var continueButton: LittBasicButton?
    private let items: [SelectableCellItem]
	private let buttonTitle: String
	private let completion: (Int, ChooseItemViewController) -> Void
	
	private lazy var tableView: UITableView = {
		let tableView = UITableView()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.delegate = self
		tableView.dataSource = self
		tableView.separatorStyle = .none
		tableView.backgroundColor = .littBackgroundColor
		return tableView
	}()
	
	@available(*, unavailable)
	required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
	init(items: [SelectableCellItem],
		 buttonTitle: String,
		 completion: @escaping(Int, ChooseItemViewController) -> Void) {
		self.buttonTitle = buttonTitle
        self.items = items
		self.completion = completion
		super.init(nibName: nil, bundle: nil)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
        view.backgroundColor = .littBackgroundColor
		configureView()
	}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        littTabBarController?.setTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        littTabBarController?.setTabBarHidden(false)
    }
}
// MARK: - Private methods
extension ChooseItemViewController {
	
	private func configureView() {
        items.forEach {
            tableView.register($0.viewClass(),
                               forCellReuseIdentifier: $0.registerIdentifier())
        }
		view.backgroundColor = .littBackgroundColor
		view.addSubview(tableView)
		tableView.pinToSuperview()
		let buttonAndOffset = addBottomButtonWithBlur(with: buttonTitle)
		continueButton = buttonAndOffset.0
		tableView.contentInset.bottom = buttonAndOffset.1
		tableView.verticalScrollIndicatorInsets.bottom = buttonAndOffset.1
        continueButton?.isEnabled = items.filter { $0.selectionType == .selected }.count != 0
		continueButton?.setTapHandler(handler: { [weak self] in
			guard let self = self,
				  let index = self.items.firstIndex(where: { $0.selectionType == .selected }) else { return }
			self.completion(index, self)
		})
	}
    
    func getHeightForItem(atIndexPath indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.row]
        return item.calculateHeight(withWidth: UIScreen.main.bounds.width - 24)
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ChooseItemViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.registerIdentifier(),
                                                 for: indexPath)
        return item.configureReusableCell(cell)
	}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        getHeightForItem(atIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        getHeightForItem(atIndexPath: indexPath)
    }
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if let oldIndex = items.firstIndex(where: { $0.selectionType == .selected }) {
            items[oldIndex].selectionType = .notSelected
		}
        
        guard items[indexPath.row].selectionType != .selected else { return }
        items[indexPath.row].selectionType = .selected
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred(intensity: 1.0)
		tableView.performBatchUpdates(nil)
		continueButton?.isEnabled = true
	}
}
