//
//  DefaultChooseItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DefaultChooseItem: SelectableCellItem {
    let title: String
    let description: String?
    let isSelected: Bool
    
    init(title: String,
         description: String?,
         isSelected: Bool) {
        self.title = title
        self.description = description
        self.isSelected = isSelected
        selectionType = isSelected ? .selected : .notSelected
    }
    
    weak var cell: UITableViewCell?
    
    var selectionType: SelectionType = .notSelected {
        didSet {
            if let cell = cell as? ChooseTableViewCell {
                let newViewModel = ChooseOptionView.ViewModel(
                    isSelected: selectionType == .selected,
                    title: cell.viewModel?.title ?? "",
                    description: cell.viewModel?.description
                )
                cell.viewModel = newViewModel
            }
        }
    }
    
    func viewClass() -> AnyClass {
        ChooseTableViewCell.self
    }
    
    func registerIdentifier() -> String {
        ChooseTableViewCell.description()
    }
    
    func configureReusableCell(_ cell: UITableViewCell) -> UITableViewCell {
        if let chooseCell = cell as? ChooseTableViewCell {
            let viewModel = ChooseOptionView.ViewModel(
                isSelected: selectionType == .selected,
                title: title,
                description: description
            )
            chooseCell.viewModel = viewModel
            self.cell = chooseCell
        }
        return cell
    }
    
    func calculateHeight(withWidth width: CGFloat) -> CGFloat {
        let viewModel = ChooseOptionView.ViewModel(isSelected: selectionType == .selected,
                                                   title: title,
                                                   description: description)
        let layout = ChooseOptionView.Layout(viewModel: viewModel,
                                             width: width)
        let rects = layout.itemRects
        let height = layout.height(itemRects: rects) + 24
        return height
    }
}
