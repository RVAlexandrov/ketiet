//
//  ExpandedChooseItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

final class ExtendedChooseItem: SelectableCellItem {
    let title: String
    let description: String
    let buttonTitle: String
    let buttonCompletion: (() -> Void)?
    let isSelected: Bool
    
    init(title: String,
         description: String,
         buttonCompletion: (() -> Void)?,
         buttonTitle: String,
         isSelected: Bool) {
        self.title = title
        self.description = description
        self.buttonTitle = buttonTitle
        self.buttonCompletion = buttonCompletion
        self.isSelected = isSelected
        selectionType = isSelected ? .selected : .notSelected
    }
    
    weak var cell: UITableViewCell?
    
    var selectionType: SelectionType = .notSelected {
        didSet {
            if let cell = cell as? ExtendedChooseTableViewCell {
                let newViewModel = ExtendedChooseOptionView.ViewModel(
                    isSelected: selectionType == .selected,
                    title: cell.viewModel?.title ?? "",
                    description: cell.viewModel?.description ?? "",
                    buttonTitle: cell.viewModel?.buttonTitle ?? "",
                    buttonCompletion: cell.viewModel?.buttonCompletion
                )
                cell.viewModel = newViewModel
            }
        }
    }
    
    func viewClass() -> AnyClass {
        ExtendedChooseTableViewCell.self
    }
    
    func registerIdentifier() -> String {
        ExtendedChooseTableViewCell.description()
    }
    
    func configureReusableCell(_ cell: UITableViewCell) -> UITableViewCell {
        if let chooseCell = cell as? ExtendedChooseTableViewCell {
            let viewModel = ExtendedChooseOptionView.ViewModel(
                isSelected: selectionType == .selected,
                title: title,
                description: description,
                buttonTitle: buttonTitle,
                buttonCompletion: buttonCompletion
            )
            chooseCell.viewModel = viewModel
            self.cell = chooseCell
        }
        return cell
    }
    
    func calculateHeight(withWidth width: CGFloat) -> CGFloat {
        let viewModel = ExtendedChooseOptionView.ViewModel(
            isSelected: selectionType == .selected,
            title: title,
            description: description,
            buttonTitle: buttonTitle,
            buttonCompletion: buttonCompletion
        )
        let layout = ExtendedChooseOptionView.Layout(viewModel: viewModel,
                                                     width: width)
        let rects = layout.itemRects
        let height = layout.height(itemRects: rects) + 24
        return height
    }
}
