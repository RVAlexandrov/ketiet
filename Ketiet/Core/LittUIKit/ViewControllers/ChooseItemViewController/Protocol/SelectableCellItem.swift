//
//  SelectableItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

protocol SelectableCellItem: AnyObject {
    var cell: UITableViewCell? { get set }
    var selectionType: SelectionType { get set }
    
    func calculateHeight(withWidth width: CGFloat) -> CGFloat
    func viewClass() -> AnyClass
    func registerIdentifier() -> String
    func configureReusableCell(_ cell: UITableViewCell) -> UITableViewCell
}
