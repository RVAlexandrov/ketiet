//
//  DismissableDetailsViewController.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 27.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class DismissableDetailsViewController: UIViewController {
	
	private struct Constants {
		static let topInset = CGFloat(40)
	}
	
	private(set) lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: .zero,
                                      style: .insetGrouped)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		if showCloseButton {
			tableView.forwardingDelegate = self
        }
        tableView.backgroundColor = .clear
		tableView.separatorStyle = .none
        tableView.animations = []
		tableView.showsVerticalScrollIndicator = false
		return tableView
	}()
	
	private lazy var closeButton: UIButton = {
        let button = UIButton.makeCloseButton()
		button.translatesAutoresizingMaskIntoConstraints = false
        button.alpha = 0
		button.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
		return button
	}()
	
	private var draggingDownToDismiss = false
	private var interactiveStartingPoint: CGPoint?
	private var dismissalAnimator: UIViewPropertyAnimator?
	private let showCloseButton: Bool
	
	init(showCloseButton: Bool = true) {
		self.showCloseButton = showCloseButton
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		UIView.animate(withDuration: CATransaction.animationDuration() / 2) {
			self.tableView.alpha = 1
			self.closeButton.alpha = 1
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		if !isBeingDismissed {
			return
		}
		UIView.animate(withDuration: CATransaction.animationDuration()) {
			self.tableView.alpha = 0
			self.closeButton.alpha = 0
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		addDismissalPanGesture()
        view.addSubview(tableView)
        view.clipsToBounds = true
        view.backgroundColor = .littOnSurfaceSecondary
        
        if !showCloseButton {
            tableView.pinToSuperview(insets: UIEdgeInsets(top: UIApplication.shared.getKeyWindow()?.safeAreaInsets.top ?? 0,
                                                          left: 0,
                                                          bottom: 0,
                                                          right: 0),
                                     useSafeArea: false)
            tableView.setContentInsetForControllersInTabbar()
        } else {
            tableView.contentInset.top += Constants.topInset
            view.addSubview(closeButton)
            NSLayoutConstraint.activate([
                closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .largeLittMargin),
                closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.littScreenEdgeMargin)
            ]
                                        + closeButton.closeButtonConstrains()
                                        + tableView.pinToSuperviewConstraints(
                                            insets: UIEdgeInsets(top: UIApplication.shared.getKeyWindow()?.safeAreaInsets.top ?? 0,
                                                                 left: 0,
                                                                 bottom: 0,
                                                                 right: 0))
            )
        }
        
	}
	
	private func addDismissalPanGesture() {
		let dismissalPanGesture = UIPanGestureRecognizer()
		dismissalPanGesture.maximumNumberOfTouches = 1
		dismissalPanGesture.addTarget(self, action: #selector(handleDismissalPan(gesture:)))
		dismissalPanGesture.delegate = self
		view.addGestureRecognizer(dismissalPanGesture)
	}
	
	private func didCancelDismissalTransition() {
		interactiveStartingPoint = nil
		dismissalAnimator = nil
		draggingDownToDismiss = false
	}
	
	@objc private func handleDismissalPan(gesture: UIPanGestureRecognizer) {
		if !draggingDownToDismiss {
			return
		}
		
		let targetAnimatedView = gesture.view
		let startingPoint: CGPoint
		
		if let p = interactiveStartingPoint {
			startingPoint = p
		} else {
			startingPoint = gesture.location(in: nil)
			interactiveStartingPoint = startingPoint
		}
		
		let currentLocation = gesture.location(in: nil)
		let progress = (currentLocation.y - startingPoint.y) / 100
		let targetShrinkScale: CGFloat = 0.86
		let targetCornerRadius: CGFloat = 16
		
		func createInteractiveDismissalAnimatorIfNeeded() -> UIViewPropertyAnimator {
			if let animator = dismissalAnimator {
				return animator
			} else {
				let animator = UIViewPropertyAnimator(duration: 0, curve: .linear, animations: {
					targetAnimatedView?.transform = .init(scaleX: targetShrinkScale, y: targetShrinkScale)
					targetAnimatedView?.layer.cornerRadius = targetCornerRadius
				})
				animator.isReversed = false
				animator.pauseAnimation()
				animator.fractionComplete = progress
				return animator
			}
		}
		
		switch gesture.state {
		case .began:
			dismissalAnimator = createInteractiveDismissalAnimatorIfNeeded()
			
		case .changed:
			dismissalAnimator = createInteractiveDismissalAnimatorIfNeeded()
			
			let actualProgress = progress
			let isDismissalSuccess = actualProgress >= 1.0
			
			dismissalAnimator?.fractionComplete = actualProgress
			
			if isDismissalSuccess {
				dismissalAnimator?.stopAnimation(false)
				dismissalAnimator?.addCompletion { [weak self] (pos) in
					switch pos {
					case .end:
						self?.dismiss(animated: true)
					default:
						fatalError("Must finish dismissal at end!")
					}
				}
				dismissalAnimator?.finishAnimation(at: .end)
			}
			
		case .ended, .cancelled:
			if dismissalAnimator == nil {
				didCancelDismissalTransition()
				return
			}
			dismissalAnimator?.pauseAnimation()
			dismissalAnimator?.isReversed = true
			gesture.isEnabled = false
			dismissalAnimator?.addCompletion { [weak self] _ in
				self?.didCancelDismissalTransition()
				gesture.isEnabled = true
			}
			dismissalAnimator?.startAnimation()
		default:
			assertionFailure("Impossible gesture state? \(gesture.state.rawValue)")
		}
	}
}

// MARK: - UIGestureRecognizerDelegate
extension DismissableDetailsViewController: UIGestureRecognizerDelegate {
	func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
						   shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
}

extension DismissableDetailsViewController: UITableViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if draggingDownToDismiss || (scrollView.isTracking && scrollView.contentOffset.y < -Constants.topInset) {
			draggingDownToDismiss = true
			scrollView.contentOffset = CGPoint(x: 0, y: -Constants.topInset)
		}
	}
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		if velocity.y > 0 && scrollView.contentOffset.y <= -Constants.topInset {
			scrollView.contentOffset = CGPoint(x: 0, y: -Constants.topInset)
		}
	}
}

