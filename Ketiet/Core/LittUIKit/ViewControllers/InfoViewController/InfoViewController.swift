//
//  InfoViewController.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 02.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import ViewAnimator

final class InfoViewController: UIViewController {
	
	private struct Constants {
		static let headerItemSpacing: CGFloat = 60
        static let bottomHeaderItemSpacing: CGFloat = .largeLittMargin
	}
	
	private let headerTitle: String
	private let infoItems: [InfoViewControllerItem]
	private let buttonTitle: String
    private lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundView = confettiView
        return tableView
    }()
    private let confettiView: ConfettiView = {
        let avocado = UIImage(named: "avocadoSmall") ?? UIImage()
        let backgroundView = ConfettiView()
        backgroundView.colors = []
        backgroundView.type = .image(avocado)
        return backgroundView
    }()
	private let buttonHandler: (InfoViewController) -> Void
    private var areItemsSet = false
	
	init(
        headerTitle: String,
        infoItems: [InfoViewControllerItem],
        buttonTitle: String,
        buttonHandler: @escaping(InfoViewController) -> Void
    ) {
		self.headerTitle = headerTitle
		self.infoItems = infoItems
		self.buttonTitle = buttonTitle
		self.buttonHandler = buttonHandler
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        confettiView.startConfetti()
		navigationController?.setNavigationBarHidden(true, animated: true)
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard !areItemsSet else { return }
        let topSpacingItem = SpaceCellItem(spaceHeight: Constants.headerItemSpacing)
        let header = SingleLabelCellItem(text: headerTitle, font: .largeTitle)
        let bottomSpacingItem = SpaceCellItem(spaceHeight: Constants.bottomHeaderItemSpacing)
        let sectionsItems = infoItems.map { LittTableViewSection(items: [$0]) }
        var sections = [LittTableViewSection(
            items: [topSpacingItem, header, bottomSpacingItem]
        )]
        sections.append(contentsOf: sectionsItems)
        tableView.sections = sections
        areItemsSet = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        confettiView.stopConfetti()
    }
	
	override func loadView() {
		view = UIView()
        view?.addSubview(tableView)
		view.backgroundColor = tableView.backgroundColor
        navigationController?.navigationBar.prefersLargeTitles = true

        let buttonAndOffset = addBottomButtonWithBlur(with: buttonTitle)
		buttonAndOffset.0.setTapHandler { [weak self] in
			guard let self = self else { return }
			self.buttonHandler(self)
		}
		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])
		tableView.contentInset.bottom = buttonAndOffset.1
		tableView.verticalScrollIndicatorInsets.bottom = buttonAndOffset.1
	}
}
