//
//  InfoViewControllerItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 02.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class InfoViewControllerItem {
	let icon: UIImage
	let tintColor: UIColor
	let title: String
	let description: String
	
	init(icon: UIImage,
		 title: String,
		 description: String,
         tintColor: UIColor = .systemGreen) {
		self.icon = icon
		self.title = title
		self.description = description
		self.tintColor = tintColor
	}
}

extension InfoViewControllerItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? InfoViewControllerTableViewCell else { return }
        cell.backgroundColor = .clear
		cell.iconImageView.image = icon
		cell.iconImageView.tintColor = tintColor
		cell.titleLabel.text = title
		cell.descriptionLabel.text = description
		cell.selectionStyle = .none
	}
	
	func viewClass() -> AnyClass {
		InfoViewControllerTableViewCell.self
	}
}
