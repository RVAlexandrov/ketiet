//
//  InfoViewControllerTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 02.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class InfoViewControllerTableViewCell: UITableViewCell {
	
	private struct Constants {
		static let iconViewSide: CGFloat = 25
	}
    
    private lazy var blurView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
	
	let iconImageView: UIImageView = {
		let view = UIImageView()
		view.contentMode = .scaleAspectFit
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	let titleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
		label.lineBreakMode = .byWordWrapping
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
		label.numberOfLines = 0
		return label
	}()
	
	let descriptionLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		label.font = .body
        label.setContentHuggingPriority(.defaultHigh + 1, for: .vertical)
        label.setContentCompressionResistancePriority(.defaultHigh + 1, for: .vertical)
		label.textColor = .secondaryLabel
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {        
        contentView.addSubviews([blurView, iconImageView, titleLabel, descriptionLabel])
        
		NSLayoutConstraint.activate([
            blurView.topAnchor.constraint(equalTo: contentView.topAnchor),
            blurView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            blurView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            blurView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
			iconImageView.heightAnchor.constraint(equalToConstant: Constants.iconViewSide),
			iconImageView.widthAnchor.constraint(equalTo: iconImageView.heightAnchor),
            iconImageView.topAnchor.constraint(equalTo: titleLabel.topAnchor),
			iconImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,
												   constant: .largeLittScreenEdgeMargin),
			iconImageView.trailingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor,
													constant: -.largeLittMargin),
			descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,
													   constant: -.largeLittScreenEdgeMargin),
			descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,
												  constant: .smallLittMargin),
			descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
													 constant: -.largeLittMargin),
			titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,
													   constant: .largeLittScreenEdgeMargin),
			titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor,
											constant: .mediumLittMargin),
			titleLabel.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor)
		])
	}
}
