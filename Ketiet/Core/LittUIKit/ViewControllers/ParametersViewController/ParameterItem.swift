//
//  ParameterItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

final class ParameterItem {
	let title: String
	let valueType: LittValueType
	let defaultValue: LittValue?
	
	init(title: String,
		 valueType: LittValueType,
		 defaultValue: LittValue? = nil) {
		self.title = title
		self.valueType = valueType
		self.defaultValue = defaultValue
	}
}
