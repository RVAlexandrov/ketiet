//
//  ParametersViewModelProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

protocol ParametersPresenterProtocol {
    
    func didCompleteValuesInput(values: [LittValue?])
}
