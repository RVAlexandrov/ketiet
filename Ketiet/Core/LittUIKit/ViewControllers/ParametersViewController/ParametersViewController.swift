//
//  ParametersViewController.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ParametersViewController: UIViewController {

	private lazy var sections = [LittTableViewSectionProtocol]()
	private let buttonTitle: String
    private var button: LittBasicButton?
	private let validatorFactory: ValidatorFactoryProtocol
    private let presenter: ParametersPresenterProtocol
	private lazy var tableView: LittTableView = {
        let view = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
		view.translatesAutoresizingMaskIntoConstraints = false
        view.animations = []
		view.insetsChangeHelper = ScrollViewContentInsetsHelper(scrollView: view)
		view.backgroundColor = .littBackgroundColor
		view.separatorStyle = .none
		view.keyboardDismissMode = .interactive
		return view
	}()
	
    init(
        title: String,
        validatorFactory: ValidatorFactoryProtocol,
        parameters: [ParameterItem],
        buttonTitle: String,
        presenter: ParametersPresenterProtocol
    ) {
		self.validatorFactory = validatorFactory
        self.buttonTitle = buttonTitle
		self.presenter = presenter
		super.init(nibName: nil, bundle: nil)
		self.navigationItem.title = title
		sections = parameters.map { param -> LittTableViewSection in
			switch param.valueType {
			case .int:
                return LittTableViewSection(items: [makeIntInputItem(
                    title: param.title,
                    validator: validatorFactory.configureValidator(forType: .float),
                    text: param.defaultValue?.text
                )])
			case .float:
				return LittTableViewSection(items: [makeFloatInputItem(
                    title: param.title,
                    validator: validatorFactory.configureValidator(forType: .float),
                    text: param.defaultValue?.text
                )])
			case .text:
				return LittTableViewSection(items: [makeTextInputItem(
                    title: param.title,
                    keyboardType: .default,
                    text: param.defaultValue?.text,
                    validator: validatorFactory.configureValidator(
                        forType: .textWithMaxLength(
                            min: 1,
                            max: 40
                        )
                    )
                )])
			case .time:
				return LittTableViewSection(items: [makeTimeInputItem(
                    title: param.title,
                    validator: validatorFactory.configureValidator(
                        forType: .textWithMaxLength(min: 1,
                                                    max: 40
                        )
                    ),
                    date: param.defaultValue?.date
                )])
			case let .select(options):
				return LittTableViewSection(items: [makeSelectorInputItem(
                    title: param.title,
                    validator: validatorFactory.configureValidator(
                        forType: .textWithMaxLength(
                            min: 1,
                            max: 100
                        )
                    ),
                    selectorOptions: options,
                    text: param.defaultValue?.text
                )])
			}
		}
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		view = UIView()
		view.backgroundColor = .littBackgroundColor
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		addTableView()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		updateButtonState()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		littTabBarController?.setTabBarHidden(true)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		littTabBarController?.setTabBarHidden(false)
	}
	
	private func addTableView() {
		view.addSubview(tableView)
        
        let buttonAndOffset = addBottomButtonWithBlur(with: buttonTitle)
        buttonAndOffset.0.setTapHandler { [weak self] in
            guard let self = self else { return }
            let values = self.sections.map { ($0.items.first as? LittTableViewInputCellItem)?.value }
            self.presenter.didCompleteValuesInput(values: values)
        }
        
        button = buttonAndOffset.0
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        tableView.contentInset.bottom = buttonAndOffset.1
        
		tableView.sections = sections
	}
	
	private func makeTextInputItem(title: String,
								   keyboardType: UIKeyboardType,
								   text: String?,
								   validator: Validator) -> LittTableViewTextInputCellItem {
		LittTableViewTextInputCellItem(title: title,
									   validator: validator,
									   keyboardType: keyboardType,
									   backgroundColor: .littBackgroundColor,
									   tableView: tableView,
									   text: text) { [weak self] in
			self?.updateButtonState()
		}
	}
	
	private func makeFloatInputItem(title: String,
									validator: Validator,
									text: String?) -> LittTableViewInputCellItem {
		LittTableViewFloatInputItemWrapper(wrapped: makeTextInputItem(title: title,
																	  keyboardType: .decimalPad,
																	  text: text,
																	  validator: validator))
	}
	
	private func makeIntInputItem(title: String,
								  validator: Validator,
								  text: String?) -> LittTableViewInputCellItem {
		LittTableViewIntInputItemWrapper(wrapped: makeTextInputItem(title: title,
																	keyboardType: .numberPad,
																	text: text,
																	validator: validator))
	}
	
	private func makeTimeInputItem(title: String,
								   validator: Validator,
								   date: Date?) -> LittTableViewInputCellItem {
		LittTableViewTimeInputCellItem(item: makeTextInputItem(title: title,
															   keyboardType: .default,
															   text: nil,
															   validator: validator),
                                       initialDate: date)
	}
	
	private func makeSelectorInputItem(title: String,
									   validator: Validator,
									   selectorOptions: [String],
									   text: String?) -> LittTableViewInputCellItem {
		LittTableViewSelectorInputCellItem(item: makeTextInputItem(title: title,
																   keyboardType: .default,
																   text: text,
																   validator: validator),
										   selectorOptions: selectorOptions)
	}
	
	private func updateButtonState() {
        guard let button = button else { return }
		for section in sections {
            if !((section.items.first as? LittTableViewInputCellItem)?.isValid ?? false) {
				button.isEnabled = false
				return
			}
		}
		button.isEnabled = true
	}
}
