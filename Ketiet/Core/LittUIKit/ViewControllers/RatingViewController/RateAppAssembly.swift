//
//  RateAppAssembly.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 14.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import StoreKit

final class RateAppAssembly {
	
	private struct Constants {
		static let rateUsAsked = "rateUsAsked"
		static let maxAskingCount = 3
	}
	
	func showRateAppController(from controller: UIViewController, completion: (() -> Void)? = nil) {
		if UserDefaults.standard.integer(forKey: Constants.rateUsAsked) >= Constants.maxAskingCount {
			completion?()
			return
		}
		let ratingVC = RatingViewController { badRating, controller in
			controller.dismiss(animated: true) {
				let oldValue = UserDefaults.standard.integer(forKey: Constants.rateUsAsked)
				UserDefaults.standard.set(oldValue + 1, forKey: Constants.rateUsAsked)
				UserDefaults.standard.synchronize()
                if !badRating, let scene = UIApplication.shared.firstWindowScene() {
					SKStoreReviewController.requestReview(in: scene)
				}
				completion?()
			}
		}
		controller.present(ratingVC, animated: true)
	}
	
	func deleteAskedRating() {
		UserDefaults.standard.removeObject(forKey: Constants.rateUsAsked)
		UserDefaults.standard.synchronize()
	}
}
