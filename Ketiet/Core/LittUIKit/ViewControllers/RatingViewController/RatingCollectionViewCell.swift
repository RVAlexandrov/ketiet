//
//  RatingCollectionViewCell.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 10/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class RatingCollectionViewCell: UICollectionViewCell {
	
	private let imageView = UIImageView(image: UIImage(systemName: "star.fill")?.withRenderingMode(.alwaysTemplate))
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initialize()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initialize()
	}
	
	private func initialize() {
		imageView.translatesAutoresizingMaskIntoConstraints = false
		imageView.tintColor = .accentColor
		contentView.addSubview(imageView)
		imageView.pinToSuperview()
	}
}
