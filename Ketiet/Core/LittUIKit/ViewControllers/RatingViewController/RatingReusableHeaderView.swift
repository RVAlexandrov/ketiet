//
//  RatingReusableHeaderView.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 11/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class RatingReusableHeaderView: UICollectionReusableView {
	
	let titleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .subTitle
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 2
		label.textAlignment = .center
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initialize()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initialize()
	}
	
	private func initialize() {
		addSubview(titleLabel)
		titleLabel.pinToSuperview(insets: UIEdgeInsets(top: .mediumLittMargin,
													   left: .largeLittMargin,
													   bottom: .mediumLittMargin,
													   right: .largeLittMargin))
	}
}
