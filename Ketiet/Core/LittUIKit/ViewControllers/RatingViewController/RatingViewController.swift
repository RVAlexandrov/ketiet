//
//  RatingViewController.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 10/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class RatingViewController: UICollectionViewController {
	
	private struct Constants {
		static let reuseIdentifier = "Cell"
		static let headerIdentifier = "Header"
		static let starSize = CGSize(width: 50, height: 50)
		static let borderWidth: CGFloat = 0.5
		static let cornerRadius: CGFloat = 20
	}
	
	private let numberOfStars : Int
	private var currentNumberOfStars = 0
	private let didSelectRating: (Bool, RatingViewController) -> Void
	private var selectedRating = -1
	
	init(numberOfStars: Int = 5,
		 didSelectRating: @escaping(Bool, RatingViewController) -> Void) {
		let layout = UICollectionViewFlowLayout()
		layout.itemSize = Constants.starSize
		layout.scrollDirection = .vertical
		layout.sectionInset = UIEdgeInsets(top: .largeLittMargin,
										   left: .largeLittMargin,
										   bottom: .largeLittMargin,
										   right: .largeLittMargin)
		layout.minimumInteritemSpacing = .smallLittMargin
		self.numberOfStars = numberOfStars
		self.didSelectRating = didSelectRating
		super.init(collectionViewLayout: layout)
		transitioningDelegate = self
		modalPresentationStyle = .custom
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		collectionView.isScrollEnabled = false
		collectionView.register(RatingCollectionViewCell.self,
								forCellWithReuseIdentifier: Constants.reuseIdentifier)
		collectionView.register(RatingReusableHeaderView.self,
								forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
								withReuseIdentifier: Constants.headerIdentifier)
		collectionView.backgroundColor = .clear
		let view = UIVisualEffectView(effect: UIBlurEffect(style: .systemThinMaterial))
		view.clipsToBounds = true
		view.layer.borderWidth = Constants.borderWidth
		view.layer.borderColor = UIColor.systemGray.cgColor
		view.layer.cornerRadius = Constants.cornerRadius
		collectionView.backgroundView = view
		collectionView.layer.cornerRadius = Constants.cornerRadius
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		currentNumberOfStars = 1
		animateNewCellInseting()
	}
	
	private func animateNewCellInseting() {
		collectionView.performBatchUpdates({
			self.collectionView.insertItems(at: [IndexPath(row: currentNumberOfStars - 1, section: 0)])
		}) { (_) in
			self.insertNewCellIfNeeded()
		}
	}

	private func insertNewCellIfNeeded() {
		if currentNumberOfStars == numberOfStars {
			return
		}
		currentNumberOfStars += 1
		animateNewCellInseting()
	}
	
	// MARK: UICollectionViewDataSource

	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return currentNumberOfStars
	}

	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.reuseIdentifier,
													  for: indexPath)
		cell.contentView.alpha = indexPath.row <= selectedRating ? 1 : 0.5
		return cell
	}

	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		selectedRating = indexPath.row
		collectionView.performBatchUpdates({
			collectionView.reloadItems(at: (0...numberOfStars-1).map { IndexPath(row: $0, section: 0) })
		}) { (_) in
			self.didSelectRating(indexPath.row < (self.numberOfStars - 2), self)
		}
	}
	
	override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
																   withReuseIdentifier: Constants.headerIdentifier,
																   for: indexPath)
		if let header = view as? RatingReusableHeaderView {
			header.titleLabel.text = "PLEASE_RATE_US".localized()
		}
		return view
	}
	
	private func headerSize() -> CGSize {
		if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
			let width = CGFloat(numberOfStars) * layout.itemSize.width + CGFloat(numberOfStars - 1) * layout.minimumInteritemSpacing
			let size = ("PLEASE_RATE_US".localized() as NSString).boundingRect(with: CGSize(width: width,
																							height: .greatestFiniteMagnitude),
																			   options: [.usesFontLeading, .usesFontLeading],
																			   attributes: [.font : UIFont.subTitle],
																			   context: nil).size
			return CGSize(width: size.width, height: 2 * size.height)
		}
		return .zero
	}
}

extension RatingViewController : UIViewControllerTransitioningDelegate {
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		let controller = DimmingBackgroundPresentationController(presentedViewController: presented, presenting: presenting)
		if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
			let size = layout.itemSize
			let sectionInsets = layout.sectionInset
			let width = CGFloat(numberOfStars) * size.width + CGFloat(numberOfStars - 1) * layout.minimumInteritemSpacing + sectionInsets.right + sectionInsets.left
			let height = size.height + headerSize().height + sectionInsets.top + sectionInsets.bottom
			let screenSize = UIScreen.main.bounds.size
			controller.finalFrameOfPresentedVC = CGRect(x: (screenSize.width - width)/2, y: (screenSize.height - height)/2, width: width, height: height)
		}
		return controller
	}
	
	func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return PresentationFromCenterAnimator()
	}
	
	func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return ToCenterDismissingAnimator()
	}
}

extension RatingViewController : UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		return headerSize()
	}
}
