//
//  CheckMarkView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class CheckMarkView: UIButton {
    
    enum Constants {
        static let size = CGSize(width: 28, height: 28)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configurationUpdateHandler = { button in
            if button.isSelected {
                button.configuration = .tinted()
                button.configuration?.baseBackgroundColor = .checkmarkViewSelectedBackgroundColor
                button.configuration?.baseForegroundColor = .systemGreen
                button.configuration?.cornerStyle = .capsule
                button.configuration?.image = UIImage(systemName: "checkmark")
            } else {
                button.configuration = .bordered()
                button.configuration?.baseForegroundColor = .systemGray4
                button.configuration?.baseBackgroundColor = .clear
                button.configuration?.cornerStyle = .capsule
                button.configuration?.background.strokeColor = .systemGray4
                button.configuration?.background.strokeWidth = 2
                button.configuration?.imagePlacement = .all
                button.configuration?.image = nil
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
