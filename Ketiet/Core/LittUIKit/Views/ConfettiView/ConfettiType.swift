//
//  ConfettiType.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.06.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

enum ConfettiType {
    case confetti
    case triangle
    case star
    case diamond
    case image(UIImage)
    
    var image: UIImage? {
        switch self {
        case .confetti:
            return UIImage(named: "confetti")
        case .triangle:
            return UIImage(named: "triangle")
        case .star:
            return UIImage(named: "star")
        case .diamond:
            return UIImage(named: "diamond")
        case let .image(image):
            return image
        }
    }
}
