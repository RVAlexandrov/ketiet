//
//  ConfettiView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.06.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit
import QuartzCore

final class ConfettiView: UIView {

    private var emitter: CAEmitterLayer?
    var colors = [UIColor(red:0.95, green:0.40, blue:0.27, alpha:1.0),
                  UIColor(red:1.00, green:0.78, blue:0.36, alpha:1.0),
                  UIColor(red:0.48, green:0.78, blue:0.64, alpha:1.0),
                  UIColor(red:0.30, green:0.76, blue:0.85, alpha:1.0),
                  UIColor(red:0.58, green:0.39, blue:0.55, alpha:1.0)]
    var intensity = Float(0.5)
    var emiterCellsCount = 5
    var type = ConfettiType.confetti
    private(set) var active = false

    func startConfetti() {
        let emitter = CAEmitterLayer()
        emitter.emitterPosition = CGPoint(x: frame.size.width / 2.0, y: 0)
        emitter.emitterShape = .line
        emitter.emitterSize = CGSize(width: frame.size.width, height: 1)
        if colors.isEmpty {
            emitter.emitterCells = (0..<emiterCellsCount).map { _ in
                let cell = CAEmitterCell(color: nil, intensity: intensity, type: type)
                cell.emissionLongitude = .pi
                cell.emissionRange = .pi / 4
                return cell
            }
        } else {
            emitter.emitterCells = colors.map { CAEmitterCell(color: $0, intensity: intensity, type: type) }
        }
        self.emitter = emitter
        layer.addSublayer(emitter)
        active = true
    }

    func stopConfetti() {
        emitter?.birthRate = 0
        active = false
    }
}
