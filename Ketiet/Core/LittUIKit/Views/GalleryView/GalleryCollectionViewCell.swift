//
//  GalleryCollectionViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

/// Ячейка галереи картинок в статье
final class GalleryCollectionViewCell: UICollectionViewCell {
	
	/// Вью в которое помещается вью из ВьюМодели, поэтому UIView
	private var view = UIView()
	
	private let backView: UIView = {
		let view = UIView()
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
	}
	
	required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
	override func prepareForReuse() {
		super.prepareForReuse()
		view.removeFromSuperview()
	}
	
	/// Обновление содержимого ячейки коллекции (Галереи)
	///
	/// - Parameter item: элемент галереи
	func update(with item: GalleryViewItem) {
		self.view = item.view
		backView.addSubview(view)
		updateConstraint()
	}
	
	private func setupViews() {
		backView.layer.cornerRadius = .littBasicCornerRadius
		
		contentView.addSubview(backView)
		translatesAutoresizingMaskIntoConstraints = false
		
		setupConstraints()
	}
	
	private func setupConstraints() {
		backView.pinToSuperview(insets: UIEdgeInsets(top: 18, left: 18, bottom: 18, right: 18), useSafeArea: false)
	}
	
	private func updateConstraint() {
		view.pinToSuperview()
	}
}
