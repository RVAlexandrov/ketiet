//
//  GalleryView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

/// Протокол элемента галереи, который нужно отобразить в галерее
protocol GalleryViewItem {
	
	/// Вью отображаемая в галерее
	var view: UIView { get }
	
	/// Подпись у Вью в галереи
	var title: String? { get }
}

protocol GalleryViewDelegate: AnyObject {
	func galleryView(_ view: GalleryView, didChangePage index: Int)
}

/// Элемент Карусель (галерея)
final class GalleryView: UIView {
	
	struct Constants {
		static let contentHeight: CGFloat = 200
		static let progressViewHeight: CGFloat = 31
		static let delayForAutoScroll: TimeInterval = 5.0
	}
	
	weak var delegate: GalleryViewDelegate?
	
	/// Источник данных Карусели (галереи)
	var content: [GalleryViewItem]? {
		didSet {
			reloadData()
		}
	}
	var useAutoscroll: Bool = false {
		didSet {
			reconfigureAutoscroll()
		}
	}
	
	private var timer: Timer?
	
	/// Прогресс бар
	lazy var progressView: PaginationView = {
		let view = PaginationView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	/// КоллекшнВью содержащий картинки/видео
	private lazy var contentView: UICollectionView = {
		let flowLayout = UICollectionViewFlowLayout()
		flowLayout.minimumLineSpacing = .leastNormalMagnitude
		flowLayout.minimumInteritemSpacing = .leastNormalMagnitude
		flowLayout.scrollDirection = .horizontal
		let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		collectionView.dataSource = self
		collectionView.delegate = self
		collectionView.register(GalleryCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
		collectionView.showsHorizontalScrollIndicator = false
		collectionView.backgroundColor = .clear
		collectionView.isPagingEnabled = true
		return collectionView
	}()
	
	/// Заголовок карусели
	private(set) lazy var captionLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 0
		label.font = .smallBody
		label.textAlignment = .center
		label.translatesAutoresizingMaskIntoConstraints = false
		label.textColor = .littSecondaryTextColor
		return label
	}()
	
	private lazy var progressViewTopConstraint = progressView.topAnchor.constraint(equalTo: captionLabel.bottomAnchor)
	private lazy var progressViewHeightConstraint = progressView.heightAnchor.constraint(equalToConstant: Constants.progressViewHeight)
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupView()
	}
	
	@available(*, unavailable)
	required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
	override func layoutSubviews() {
		super.layoutSubviews()
		progressView.numberOfPages = content?.count ?? 0
	}
	
	/// Обновление содержимого
	func reloadData() {
		contentView.reloadData()
		
		guard let content = content else { return }
		captionLabel.text = content.first?.title
		if content.count == 1 {
			progressView.isHidden = true
			progressViewTopConstraint.constant = 12
			progressViewHeightConstraint.constant = 0
		} else {
			progressView.isHidden = false
			progressViewTopConstraint.constant = 0
			progressViewHeightConstraint.constant = Constants.progressViewHeight
		}
		
		reconfigureAutoscroll()
	}
	
	func setVisibleItemIndex(_ index: Int, animated: Bool = false) {
		guard index < (content?.count ?? 0) else {
			assertionFailure("Wrong index has been passed")
			return
		}
		DispatchQueue.main.async {
			self.contentView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: animated)
			self.progressView.currentPage = index
		}
	}
}

private extension GalleryView {
	private func setupView() {
		addSubviews([contentView, progressView, captionLabel])
		setupConstraint()
	}
	
	/// Установка констреинтов
	private func setupConstraint() {
		NSLayoutConstraint.activate([
			contentView.topAnchor.constraint(equalTo: topAnchor),
			contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
			contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
			contentView.heightAnchor.constraint(equalToConstant: Constants.contentHeight),
			
			progressViewTopConstraint,
			progressView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 18),
			progressView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -18),
			progressViewHeightConstraint,
			
			captionLabel.topAnchor.constraint(equalTo: contentView.bottomAnchor),
			captionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 18),
			captionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -18) ,
			progressView.bottomAnchor.constraint(equalTo: bottomAnchor)
		])
	}
	
	/// Анимация заголовка карусели в зависимости от положения содержимого
	///
	/// - Parameters:
	///   - position: позиция содержимого по оси X
	///   - row: индекс текущей строки
	private func animateCaption(with position: CGFloat, row: Int) {
		let offset = position - CGFloat(row) * contentView.frame.width
		let percent = offset / frame.width * 2
		if percent > 0 {
			captionLabel.alpha = 1 - percent
		} else {
			captionLabel.alpha = 1 + percent
		}
	}
	
	private func reconfigureAutoscroll() {
		stopAutoScrollTimer()
		
		guard let contentNumber = content?.count else {
			return
		}
		
		if contentNumber > 1 && useAutoscroll {
			startAutoScrollTimer()
		}
	}
	
	private func changePageProgress() {
		guard let numberOfContent = content?.count, contentView.frame.width > 0, numberOfContent > 1 else { return }
		let pageNumber = Int(contentView.contentOffset.x / contentView.frame.width)
		if  progressView.currentPage != pageNumber {
			progressView.currentPage = pageNumber
			delegate?.galleryView(self, didChangePage: pageNumber)
		}
	}
	
	private func startAutoScrollTimer() {
		let timer = Timer(timeInterval: Constants.delayForAutoScroll, repeats: false) { [weak self] _ in
			self?.stopAutoScrollTimer()
			self?.scrollNext()
		}
		
		RunLoop.current.add(timer, forMode: .common)
		self.timer = timer
	}
	
	private func stopAutoScrollTimer() {
		timer?.invalidate()
		timer = nil
	}
	
	private func scrollNext() {
		guard let numberOfContent = content?.count, numberOfContent > 1 else { return }
		let currentPosition = Int(contentView.contentOffset.x / contentView.frame.width)
		
		let nextIndex: Int
		if currentPosition >= numberOfContent - 1 {
			nextIndex = 0
		} else {
			nextIndex = currentPosition + 1
		}
		
		contentView.setContentOffset(CGPoint(x: CGFloat(nextIndex) * contentView.frame.width, y: 0), animated: true)
	}
}

extension GalleryView: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return content?.count ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
		
		if let cell = cell as? GalleryCollectionViewCell {
			guard let content = content else { return cell }
			let galleryItem = content[indexPath.row]
			cell.update(with: galleryItem)
		}
		
		return cell
	}
}

extension GalleryView: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView,
							   layout collectionViewLayout: UICollectionViewLayout,
							   sizeForItemAt indexPath: IndexPath) -> CGSize {
		let size = CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
		return size
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		stopAutoScrollTimer()
		
		let visibleRect = CGRect(origin: contentView.contentOffset, size: contentView.bounds.size)
		let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
		
		if let visibleIndexPath = contentView.indexPathForItem(at: visiblePoint) {
			animateCaption(with: contentView.contentOffset.x, row: visibleIndexPath.row)
			let galleryItem = content?[visibleIndexPath.row]
			captionLabel.text = galleryItem?.title
		}
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		reconfigureAutoscroll()
		changePageProgress()
	}
	
	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
		reconfigureAutoscroll()
		changePageProgress()
	}
}
