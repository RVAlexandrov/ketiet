//
//  NotificationView.swift
//  Ketiet
//
//  Created by Aleksandrov Roman on 28.04.2021.
//  Copyright © 2021 LittDev. All rights reserved.
//

import UIKit

class NotificationView: UIView {

    private var timer: Timer?
    
    var lifeСycleNotification = 2.0
        
    lazy var tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(recognizer:)))
    
    var tapAction: (() -> Void)?
    
    var title: String? {
        didSet {
            layoutUI()
        }
    }
    
    var image: UIImage? {
        didSet {
            layoutUI()
        }
    }

    private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        label.numberOfLines = 0
        label.textColor = .systemBackground
        label.textAlignment = .center
        return label
    }()
    
    private lazy var widthConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self,
                                                                              attribute: .width,
                                                                              relatedBy: .equal,
                                                                              toItem: nil,
                                                                              attribute: .notAnAttribute,
                                                                              multiplier: 1,
                                                                              constant: 0)
    
    private lazy var heightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self,
                                                                               attribute: .height,
                                                                               relatedBy: .equal,
                                                                               toItem: nil,
                                                                               attribute: .notAnAttribute,
                                                                               multiplier: 1,
                                                                               constant: 0)
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .systemBackground
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    init(title: String?, image: UIImage?) {
        super.init(frame: .zero)
        self.title = title
        self.image = image
        layoutUI()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showNotificationAnimate() {
        UIApplication.shared.lastWindowScene()?.windows.last?.addSubview(self)
        activateConstraints()
        transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 5,
                       options: .curveEaseInOut,
                       animations: {
                        self.transform = .identity
                        self.startTimer()
                       },
                       completion: nil)
    }
    
    func closeNotificationAnimate() {
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.alpha = 0
                       },
                       completion: { _ in
                        self.removeFromSuperview()
                       })
    }
}

// MARK: - Private methods
extension NotificationView {
    private var notificationWidth: CGFloat {
        UIScreen.main.bounds.width * 0.75
    }
    
    private func activateConstraints() {
        guard let superview = superview else { return }
        NSLayoutConstraint.activate([
            centerYAnchor.constraint(equalTo: superview.centerYAnchor),
            centerXAnchor.constraint(equalTo: superview.centerXAnchor),
            widthConstraint,
            heightConstraint
        ])
    }
    
    private func setupView() {
        backgroundColor = .label
        layer.cornerRadius = 12
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
        addGestureRecognizer(tap)

        stackView.addArrangedSubviews([titleLabel, imageView])
        addSubview(stackView)
        stackView.pinToSuperview(insets: UIEdgeInsets(top: 0,
                                                      left: .largeLittMargin,
                                                      bottom: 0,
                                                      right: .largeLittMargin))
    }
    
    private func layoutUI() {
        let maxWidth = UIScreen.main.bounds.width * 0.75
        
        var height: CGFloat = 0
        var width: CGFloat = 0
        
        if let title = title {
            height += title.height(withWidth: maxWidth,
                                   font: titleLabel.font)
            
            width += title.width(withHeight: height,
                                 font: titleLabel.font)
            
            titleLabel.text = title
            titleLabel.isHidden = false
        } else {
            titleLabel.isHidden = true
        }
        
        if let image = image {
            width += 16
            imageView.image = image
            imageView.isHidden = false
        } else {
            imageView.isHidden = true
        }
        
        if image != nil, title != nil {
            width += 8
        }

        if width <= maxWidth {
            widthConstraint.constant = width + 32 + 8
            heightConstraint.constant = height + 20
        } else {
            widthConstraint.constant = maxWidth
            heightConstraint.constant = height + 20
        }

        setNeedsLayout()
    }

    private func startTimer() {
        guard lifeСycleNotification > 0 else { return }
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(updateTimer(sender:)),
                                     userInfo: nil,
                                     repeats: true)
    }

    @objc
    private func updateTimer(sender: Timer) {
        lifeСycleNotification -= 1
        if lifeСycleNotification <= 0 {
            sender.invalidate()
            closeNotificationAnimate()
        }
    }
    
    @objc
    private func handleTap(recognizer: UITapGestureRecognizer) {
        if let timer = timer { timer.invalidate() }
        closeNotificationAnimate()
        tapAction?()
    }
}
