//
//  LittRadioButton.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class LittRadioButton: UIButton {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    @available(*, unavailable)
    public override init(frame: CGRect) { fatalError("init(coder:) has not been implemented") }
    
    private var color: UIColor
    
    private lazy var roundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = color
        view.isUserInteractionEnabled = false
        view.isHidden = true
        return view
    }()
    
    var touchCompletion: (() -> Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        roundView.layer.cornerRadius = roundView.frame.height / 2
    }
    
    /// Состояние обозначающее наличие метки внутри кнопки
    var isSelectedState = false {
        didSet {
            roundView.isHidden = !isSelectedState
            let currentColor = isSelectedState ? UIColor.systemGreen : color
            layer.borderColor = currentColor.cgColor
            roundView.backgroundColor = currentColor
        }
    }
    
    init(color: UIColor,
         touchCompletion: (() -> Void)?) {
        self.color = color
        self.touchCompletion = touchCompletion
        super.init(frame: .zero)
        configureView()
        configureTouchAction()
    }
}

// MARK: - Private merhods
extension LittRadioButton {
    
    private func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        layer.borderColor = color.cgColor
        layer.borderWidth = 2.0
        
        addSubview(roundView)
        
        NSLayoutConstraint.activate([
            roundView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5),
            roundView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5),
            roundView.centerYAnchor.constraint(equalTo: centerYAnchor),
            roundView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    private func configureTouchAction() {
        addTarget(self, action: #selector(didTapRadioButton), for: .touchUpInside)
    }
    
    @objc
    private func didTapRadioButton() {
        touchCompletion?()
    }
}
