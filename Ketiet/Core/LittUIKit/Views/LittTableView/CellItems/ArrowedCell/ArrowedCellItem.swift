//
//  ArrowedCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 05.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class ArrowedCellItem {
    
    private let title: String
    private let imageSystemString: String?
    private let imageColor: UIColor?
    private let cellColor: UIColor
    private let selectionHandler: (() -> Void)?
    private weak var cell: UITableViewCell?
    
    init(title: String,
         imageSystemString: String? = nil,
         imageColor: UIColor? = nil,
         cellColor: UIColor? = .littSecondaryBackgroundColor,
         selectionHandler: (() -> Void)? = nil) {
        self.title = title
        self.imageSystemString = imageSystemString
        self.imageColor = imageColor
        self.cellColor = cellColor ?? .littSecondaryBackgroundColor
        self.selectionHandler = selectionHandler
    }
}

// MARK: - LittTableViewCellItemProtocol
extension ArrowedCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        var conf = UIListContentConfiguration.cell()
        conf.text = title
        if let imageString = imageSystemString {
            conf.image = UIImage(systemName: imageString)
            conf.imageProperties.tintColor = imageColor
        }
        view.contentConfiguration = conf
        view.accessoryType = .disclosureIndicator
        
        self.cell = view
    }
    
    func viewClass() -> AnyClass {
        UITableViewCell.self
    }
    
    func didSelect() {
        selectionHandler?()
    }
}
