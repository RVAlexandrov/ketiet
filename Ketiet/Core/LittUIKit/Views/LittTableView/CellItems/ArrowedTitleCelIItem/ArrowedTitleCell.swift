//
//  ArrowedTitleCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 16.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class ArrowedTitleCell: UITableViewCell {

    let label: UILabel = {
        let label = UILabel()
        label.font = .body
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    let arrowButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "arrow.right"), for: .normal)
        button.tintColor = .accentColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .littSecondaryBackgroundColor
        button.layer.cornerRadius = 8
        button.configuration?.contentInsets = NSDirectionalEdgeInsets(
            top: 4,
            leading: 2,
            bottom: 4,
            trailing: 2
        )
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureCell() {
        contentView.addSubviews([label, arrowButton])
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            label.bottomAnchor.constraint(greaterThanOrEqualTo: contentView.bottomAnchor),
            label.trailingAnchor.constraint(equalTo: arrowButton.leadingAnchor, constant: .largeLittMargin),
            arrowButton.topAnchor.constraint(equalTo: label.topAnchor),
            arrowButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
            arrowButton.heightAnchor.constraint(equalToConstant: 25),
            arrowButton.widthAnchor.constraint(equalToConstant: 30),
            arrowButton.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor)
        ])
    }
}
