//
//  ArrowedTitleCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 16.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class ArrowedTitleCellItem {
    
    private let text: String
    private let lineBreakMode: NSLineBreakMode
    private let textAlignment: NSTextAlignment
    private let font: UIFont
    private let textColor: UIColor
    private let backgroundColor: UIColor?
    private let didSelectHandler: (ArrowedTitleCellItem) -> Void
    
    init(text: String,
         lineBreakMode: NSLineBreakMode = .byWordWrapping,
         textAlignment: NSTextAlignment = .left,
         font: UIFont = .subTitle,
         textColor: UIColor = .label,
         backgroundColor: UIColor? = .clear,
         didSelectHandler: @escaping(ArrowedTitleCellItem) -> Void) {
        self.text = text
        self.lineBreakMode = lineBreakMode
        self.textAlignment = textAlignment
        self.font = font
        self.textColor = textColor
        self.backgroundColor = backgroundColor
        self.didSelectHandler = didSelectHandler
    }
}

extension ArrowedTitleCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? ArrowedTitleCell else { return }
        cell.label.font = font
        cell.label.textAlignment = textAlignment
        cell.label.lineBreakMode = lineBreakMode
        cell.label.textColor = textColor
        cell.label.numberOfLines = 0
        cell.label.text = text
        cell.arrowButton.addTarget(self,
                                   action: #selector(didTapButton),
                                   for: .touchUpInside)
        if backgroundColor != nil {
            view.contentView.backgroundColor = backgroundColor
            view.backgroundColor = backgroundColor
        }
    }
    
    @objc
    private func didTapButton() {
        didSelectHandler(self)
    }
    
    func viewClass() -> AnyClass {
        return ArrowedTitleCell.self
    }
    
    func didSelect() {
        didSelectHandler(self)
    }
}
