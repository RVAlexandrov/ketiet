//
//  BadgeCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class BadgesCellItem {
    let badgesType: [OutlineBadgeType]
    
    init(badgesType: [OutlineBadgeType]) {
        self.badgesType = badgesType
    }
}

extension BadgesCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? BadgesCell else { return }
        
        let builder = OutlineBadgeViewConfigurationBuilder()
        let views: [OutlineBadgeView] = badgesType.map {
            let model = builder.build(for: $0)
            let view = OutlineBadgeView()
            view.configuration = model
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }
        
        cell.stackView.addArrangedSubviews(views)
    }
    
    func viewClass() -> AnyClass {
        BadgesCell.self
    }
    
}
