//
//  WeightByDaysTableViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 28.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ButtonTableViewCell: UITableViewCell {
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var button: LittBasicButton?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
    }
    
    func configureButton(
        title: String,
        color: UIColor,
        handler: @escaping () -> Void
    ) {
        guard button == nil else { return }
        let button = LittBasicButton(
            title: title,
            config: .filledLarge(color)
        )
        button.setTapHandler(handler: handler)
        self.button = button
        setupView(button: button)
    }
    
    func configureButton(configuration: UIButton.Configuration,
                         handler: @escaping () -> Void) {
        guard button == nil else { return }
        let button = LittBasicButton(title: configuration.title ?? "",
                                     config: .custom(configuration))
        button.configuration = configuration
        button.setNeedsUpdateConfiguration()
        button.setTapHandler(handler: handler)
        self.button = button
        setupView(button: button)
    }
}

// MARK: - Private methods
extension ButtonTableViewCell {
    private func setupView(button: LittBasicButton) {
        backgroundColor = .clear
        contentView.addSubview(button)
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: .mediumLittMargin
            ),
            button.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            button.bottomAnchor.constraint(
                equalTo: contentView.bottomAnchor,
                constant: -.mediumLittMargin
            ),
            button.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        ])
    }
}
