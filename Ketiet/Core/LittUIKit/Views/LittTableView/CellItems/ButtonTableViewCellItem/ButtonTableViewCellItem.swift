//
//  WeightByDaysTableViewCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 28.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ButtonTableViewCellItem {
    private let color: UIColor
    private let title: String
    private let handler: () -> Void
    private let configuration: UIButton.Configuration?
    
    init(title: String,
         color: UIColor,
         handler: @escaping () -> Void) {
        self.title = title
        self.color = color
        self.handler = handler
        self.configuration = nil
    }
    
    init(configuration: UIButton.Configuration,
         handler: @escaping () -> Void) {
        self.title = configuration.title ?? ""
        self.color = configuration.baseBackgroundColor ?? .accentColor
        self.handler = handler
        self.configuration = configuration
    }
}

// MARK: - LittTableViewCellItemProtocol
extension ButtonTableViewCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? ButtonTableViewCell else { return }
        if let configuration = configuration {
            cell.configureButton(
                configuration: configuration,
                handler: handler
            )
        } else {
            cell.configureButton(
                title: title,
                color: color,
                handler: handler
            )
        }
    }
    
    func viewClass() -> AnyClass {
        ButtonTableViewCell.self
    }
}
