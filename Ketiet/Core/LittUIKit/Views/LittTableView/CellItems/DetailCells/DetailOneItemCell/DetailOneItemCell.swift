//
//  DetailCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DetailOneItemCell: UITableViewCell {
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var detailView: DetailView = {
        let view = DetailView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupView()
    }
    
    func configureCell(model: DetailViewModel) {
        detailView.title = model.title
        detailView.value = model.value
        detailView.chipValue = model.valueDiff
        detailView.chipTextColor = model.valueDiffTrend?.color
    }
}

// MARK: - Private methods
extension DetailOneItemCell {
    private func setupView() {
        contentView.addSubviews([detailView])
        backgroundColor = .littBackgroundColor
        NSLayoutConstraint.activate([
            detailView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallLittMargin),
            detailView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            detailView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.smallLittMargin),
            detailView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
    }
}
