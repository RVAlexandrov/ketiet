//
//  DetailOneItemCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DetailOneItemCellItem {
    
    let model: DetailViewModel
    
    init(model: DetailViewModel) {
        self.model = model
    }
}

// MARK: - LittTableViewCellItemProtocol
extension DetailOneItemCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? DetailOneItemCell else { return }
        cell.configureCell(model: model)
    }
    
    func viewClass() -> AnyClass {
        DetailOneItemCell.self
    }
}
