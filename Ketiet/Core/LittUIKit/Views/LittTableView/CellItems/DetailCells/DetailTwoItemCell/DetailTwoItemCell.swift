//
//  DetailTwoItemCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DetailTwoItemCell: UITableViewCell {
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var firstDetailView: DetailView = {
        let view = DetailView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var secondDetailView: DetailView = {
        let view = DetailView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupView()
    }
    
    func configureCell(firstModel: DetailViewModel,
                       secondModel: DetailViewModel) {
        firstDetailView.title = firstModel.title
        firstDetailView.value = firstModel.value
        firstDetailView.chipValue = firstModel.valueDiff
        firstDetailView.chipTextColor = firstModel.valueDiffTrend?.color
        
        secondDetailView.title = secondModel.title
        secondDetailView.value = secondModel.value
        secondDetailView.chipValue = secondModel.valueDiff
        secondDetailView.chipTextColor = secondModel.valueDiffTrend?.color
    }
}

// MARK: - Private methods
extension DetailTwoItemCell {
    private func setupView() {
        backgroundColor = .littBackgroundColor
        contentView.addSubviews([firstDetailView, secondDetailView])
        NSLayoutConstraint.activate([
            firstDetailView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallLittMargin),
            firstDetailView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            firstDetailView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.smallLittMargin),
            firstDetailView.trailingAnchor.constraint(equalTo: secondDetailView.leadingAnchor, constant: -.mediumLittMargin),
            firstDetailView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 2 - .largeLittMargin - .mediumLittMargin / 2),
            
            secondDetailView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallLittMargin),
            secondDetailView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.smallLittMargin),
            secondDetailView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        ])
    }
}
