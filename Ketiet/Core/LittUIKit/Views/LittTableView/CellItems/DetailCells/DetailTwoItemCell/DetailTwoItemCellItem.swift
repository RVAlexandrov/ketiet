//
//  DetailTwoItemCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DetailTwoItemCellItem {
    
    let firstViewModel: DetailViewModel
    let secondViewModel: DetailViewModel
    
    init(firstViewModel: DetailViewModel,
         secondViewModel: DetailViewModel) {
        self.firstViewModel = firstViewModel
        self.secondViewModel = secondViewModel
    }
}

// MARK: - LittTableViewCellItemProtocol
extension DetailTwoItemCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? DetailTwoItemCell else { return }
        cell.configureCell(firstModel: firstViewModel, secondModel: secondViewModel)
    }
    
    func viewClass() -> AnyClass {
        DetailTwoItemCell.self
    }
}
