//
//  DetailView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DetailView: UIView {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .subTitle
        label.text = "DAYLY_AVERAGE".localized()
        return label
    }()
    
    private lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        return label
    }()
    
    private var chipView = ChipLabelView(text: "text",
                                         textColor: .systemGreen,
                                         chipColor: .littBackgroundColor)
    
    var title: String {
        didSet {
            titleLabel.text = title
        }
    }
    
    var value: String {
        didSet {
            valueLabel.text = value
        }
    }
    
    var chipValue: String? {
        didSet {
            if let chipValue = chipValue {
                chipView.setText(chipValue)
                chipView.isHidden = false
            } else {
                chipView.isHidden = true
            }
        }
    }
    
    var chipTextColor: UIColor? {
        didSet {
            if let chipTextColor = chipTextColor {
                chipView.setColor(chipTextColor)
                chipView.isHidden = false
            } else {
                chipView.isHidden = true
            }
        }
    }
    
    init(title: String = "title",
         value: String = "value",
         percentValue: String? = nil,
         percentColor: UIColor? = nil) {
        self.chipValue = percentValue
        self.chipTextColor = percentColor
        self.title = title
        self.value = value
        
        super.init(frame: .zero)
        
        setupView()
    }
    
    override init(frame: CGRect) {
        self.chipValue = "percentValue"
        self.chipTextColor = .systemRed
        self.title = "title"
        self.value = "value"
        
        super.init(frame: frame)
        
        setupView()
    }

}

// MARK: - Private methods
extension DetailView {
    private func setupView() {
        backgroundColor = .littSecondaryBackgroundColor
        layer.cornerRadius = .littBasicCornerRadius
        addSubviews([titleLabel, valueLabel, chipView])
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: .mediumLittMargin),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            
            valueLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .mediumLittMargin),
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            valueLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.mediumLittMargin),
            
            chipView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.mediumLittMargin),
            chipView.centerYAnchor.constraint(equalTo: valueLabel.centerYAnchor)
        ])
    }
}
