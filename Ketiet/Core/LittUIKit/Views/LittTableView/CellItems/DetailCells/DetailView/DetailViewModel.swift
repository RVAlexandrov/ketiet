//
//  DetailViewModel.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct DetailViewModel {
    let title: String
    let value: String
    let valueDiff: String?
    let valueDiffTrend: Trend?
}
