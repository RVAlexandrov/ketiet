//
//  DIshHeaderCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 08.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class DishHeaderCellItem {
    
    var mealString: String
    var dishString: String?
    var weightString: String
    
    init(mealString: String,
         dishString: String?,
         weightString: String) {
        self.mealString = mealString
        self.dishString = dishString
        self.weightString = weightString
    }
}

extension DishHeaderCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? DishHeaderCell else { return }
        cell.mealLabel.text = mealString
        if let dishString = dishString {
            cell.dishLabel.text = dishString
            cell.dishLabel.isHidden = false
        } else {
            cell.dishLabel.isHidden = true
        }
        cell.backgroundColor = .clear
        cell.weightLabel.text = weightString
    }
    
    func viewClass() -> AnyClass {
        DishHeaderCell.self
    }
}
