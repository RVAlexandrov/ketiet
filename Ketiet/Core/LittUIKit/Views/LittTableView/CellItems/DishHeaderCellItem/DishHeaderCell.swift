//
//  DishHeaderCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 08.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class DishHeaderCell: UITableViewCell {
    
    let mealLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .title
        label.numberOfLines = 0
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    let dishLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .subTitle
        label.numberOfLines = 0
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    let weightLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        label.textColor = .systemGray
        return label
    }()
    
    private let separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .systemGray
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private methods
extension DishHeaderCell {
    private func configureCell() {
        selectionStyle = .none

        contentView.addSubviews([mealLabel, separatorView, dishLabel, weightLabel])
        
        NSLayoutConstraint.activate([
            mealLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            mealLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            mealLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            separatorView.topAnchor.constraint(equalTo: mealLabel.bottomAnchor, constant: .mediumLittMargin),
            separatorView.heightAnchor.constraint(equalToConstant: 0.33),
            separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            dishLabel.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: .largeLittMargin),
            dishLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            dishLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            weightLabel.topAnchor.constraint(equalTo: dishLabel.bottomAnchor, constant: .mediumLittMargin),
            weightLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            weightLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            weightLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
