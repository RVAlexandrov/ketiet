//
//  HiddenElementsLinkTableViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class HiddenElementsLinkTableViewCell: UITableViewCell {
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .littSecondaryBackgroundColor
        view.layer.cornerRadius = .littBasicCornerRadius
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.font = .body
        return label
    }()
    
    let button: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = .bodySemibold
        button.tintColor = .accentColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.configuration?.contentInsets = .zero
        button.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureCell() {
        contentView.addSubview(containerView)
        backgroundColor = .littBackgroundColor
        containerView.addSubviews([titleLabel, descriptionLabel, button])
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .littMediumWellMargin),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.logicBlockLittMargin),
            
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .mediumLittMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.logicBlockLittMargin),
            
            button.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: .mediumLittMargin),
            button.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            button.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -.mediumLittMargin)
        ])
    }
}
