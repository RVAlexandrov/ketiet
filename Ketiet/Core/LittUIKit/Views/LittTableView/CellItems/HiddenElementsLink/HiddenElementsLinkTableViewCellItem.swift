//
//  HiddenElementsLinkTableViewCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class HiddenElementsLinkTableViewCellItem {
    private let dishRecipe: DishRecipe
    private let buttonModel: ButtonModel
    
    init(dishRecipe: DishRecipe,
         buttonModel: ButtonModel) {
        self.dishRecipe = dishRecipe
        self.buttonModel = buttonModel
    }
}

extension HiddenElementsLinkTableViewCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? HiddenElementsLinkTableViewCell,
              let cookingSteps = dishRecipe.cookingSteps,
              !cookingSteps.isEmpty,
              let firstStep = cookingSteps.first
        else { return }
        
        cell.titleLabel.text = firstStep.title
        cell.descriptionLabel.text = firstStep.description
        cell.button.setTitle(buttonModel.title, for: .normal)
        cell.button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    func viewClass() -> AnyClass {
        return HiddenElementsLinkTableViewCell.self
    }
    
    func didSelect() {
        buttonModel.action?()
    }
    
    @objc
    private func didTapButton() {
        buttonModel.action?()
    }
}
