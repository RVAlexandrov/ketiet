//
//  BMICell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.12.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

class IconDescriptionCell: UITableViewCell {
    
    private lazy var iconView = SquareIconView()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .bodySemibold
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .smallBody
        label.textColor = .systemGray
        label.numberOfLines = 0
        return label
    }()
    
    var viewModel: ViewModel? {
        didSet {
            guard let viewModel = viewModel,
            viewModel != oldValue else { return }
            
            iconView.iconImageType = viewModel.systemImageString
            iconView.backgroundColor = viewModel.imageColor ?? .systemBlue
            titleLabel.text = viewModel.title
            descriptionLabel.text = viewModel.description
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let viewModel = viewModel else { return }
        
        let layout = Layout(viewModel: viewModel,
                            width: bounds.width)
        zip([iconView, titleLabel,descriptionLabel],
            layout.itemRects)
            .forEach { (view, frame) in
                view.frame = frame
            }
    }
    
    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?) {
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        contentView.addSubviews([iconView,
                                 titleLabel,
                                 descriptionLabel])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Layout
extension IconDescriptionCell {
    struct Layout {
        let viewModel: ViewModel
        let width: CGFloat
        
        var itemRects: [CGRect] {
            var rects: [CGRect] = []
            
            let iconImageFrame = CGRect(
                x: CGFloat.systemCellSpacing,
                y: CGFloat.largeLittMargin,
                width: 28,
                height: 28
            )
            rects.append(iconImageFrame)
            
            let titleWidth = width - CGFloat.largeLittMargin - iconImageFrame.maxX - CGFloat.largeLittMargin
            let titleHeight = viewModel
                .title
                .height(
                    withWidth: titleWidth,
                    font: .bodySemibold
                )
            let titleFrame = CGRect(
                x: iconImageFrame.maxX + CGFloat.largeLittMargin,
                y: CGFloat.largeLittMargin,
                width: titleWidth,
                height: titleHeight
            )
            rects.append(titleFrame)
            
            let descriptionHeight = viewModel
                .description
                .height(
                    withWidth: titleWidth,
                    font: .smallBody
                )
            let descriptionFrame = CGRect(
                x: iconImageFrame.maxX + CGFloat.largeLittMargin,
                y: titleFrame.maxY + CGFloat.smallLittMargin,
                width: titleWidth,
                height: descriptionHeight
            )
            rects.append(descriptionFrame)
            
            return rects
        }
        
        func height(itemRects: [CGRect]) -> CGFloat {
            max(itemRects.last?.maxY ?? 0, itemRects.first?.maxY ?? 0) + CGFloat.littScreenEdgeMargin
        }
    }
}

// MARK: - ViewModel
extension IconDescriptionCell {
    struct ViewModel: Equatable {
        let systemImageString: ImageStringType
        let imageColor: UIColor?
        let title: String
        let description: String
    }
}
