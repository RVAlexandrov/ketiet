//
//  BMICellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.12.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

protocol IconDecriptionCellItemDelegate: AnyObject {
    func didSelectIconDescriptionCellItem(item: IconDescriptionCellItem)
}

final class IconDescriptionCellItem {
            
    var viewModel: IconDescriptionCell.ViewModel? {
        didSet {
            guard let cell = cell else { return }
            configure(view: cell)
        }
    }
    
    weak var delegate: IconDecriptionCellItemDelegate?
    
    private weak var cell: IconDescriptionCell?
        
    init(viewModel: IconDescriptionCell.ViewModel) {
        self.viewModel = viewModel
    }
}

// MARK: - LittTableViewCellItemProtocol
extension IconDescriptionCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? IconDescriptionCell else { return }
        cell.viewModel = viewModel
    }
    
    func viewClass() -> AnyClass {
        IconDescriptionCell.self
    }
    
    func height(for boundingRect: CGRect) -> CGFloat {
        guard let viewModel = viewModel else { return 0 }
        let layout = IconDescriptionCell.Layout(viewModel: viewModel,
                                                width: boundingRect.width)
        return layout.height(itemRects: layout.itemRects)
    }
    
    func didSelect() {
        delegate?.didSelectIconDescriptionCellItem(item: self)
    }
}
