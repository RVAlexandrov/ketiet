//
//  File.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit
import AlamofireImage

final class ImageCellItem {
    private let imageHeight: CGFloat
    private let imageWrapper: ImageStringType
    
    init(imageHeight: CGFloat = 300,
         imageWrapper: ImageStringType) {
        self.imageHeight = imageHeight
        self.imageWrapper = imageWrapper
    }
}

// MARK: - LittTableViewItemProtocol
extension ImageCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? ImageCell else { return }
        switch imageWrapper {
        case .asset(let asset):
            cell.mainImageView.image = UIImage(named: asset)
        case .url(let stringUrl):
            cell.mainImageView.showShimmer()
            if let imageURL = URL(string: stringUrl) {
                cell.mainImageView.showShimmer()
                cell.mainImageView.af.setImage(withURL: imageURL,
                                               filter: RoundedCornersFilter(radius: .littMediumCornerRadius),
                                               imageTransition: .crossDissolve(0.3),
                                               runImageTransitionIfCached: false) { _ in
                    cell.mainImageView.hideShimmer()
                }
            }
        case .system(let systemImageString):
            cell.mainImageView.image = UIImage(systemName: systemImageString)
        }
        cell.heightConstraint.constant = imageHeight
    }
    
    func viewClass() -> AnyClass {
        ImageCell.self
    }
}
