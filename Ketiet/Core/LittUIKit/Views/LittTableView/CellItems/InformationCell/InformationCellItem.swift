//
//  InformationCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 13.12.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class InformationCellItem {
    private let title: String
    private let systemImageString: String?
    private let imageColor: UIColor?
    
    init(title: String,
         systemImageString: String? = nil,
         imageColor: UIColor? = nil) {
        self.title = title
        self.systemImageString = systemImageString
        self.imageColor = imageColor
    }
}

// MARK: - LittTableViewCellItemProtocol
extension InformationCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        var conf = UIListContentConfiguration.cell()
        conf.text = title
        if let imageString = systemImageString {
            conf.image = UIImage(systemName: imageString)
            conf.imageProperties.tintColor = imageColor ?? .accentColor
        }
        
        view.contentConfiguration = conf
        view.selectionStyle = .none
    }
    
    func viewClass() -> AnyClass {
        UITableViewCell.self
    }
}
