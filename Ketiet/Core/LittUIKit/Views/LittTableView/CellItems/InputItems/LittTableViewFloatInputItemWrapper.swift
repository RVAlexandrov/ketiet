//
//  LittTableViewFloatInputItemWrapper.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class LittTableViewFloatInputItemWrapper {
	private let wrapped: LittTableViewInputCellItem
	private lazy var formatter = NumberFormatter()
	
	init(wrapped: LittTableViewInputCellItem) {
		self.wrapped = wrapped
	}
}

extension LittTableViewFloatInputItemWrapper: LittTableViewInputCellItem {
	var isValid: Bool {
		wrapped.isValid
	}
	
	var value: LittValue? {
		switch wrapped.value {
		case .int, .date:
			return .none
		case let .float(value):
			return .float(value)
		case .none:
			return .none
		case let .text(value):
			if let float = formatter.numberFrom(value)?.floatValue {
				return .float(float)
			}
			return .none
		}
	}
	
	func configure(view: UITableViewCell) {
		wrapped.configure(view: view)
	}
	
	func viewClass() -> AnyClass {
		wrapped.viewClass()
	}
}
