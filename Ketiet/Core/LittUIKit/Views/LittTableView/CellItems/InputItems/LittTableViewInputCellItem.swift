//
//  LittTableViewInputCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 10.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol LittTableViewInputCellItem: LittTableViewCellItemProtocol {
	var isValid: Bool { get }
	var value: LittValue? { get }
}
