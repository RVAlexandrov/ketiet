//
//  LittTableViewIntInputItemWrapper.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class LittTableViewIntInputItemWrapper {
	private let wrapped: LittTableViewInputCellItem
	private lazy var formatter = NumberFormatter()
	
	init(wrapped: LittTableViewInputCellItem) {
		self.wrapped = wrapped
	}
}

extension LittTableViewIntInputItemWrapper: LittTableViewInputCellItem {
	var isValid: Bool {
		wrapped.isValid
	}
	
	var value: LittValue? {
		switch wrapped.value {
		case let .float(value):
			return .int(Int(value))
		case .none, .date:
			return .none
		case let .text(value):
			if let float = formatter.numberFrom(value)?.intValue {
				return .int(float)
			}
			return .none
		case let .int(value):
			return .int(value)
		}
	}
	
	func configure(view: UITableViewCell) {
		wrapped.configure(view: view)
	}
	
	func viewClass() -> AnyClass {
		wrapped.viewClass()
	}
}

