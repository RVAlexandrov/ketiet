//
//  LittTableViewSelectorInputCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.01.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class LittTableViewSelectorInputCellItem: NSObject {
	
	private let item: LittTableViewTextInputCellItem
	private weak var cell: LittTableViewTextInputCell?
	private let selectorOptions: [String]
	private lazy var inputView: UIPickerView = {
		let picker = UIPickerView()
		picker.translatesAutoresizingMaskIntoConstraints = false
		picker.delegate = self
		picker.dataSource = self
		return picker
	}()
    private lazy var inputAccessoryView: UIView = LittTextFieldView.configureDoneAccessoryView()
	
	init(item: LittTableViewTextInputCellItem, selectorOptions: [String]) {
		self.item = item
		self.selectorOptions = selectorOptions
		super.init()
	}
}

extension LittTableViewSelectorInputCellItem: UIPickerViewDataSource {
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		selectorOptions.count
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		selectorOptions[row]
	}
}

extension LittTableViewSelectorInputCellItem: UIPickerViewDelegate {
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		guard let cell = cell else { return }
		cell.textInput.textField.text = selectorOptions[row]
	}
}

extension LittTableViewSelectorInputCellItem: LittTableViewInputCellItem {
	var isValid: Bool {
		item.isValid
	}
	
	var value: LittValue? {
		item.value
	}
}

extension LittTableViewSelectorInputCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		item.configure(view: view)
		guard let cell = view as? LittTableViewTextInputCell else { return }
		cell.textInput.textField.inputView = inputView
		cell.textInput.textField.inputAccessoryView = inputAccessoryView
		self.cell = cell
	}
	
	func viewClass() -> AnyClass {
		item.viewClass()
	}
}
