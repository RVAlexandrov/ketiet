//
//  LittTableViewTextInputCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 10.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class LittTableViewTextInputCell: UITableViewCell {
	
	let textInput = LittTextFieldView()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
		contentView.addSubview(textInput)
		contentView.backgroundColor = .littBackgroundColor
        textInput.pinToSuperview(insets: .zero)
	}
}
