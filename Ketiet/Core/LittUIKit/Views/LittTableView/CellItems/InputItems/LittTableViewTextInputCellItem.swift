//
//  LittTableViewTextInputItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 10.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class LittTableViewTextInputCellItem {
	private let title: String
	private let validator: Validator
	private let keyboardType: UIKeyboardType
	private let backgroundColor: UIColor
	private let updateHandler: () -> Void
	private weak var tableView: LittTableView?
	private(set) var text: String?
	private(set) var isValid = false
	
	init(title: String,
		 validator: Validator,
		 keyboardType: UIKeyboardType,
		 backgroundColor: UIColor,
		 tableView: LittTableView,
		 text: String? = nil,
		 updateHandler: @escaping() -> Void) {
		self.tableView = tableView
		self.title = title
		self.validator = validator
		self.keyboardType = keyboardType
		self.backgroundColor = backgroundColor
		self.text = text
		self.updateHandler = updateHandler
	}
}

extension LittTableViewTextInputCellItem: LittTableViewInputCellItem {
	var value: LittValue? {
		if let text = text {
			return .text(text)
		}
		return .none
	}
}

extension LittTableViewTextInputCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? LittTableViewTextInputCell else { return }
		cell.textInput.set(title: title,
						   validator: validator,
						   keyboardType: keyboardType,
						   textFieldBackgroundColor: backgroundColor,
						   delegate: self)
		cell.textInput.textField.text = text
		if isValid != cell.textInput.isValid || text != nil {
			cell.textInput.forceValidate(animated: false)
			isValid = cell.textInput.isValid
		}
		cell.selectionStyle = .none
	}
	
	func viewClass() -> AnyClass {
		LittTableViewTextInputCell.self
	}
}

extension LittTableViewTextInputCellItem: LittTextFieldViewDelegate {
	
	func textFieldDidChange(view: LittTextFieldView) {
		text = view.textField.text
		isValid = view.isValid
		updateHandler()
	}
	
	func didUpdateErrorState(view: LittTextFieldView) {
		tableView?.endUpdates()
	}
	
	func willUpdateErrorState(view: LittTextFieldView) {
		tableView?.beginUpdates()
	}
}
