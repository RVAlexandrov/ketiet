//
//  LittTableViewTimeInputCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class LittTableViewTimeInputCellItem {
	private let item: LittTableViewTextInputCellItem
	private weak var cell: LittTableViewTextInputCell?
    
	private lazy var inputView: UIDatePicker = {
		let picker = UIDatePicker()
		picker.translatesAutoresizingMaskIntoConstraints = false
		picker.datePickerMode = .time
        picker.preferredDatePickerStyle = .wheels
		picker.addTarget(self, action: #selector(updateItemValue), for: .valueChanged)
		return picker
	}()
    
    private lazy var inputAccessoryView = LittTextFieldView.configureDoneAccessoryView()
    
	private lazy var dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
        formatter.timeStyle = .short
		return formatter
	}()
	
    init(
        item: LittTableViewTextInputCellItem,
        initialDate: Date?
    ) {
		self.item = item
        if let date = initialDate {
            inputView.date = date
        }
	}
	
	@objc private func updateItemValue() {
		guard let cell = cell else { return }
		cell.textInput.textField.text = dateFormatter.string(from: inputView.date)
		cell.textInput.forceValidate(animated: true)
		item.textFieldDidChange(view: cell.textInput)
	}
}

extension LittTableViewTimeInputCellItem: LittTableViewInputCellItem {
	var isValid: Bool {
		item.isValid
	}
	
	var value: LittValue? {
		switch item.value {
		case .text:
			return .date(inputView.date)
		default:
            return nil
		}
	}
}

extension LittTableViewTimeInputCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		item.configure(view: view)
		guard let cell = view as? LittTableViewTextInputCell else { return }
		cell.textInput.textField.inputView = inputView
		cell.textInput.textField.inputAccessoryView = inputAccessoryView
		self.cell = cell
        updateItemValue()
	}
	
	func viewClass() -> AnyClass {
		item.viewClass()
	}
}
