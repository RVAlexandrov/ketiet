//
//  LittValue.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

enum LittValue {
	case text(String)
	case float(Float)
	case int(Int)
	case date(Date)
	
	var float: Float? {
		switch self {
		case let .float(value):
			return value
		default:
			return nil
		}
	}
	
	var int: Int? {
		switch self {
		case let .int(value):
			return value
		default:
			return nil
		}
	}
	
	var text: String? {
		switch self {
		case let .text(value):
			return value
		default:
			return nil
		}
	}
	
	var date: Date? {
		switch self {
		case let .date(value):
			return value
		default:
			return nil
		}
	}
}
