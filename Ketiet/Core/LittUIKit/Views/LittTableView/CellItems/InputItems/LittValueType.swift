//
//  LittValueType.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

enum LittValueType {
	case text
	case float
	case int
	case time
	case select([String])
}
