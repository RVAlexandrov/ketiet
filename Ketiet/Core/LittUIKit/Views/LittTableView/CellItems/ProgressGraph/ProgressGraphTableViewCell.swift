//
//  WaterProgressGraphTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProgressGraphTableViewCell: UITableViewCell {
	
	struct Constants {
		static let chartViewHeight: CGFloat = 350
	}
	
	private(set) lazy var chartView: LineChart = {
		let chartView = LineChart()
		chartView.translatesAutoresizingMaskIntoConstraints = false
		return chartView
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
		contentView.addSubview(chartView)
        backgroundColor = .littBackgroundColor
		chartView.heightAnchor.constraint(equalToConstant: Constants.chartViewHeight).isActive = true
		chartView.pinToSuperview(insets: UIEdgeInsets(top: 0,
													  left: .zero,
													  bottom: .largeLittMargin,
													  right: .zero))
	}
}
