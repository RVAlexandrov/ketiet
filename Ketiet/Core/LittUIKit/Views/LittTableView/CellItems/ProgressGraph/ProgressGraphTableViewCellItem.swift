//
//  ProgressGraphTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class ProgressGraphTableViewCellItem {
	
	private let service: ProgressServiceProtocol
    private let accentColor: UIColor
	private lazy var values = service.getValuesDueToday()
	private weak var cell: ProgressGraphTableViewCell?
	
	init(
        service: ProgressServiceProtocol,
        accentColor: UIColor
    ) {
		self.service = service
        self.accentColor = accentColor
	}
	
	func reloadData() {
		guard let cell = cell else { return }
		values = service.getValuesDueToday()
		cell.chartView.setup(with: values ?? [], lineColor: accentColor)
	}
}

extension ProgressGraphTableViewCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? ProgressGraphTableViewCell else { return }
		self.cell = cell
		cell.selectionStyle = .none
		cell.chartView.setup(with: values ?? [], lineColor: accentColor)
	}
	
	func viewClass() -> AnyClass {
		ProgressGraphTableViewCell.self
	}
}
