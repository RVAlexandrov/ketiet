//
//  DietProgressCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 06.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProgressCellItem {
	
	private let dietService: DietServiceProtocol
	private var description: String?
	private var progress: Float = 0
	private weak var cell: ProgressTableViewCell?
	private var observerToken: NSObjectProtocol?
	
	init(dietService: DietServiceProtocol) {
		self.dietService = dietService
		dietService.requestCurrentDiet { [weak self] _ in
			self?.updateCell()
		}
		observerToken = NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
															   object: nil,
															   queue: .main, using: { [weak self] _ in
			self?.updateCell()
		})
	}
	
	private func setCurrentDiet() {
		if let diet = dietService.getCurrentDiet(), let currentDay = dietService.getCurrentDayIndex() {
			description = "DAY".localized() + " \(currentDay + 1) / \(diet.days.count)"
			progress = Float(currentDay + 1) / Float(diet.days.count)
		} else {
			progress = 0
			description = "TAP_TO_SEE_DETAILS".localized()
		}
	}
	
	func updateCell() {
		setCurrentDiet()
		if let cell = cell {
			configure(view: cell)
		}
	}
}

extension ProgressCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? ProgressTableViewCell else { return }
		self.cell = cell
        cell.selectionStyle = .none
		cell.descriptionLabel.text = description
		cell.progressView.progress = progress
		cell.progressView.isHidden = progress == 0
	}
	
	func viewClass() -> AnyClass {
		ProgressTableViewCell.self
	}
}
