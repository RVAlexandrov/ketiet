//
//  DietProgressTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 06.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProgressTableViewCell: UITableViewCell {
	
	private struct Constants {
		static let progressHeight: CGFloat = 15
        static let cornerRadius: CGFloat = .littBasicCornerRadius
	}
	
	let progressView: UIProgressView = {
		let progress = UIProgressView(progressViewStyle: .default)
		progress.translatesAutoresizingMaskIntoConstraints = false
		progress.progressTintColor = .accentColor
		progress.clipsToBounds = true
		progress.layer.cornerRadius = Constants.progressHeight / 2
		return progress
	}()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        return label
    }()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
    private func setupSubviews() {
        selectionStyle = .default
        
        contentView.addSubviews([descriptionLabel, progressView])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: .largeLittMargin
            ),
            descriptionLabel.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: .largeLittMargin
            ),
            descriptionLabel.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: -.largeLittMargin
            ),
            progressView.topAnchor.constraint(
                equalTo: descriptionLabel.bottomAnchor,
                constant: .largeLittMargin
            ),
            progressView.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: .largeLittMargin
            ),
            progressView.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: -.largeLittMargin
            ),
            progressView.bottomAnchor.constraint(
                equalTo: contentView.bottomAnchor,
                constant: -.largeLittMargin
            ),
            progressView.heightAnchor.constraint(
                equalToConstant: Constants.progressHeight
            )
        ])
    }
}
