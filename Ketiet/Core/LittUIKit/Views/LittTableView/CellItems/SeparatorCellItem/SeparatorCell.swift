//
//  SeparatorCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class SeparatorCell: UITableViewCell {
    
    private var insets: UIEdgeInsets = .zero
    
    let fictionView: UIView = {
        let separatorView = UIView()
        separatorView.backgroundColor = .clear
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        return separatorView
    }()
    
    let separatorView: UIView = {
        let separatorView = UIView()
        separatorView.backgroundColor = .systemGray2
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        return separatorView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
    }
    
    lazy var heightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: fictionView,
                                                                       attribute: .height,
                                                                       relatedBy: .equal,
                                                                       toItem: nil,
                                                                       attribute: .notAnAttribute,
                                                                       multiplier: 1,
                                                                       constant: 0)
    
    override func layoutSubviews() {
        super.layoutSubviews()
        separatorView.frame = CGRect(x: insets.left,
                                     y: insets.top,
                                     width: contentView.frame.width - insets.left - insets.right,
                                     height: 0.5)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setSeparatorMargins(insets: UIEdgeInsets) {
        self.insets = insets
        heightConstraint.constant = insets.top + insets.bottom
    }
}

extension SeparatorCell {
    private func configureCell() {
        selectionStyle = .none
        backgroundColor = .clear
        
        contentView.addSubview(fictionView)
        
        fictionView.addSubview(separatorView)
        
        NSLayoutConstraint.activate([
            fictionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            fictionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            fictionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            fictionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            heightConstraint
        ])
    }
}
