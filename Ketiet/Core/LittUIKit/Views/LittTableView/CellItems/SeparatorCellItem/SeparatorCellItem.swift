//
//  SeparatorItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class SeparatorCellItem {
    
    private let separatorColor: UIColor
    private let separatorInsets: UIEdgeInsets
    
    init(separatorColor: UIColor = .systemGray2,
         separatorInsets: UIEdgeInsets = UIEdgeInsets(top: .largeLittMargin,
                                                      left: .largeLittMargin,
                                                      bottom: 0,
                                                      right: .largeLittMargin)) {
        self.separatorColor = separatorColor
        self.separatorInsets = separatorInsets
    }
}

// MARK: - LittTableViewCellItemProtocol
extension SeparatorCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? SeparatorCell else { return }
        cell.separatorView.backgroundColor = separatorColor
        cell.setSeparatorMargins(insets: separatorInsets)
    }
    
    func viewClass() -> AnyClass {
        SeparatorCell.self
    }
}
