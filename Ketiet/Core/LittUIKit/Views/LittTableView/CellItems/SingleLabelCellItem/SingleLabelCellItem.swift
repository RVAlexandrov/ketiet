//
//  LegendHeaderItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class SingleLabelCellItem {
	
	private let text: String
	private let lineBreakMode: NSLineBreakMode
	private let textAlignment: NSTextAlignment
	private let font: UIFont
	private let textColor: UIColor
	private let backgroundColor: UIColor?
	
	init(text: String,
		 lineBreakMode: NSLineBreakMode = .byWordWrapping,
		 textAlignment: NSTextAlignment = .center,
		 font: UIFont = .title,
		 textColor: UIColor = .label,
         backgroundColor: UIColor? = .clear) {
		self.text = text
		self.lineBreakMode = lineBreakMode
		self.textAlignment = textAlignment
		self.font = font
		self.textColor = textColor
		self.backgroundColor = backgroundColor
	}
}

extension SingleLabelCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
        guard let cell = view as? SingleLabelCell else { return }
        cell.label.font = font
        cell.label.textAlignment = textAlignment
        cell.label.lineBreakMode = lineBreakMode
        cell.label.textColor = textColor
        cell.label.numberOfLines = 0
        cell.label.text = text
		if backgroundColor != nil {
			view.contentView.backgroundColor = backgroundColor
			view.backgroundColor = backgroundColor
		}
	}
	
	func viewClass() -> AnyClass {
		return SingleLabelCell.self
	}
}
