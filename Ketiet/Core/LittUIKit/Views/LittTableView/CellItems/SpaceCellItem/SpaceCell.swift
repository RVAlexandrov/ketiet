//
//  SpaceCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class SpaceCell: UITableViewCell {
    
    let spaceView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var heightConstraint: NSLayoutConstraint = NSLayoutConstraint.init(item: spaceView,
                                                                            attribute: .height,
                                                                            relatedBy: .equal,
                                                                            toItem: nil,
                                                                            attribute: .notAnAttribute,
                                                                            multiplier: 1,
                                                                            constant: 0)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureCell() {
        contentView.addSubview(spaceView)
        
        backgroundColor = .clear
        
        NSLayoutConstraint.activate([
            spaceView.topAnchor.constraint(equalTo: contentView.topAnchor),
            spaceView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            spaceView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            spaceView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            heightConstraint
        ])
    }
}
