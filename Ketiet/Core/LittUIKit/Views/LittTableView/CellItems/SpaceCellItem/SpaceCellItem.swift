//
//  SpaceCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class SpaceCellItem {
    private let spaceHeight: CGFloat
    
    init(spaceHeight: CGFloat) {
        self.spaceHeight = spaceHeight
    }
}

// MARK: - LittTableViewItemProtocol
extension SpaceCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? SpaceCell else { return }
        cell.heightConstraint.constant = spaceHeight
    }
    
    func viewClass() -> AnyClass {
        SpaceCell.self
    }
}
