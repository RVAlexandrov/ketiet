//
//  ValueDisplayingTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 19.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ValueDisplayingTableViewCellItem {
    
    enum Style {
        case basic
        case inputValue
    }
	
    private let title: String
    private let titleColor: UIColor
    private var value: String
    private var valueColor: UIColor
    private let style: Style
    private let imageSystemString: String?
    private let imageColor: UIColor?
    private let cellColor: UIColor
    private let selectionHandler: (() -> Void)?
    private weak var cell: UITableViewCell?
	
    init(title: String,
         titleColor: UIColor = .label,
         value: String,
         valueColor: UIColor = .secondaryLabel,
         style: Style = .basic,
         imageSystemString: String? = nil,
         imageColor: UIColor? = nil,
         cellColor: UIColor = .littSecondaryBackgroundColor,
         selectionHandler: (() -> Void)? = nil) {
        self.title = title
        self.titleColor = titleColor
        self.value = value
        self.valueColor = valueColor
        self.style = style
        self.imageSystemString = imageSystemString
        self.imageColor = imageColor
        self.cellColor = cellColor
        self.selectionHandler = selectionHandler
    }
}

// MARK: - LittTableViewCellItemProtocol
extension ValueDisplayingTableViewCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
        switch style {
        case .basic:
            configureBasicCell(view: view)
        case .inputValue:
            configureInputValueCell(view: view)
        }
	}
	
    func viewClass() -> AnyClass {
        UITableViewCell.self
    }
    
    func didSelect() {
        selectionHandler?()
    }
    
    func reloadValue(newValue: String) {
        value = newValue
        var conf = UIListContentConfiguration.valueCell()
        conf.secondaryText = newValue
        conf.text = title
        conf.textProperties.color = titleColor
        conf.secondaryTextProperties.color = valueColor
        cell?.contentConfiguration = conf
    }
}

extension ValueDisplayingTableViewCellItem {
    private func configureBasicCell(view: UITableViewCell) {
        var conf = UIListContentConfiguration.valueCell()
        conf.secondaryText = value
        conf.secondaryTextProperties.color = valueColor
        conf.text = title
        conf.textProperties.color = titleColor
        if let imageString = imageSystemString {
            conf.image = UIImage(systemName: imageString)
            conf.imageProperties.tintColor = imageColor
        }
        view.contentConfiguration = conf
        view.selectionStyle = selectionHandler == nil ? .none : .default
        
        self.cell = view
    }
    
    private func configureInputValueCell(view: UITableViewCell) {
        var conf = UIListContentConfiguration.valueCell()
        conf.secondaryText = value
        conf.text = title
        conf.textProperties.color = titleColor
        conf.secondaryTextProperties.color = .systemBlue
        if let imageString = imageSystemString {
            conf.image = UIImage(systemName: imageString)
            conf.imageProperties.tintColor = imageColor
        }
        view.contentConfiguration = conf
        view.selectionStyle = .default
        
        self.cell = view
    }
    
    func height(for boundingRect: CGRect) -> CGFloat {
        switch style {
        case .basic:
            return UITableView.automaticDimension
        case .inputValue:
            return 50.0
        }
    }
}
