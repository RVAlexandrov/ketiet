//
//  BasicFooterItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

class BasicFooterItem {
    let text: String
    
    init(text: String) {
        self.text = text
    }
}

extension BasicFooterItem: LittTableViewHeaderFooterProtocol {
    func configure(view: UITableViewHeaderFooterView) {
        var configuration = UIListContentConfiguration.groupedFooter()
        configuration.text = text
        view.contentConfiguration = configuration
    }
    
    func viewClass() -> AnyClass {
        UITableViewHeaderFooterView.self
    }
}
