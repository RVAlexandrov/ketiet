//
//  LinkFooterItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 31.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

class LinkFooterItem {
    let text: String
    let linkURLString: String
    
    init(text: String, linkURLString: String) {
        self.text = text
        self.linkURLString = linkURLString
    }
}

extension LinkFooterItem: LittTableViewHeaderFooterProtocol {
    func configure(view: UITableViewHeaderFooterView) {
        guard let view = view as? LinkFooterItemView else { return }
        view.button.setTitle(text, for: .normal)
        view.button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    func viewClass() -> AnyClass {
        LinkFooterItemView.self
    }
    
    @objc
    private func didTapButton() {
        if let link = URL(string: linkURLString) {
            UIApplication.shared.open(link)
        }
    }
}
