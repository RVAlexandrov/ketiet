//
//  LinkFooterTableViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 31.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

class LinkFooterItemView: UITableViewHeaderFooterView {
    
    lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        button.titleLabel?.textAlignment = .left
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        configureView()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LinkFooterItemView {
    private func configureView() {
        addSubview(button)
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 2 * .systemCellSpacing),
            button.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -2 * .systemCellSpacing),
            button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2),
        ])
    }
}
