//
//  HeaderViewItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class HeaderViewItem {
    
    private let text: String
    private let lineBreakMode: NSLineBreakMode
    private let textAlignment: UIListContentConfiguration.TextProperties.TextAlignment
    private let font: UIFont
    private let textColor: UIColor
    
    init(
        text: String,
        lineBreakMode: NSLineBreakMode = .byWordWrapping,
        textAlignment: UIListContentConfiguration.TextProperties.TextAlignment = .natural,
        font: UIFont = .subTitle,
        textColor: UIColor = .label
    ) {
        self.text = text
        self.lineBreakMode = lineBreakMode
        self.textAlignment = textAlignment
        self.font = font
        self.textColor = textColor
    }
}

// MARK: - LittTableViewHeaderFooterProtocol
extension HeaderViewItem: LittTableViewHeaderFooterProtocol {
    func configure(view: UITableViewHeaderFooterView) {
        var configuration = view.defaultContentConfiguration()
        configuration.text = text
//        listGroupedHeaderFooter
        configuration.textProperties.alignment = textAlignment
        configuration.textProperties.lineBreakMode = lineBreakMode
        configuration.textProperties.color = textColor
        configuration.textProperties.numberOfLines = 0
        configuration.textProperties.font = font
        configuration.textProperties.transform = .none
        view.contentConfiguration = configuration
    }
    
    func viewClass() -> AnyClass {
        UITableViewHeaderFooterView.self
    }
}
