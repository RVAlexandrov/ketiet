//
//  PlainHeaderItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 03.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class PlainHeaderItem {
    
    private let text: String
    private let font: UIFont
    private let textColor: UIColor
    private let backgroundColor: UIColor
    
    init(
        text: String,
        font: UIFont = .title,
        textColor: UIColor = .label,
        backgroundColor: UIColor = .littBackgroundColor
    ) {
        self.text = text
        self.font = font
        self.textColor = textColor
        self.backgroundColor = backgroundColor
    }
}

extension PlainHeaderItem: LittTableViewHeaderFooterProtocol {
    
    func configure(view: UITableViewHeaderFooterView) {
        var configuration = UIListContentConfiguration.prominentInsetGroupedHeader()
        configuration.text = text
        configuration.textProperties.font = font
        configuration.textProperties.color = textColor
    
        var backgroundConfiguration = UIBackgroundConfiguration.listGroupedHeaderFooter()
        backgroundConfiguration.backgroundColor = backgroundColor
        
        view.contentConfiguration = configuration
        view.backgroundConfiguration = backgroundConfiguration
    }
    
    func viewClass() -> AnyClass {
        UITableViewHeaderFooterView.self
    }
}
