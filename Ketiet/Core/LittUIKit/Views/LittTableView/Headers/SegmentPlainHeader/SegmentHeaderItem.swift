//
//  SegmentHeaderItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 30.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class SegmentHeaderItem {
    
    private let itemTitles: [String]
    private let text: String
    private let lineBreakMode: NSLineBreakMode
    private let textAlignment: NSTextAlignment
    private let font: UIFont
    private let textColor: UIColor
    private weak var view: SegmentHeaderView?
    
    private let didChangeValueHandler: (SegmentHeaderItem, Int) -> Void
    
    private var selectedSegment: Int = 0
    
    init(text: String,
         lineBreakMode: NSLineBreakMode = .byWordWrapping,
         textAlignment: NSTextAlignment = .left,
         font: UIFont = .title,
         textColor: UIColor = .label,
         itemTitles: [String],
         didChangeValueHandler: @escaping(SegmentHeaderItem, Int) -> Void) {
        self.text = text
        self.lineBreakMode = lineBreakMode
        self.textAlignment = textAlignment
        self.font = font
        self.textColor = textColor
        self.itemTitles = itemTitles
        self.didChangeValueHandler = didChangeValueHandler
    }
}

// MARK: - LittTableViewHeaderFooterProtocol
extension SegmentHeaderItem: LittTableViewHeaderFooterProtocol {
    func configure(view: UITableViewHeaderFooterView) {
        guard let view = view as? SegmentHeaderView else { return }
        self.view = view
        view.label.font = font
        view.label.textAlignment = textAlignment
        view.label.lineBreakMode = lineBreakMode
        view.label.textColor = textColor
        view.label.numberOfLines = 0
        view.label.text = text
        
        view.segmentControl.removeAllSegments()
        itemTitles.enumerated().forEach {
            view.segmentControl.insertSegment(withTitle: $0.element,
                                              at: $0.offset,
                                              animated: false)
        }
        view.segmentControl.addTarget(self,
                                      action: #selector(segmentSelectionChanged),
                                      for: .valueChanged)
        view.segmentControl.selectedSegmentIndex = selectedSegment
    }
    
    func viewClass() -> AnyClass {
        SegmentHeaderView.self
    }
    
    func height(for boundingRect: CGRect) -> CGFloat {
        let basicTableViewMargin: CGFloat = 20
        let textHeight = text.height(withWidth: UIScreen.main.bounds.width - 2 * basicTableViewMargin, font: font)
        return 55 + textHeight
    }
    
    @objc
    private func segmentSelectionChanged() {
        guard let view = view else { return }
        let segmentIndex = view.segmentControl.selectedSegmentIndex
        didChangeValueHandler(self, segmentIndex)
        selectedSegment = segmentIndex
        let generator = UISelectionFeedbackGenerator()
        generator.selectionChanged()
    }
}
