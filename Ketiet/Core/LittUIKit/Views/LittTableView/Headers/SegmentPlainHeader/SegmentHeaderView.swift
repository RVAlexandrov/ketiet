//
//  SegmentHeaderVew.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 30.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class SegmentHeaderView: UITableViewHeaderFooterView {
        
    let label: UILabel = {
        let label = UILabel()
        label.font = .body
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let segmentControl: UISegmentedControl = {
        let control = UISegmentedControl()
        control.translatesAutoresizingMaskIntoConstraints = false
        control.selectedSegmentIndex = 0
        return control
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SegmentHeaderView {
    
    private func configureView() {
        contentView.addSubviews([label, segmentControl])
        
        let heightSegmentControllHeight = segmentControl.heightAnchor.constraint(equalToConstant: 31)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            segmentControl.topAnchor.constraint(equalTo: label.bottomAnchor, constant: .mediumLittMargin),
            segmentControl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            segmentControl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            heightSegmentControllHeight
        ])
    }
}
