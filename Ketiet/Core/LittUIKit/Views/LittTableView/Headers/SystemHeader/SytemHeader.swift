//
//  SytemHeader.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 25.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

final class SystemHeaderItem {
    
    private let text: String
    
    init(text: String) {
        self.text = text
    }
}

// MARK: - LittTableViewHeaderFooterProtocol
extension SystemHeaderItem: LittTableViewHeaderFooterProtocol {
    
    func configure(view: UITableViewHeaderFooterView) {
        var configuration = UIListContentConfiguration.groupedHeader()
        configuration.text = text
        configuration.directionalLayoutMargins = .init(
            top: .mediumLittMargin,
            leading: .systemCellSpacing,
            bottom: .mediumLittMargin,
            trailing: .systemCellSpacing
        )
        
        view.contentConfiguration = configuration
    }
    
    func viewClass() -> AnyClass {
        UITableViewHeaderFooterView.self
    }
}
