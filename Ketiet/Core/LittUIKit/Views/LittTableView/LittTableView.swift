//
//  LittTableView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import ViewAnimator

final class LittTableView: UITableView {
	
	var shouldAutoReload = true
	
	var animations = [AnimationType.from(direction: .bottom, offset: 30)]
	
	weak var forwardingDelegate: UITableViewDelegate?
	
	var sections: [LittTableViewSectionProtocol] = [] {
		didSet {
			sections.forEach { $0.registerViews(to: self) }
			if !shouldAutoReload {
				return
			}
			reloadDataWithAnimation()
		}
	}
	
	var insetsChangeHelper: ScrollViewContentInsetsHelper?
	
	override init(frame: CGRect, style: UITableView.Style) {
		super.init(frame: frame, style: style)
		dataSource = self
		delegate = self
		rowHeight = UITableView.automaticDimension
		estimatedRowHeight = UITableView.automaticDimension
		sectionHeaderHeight = UITableView.automaticDimension
        estimatedSectionHeaderHeight = UITableView.automaticDimension
        sectionFooterHeight = UITableView.automaticDimension
        estimatedSectionFooterHeight = UITableView.automaticDimension
        sectionHeaderTopPadding = .logicBlockLittMargin
        contentInset.top = -.logicBlockLittMargin
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func reloadDataWithAnimation() {
		if animations.isEmpty {
			reloadData()
			return
		}
		UIView.animate(views: visibleCells,
					   animations: animations,
					   reversed: true,
					   initialAlpha: 1.0,
					   finalAlpha: 0.0) {
			self.reloadData()
			UIView.animate(views: self.visibleCells, animations: self.animations)
		}
	}
}

extension LittTableView: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return sections.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return sections[section].items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let item = sections[indexPath.section].items[indexPath.row]
		let cell = tableView.dequeueReusableCell(withIdentifier: item.viewIdentifier(), for: indexPath)
		item.configure(view: cell)
		return cell
	}
}

extension LittTableView: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
		sections[indexPath.section].items[indexPath.row].didSelect()
	}
	
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let horizontalInset: CGFloat = tableView.style == .insetGrouped ? 32 : 0
        return sections[indexPath.section].items[indexPath.row]
            .height(
                for: CGRect(
                    origin: .zero,
                    size: CGSize(
                        width: bounds.width - horizontalInset,
                        height: bounds.height)
                )
            )
    }
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		guard let header = sections[section].header,
			let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: header.viewIdentifier()) else { return nil }
		header.configure(view: view)
		return view
	}
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let footer = sections[section].footer,
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: footer.viewIdentifier()) else { return nil }
        footer.configure(view: view)
        return view
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let footer = sections[section].footer else { return 0 }
        let horizontalInset: CGFloat = tableView.style == .insetGrouped ? 32 : 0
        return footer
            .height(
                for: CGRect(
                    origin: .zero,
                    size: CGSize(
                        width: bounds.width - horizontalInset,
                        height: bounds.height)
                )
            )
    }
	
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let header = sections[section].header else { return 0 }
        let horizontalInset: CGFloat = tableView.style == .insetGrouped ? 32 : 0
        
        return header
            .height(
                for: CGRect(
                    origin: .zero,
                    size: CGSize(
                        width: bounds.width - horizontalInset,
                        height: bounds.height)
                )
            )
    }
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		forwardingDelegate?.tableView?(tableView, willDisplay: cell, forRowAt: indexPath)
	}
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		forwardingDelegate?.scrollViewWillEndDragging?(scrollView,
													   withVelocity: velocity,
													   targetContentOffset: targetContentOffset)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		forwardingDelegate?.scrollViewDidScroll?(scrollView)
	}
}
