//
//  LittTableViewCellItemProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol LittTableViewCellItemProtocol: LittTableViewItemProtocol {
	
	func configure(view: UITableViewCell)
	func didSelect()
}

extension LittTableViewCellItemProtocol {
	
	func didSelect() {}
}
