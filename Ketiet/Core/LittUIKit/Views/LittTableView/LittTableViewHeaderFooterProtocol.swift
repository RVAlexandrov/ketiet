//
//  LittTableViewHeaderFooterProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol LittTableViewHeaderFooterProtocol: LittTableViewItemProtocol {
	
	func configure(view: UITableViewHeaderFooterView)
}
