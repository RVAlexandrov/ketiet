//
//  LittTableViewItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol LittTableViewItemProtocol {
	
	func viewClass() -> AnyClass
	func height(for boundingRect: CGRect) -> CGFloat
}

extension LittTableViewItemProtocol {
	
	func viewIdentifier() -> String {
		String(describing: viewClass())
	}
	
	func height(for boundingRect: CGRect) -> CGFloat {
        UITableView.automaticDimension
	}
}
