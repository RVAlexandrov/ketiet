//
//  LittTableViewExpandableSection.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 12.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class LittTableViewExpandableSection: LittTableViewSection {
	
	private var expanded = false
	private let firstSectionItem: LittTableViewFirstExpandableSectionItem
	private let sectionIndex: Int
	private let originalItems: [LittTableViewCellItemProtocol]
	private weak var tableView: LittTableView?

	init(header: LittTableViewHeaderFooterProtocol? = nil,
		 firstSectionItem: LittTableViewFirstExpandableSectionItem,
		 sectionIndex: Int,
		 items: [LittTableViewCellItemProtocol],
		 footer: LittTableViewHeaderFooterProtocol? = nil,
		 tableView: LittTableView) {
		self.firstSectionItem = firstSectionItem
		self.sectionIndex = sectionIndex
		originalItems = items
		self.tableView = tableView
		super.init(header: header, items: [firstSectionItem], footer: footer)
		firstSectionItem.setTapHandler { [weak self] in
			self?.toggleExpaned()
		}
	}
	
	private func toggleExpaned() {
		expanded.toggle()
		items = expanded ? [firstSectionItem] + originalItems : [firstSectionItem]
		if let tableView = tableView {
			registerViews(to: tableView)
		}
		let indexPaths = originalItems.enumerated().map { IndexPath(row: $0.offset + 1, section: sectionIndex) }
		if expanded {
			tableView?.performBatchUpdates({ tableView?.insertRows(at: indexPaths, with: .top) })
		} else {
			tableView?.performBatchUpdates({ tableView?.deleteRows(at: indexPaths, with: .top) })
		}
	}
}


