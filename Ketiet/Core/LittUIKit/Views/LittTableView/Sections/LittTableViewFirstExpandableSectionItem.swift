//
//  LittTableViewFirstExpandableSectionCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 12.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol LittTableViewFirstExpandableSectionItem: LittTableViewCellItemProtocol {
	func setTapHandler(_ handler: @escaping() -> Void)
}
