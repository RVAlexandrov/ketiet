//
//  LittTableViewSection.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

class LittTableViewSection: LittTableViewSectionProtocol {
	
	let header: LittTableViewHeaderFooterProtocol?
	var items: [LittTableViewCellItemProtocol]
	let footer: LittTableViewHeaderFooterProtocol?
	
	init(header: LittTableViewHeaderFooterProtocol? = nil,
		 items: [LittTableViewCellItemProtocol] = [],
		 footer: LittTableViewHeaderFooterProtocol? = nil) {
		self.header = header
		self.items = items
		self.footer = footer
	}
}
