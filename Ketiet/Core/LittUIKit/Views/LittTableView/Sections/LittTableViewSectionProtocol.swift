//
//  File.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol LittTableViewSectionProtocol: AnyObject {
	var header: LittTableViewHeaderFooterProtocol? { get }
	var items: [LittTableViewCellItemProtocol] { get set }
	var footer: LittTableViewHeaderFooterProtocol? { get }
	func registerViews(to tableView: LittTableView)
}

extension LittTableViewSectionProtocol {
	
	var header: LittTableViewHeaderFooterProtocol? {
		return nil
	}
	
	var footer: LittTableViewHeaderFooterProtocol? {
		return nil
	}
	
	func registerViews(to tableView: LittTableView) {
		let headersFooters = [header, footer].compactMap { $0 }
		headersFooters.forEach { tableView.register($0.viewClass(), forHeaderFooterViewReuseIdentifier: $0.viewIdentifier()) }
		items.forEach { tableView.register($0.viewClass(), forCellReuseIdentifier: $0.viewIdentifier()) }
	}
}
