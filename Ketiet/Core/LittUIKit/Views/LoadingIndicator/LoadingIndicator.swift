//
//  LoadingIndicator.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class LoadingIndicator: UIView {
	
	private struct Constants {
		static let underlayingViewCornerRadius: CGFloat = 20
		static let stackViewSpacing: CGFloat = 10
		static let stackViewInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
	}
	
	private let activityIndicator: UIActivityIndicatorView = {
		let indicator = UIActivityIndicatorView(style: .large)
		indicator.translatesAutoresizingMaskIntoConstraints = false
		return indicator
	}()
	
	let label: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.textAlignment = .center
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 2
		label.font = .smallBody
		label.text = "LOADING".localized()
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		isHidden = true
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func startAnimating() {
		isHidden = false
		UIView.animate(withDuration: CATransaction.animationDuration(),
					   animations: {
			self.alpha = 1
		}) { _ in
			self.activityIndicator.startAnimating()
		}
	}
	
	func stopAnimating() {
		UIView.animate(withDuration: CATransaction.animationDuration(),
					   animations: {
			self.alpha = 0
		}) { _ in
			self.isHidden = true
			self.activityIndicator.stopAnimating()
		}
	}
	
	private func setupSubviews() {
		let underlayingView = UIView()
		underlayingView.backgroundColor = .littSecondaryBackgroundColor
		underlayingView.translatesAutoresizingMaskIntoConstraints = false
		underlayingView.clipsToBounds = true
		underlayingView.layer.cornerRadius = Constants.underlayingViewCornerRadius
		addSubview(underlayingView)
		let stackView = UIStackView(arrangedSubviews: [activityIndicator, label])
		stackView.axis = .vertical
		stackView.alignment = .center
		stackView.translatesAutoresizingMaskIntoConstraints = false
		stackView.spacing = Constants.stackViewSpacing
		underlayingView.addSubview(stackView)
		stackView.pinToSuperview(insets: Constants.stackViewInsets)
		NSLayoutConstraint.activate([
			underlayingView.widthAnchor.constraint(equalTo: activityIndicator.widthAnchor, multiplier: 2),
			underlayingView.heightAnchor.constraint(equalTo: underlayingView.widthAnchor),
			underlayingView.centerXAnchor.constraint(equalTo: centerXAnchor),
			underlayingView.centerYAnchor.constraint(equalTo: centerYAnchor)
		])
	}
}
