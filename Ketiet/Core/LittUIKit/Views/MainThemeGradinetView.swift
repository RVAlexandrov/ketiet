//
//  MainThemeGradinetView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class MainThemeGradinetView: UIView {
	
	private var gradientLayer: CAGradientLayer? {
		return layer as? CAGradientLayer
	}
	
	override class var layerClass: AnyClass {
		return CAGradientLayer.self
	}
	
	func animateStandardGradient() {
		gradientLayer?.animateStandardGradient()
	}
}
