//
//  OutlineBadgeTypr.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

enum OutlineBadgeType {
    case keto
    case vega
    
    init(with category: DishCategory) {
        switch category {
        case .keto:
            self = .keto
        case .vega:
            self = .vega
        }
    }
}
