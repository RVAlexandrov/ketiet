//
//  OutlineBadgeView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

class OutlineBadgeView: UIView {
    var configuration: Configuation {
        didSet {
            if let imageString = configuration.imageSystemString {
                imageView.isHidden = false
                imageView.image = UIImage(systemName: imageString)
                imageView.tintColor = configuration.imageColor ?? .systemGreen
            } else {
                imageView.isHidden = true
            }
            
            titleLabel.text = configuration.title
            titleLabel.textColor = configuration.accentColor ?? .systemGreen
            layer.borderColor = configuration.accentColor?.cgColor ?? UIColor.systemGreen.cgColor
        }
    }
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBodySemibold
        return label
    }()
    
    override init(frame: CGRect) {
        configuration = Configuation(title: "",
                                     imageSystemString: nil,
                                     imageColor: nil,
                                     accentColor: nil)
        super.init(frame: frame)
        setupView()
    }
    
    init(configuration: Configuation) {
        self.configuration = configuration
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension OutlineBadgeView {
    private func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .clear
        layer.cornerRadius = 8
        layer.borderWidth = 1.5
        layer.borderColor = UIColor.systemGreen.cgColor
        
        let stackView = UIStackView()
        stackView.addArrangedSubviews([imageView, titleLabel])
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(stackView)
        
        stackView.pinToSuperview(insets: UIEdgeInsets(top: .smallLittMargin,
                                                      left: .smallLittMargin,
                                                      bottom: .smallLittMargin,
                                                      right: .smallLittMargin),
                                 useSafeArea: false)
    }
}

extension OutlineBadgeView {
    struct Configuation {
        var title: String
        var imageSystemString: String?
        var imageColor: UIColor?
        var accentColor: UIColor?
    }
}
