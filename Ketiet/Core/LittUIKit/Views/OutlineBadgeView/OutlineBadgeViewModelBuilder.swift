//
//  OutlineBadgeViewModelBuilder.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct OutlineBadgeViewConfigurationBuilder {
    func build(for type: OutlineBadgeType) -> OutlineBadgeView.Configuation {
        switch type {
        case .keto:
            return OutlineBadgeView.Configuation(title: "keto",
                                                 imageSystemString: nil,
                                                 imageColor: nil,
                                                 accentColor: .systemGreen)
        case .vega:
            return OutlineBadgeView.Configuation(title: "vega",
                                                 imageSystemString: "leaf.fill",
                                                 imageColor: .systemGreen,
                                                 accentColor: .systemGreen)
        }
    }
}
