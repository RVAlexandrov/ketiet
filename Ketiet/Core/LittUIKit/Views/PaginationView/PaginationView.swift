//
//  PaginationView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class PaginationView: UIView {
	
	/// Индекс текущей страницы
	var currentPage = 0 {
		didSet {
			guard numberOfPages > currentPage else {
				return
			}
			update()
		}
	}
	
	/// Количество точек (страниц)
	var numberOfPages: Int = 0 {
		didSet {
			setupDotLayers()
			isHidden = hideForSinglePage && numberOfPages <= 1
		}
	}
	
	/// Цвет неактивных точек
	var inactiveTintColor: UIColor = .systemGray {
		didSet {
			setNeedsLayout()
			layoutIfNeeded()
		}
	}
	
	/// Цвет активной точки (страницы)
	var currentPageTintColor: UIColor = .accentColor {
		didSet {
			setNeedsLayout()
			layoutIfNeeded()
		}
	}
	
	private let limit = 5
	private var fullScaleIndex = [0, 1, 2]
	private var dotLayers: [CALayer] = []
    private var containerLayer: CALayer = CALayer()
	private var diameter: CGFloat = 7
	private var centerIndex: Int {
		return fullScaleIndex[1]
	}
	
	private var padding: CGFloat = 8 {
		didSet {
			updateDotLayersLayout()
		}
	}
	
	private var minScaleValue: CGFloat = 0.4 {
		didSet {
			setNeedsLayout()
			layoutIfNeeded()
		}
	}
	
	private var middleScaleValue: CGFloat = 0.7 {
		didSet {
			setNeedsLayout()
			layoutIfNeeded()
		}
	}
	
	private var hideForSinglePage: Bool = true {
		didSet {
			setNeedsLayout()
			layoutIfNeeded()
		}
	}
	
	private var inactiveTransparency: CGFloat = 0.4 {
		didSet {
			setNeedsLayout()
			layoutIfNeeded()
		}
	}

	override var intrinsicContentSize: CGSize {
		return sizeThatFits(CGSize.zero)
	}
	
	override func sizeThatFits(_ size: CGSize) -> CGSize {
		let minValue = min(7, numberOfPages)
		return CGSize(width: CGFloat(minValue) * diameter + CGFloat(minValue - 1) * padding, height: diameter)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		update()
	}
}

extension PaginationView {
	
	private func setupDotLayers() {
        layer.addSublayer(containerLayer)
        containerLayer.backgroundColor = UIColor.littBackgroundColor.cgColor
        
		dotLayers.forEach { $0.removeFromSuperlayer() }
		dotLayers.removeAll()
		
		(0..<numberOfPages).forEach { _ in
			let dotLayer = CALayer()
            layer.addSublayer(dotLayer)
			dotLayers.append(dotLayer)
		}
		
		updateDotLayersLayout()
		setNeedsLayout()
		layoutIfNeeded()
		invalidateIntrinsicContentSize()
	}
	
	private func updateDotLayersLayout() {
		let floatCount = CGFloat(numberOfPages)
		let xPosition = (bounds.size.width - diameter * floatCount - padding * (floatCount - 1)) * 0.5
		let yPosition = (bounds.size.height - diameter) * 0.5
		var frame = CGRect(x: xPosition, y: yPosition, width: diameter, height: diameter)
        
        containerLayer.frame = CGRect(x: xPosition - diameter,
                                      y: yPosition - diameter / 2,
                                      width: (floatCount + 2) * diameter + (floatCount - 1) * padding,
                                      height: 2 * diameter)
        containerLayer.cornerRadius = (2 * diameter) / 2
		
		dotLayers.forEach {
			$0.cornerRadius = diameter / 2
			$0.frame = frame
			frame.origin.x += diameter + padding
		}
	}
	
	private func setupDotLayersPosition() {
		let centerLayer = dotLayers[centerIndex]
		centerLayer.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
		
		dotLayers.enumerated().filter {
			$0.offset != centerIndex
		}.forEach {
			let index = abs($0.offset - centerIndex)
			let interval = $0.offset > centerIndex ? diameter + padding : -(diameter + padding)
			$0.element.position = CGPoint(x: centerLayer.position.x + interval * CGFloat(index), y: $0.element.position.y)
		}
	}
	
	private func setupDotLayersScale() {
		dotLayers.enumerated().forEach {
			guard let first = fullScaleIndex.first, let last = fullScaleIndex.last else {
				return
			}
			
			var transform = CGAffineTransform.identity
			if !fullScaleIndex.contains($0.offset) {
				var scaleValue: CGFloat = 0
				if abs($0.offset - first) == 1 || abs($0.offset - last) == 1 {
					scaleValue = min(middleScaleValue, 1)
				} else if abs($0.offset - first) == 2 || abs($0.offset - last) == 2 {
					scaleValue = min(minScaleValue, 1)
				} else {
					scaleValue = 0
				}
				transform = transform.scaledBy(x: scaleValue, y: scaleValue)
			}
			
			$0.element.setAffineTransform(transform)
		}
	}
	
	private func update() {
		dotLayers.enumerated().forEach {
			$0.element.backgroundColor = $0.offset == currentPage
				? currentPageTintColor.cgColor
				: inactiveTintColor.withAlphaComponent(inactiveTransparency).cgColor
		}
		
		guard numberOfPages > limit else {
			return
		}
		
		changeFullScaleIndexsIfNeeded()
		setupDotLayersPosition()
		setupDotLayersScale()
	}
	
	private func changeFullScaleIndexsIfNeeded() {
		fullScaleIndex[0] = currentPage - 1
		fullScaleIndex[1] = currentPage
		fullScaleIndex[2] = currentPage + 1
	}
}
