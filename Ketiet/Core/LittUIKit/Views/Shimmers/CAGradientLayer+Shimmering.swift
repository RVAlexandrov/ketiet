//
//  ShimmerLayer.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 19.06.2020.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

extension CAGradientLayer {
	
	private struct Constants {
		static let animationDuration = TimeInterval(1)
	}
	
	func startShimmering() {
		locations = [0.0, 0.5, 1.0]
		startPoint = .zero
		endPoint = CGPoint(x: 1, y: 1)
		colors = [UIColor.littGradientColor.cgColor,
				  UIColor.white.cgColor,
				  UIColor.littGradientColor.cgColor]
		let animation = CABasicAnimation(keyPath: NSStringFromSelector(#selector(getter: CAGradientLayer.locations)))
		animation.duration = Constants.animationDuration
		animation.repeatCount = .infinity
		animation.isRemovedOnCompletion = false
		animation.fromValue = [-1.0, -0.5, 0.0]
		animation.toValue = [1.0, 1.5, 2.0]
		add(animation, forKey: nil)
	}
}
