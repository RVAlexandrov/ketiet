//
//  ShimmerView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.06.2020.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

final class ShimmerView: UIView {
	
	override class var layerClass: AnyClass {
		return CAGradientLayer.self
	}
	
	override func didMoveToSuperview() {
		(layer as? CAGradientLayer)?.startShimmering()
	}
	
	override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
		(layer as? CAGradientLayer)?.colors = [UIColor.littGradientColor.cgColor,
											UIColor.white.cgColor,
											UIColor.littGradientColor.cgColor]
	}
}
