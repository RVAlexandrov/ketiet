//
//  UIView+Shimmer.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 19.06.2020.
//  Copyright © 2020 Alexander. All rights reserved.
//

import UIKit

extension UIView {

	func showShimmer() {
		if subviews.last is ShimmerView {
			return
		}
		subviews.forEach { $0.isHidden = true }
		let view = ShimmerView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.layer.cornerRadius = shimmerCornerRadius()
		view.layer.maskedCorners = layer.maskedCorners
		addSubview(view)
		NSLayoutConstraint.activate([
			view.topAnchor.constraint(equalTo: topAnchor),
			view.leadingAnchor.constraint(equalTo: leadingAnchor),
			view.trailingAnchor.constraint(equalTo: trailingAnchor),
			view.bottomAnchor.constraint(equalTo: bottomAnchor)
		])
	}
	
	func hideShimmer() {
		guard subviews.last is ShimmerView else { return }
		subviews.last?.removeFromSuperview()
		subviews.forEach { $0.isHidden = false }
	}
	
	func shimmerCornerRadius() -> CGFloat {
		if let label = self as? UILabel {
			return label.font.lineHeight / 2
		}
		return layer.cornerRadius
	}
}
