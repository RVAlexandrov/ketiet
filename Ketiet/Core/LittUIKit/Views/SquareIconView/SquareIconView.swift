//
//  SquareIconView.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 29.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

class SquareIconView: UIView {
    
    var iconImageType: ImageStringType? {
        didSet {
            guard let iconImageType = iconImageType else { return }
            iconImageView.setImageType(iconImageType)
        }
    }
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        layer.cornerRadius = 4
        
        addSubview(iconImageView)
        
        NSLayoutConstraint.activate(
            iconImageView.pinToSuperviewConstraints(
                insets: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2),
                useSafeArea: false
            )
        )
    }
}
