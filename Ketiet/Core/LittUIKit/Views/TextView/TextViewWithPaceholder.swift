//
//  TextViewWithPaceholder.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 04.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class TextViewWithPaceholder: UITextView {
    
    private let placeholderLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .littSecondaryTextColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPlacehodler(_ placeholder: String?) {
        placeholderLabel.textAlignment = textAlignment
        placeholderLabel.text = placeholder
        placeholderLabel.font = font
        updatePlaceholderVisibility()
    }
    
    private func setup() {
        textStorage.delegate = self
        addSubview(placeholderLabel)
        NSLayoutConstraint.activate([
            placeholderLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            placeholderLabel.widthAnchor.constraint(equalTo: widthAnchor),
            placeholderLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    private func updatePlaceholderVisibility() {
        placeholderLabel.isHidden = !text.isEmpty
    }
}

extension TextViewWithPaceholder: NSTextStorageDelegate {
    func textStorage(
        _ textStorage: NSTextStorage,
        didProcessEditing
            editedMask: NSTextStorage.EditActions,
        range editedRange: NSRange,
        changeInLength delta: Int
    ) {
        updatePlaceholderVisibility()
    }
}
