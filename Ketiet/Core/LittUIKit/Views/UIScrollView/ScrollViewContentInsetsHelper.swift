//
//  ScrollViewContentInsetsHelper.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 04.11.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ScrollViewContentInsetsHelper: KeyboardEventsListnerHelper {
	
	init(scrollView: UIScrollView) {
		let willShowAction: Action = { [weak scrollView] notification in
			if let keyboardRect = notification.keyboardRect(for: UIResponder.keyboardFrameEndUserInfoKey),
			   let animationCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber,
			   let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber {
				scrollView?.handleBottomInsetChange(keyboardRect.height, animationCurve: animationCurve, duration: duration)
			}
		}
		let didHideAction: Action = { [weak scrollView] notification in
			if let keyboardRect = notification.keyboardRect(for: UIResponder.keyboardFrameBeginUserInfoKey),
			   let animationCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber,
			   let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber {
				scrollView?.handleBottomInsetChange(-keyboardRect.height, animationCurve: animationCurve, duration: duration)
			}
		}
		super.init(willShowAction: willShowAction, didHideAction: didHideAction)
	}
}

extension UIScrollView {

	func handleBottomInsetChange(_ keyboardHeight: CGFloat, animationCurve: NSNumber, duration: NSNumber) {
		var insets = contentInset
		insets.bottom += keyboardHeight
		UIView.animate(withDuration: duration.doubleValue,
					   delay: 0,
					   options: UIView.AnimationOptions(rawValue: animationCurve.uintValue)) {
			self.contentInset = insets
		}
	}
}

extension Notification {
	
	func keyboardRect(for key: String) -> CGRect? {
		guard let isLocal = userInfo?[UIResponder.keyboardIsLocalUserInfoKey] as? NSNumber, isLocal.boolValue else {
			return nil
		}
		guard let frameValue = userInfo?[key] as? NSValue else {
			return nil
		}
		return frameValue.cgRectValue
	}
}
