//
//  UIScrollView+Convenience.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

extension UIScrollView {
	
	func setContentInsetForControllersInTabbar() {
		let safaAreaInset = UIApplication.shared.getKeyWindow()?.safeAreaInsets.bottom ?? 0
        contentInset.bottom = safaAreaInset + UIView.bottomInset
        verticalScrollIndicatorInsets.bottom = safaAreaInset + UIView.bottomInset
	}
}
