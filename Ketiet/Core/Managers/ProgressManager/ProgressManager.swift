//
//  ProgressManager.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 19.07.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import Foundation

protocol ProgressManagerProtocol {
    func getCurrentDayStringValue() -> String
    func getDayStringValue(forDayNumber index: Int) -> String
}

/// Manager для получения готовой строки значения того или иного параметра
struct ProgressManager {
    private let progressService: ProgressServiceProtocol
    private let measurementService: MeasurementServiceProtocol
    
    var updateValueHandler: (() -> String)?
    
    init(progressService: ProgressServiceProtocol,
         measurementService: MeasurementServiceProtocol) {
        self.progressService = progressService
        self.measurementService = measurementService
    }
}

// MARK: - ProgressManagerProtocol
extension ProgressManager: ProgressManagerProtocol {
    func getCurrentDayStringValue() -> String {
        prepareString(fromValue: progressService.getCurrentDayValue())
    }
    
    func getDayStringValue(forDayNumber index: Int) -> String {
        if let allValues = progressService.getAllValues(),
           allValues.count - 1 >= index {
            return prepareString(fromValue: allValues[index])
        } else {
            return "NO_DATA".localized()
        }
    }
}

// MARK: - Private methods
extension ProgressManager {
    private func prepareString(fromValue value: Float?) -> String {
        if let value = value,
           value != 0 {
            
            // значение присутствует и оно не равно '0'
            return measurementService.format(value: value)
            
        } else {
            
            // значение отсутствует или оно равно '0'
            return "NO_DATA".localized()
            
        }
    }
}
