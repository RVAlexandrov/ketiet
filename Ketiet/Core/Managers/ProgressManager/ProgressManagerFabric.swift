//
//  ProgressManagerFabric.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 19.07.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore

protocol ProgressManagerFabricProtocol {
    func createManager(forType type: ProgressValueType) -> ProgressManagerProtocol
}

struct ProgressManagerFabric { }

extension ProgressManagerFabric: ProgressManagerFabricProtocol {
    func createManager(forType type: ProgressValueType) -> ProgressManagerProtocol {
        switch type {
        case .weight:
            return ProgressManager(progressService: EveryDayInputProgressService.weightProgressService,
                                   measurementService: MeasurementService(type: .weight))
        case .water:
            return ProgressManager(progressService: EveryDayInputProgressService.waterProgressService,
                                   measurementService: MeasurementService(type: .volume))
        case .chest:
            return ProgressManager(progressService: EveryDayInputProgressService.chestProgressService,
                                   measurementService: MeasurementService(type: .tall))
        case .hips:
            return ProgressManager(progressService: EveryDayInputProgressService.hipsProgressService,
                                   measurementService: MeasurementService(type: .tall))
        case .waist:
            return ProgressManager(progressService: EveryDayInputProgressService.waistProgressService,
                                   measurementService: MeasurementService(type: .tall))
        }
    }
}
