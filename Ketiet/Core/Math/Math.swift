//
//  Math.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import CoreGraphics

func map(value: CGFloat, startA: CGFloat, endA: CGFloat, startB: CGFloat, endB: CGFloat) -> CGFloat {
	if startA == endA {
		return value > startA ? endB : startB
	}
	return ((value - startA) / (endA - startA)) * (endB - startB) + startB
}

func clamp(_ value: CGFloat, minValue: CGFloat, maxValue: CGFloat) -> CGFloat {
	return min(maxValue, max(minValue, value))
}
