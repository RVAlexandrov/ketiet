//
//  Goal.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 09.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

enum Goal: String, CaseIterable {
    case saveWeight
    case loseWeight
    case gainWeight
	
	init?(description: String) {
		if description == Goal.saveWeight.description {
			self = .saveWeight
		} else if description == Goal.loseWeight.description {
			self = .loseWeight
		} else if description == Goal.gainWeight.description {
			self = .gainWeight
		} else {
			return nil
		}
	}
	
	var description: String {
		(rawValue.uppercased() + "_GOAL_CARD_TITLE").localized()
	}
	
	var imageName: String {
		rawValue + "Image"
	}
	
	var metricsChangeDirection: BodyMetricChangeDirection {
		switch self {
		case .gainWeight:
			return .increase
		case .loseWeight:
			return .decrease
		case .saveWeight:
			return .saveCurrentValue
		}
	}
	
	var ketoWeightChangeMode: WeightChangeMode {
		switch self {
		case .gainWeight:
			return .support
		case .loseWeight:
			return .reduce
		case .saveWeight:
			return .support
		}
	}
    
    var descriptionString: String {
        switch self {
        case .gainWeight:
            return "GAIN_WEIGHT_DESCRIPTION_STRING".localized()
        case .loseWeight:
            return "LOSE_WEIGHT_DESCRIPTION_STRING".localized()
        case .saveWeight:
            return "SAVE_WEIGHT_DESCRIPTION_STRING".localized()
        }
    }
}
