//
//  Nutrient.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 14.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

struct Nutrient {
    let type: NutrientType
    var value: Float
    
    var stringValue: String {
        switch type {
        case .fat, .protein, .carbohydrate:
            let formatter = MassFormatter()
            formatter.isForPersonMassUse = true
			formatter.numberFormatter.maximumFractionDigits = 1
			formatter.numberFormatter.minimumFractionDigits = 1
            return formatter.string(fromValue: Double(value), unit: .gram)
        case .calories:
            return String(value.rounded().cleanValue) + " " + MeasurementService(type: .energy).measurement()
        }
    }
}
