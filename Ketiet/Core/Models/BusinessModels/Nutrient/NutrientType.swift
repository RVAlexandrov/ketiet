//
//  NutrientType.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 15.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum NutrientType: String {
    case fat
    case protein
    case carbohydrate
    case calories
    
    var accentColor: UIColor {
        switch self {
        case .fat: return .systemYellow
        case .protein: return .systemRed
        case .carbohydrate: return .systemBlue
        case .calories: return .systemOrange
        }
    }
    
    var shortString: String {
        switch self {
		case .fat: return "FATS_SHORT".localized() + ":"
		case .protein: return "PROTEINS_SHORT".localized() + ":"
		case .carbohydrate: return "CARBOHYDRATES_SHORT".localized() + ":"
		case .calories: return "TOTAL".localized() + ":"
        }
    }
    
    var fullString: String {
        switch self {
		case .fat: return "FAT".localized()
		case .protein: return "PROTEIN".localized()
		case .carbohydrate: return "CARBOHYDRATE".localized()
		case .calories: return "TOTAL_CCAL".localized()
        }
    }
}
