//
//  Trend.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum Trend {
    case positive
    case negative
    case neutral
    
    var color: UIColor {
        switch self {
        case .positive:
            return .systemGreen
        case .negative:
            return .systemRed
        case .neutral:
            return .systemBackground
        }
    }
}
