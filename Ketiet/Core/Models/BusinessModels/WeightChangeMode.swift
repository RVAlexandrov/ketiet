//
//  WeightChangeMode.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 17.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

enum WeightChangeMode {
    case support
    case reduce
    case gain
}
