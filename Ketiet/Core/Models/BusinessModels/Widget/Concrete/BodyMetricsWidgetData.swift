//
//  BodyMetricsWidgetData.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Foundation
import Combine

final class BodyMetricsWidgetData: WidgetData {
    var position: Int?
    weak var cell: WidgetCellProtocol?
    let type: WidgetType = .bodyMetrics
    
	private(set) var initialBreastVolume: Float?
	var currentBreastVolume: Float?
    var diffBreastVolume: Float?
    
	private(set) var initialWaistVolume: Float?
	var currentWaistVolume: Float?
    var diffWaistVolume: Float?
    
	private(set) var initialHipsVolume: Float?
	var currentHipsVolume: Float?
    var diffHipsVolume: Float?
    
	private let breastProgressService: ProgressServiceProtocol
	private let waistProgressService: ProgressServiceProtocol
	private let hipsProgressService: ProgressServiceProtocol
    private let dietService: DietServiceProtocol
	let measurementService: MeasurementServiceProtocol
	let updateBodyMetricVolumeHandler: (BodyMetricsWidgetData, BodyMetricType) -> Void

    private var chestSubscriber: AnyCancellable?
    private var waistSubscriber: AnyCancellable?
    private var hipsSubscriber: AnyCancellable?
    
	init(dietService: DietServiceProtocol,
        breastProgressService: ProgressServiceProtocol,
		waistProgressService: ProgressServiceProtocol,
		hipsProgressService: ProgressServiceProtocol,
		measurementService: MeasurementServiceProtocol,
		updateBodyMetricVolumeHandler: @escaping(BodyMetricsWidgetData, BodyMetricType) -> Void) {
		self.measurementService = measurementService
		self.breastProgressService = breastProgressService
		self.waistProgressService = waistProgressService
		self.hipsProgressService = hipsProgressService
        self.dietService = dietService
		self.updateBodyMetricVolumeHandler = updateBodyMetricVolumeHandler
		
		currentBreastVolume = BodyMetricsWidgetData.current(from: breastProgressService,
                                                            dietService: dietService)
		currentWaistVolume = BodyMetricsWidgetData.current(from: waistProgressService,
                                                           dietService: dietService)
		currentHipsVolume = BodyMetricsWidgetData.current(from: hipsProgressService,
                                                          dietService: dietService)
		
		initialBreastVolume = initial(from: breastProgressService)
		initialWaistVolume = initial(from: waistProgressService)
		initialHipsVolume = initial(from: hipsProgressService)
		
		diffBreastVolume = diff(intial: initialBreastVolume, current: currentBreastVolume)
		diffWaistVolume = diff(intial: initialWaistVolume, current: currentWaistVolume)
		diffHipsVolume = diff(intial: initialHipsVolume, current: currentHipsVolume)
        
        hipsSubscriber = hipsProgressService.publisher.sink(receiveValue: { _ in
            self.updateHips()
            DispatchQueue.main.async {
                self.cell?.updateView()
            }
        })
        
        waistSubscriber = waistProgressService.publisher.sink(receiveValue: { _ in
            self.updateWaist()
            DispatchQueue.main.async {
                self.cell?.updateView()
            }
        })
        
        chestSubscriber = breastProgressService.publisher.sink(receiveValue: { _ in
            self.updateChest()
            DispatchQueue.main.async {
                self.cell?.updateView()
            }
        })
    }
    
    private func updateHips() {
        currentHipsVolume = BodyMetricsWidgetData.current(from: hipsProgressService,
                                                          dietService: dietService)
        initialHipsVolume = initial(from: hipsProgressService)
        diffHipsVolume = diff(intial: initialHipsVolume, current: currentHipsVolume)
    }
    
    private func updateWaist() {
        currentWaistVolume = BodyMetricsWidgetData.current(from: waistProgressService,
                                                           dietService: dietService)
        initialWaistVolume = initial(from: waistProgressService)
        diffWaistVolume = diff(intial: initialWaistVolume, current: currentWaistVolume)
    }
    
    private func updateChest() {
        currentBreastVolume = BodyMetricsWidgetData.current(from: breastProgressService,
                                                            dietService: dietService)
        initialBreastVolume = initial(from: breastProgressService)
        diffBreastVolume = diff(intial: initialBreastVolume, current: currentBreastVolume)
    }
	
	private func initial(from progressService: ProgressServiceProtocol) -> Float? {
		if let first = progressService.getAllValues()?.first, first != 0 {
			return first
		}
		return nil
	}
	
    static private func current(
        from progressService: ProgressServiceProtocol,
        dietService: DietServiceProtocol
    ) -> Float? {
        guard let currentDayIndex = dietService.getCurrentDayIndex(),
              let current = progressService.getAllValues()?[currentDayIndex],
              current != 0 else { return nil }
        return current
    }
	
	private func diff(intial: Float?,
                      current: Float?) -> Float? {
		if let initial = intial, initial != 0, let current = current {
			return  current - initial
		}
		return nil
	}
}
