//
//  HeaderWidgetData.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 09.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Combine
import UIKit

final class HeaderWidgetData {
	weak var cell: HeaderWidgetView?
	private(set) var profile: ProfileProtocol?
	private(set) var diet: DietProtocol?
	private lazy var subscribers = Set<AnyCancellable>()
	private let profileService: ProfileServiceProtocol
	private let dietService: DietServiceProtocol
	private var notificatiopnTokens: [NSObjectProtocol]?
	
	var currentDayIndex: Int? {
		dietService.getCurrentDayIndex()
	}
	
	init(profileService: ProfileServiceProtocol,
		 dietService: DietServiceProtocol) {
		self.profileService = profileService
		self.dietService = dietService
		let group = DispatchGroup()
		group.enter()
		profileService.getProfile { [weak self] profile in
			self?.profile = profile
			group.leave()
		}
		group.enter()
		dietService.requestCurrentDiet { [weak self] diet in
			self?.diet = diet
			group.leave()
		}
		group.notify(queue: .main) { self.cell?.updateView() }
        let removingDietToken = dietService.notificationCenter.addObserver(forName: .didRemoveCurrentDietNotification,
																		   object: nil,
																		   queue: .main) { [weak self] _ in
			self?.diet = nil
			self?.cell?.updateView()
		}
        let addingDietToken = dietService.notificationCenter.addObserver(forName: .didSetCurrentDietNotification,
																		 object: nil,
																		 queue: .main) { _ in
			self.diet = self.dietService.getCurrentDiet()
			self.cell?.updateView()
		}
		let becomeActiveToken = NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
																	   object: nil,
																	   queue: .main) { [weak self] _ in
			self?.cell?.updateView()
		}
		notificatiopnTokens = [removingDietToken, addingDietToken, becomeActiveToken]
        profileService.imagePublisher.sink { [weak self] _ in
            self?.cell?.updateView()
        }.store(in: &subscribers)
        profileService.namePublisher.sink { [weak self] _ in
            self?.cell?.updateView()
        }.store(in: &subscribers)
	}
}
