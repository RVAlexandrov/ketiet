//
//  NearestWidgetData.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Combine

final class NearestMeal {
    let index: Int
    let mealName: String
    let dishIds: [String]
    let dishWeights: [Float]
    let dishNames: [String]
    let kcal: Float
	let energyMeasurement: String
    let fats: Float
    let proteins: Float
    let carbohydrates: Float
	let nutriensMeasurement: String
    var isEaten: Bool
	
    init(index: Int,
         mealName: String,
         dishIds: [String],
         dishWeights: [Float],
         dishNames: [String],
         kcal: Float,
         energyMeasurement: String,
         fats: Float,
         proteins: Float,
         carbohydrates: Float,
         nutriensMeasurement: String,
         isEaten: Bool) {
		self.index = index
		self.mealName = mealName
        self.dishIds = dishIds
        self.dishWeights = dishWeights
		self.dishNames = dishNames
		self.kcal = kcal
		self.energyMeasurement = energyMeasurement
		self.fats = fats
		self.proteins = proteins
		self.carbohydrates = carbohydrates
		self.nutriensMeasurement = nutriensMeasurement
		self.isEaten = isEaten
	}
}

final class NearestMealWidgetData: WidgetData {
    var position: Int?
    var cell: WidgetCellProtocol?
    var type: WidgetType = .nearestMeal
    var nearestMeals: [NearestMeal]?
    
    private var subscriber = AnyCancellable({})
    
    private let dietService: DietServiceProtocol
    
    init(dietService: DietServiceProtocol) {
        self.dietService = dietService
        updateData()
        
        subscriber = dietService.publisher.sink(receiveValue: { [weak self] _ in
            self?.updateData()
            DispatchQueue.main.async {
                self?.cell?.updateView()
            }
        })
    }
    
    func setDoneForMeal(at index: Int, newValue: Bool) {
        guard let dietDay = dietService.getCurrentDay(), index <= dietDay.meals.count - 1 else { return }
        let meal = dietDay.meals[index]
        meal.dishes.forEach {
            dietService.setCurrentDietDishStatus(newValue,
                                                 for: $0,
                                                 inMeal: meal.id,
												 inDay: dietDay.id,
												 completion: { _ in })
        }
    }
}

// MARK: - Private methods
extension NearestMealWidgetData {
    private func updateData() {
        guard let dietDay = dietService.getCurrentDay() else {
            nearestMeals = nil
            return
        }
        
        nearestMeals = dietDay.meals.enumerated().map { dietMeal -> NearestMeal in
            let energyService = MeasurementService(type: .energy)
            let weightService = MeasurementService(type: .smallWeight, unitStyle: .short)
            let meal = NearestMeal(index: dietMeal.offset,
                                   mealName: dietMeal.element.name,
                                   dishIds: dietMeal.element.dishes.map { $0.dishId },
                                   dishWeights: dietMeal.element.dishes.map { $0.weightInGrams },
                                   dishNames: dietMeal.element.dishes.map { $0.name },
                                   kcal: round(dietMeal.element.calories),
                                   energyMeasurement: energyService.measurement(),
                                   fats: round(weightService.convertToLocale(value: dietMeal.element.fats)),
                                   proteins: round(weightService.convertToLocale(value: dietMeal.element.proteins)),
                                   carbohydrates: round(weightService.convertToLocale(value: dietMeal.element.carbohydrates)),
                                   nutriensMeasurement: weightService.measurement(),
                                   isEaten: dietMeal.element.isEaten)
            return meal
        }
    }
}
