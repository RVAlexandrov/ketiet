//
//  NutritionMetricsWidgetData.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Foundation
import Combine

final class NutritionMetricsWidgetData: WidgetData {
    var position: Int?
    var cell: WidgetCellProtocol?
    let type: WidgetType = .nutritionMetrics
    private(set) var nutrients: NutritionMetrics = .zero()
    private var subscriber = AnyCancellable({})
    private let dietService: DietServiceProtocol
    
    init(dietService: DietServiceProtocol) {
        self.dietService = dietService
        subscriber = dietService.publisher.sink { [weak self] _ in
            self?.updateData()
            DispatchQueue.main.async {
                self?.cell?.updateView()
            }
        }
        updateData()
    }
    
    private func updateData() {
        guard let currentDay = dietService.getCurrentDay() else {
            nutrients = .zero()
            return
        }
        nutrients = NutritionMetrics(
            currentCalories: round(currentDay.getAlreadyCalories()),
            targetCalories: round(currentDay.getDayCalories()),
            currentFats: round(currentDay.getAlreadyFats()),
            targetFats: round(currentDay.getDayFats()),
            currentProteins: round(currentDay.getAlreadyProteins()),
            targetProteins: round(currentDay.getDayProteins()),
            currentCarbohydrates: round(currentDay.getAlreadyCarbohydrates()),
            targetCarbohydrates: round(currentDay.getDayCarbohydrates())
        )
    }
    
    func update() {
        guard let currentDay = dietService.getCurrentDay() else { return }
        
        let nutrients = NutritionMetrics(
            currentCalories: round(currentDay.getAlreadyCalories()),
            targetCalories: round(currentDay.getDayCalories()),
            currentFats: round(currentDay.getAlreadyFats()),
            targetFats: round(currentDay.getDayFats()),
            currentProteins: round(currentDay.getAlreadyProteins()),
            targetProteins: round(currentDay.getDayProteins()),
            currentCarbohydrates: round(currentDay.getAlreadyCarbohydrates()),
            targetCarbohydrates: round(currentDay.getDayCarbohydrates())
        )
        
        if self.nutrients != nutrients {
            self.nutrients = nutrients
            cell?.updateView()
        }
    }
}
