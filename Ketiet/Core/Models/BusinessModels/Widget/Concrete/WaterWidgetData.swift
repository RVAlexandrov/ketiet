//
//  WaterWidgetData.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Foundation
import Combine

final class WaterWidgetData: WidgetData {
	weak var cell: WidgetCellProtocol?
    var position: Int?
    let type: WidgetType = .water
	private let progressService: ProgressServiceProtocol
	private(set) var currentWater: Double
    private(set) var targetWater: Double = 0
    let profileService: ProfileServiceProtocol
    let inputValueHandler: (WaterWidgetData) -> Void
    let measurementService: MeasurementServiceProtocol
	let firstFastInputValue: Int
    private var subscriber: AnyCancellable?
    
    init(profileService: ProfileServiceProtocol,
		 firstFastInputValue: Int,
         progressService: ProgressServiceProtocol,
         measurementService: MeasurementServiceProtocol,
         inputValueHandler: @escaping (WaterWidgetData) -> Void) {
		self.currentWater = Double(progressService.getCurrentDayValue() ?? 0)
		self.firstFastInputValue = firstFastInputValue
		self.progressService = progressService
        self.measurementService = measurementService
        self.inputValueHandler = inputValueHandler
        self.profileService = profileService
        
        profileService.getTargetWater { [weak self] water in
            self?.targetWater = water
            self?.cell?.updateView()
        }
        
        subscriber = progressService.publisher.sink(receiveValue: { [weak self] _ in
            self?.updateData()
            DispatchQueue.main.async {
                self?.cell?.updateView()
            }
        })
    }
    
    func didSetCurrentWater(_ value: Double) {
        progressService.setCurrentDayValue(Float(value))
    }
}

// MARK: - Private methods
extension WaterWidgetData {
    private func updateData() {
        currentWater = Double(progressService.getCurrentDayValue() ?? 0)
    }
}
