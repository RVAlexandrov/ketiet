//
//  WeightWidgetData.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Foundation
import Combine

final class WeightWidgetData: WidgetData {
    var position: Int?
    let type: WidgetType = .weight
    weak var cell: WidgetCellProtocol?
    
	var currentWeight: Float?
    var lastInputWeight: Float?
    var lastInputDate: String?
    var startWeight: Float?
	private let weightProgressService: ProgressServiceProtocol
	private let dietService: DietServiceProtocol
	let inputValueHandler: (WeightWidgetData) -> Void
	let measurementService: MeasurementServiceProtocol
    private var subscriber: AnyCancellable?
    
	init(weightProgressService: ProgressServiceProtocol,
		 dietService: DietServiceProtocol,
		 measurementService: MeasurementServiceProtocol,
		 inputValueHandler: @escaping(WeightWidgetData) -> Void) {
		self.weightProgressService = weightProgressService
		self.dietService = dietService
		self.measurementService = measurementService
		self.inputValueHandler = inputValueHandler
        
        updateData()
        
        subscriber = weightProgressService.publisher.sink(receiveValue: { [weak self]_ in
            self?.updateData()
            DispatchQueue.main.async {
                self?.cell?.updateView()
            }
        })
    }
    
    private func updateData() {
        let allValues = weightProgressService.getAllValues()
        
        guard let currentDayIndex = dietService.getCurrentDayIndex(), let values = allValues else { return }
        
        currentWeight = weightProgressService.getAllValues()?[currentDayIndex]
        
        let valuesBeforeToday = Array(values.dropLast(values.count - currentDayIndex))
        let reversedValuesBeforeToday = valuesBeforeToday.reversed()
        if let indexInReversed = reversedValuesBeforeToday.firstIndex(where: { $0 != 0 })?.base {
            let lastInputIndex = valuesBeforeToday.index(before: indexInReversed)
            lastInputWeight = allValues?[lastInputIndex]
            
            let lastInputDateComponents = Date().dateComponentsByAdding(DateComponents(day: lastInputIndex - currentDayIndex),
                                                                        resultUnints: [.month, .day, .year])
            guard let date = Calendar.current.date(from: lastInputDateComponents) else { return }
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeStyle = .none
            lastInputDate = formatter.string(from: date)
        } else {
            lastInputDate = nil
            lastInputWeight = nil
        }
    }
}
