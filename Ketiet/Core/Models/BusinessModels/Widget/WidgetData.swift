//
//  Widget.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol WidgetData {
    var type: WidgetType { get }
    var position: Int? { get set }
    var cell: WidgetCellProtocol? { get set }
    func updateCell()
}

extension WidgetData {
    func updateCell() {
        cell?.updateView()
    }
}
