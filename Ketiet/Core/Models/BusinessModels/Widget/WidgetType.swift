//
//  Widget.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation
import UIKit

enum WidgetType: String, CaseIterable {
    case weight
    case nearestMeal
    case water
    case nutritionMetrics
    case bodyMetrics
    case feelings
    case training
	
	static var allCases: [WidgetType] {
		[.weight,
		 .water,
		 .nearestMeal,
		 .bodyMetrics,
		 .nutritionMetrics]
	}
    
    var emojiTitle: String {
        switch self {
        case .weight:
            return "⚖️"
        case .nearestMeal:
            return "🥑"
        case .water:
            return "💧"
        case .nutritionMetrics:
            return "📊"
        case .bodyMetrics:
            return "📏"
        default:
            return "☢️"
        }
    }
    
    var imageString: String {
        switch self {
        case .weight:
            return "speedometer"
        case .nearestMeal:
            return "leaf.fill"
        case .water:
            return "drop.fill"
        case .nutritionMetrics:
            return "circles.hexagongrid"
        case .bodyMetrics:
            return "figure.wave"
        default:
            return "eyes.inverse"
        }
    }
    
    var imageStringColor: UIColor {
        switch self {
        case .weight:
            return .systemOrange
        case .nearestMeal:
            return .systemGreen
        case .water:
            return .systemBlue
        case .nutritionMetrics:
            return .systemPink
        case .bodyMetrics:
            return .systemTeal
        default:
            return .accentColor
        }
    }
    
    var generalTitle: String {
        switch self {
        case .weight:
			return "WEIGHT".localized()
        case .nearestMeal:
			return "MEAL".localized()
        case .water:
			return "WATER".localized()
        case .nutritionMetrics:
			return "PROTEINS_FATS_CARBOHYDRATES_SHORT".localized()
        case .bodyMetrics:
			return "BODY_METRICS".localized()
        default:
            return "?"
        }
    }
}
