//
//  ButtonWrapper.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct ButtonModel {
    let title: String
    let action: (() -> Void)?
}
