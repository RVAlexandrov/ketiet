//
//  CookingStep.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct CookingStep {
    let title: String
    let description: String
    let imageWrapper: ImageStringType?
    
    init(
        title: String,
        description: String,
        imageWrapper: ImageStringType? = nil
    ) {
        self.title = title
        self.description = description
        self.imageWrapper = imageWrapper
    }
}
