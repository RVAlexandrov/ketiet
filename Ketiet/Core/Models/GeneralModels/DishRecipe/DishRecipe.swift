//
//  Recipe.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct DishRecipe {
    let weightInGrams: Int
    let cookingSteps: [CookingStep]?
}
