//
//  DishCategory.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 24.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

enum DishCategory {
    case keto
    case vega
}
