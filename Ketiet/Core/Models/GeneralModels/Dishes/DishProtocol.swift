//
//  DishProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol DishProtocol: AnyObject {
    var dishId: String { get }
	var name: String { get }
	var weightInGrams: Float { get set }
	var products: [ProductProtocol] { get }
	var dishRecipe: DishRecipe { get }
	var nutriens: FoodNutrients { get } // на 100 грамм
	var imageURL: URL? { get }
    var weightedNutrients: FoodNutrients { get }
}

extension DishProtocol {
    var weightedNutrients: FoodNutrients {
        let coeff = weightInGrams / 100
        return FoodNutrients(proteins: coeff * nutriens.proteins,
                             carbohydrates: coeff * nutriens.carbohydrates,
                             fat: coeff * nutriens.fat)
    }
    
    var categories: [DishCategory] {
        if nutriens.doesSatisfyKeto() {
            return [.keto]
        } else {
            return []
        }
    }
}
