//
//  File.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 19.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class SimpleDish: DishProtocol {
	
    let dishId: String
	let name: String
    var weightInGrams: Float
	let products: [ProductProtocol]
	let dishRecipe: DishRecipe
	private(set) var nutriens: FoodNutrients
	let imageURL: URL?
	
    init(dishId: String,
         name: String,
		 weightInGrams: Float = 100,
         dishRecipe: DishRecipe = DishRecipe(weightInGrams: 100,
                                             cookingSteps: []),
		 nutriens: FoodNutrients,
		 products: [ProductProtocol] = [],
		 imageURL: URL? = nil) {
        self.dishId = dishId
		self.name = name
        self.weightInGrams = weightInGrams
		self.dishRecipe = dishRecipe
		self.nutriens = nutriens
		self.products = products
		self.imageURL = imageURL
	}
}
