//
//  DishesQueue.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 10.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

/// Вспомогательная сущность для выбора блюд в меню на день, например для того, чтоб не возникало ситуации, при которой одно и тоже блюдо встечается очень часто, а другое не встечается вообще, при составленнии меню можно использовать её в качестве поставщика блюд
final class SimpleQueue<Type> {
	
	private var elements: [Type]
	
	init(elements: [Type]) {
		self.elements = elements
	}
	
	func dequeue() -> Type? {
		guard let firstElement = elements.first else { return nil }
		elements.removeFirst()
		elements.append(firstElement)
		return firstElement
	}
}

//extension SimpleQueue: DishProviderProtocol where Type: DishProtocol {
//    
//    var dishesCount: Int {
//        elements.count
//    }
//	
//	func nextDish() -> DishProtocol? {
//		return dequeue()
//	}
//}
