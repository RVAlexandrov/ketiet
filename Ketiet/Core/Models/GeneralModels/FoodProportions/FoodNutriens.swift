//
//  FoodNutrients.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct KetoPercentsConstants {
    static let proteinsPercent: Float = 0.20
    static let fatPercent: Float = 0.75
    static let carbohydratesPercent: Float = 0.05
}

struct CaloriesConstants {
	static let caloriesInProteinGram: Float = 4
	static let caloriesInCarbohydratesGram: Float = 4
	static let caloriesInFatGram: Float = 9
}

struct FoodNutrients {
    
    static let zero = FoodNutrients(proteins: 0, carbohydrates: 0, fat: 0)
    
    static func + (lhs: FoodNutrients, rhs: FoodNutrients) -> FoodNutrients {
        FoodNutrients(proteins: lhs.proteins + rhs.proteins,
                      carbohydrates: lhs.carbohydrates + rhs.carbohydrates,
                      fat: lhs.fat + rhs.fat)
    }
	
	let proteins: Float
	let carbohydrates: Float
	let fat: Float
	var calories: Float {
		return proteins * CaloriesConstants.caloriesInProteinGram +
			carbohydrates * CaloriesConstants.caloriesInCarbohydratesGram +
			fat * CaloriesConstants.caloriesInFatGram
	}
	
	init(proteins: Float, carbohydrates: Float, fat: Float) {
		self.proteins = proteins
		self.carbohydrates = carbohydrates
		self.fat = fat
	}
	
	func doesSatisfy(_ requirement: FoodNutrients, with inaccuracy: Float) -> Bool {
		return Range(baseValue: requirement.proteins, inaccuracy: inaccuracy).contains(proteins) &&
			Range(baseValue: requirement.carbohydrates, inaccuracy: inaccuracy).contains(carbohydrates) &&
			Range(baseValue: requirement.fat, inaccuracy: inaccuracy).contains(fat) &&
			Range(baseValue: requirement.calories, inaccuracy: inaccuracy).contains(calories)
	}
	
	func apply(proteinsMultiplier: Float = 1,
			   carbohydratesMultiplier: Float = 1,
			   fatMultiplier: Float = 1) -> FoodNutrients {
		return FoodNutrients(proteins: proteins * proteinsMultiplier,
							carbohydrates: carbohydrates * carbohydratesMultiplier,
							fat: fat * fatMultiplier)
	}
	
	func nutrient(for type: NutrientType) -> Nutrient {
		let value: Float
		switch type {
		case .calories:
			value = calories
		case .protein:
			value = proteins
		case .carbohydrate:
			value = carbohydrates
		case .fat:
			value = fat
		}
		return Nutrient(type: type, value: value)
	}
    
    func doesSatisfyKeto() -> Bool {
        let allWeight = carbohydrates + proteins + fat
        return Range(baseValue: allWeight * KetoPercentsConstants.carbohydratesPercent,
                     inaccuracy: 0.1)
            .contains(carbohydrates)
    }
}

extension Range where Bound == Float {
	
	init(baseValue: Float, inaccuracy: Float) {
		self.init(uncheckedBounds: (lower: (1 - inaccuracy) * baseValue, upper: (1 + inaccuracy) * baseValue))
	}
}

extension FoodNutrients: CustomStringConvertible {
	var description: String {
		return "Calories: \(calories)\n" +
				"Fat: \(fat)\n" +
				"Proteins: \(proteins)\n" +
				"Carbohydrates: \(carbohydrates)\n"
	}
}
