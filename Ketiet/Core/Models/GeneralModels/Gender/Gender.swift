//
//  Gender.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum Gender: String, CaseIterable {
	case male
	case female
	
	init?(title: String) {
		if title == Gender.male.title {
			self = .male
		} else if title == Gender.female.title {
			self = .female
		} else {
			return nil
		}
	}
	
	var title: String {
		switch self {
		case .male:
			return "MALE_GENDER_CARD_TITLE".localized()
		case .female:
			return "FEMALE_GENDER_CARD_TITLE".localized()
		}
	}
	
	var imageName: String {
		switch self {
		case .female:
			return "female_gender_image"
		case .male:
			return "male_gender_image"
		}
	}
	
	var color: UIColor {
		switch self {
		case .male:
			return .systemBlue
		case .female:
			return .systemPink
		}
	}
	
	func mifflinConstants() -> WeightCalculationConstants {
		switch self {
		case .male:
			return .mifflinSanJeorMale
		case .female:
			return .mifflinSanJeorFemale
		}
	}
	
	func harrisBenedictConstants() -> WeightCalculationConstants {
		switch self {
		case .male:
			return .harrisBenedictMale
		case .female:
			return .harrisBenedictFemale
		}
	}
}
