//
//  Product.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

final class Product: ProductProtocol {
	
	let name: String
	var quantity: Quantity
	
	init(name: String,
		 quantity: Quantity) {
		self.name = name
		self.quantity = quantity
	}
}
