//
//  ProductProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol ProductProtocol: AnyObject {
	
	var name: String { get }
	var quantity: Quantity { get set }
}
