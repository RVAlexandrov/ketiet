//
//  ProductRestrictions.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

struct ProductRestrictions: OptionSet {
	
	let rawValue: Int64
	
	static let isSaltyProductsAllowed = ProductRestrictions(rawValue: 1 << 0)
	static let isSweetProdictsAllowed = ProductRestrictions(rawValue: 1 << 1)
	static let isFatyProductsAllowed = ProductRestrictions(rawValue: 1 << 2)
	static let isFishProductsAllowed = ProductRestrictions(rawValue: 1 << 3)
	static let isMeatProductsAllowed = ProductRestrictions(rawValue: 1 << 4)
	static let isLactoseProductsAllowed = ProductRestrictions(rawValue: 1 << 5)
}
