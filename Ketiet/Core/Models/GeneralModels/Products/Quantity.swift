//
//  Quantity.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 19.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

enum Quantity {
	case grams(Float)
	case count(Int)
	case teaSpoon(Int)
	case toTaste
    case tableSpoon(Int)
	
	init?(coreDataType: Int16, value: Float) {
		switch coreDataType {
		case 1:
			self = .grams(value)
		case 2:
			self = .count(Int(value))
		case 3:
			self = .teaSpoon(Int(value))
		case 4:
			self = .toTaste
        case 5:
            self = .tableSpoon(Int(value))
		default:
			return nil
		}
	}
	
	func multiply(by multiplier: Float) -> Quantity {
		switch self {
		case let .grams(weight):
			return .grams(round(weight * multiplier))
		case let .count(count):
			return .count(count * Int(round(multiplier)))
		case let .teaSpoon(count):
			return .teaSpoon(count * Int(round(multiplier)))
        case let .tableSpoon(count):
            return .tableSpoon(count * Int(round(multiplier)))
		case .toTaste:
			return self
		}
	}
	
	mutating func multiplied(by multiplier: Float) {
		self = multiply(by: multiplier)
	}
	
	func typeValueForCoreData() -> Int16 {
		switch self {
		case .grams:
			return 1
		case .count:
			return 2
		case .teaSpoon:
			return 3
		case .toTaste:
			return 4
        case .tableSpoon:
            return 5
		}
	}
	
	func valueForCoreData() -> Float {
		switch self {
		case let .grams(weight):
			return weight
		case let .count(count):
			return Float(count)
		case let .teaSpoon(count):
			return Float(count)
        case let .tableSpoon(count):
            return Float(count)
		case .toTaste:
			return 0
		}
	}
}

extension Quantity: CustomStringConvertible {
	var description: String {
		switch self {
		case let .grams(weight):
            let measurement = Measurement(value: Double(weight),
                                          unit: UnitMass.grams)
            let formatter = MeasurementFormatter()
            formatter.unitOptions = .providedUnit
			return formatter.string(from: measurement)
		case let .count(count):
			return count.description
		case let .teaSpoon(count):
            let formatter = MeasurementFormatter()
            formatter.unitOptions = .providedUnit
            return formatter.string(from: Measurement(value: Double(count),
                                                      unit: UnitVolume.teaspoons))
        case let .tableSpoon(count):
            let formatter = MeasurementFormatter()
            formatter.unitOptions = .providedUnit
            return formatter.string(from: Measurement(value: Double(count),
                                                      unit: UnitVolume.tablespoons))
        case .toTaste:
			return "TO_TASTE".localized()
		}
	}
}
