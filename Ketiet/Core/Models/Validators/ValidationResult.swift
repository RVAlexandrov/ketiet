//
//  ValidationResult.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct ValidationResult {
    let isValid: Bool
    let invalidMessage: String
}
