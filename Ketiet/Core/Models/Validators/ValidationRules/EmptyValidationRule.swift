//
//  EmptyValidationRule.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct EmptyValidationRule: ValidationRule {
    var isIntermediate: Bool
        
    func validationResult(_ string: String) -> ValidationResult {
        return string.isEmpty ?
        ValidationResult(isValid: false, invalidMessage: "EMPTY_FIELD_ERROR".localized()) :
            ValidationResult(isValid: true, invalidMessage: "")
    }
}
