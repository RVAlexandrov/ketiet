//
//  FloatValueValidationRule.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct FloatValueValidationRule: ValidationRule {
	private let isEmptyAllowed: Bool
	private let formatter = NumberFormatter()
	let isIntermediate: Bool
	
	init(isEmptyAllowed: Bool, isIntermediate: Bool) {
		self.isEmptyAllowed = isEmptyAllowed
		self.isIntermediate = isIntermediate
	}
	
	func validationResult(_ string: String) -> ValidationResult {
		if string.isEmpty {
			return isEmptyAllowed ?
                ValidationResult(isValid: true, invalidMessage: "") :
				ValidationResult(isValid: false, invalidMessage: "EMPTY_FIELD_ERROR".localized())
		}
		return formatter.validationResult(string)
	}
}
