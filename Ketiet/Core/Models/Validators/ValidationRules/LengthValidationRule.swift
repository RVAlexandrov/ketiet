//
//  LengthValidationRule.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct LengthValidationRule: ValidationRule {
    private let maxLength: Int
	private let minLenght: Int
    let isIntermediate: Bool
	
	init(maxLength: Int, minLenght: Int, isIntermediate: Bool) {
		self.maxLength = maxLength
		self.minLenght = minLenght
		self.isIntermediate = isIntermediate
	}
    
    func validationResult(_ string: String) -> ValidationResult {
		return !(minLenght...maxLength).contains(string.count) ?
            ValidationResult(isValid: false, invalidMessage: generateInvalidMessage()) :
            ValidationResult(isValid: true, invalidMessage: "")
    }
}

extension LengthValidationRule {
    private func generateInvalidMessage() -> String {
		"ALLOWED_SYMBOLS_COUNT".localized() + " \(minLenght) - \(maxLength)"
    }
}
