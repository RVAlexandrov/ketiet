//
//  MeasurementValidationRule.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct MeasurementValidationRule: ValidationRule {
    var isIntermediate: Bool
    
    func validationResult(_ string: String) -> ValidationResult {
        if string.isEmpty {
            return ValidationResult(isValid: false, invalidMessage: "EMPTY_FIELD_ERROR".localized())
        }
        if string.commaSeparatedValue != nil {
            return ValidationResult(isValid: true, invalidMessage: "")
        } else {
            return ValidationResult(isValid: false, invalidMessage: "INVALID_SYMBOLS".localized())
        }
    }
}
