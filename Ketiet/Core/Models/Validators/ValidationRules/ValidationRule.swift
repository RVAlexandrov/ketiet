//
//  ValidationRule.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol ValidationRule {
    func validationResult(_ string: String) -> ValidationResult
    var isIntermediate: Bool { get }
}
