//
//  Validator.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol ValidatorProtocol {
    func checkFullValidation(ofString: String) -> ValidationResult
    func checkIntermediateValidation(ofString: String) -> ValidationResult
}

extension ValidatorProtocol {
    func checkValidation(ofString string: String, isIntermediate: Bool) -> ValidationResult {
        return isIntermediate ? checkIntermediateValidation(ofString: string) : checkFullValidation(ofString: string)
    }
}

final class Validator {
    private var rules: [ValidationRule]
    
    init(withRules rules: [ValidationRule]) {
        self.rules = rules
    }
}

extension Validator: ValidatorProtocol {

    func checkFullValidation(ofString string: String) -> ValidationResult {
        let succesValidationResult = ValidationResult(isValid: true, invalidMessage: "")
        let validationResult = rules.filter { !$0.validationResult(string).isValid }.last?.validationResult(string)
        return validationResult ?? succesValidationResult
    }
    
    func checkIntermediateValidation(ofString string: String) -> ValidationResult {
        let succesValidationResult = ValidationResult(isValid: true, invalidMessage: "")
        let intermediateRules = rules.filter { $0.isIntermediate }
        let validationResult = intermediateRules.filter { !$0.validationResult(string).isValid }.last?.validationResult(string)
        return validationResult ?? succesValidationResult
    }
}
