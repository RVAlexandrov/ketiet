//
//  ValidatorFactory.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol ValidatorFactoryProtocol {
	func configureValidator(forType type: ValidatorType) -> Validator
}

final class ValidatorFactory: ValidatorFactoryProtocol {
	func configureValidator(forType type: ValidatorType) -> Validator {
        switch type {
        case let .textWithMaxLength(min, max): return configureTextValidator(min: min, max: max)
        case     .float:    			   	   return configureFloatValidator()
        }
    }
}

extension ValidatorFactory {
	private func configureTextValidator(min: Int, max: Int) -> Validator {
        return Validator(withRules: [LengthValidationRule(maxLength: max, minLenght: min, isIntermediate: true)])
    }
    
	private func configureFloatValidator() -> Validator {
        return Validator(withRules: [FloatValueValidationRule(isEmptyAllowed: false, isIntermediate: true)])
    }
}
