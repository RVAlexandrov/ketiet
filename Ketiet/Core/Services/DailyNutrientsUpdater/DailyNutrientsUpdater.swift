//
//  DailyNutrientsUpdater.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 23.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import Combine

final class DailyNutrientsUpdater {
    
    private let storage = DailyNutrientsStorage()
    private var subscriber: AnyCancellable?
    
    init() {
        subscriber = DietService.shared.publisher.sink { [weak self] _ in
            guard let currentDay = DietService.shared.getCurrentDay() else {
                self?.storage.widgetData = .zero()
                return
            }
            self?.storage.widgetData = NutritionMetrics(
                currentCalories: round(currentDay.getAlreadyCalories()),
                targetCalories: round(currentDay.getDayCalories()),
                currentFats: round(currentDay.getAlreadyFats()),
                targetFats: round(currentDay.getDayFats()),
                currentProteins: round(currentDay.getAlreadyProteins()),
                targetProteins: round(currentDay.getDayProteins()),
                currentCarbohydrates: round(currentDay.getAlreadyCarbohydrates()),
                targetCarbohydrates: round(currentDay.getDayCarbohydrates())
            )
        }
    }
}
