//
//  MealDetailProviderProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol MealDetailProviderProtocol {
	
	var currentDisplayedMealIndex: Int { get }
	func numberOfDisplayedMeals() -> Int
	func meal(at index: Int) -> NearestMeal
}
