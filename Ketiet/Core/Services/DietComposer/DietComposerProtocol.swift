//
//  DietComposerProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol DietComposerProtocol {
    func makeDiet(withId id: Int16) -> Diet
}
