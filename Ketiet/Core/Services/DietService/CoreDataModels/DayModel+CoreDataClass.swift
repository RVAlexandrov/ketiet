//
//  DayModel+CoreDataClass.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData

@objc(DayModel)
public class DayModel: NSManagedObject {
	
	func makeDay() -> DietDay? {
		guard let meals = meals?.array as? [MealModel] else {
			assertionFailure("Meals should exist")
			return nil
		}
		return DietDay(id: Int(id),
					   meals: meals.compactMap { $0.makeMeal() } )
	}
}
