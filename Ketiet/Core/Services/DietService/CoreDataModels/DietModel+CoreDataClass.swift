//
//  DietModel+CoreDataClass.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData

@objc(DietModel)
public class DietModel: NSManagedObject {
	
    func makeDiet() -> Diet? {
        guard let name = name,
              let days = days?.array as? [DayModel],
              let status = DietStatus(int: status) else {
                  assertionFailure("Name, days and status should exist")
                  return nil
              }
        return Diet(id: id,
                    name: name,
                    days: days.compactMap { $0.makeDay() },
                    startedDate: startDate,
                    status: status)
	}
}
