//
//  DietModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData


extension DietModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DietModel> {
        let request = NSFetchRequest<DietModel>(entityName: "DietModel")
		request.returnsObjectsAsFaults = false
		request.relationshipKeyPathsForPrefetching = ["days"]
		return request
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var startDate: Date?
    @NSManaged public var status: Int16
    @NSManaged public var days: NSOrderedSet?

}

// MARK: Generated accessors for days
extension DietModel {

    @objc(insertObject:inDaysAtIndex:)
    @NSManaged public func insertIntoDays(_ value: DayModel, at idx: Int)

    @objc(removeObjectFromDaysAtIndex:)
    @NSManaged public func removeFromDays(at idx: Int)

    @objc(insertDays:atIndexes:)
    @NSManaged public func insertIntoDays(_ values: [DayModel], at indexes: NSIndexSet)

    @objc(removeDaysAtIndexes:)
    @NSManaged public func removeFromDays(at indexes: NSIndexSet)

    @objc(replaceObjectInDaysAtIndex:withObject:)
    @NSManaged public func replaceDays(at idx: Int, with value: DayModel)

    @objc(replaceDaysAtIndexes:withDays:)
    @NSManaged public func replaceDays(at indexes: NSIndexSet, with values: [DayModel])

    @objc(addDaysObject:)
    @NSManaged public func addToDays(_ value: DayModel)

    @objc(removeDaysObject:)
    @NSManaged public func removeFromDays(_ value: DayModel)

    @objc(addDays:)
    @NSManaged public func addToDays(_ values: NSOrderedSet)

    @objc(removeDays:)
    @NSManaged public func removeFromDays(_ values: NSOrderedSet)

}
