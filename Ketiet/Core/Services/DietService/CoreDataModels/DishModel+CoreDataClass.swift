//
//  DishModel+CoreDataClass.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//
//

import Foundation
import CoreData

@objc(DishModel)
public class DishModel: NSManagedObject {
    func makeDishModel() -> DietDish? {
        guard let dishId = dishId else { return nil }
        let generator = DishIdGenerator()
        let simpleDish = generator.getDish(with: dishId)
        
        return DietDish(id: Int(id),
                        dishId: dishId,
                        eaten: eaten,
                        name: simpleDish.name,
                        weightInGrams: dishWeight,
                        products: simpleDish.products,
                        dishRecipe: simpleDish.dishRecipe,
                        nutriens: simpleDish.nutriens,
                        imageURL: simpleDish.imageURL)
    }
}
