//
//  DishModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//
//

import Foundation
import CoreData


extension DishModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DishModel> {
        return NSFetchRequest<DishModel>(entityName: "DishModel")
    }

    @NSManaged public var id: Int16
    @NSManaged public var dishId: String?
    @NSManaged public var dishWeight: Float
    @NSManaged public var eaten: Bool

}

extension DishModel : Identifiable {

}
