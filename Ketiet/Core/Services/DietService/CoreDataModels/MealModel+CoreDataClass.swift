//
//  MealModel+CoreDataClass.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData

@objc(MealModel)
public class MealModel: NSManagedObject {

	func makeMeal() -> DietMeal? {
		guard let name = name,
			let dishes = dishes?.array as? [DishModel] else {
			assertionFailure("Name and dishes should exist")
			return nil
		}
		return DietMeal(id: Int(id),
						name: name,
						dishes: dishes.compactMap { $0.makeDishModel() })
	}
}
