//
//  MealModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData


extension MealModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MealModel> {
        return NSFetchRequest<MealModel>(entityName: "MealModel")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var dishes: NSOrderedSet?

}

// MARK: Generated accessors for dishes
extension MealModel {

    @objc(insertObject:inDishesAtIndex:)
    @NSManaged public func insertIntoDishes(_ value: DishModel, at idx: Int)

    @objc(removeObjectFromDishesAtIndex:)
    @NSManaged public func removeFromDishes(at idx: Int)

    @objc(insertDishes:atIndexes:)
    @NSManaged public func insertIntoDishes(_ values: [DishModel], at indexes: NSIndexSet)

    @objc(removeDishesAtIndexes:)
    @NSManaged public func removeFromDishes(at indexes: NSIndexSet)

    @objc(replaceObjectInDishesAtIndex:withObject:)
    @NSManaged public func replaceDishes(at idx: Int, with value: DishModel)

    @objc(replaceDishesAtIndexes:withDishes:)
    @NSManaged public func replaceDishes(at indexes: NSIndexSet, with values: [DishModel])

    @objc(addDishesObject:)
    @NSManaged public func addToDishes(_ value: DishModel)

    @objc(removeDishesObject:)
    @NSManaged public func removeFromDishes(_ value: DishModel)

    @objc(addDishes:)
    @NSManaged public func addToDishes(_ values: NSOrderedSet)

    @objc(removeDishes:)
    @NSManaged public func removeFromDishes(_ values: NSOrderedSet)

}
