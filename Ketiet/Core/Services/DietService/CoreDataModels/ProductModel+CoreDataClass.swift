//
//  ProductModel+CoreDataClass.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData

@objc(ProductModel)
public class ProductModel: NSManagedObject {

	func makeProduct() -> ProductProtocol? {
		guard let name = name,
			  let quantity = Quantity(coreDataType: quantityType, value: quantityValue) else {
			assertionFailure("Incorrect property values")
			return nil
		}
		return Product(name: name, quantity: quantity)
	}
}
