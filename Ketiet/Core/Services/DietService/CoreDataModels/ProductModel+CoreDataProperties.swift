//
//  ProductModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData

extension ProductModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProductModel> {
        return NSFetchRequest<ProductModel>(entityName: "ProductModel")
    }

    @NSManaged public var quantityValue: Float
    @NSManaged public var quantityType: Int16
    @NSManaged public var name: String?
    @NSManaged public var imageURL: URL?
}
