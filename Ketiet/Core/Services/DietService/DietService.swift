//
//  DietService.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 23.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import CoreData
import Combine
import UIKit

final class DietService {
	
	private enum Constants {
		static let currentDietIdKey = "currentDietIdKey"
	}
	
	static let shared = DietService()
	private(set) lazy var notificationCenter = NotificationCenter()
	private var currentDiet: Diet?
	private var currentDietModel: DietModel?
	private var allDiets: [Diet]?
	private var allDietModels: [DietModel]?
	private lazy var persistentContainer = NSPersistentContainer(name: "DietModel")
	private lazy var backgroundContext = persistentContainer.newBackgroundContext()
	private lazy var dietIds = IndexSet()
	private lazy var defaults = UserDefaults.standard
	private lazy var requestingDietGroup = DispatchGroup()
	private let privateQueue = DispatchQueue(label: "DietServiceQueue")
    private let subject = PassthroughSubject<DietMeal, Never>()
    private(set) lazy var publisher = subject.eraseToAnyPublisher()
	
	private init() {
		privateQueue.async {
			self.requestingDietGroup.enter()
			self.persistentContainer.loadPersistentStores { [weak self] _, error in
				if let error = error {
					assertionFailure(error.localizedDescription)
					self?.requestingDietGroup.leave()
				} else {
					self?.loadDiets()
				}
			}
		}
		NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
											   object: nil,
											   queue: .main) { [weak self] _ in
			self?.checkCurrentDietIsFinished()
		}
	}
	
	private func loadDiets() {
		backgroundContext.perform { [weak self] in
			guard let self = self else { return }
			do {
				self.allDietModels = try self.backgroundContext.fetch(DietModel.fetchRequest())
				self.allDietModels?.forEach { self.dietIds.insert( Int($0.id)) }
				self.allDiets = self.allDietModels?.compactMap { $0.makeDiet() }
				let currentDietId = self.defaults.integer(forKey: Constants.currentDietIdKey)
				if let currentDietIndex = self.allDietModels?.firstIndex(where: { $0.id == currentDietId }) {
					self.currentDietModel = self.allDietModels?[currentDietIndex]
					self.currentDiet = self.allDiets?[currentDietIndex]
				}
			} catch {
				assertionFailure(error.localizedDescription)
			}
			self.requestingDietGroup.leave()
		}
	}
	
	private func checkCurrentDietIsFinished() {
		guard let diet = currentDiet, let dietStart = diet.startedDate else { return }
		let start = Calendar.current.startOfDay(for: dietStart)
		let end = Calendar.current.startOfDay(for: Date())
		guard let daysDiff = Calendar.current.dateComponents([.day], from: start, to: end).day,
			  daysDiff > diet.days.count else { return }
		setStatus(.completed(success: true), for: diet) { [weak self] result in
			switch result {
			case .success:
                self?.notificationCenter.post(Notification(name: .didFinishCurrentDietNotification))
			case .failure:
				break
			}
		}
	}
    
    private func cancelCurrentDietIfExists(completion: @escaping (Result<Void, Error>) -> Void) {
        guard let diet = currentDiet else {
            return completion(.success(()))
        }
        setStatus(
            .cancelled,
            for: diet,
            completion: completion
        )
    }
    
    private func appendNewAndMakeCurrent(
        diet: Diet,
        completion: @escaping(Result<Void, Error>) -> Void
    ) {
        backgroundContext.perform { [weak self] in
            guard let self = self else { return }
            let newDietId = (self.dietIds.max() ?? 0) + 1
            self.currentDietModel = diet.dietModel(context: self.backgroundContext, id: Int16(newDietId))
            do {
                try self.backgroundContext.save()
                self.currentDiet = diet
                if let newDiet = self.currentDietModel {
                    self.allDietModels?.append(newDiet)
                }
                self.allDiets?.append(diet)
                self.dietIds.insert(newDietId)
                self.defaults.set(newDietId, forKey: Constants.currentDietIdKey)
                DispatchQueue.main.async {
                    self.notificationCenter.post(name: .didSetCurrentDietNotification, object: self)
                    completion(.success(()))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
}

extension DietService: DietServiceProtocol {
	
	func requestAllDiets(compeltion: @escaping ([DietProtocol]?) -> Void) {
		requestingDietGroup.wait()
		DispatchQueue.main.async {
			compeltion(self.allDiets)
		}
	}
	
	func addDiet(diet: Diet, completion: @escaping ((Result<Void, Error>) -> Void)) {
		requestingDietGroup.wait()
		privateQueue.async {
			let newDietId = (self.dietIds.max() ?? 0) + 1
			guard let newModel = diet.dietModel(context: self.backgroundContext, id: Int16(newDietId)) else {
				DispatchQueue.main.async {
					completion(.failure(NSError(domain: "",
												code: 1,
												userInfo: [NSLocalizedDescriptionKey: "Unable to create diet"])))
				}
				return
			}
			self.backgroundContext.perform { [weak self] in
				self?.backgroundContext.insert(newModel)
				self?.backgroundContext.saveIfNeeded { result in
					switch result {
					case .success:
						self?.allDietModels?.append(newModel)
						self?.allDiets?.append(diet)
						DispatchQueue.main.async {
							completion(.success(()))
						}
					case let .failure(error):
						DispatchQueue.main.async {
							completion(.failure(error))
						}
					}
				}
			}
		}
	}
	
	func requestCurrentDiet(completion: @escaping (DietProtocol?) -> Void) {
		requestingDietGroup.wait()
		completion(self.currentDiet)
	}
    
    func getDiet(withId id: Int16,
                 completion: @escaping(DietProtocol?) -> Void) {
        requestingDietGroup.wait()
        let diet = allDiets?.first(where: { $0.id == id })
        DispatchQueue.main.async {
            completion(diet)
        }
    }
	
	func setCurrentDiet(_ diet: Diet,
						completion: @escaping (Result<Void, Error>) -> Void) {
		diet.status = .inProgress
		diet.startedDate = Date()
		requestingDietGroup.wait()
		privateQueue.async {
			if diet === self.currentDiet {
				self.currentDiet?.status = .cancelled
				self.currentDietModel?.status = DietStatus.cancelled.value
				self.backgroundContext.saveIfNeeded()
                self.notificationCenter.post(name: .didRemoveCurrentDietNotification, object: self)
			}
			if let index = self.allDiets?.firstIndex(where: { $0 === diet }) {
				self.currentDiet = diet
				self.currentDietModel = self.allDietModels?[index]
				self.currentDietModel?.startDate = self.currentDiet?.startedDate
				self.currentDietModel?.status = DietStatus.inProgress.value
				self.backgroundContext.saveIfNeeded()
				self.defaults.set(self.currentDietModel?.id, forKey: Constants.currentDietIdKey)
                self.notificationCenter.post(name: .didSetCurrentDietNotification, object: self)
				completion(.success(()))
			} else {
                self.cancelCurrentDietIfExists { _ in
                    self.appendNewAndMakeCurrent(
                        diet: diet,
                        completion: completion
                    )
                }
			}
		}
	}
	
	func removeDiet(_ diet: DietProtocol, completion: @escaping(Result<Void, Error>) -> Void) {
		guard let index = self.allDiets?.firstIndex(where: { $0 === diet }), let dietModel = allDietModels?[index] else {
			return completion(.failure(NSError(domain: "",
											   code: 1,
											   userInfo: [NSLocalizedDescriptionKey: "Unable to find diet in array"])))
		}
		privateQueue.async {
			self.backgroundContext.perform { [weak self] in
				self?.backgroundContext.delete(dietModel)
				self?.backgroundContext.saveIfNeeded { result in
					switch result {
					case .success:
						self?.allDietModels?.remove(at: index)
						self?.allDiets?.remove(at: index)
						if diet === self?.currentDiet {
							self?.currentDiet = nil
							self?.currentDietModel = nil
							DispatchQueue.main.async {
                                self?.notificationCenter.post(name: .didRemoveCurrentDietNotification, object: self)
							}
						}
					case .failure:
						break
					}
					DispatchQueue.main.async {
						completion(result)
					}
				}
			}
		}
	}
	
	func getCurrentDiet() -> DietProtocol? {
		currentDiet
	}
	
	func getCurrentDay() -> DietDayProtocol? {
		if let index = getCurrentDayIndex() {
			return currentDiet?.days[index]
		}
		return nil
	}
	
	func hasCurrentDiet() -> Bool {
        return defaults.integer(forKey: Constants.currentDietIdKey) != 0
	}
	
	func getCurrentDayIndex() -> Int? {
		guard let currentDiet = currentDiet, let startDate = currentDiet.startedDate else {
			return nil
		}
		switch currentDiet.status {
		case .inProgress:
            let fromDate = Calendar.current.startOfDay(for: startDate)
            let toDate = Calendar.current.startOfDay(for: Date())
            guard let daysOffset = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day else {
                return nil
            }
            if daysOffset >= currentDiet.days.count {
                return currentDiet.days.count - 1
            }
            return daysOffset
		case .completed:
			return currentDiet.days.count - 1
		case .notStarted, .cancelled:
			return nil
		}
	}
	
	func dateByDayIndex(_ dayIndex: Int) -> Date? {
		guard let startDate = currentDiet?.startedDate else {
			return nil
		}
		return Calendar.current.startOfDay(for: startDate).dateByAdding(DateComponents(day: dayIndex),
																		resultUnints: [.day, .month, .year])
	}
	
	func setStatus(_ status: DietStatus,
				   for diet: DietProtocol,
				   completion: @escaping(Result<Void, Error>) -> Void) {
		guard let index = allDiets?.firstIndex(where: { $0 === diet }), diet.status != status else {
			return completion(.failure(NSError(domain: "",
											   code: 1,
											   userInfo: [NSLocalizedDescriptionKey: "Wrong diet or same status"])))
		}
		privateQueue.async {
			self.allDietModels?[index].status = status.value
			self.backgroundContext.saveIfNeeded { result in
				switch result {
				case .success:
					self.allDiets?[index].status = status
					if diet === self.currentDiet, status == .cancelled {
						self.currentDiet = nil
						self.currentDietModel = nil
						self.defaults.removeObject(forKey: Constants.currentDietIdKey)
						DispatchQueue.main.async {
                            self.notificationCenter.post(name: .didRemoveCurrentDietNotification, object: self)
						}
					}
					if status == .inProgress {
						self.currentDiet = self.allDiets?[index]
						self.currentDiet?.startedDate = Date()
						self.currentDietModel = self.allDietModels?[index]
						self.currentDietModel?.startDate = self.currentDiet?.startedDate
						self.backgroundContext.saveIfNeeded()
						DispatchQueue.main.async {
                            self.notificationCenter.post(name: .didSetCurrentDietNotification, object: self)
						}
					}
				case .failure:
					break
				}
				DispatchQueue.main.async {
					completion(result)
				}
			}
		}
	}
    
    func setNewDish(dish: DietDish,
                    dayIndex: Int,
                    mealIndex: Int,
                    completion: @escaping(Result<Void, Error>) -> Void) {
        guard let days = currentDietModel?.days?.array as? [DayModel],
              let meals = days[dayIndex].meals?.array as? [MealModel],
              let dishModel = dish.makeDishModel(context: self.backgroundContext) else {
            return
        }
        
        meals[mealIndex].replaceDishes(at: dish.id, with: dishModel)
                
        privateQueue.async {
            self.backgroundContext.saveIfNeeded { [weak self] result in
                switch result {
                case .success:
                    let meal = self?.currentDiet?.concreteDays[dayIndex].concreteMeals[mealIndex]
                    meal?.concreteDishes[dish.id] = dish
                    if let strMeal = self?.currentDiet?.concreteDays[dayIndex].concreteMeals[mealIndex] {
                        self?.subject.send(strMeal)
                    }
                case let .failure(error):
                    assertionFailure(error.localizedDescription)
                }
                
                DispatchQueue.main.async {
                    completion(result)
                }
            }
        }
    }
	
	func setCurrentDietDishStatus(_ eaten: Bool,
								  for dish: DietDishProtocol,
								  inMeal mealIndex: Int,
								  inDay dayIndex: Int,
								  completion: @escaping(Result<Void, Error>) -> Void) {
        
		guard let days = currentDietModel?.days?.array as? [DayModel],
			let meals = days[dayIndex].meals?.array as? [MealModel],
			let dishes = meals[mealIndex].dishes?.array as? [DishModel] else {
				return
		}
        
        let currentDish = dishes[dish.id]
        currentDish.eaten = eaten
        
        meals[mealIndex].replaceDishes(at: dish.id, with: currentDish)
        
		privateQueue.async {
            self.backgroundContext.saveIfNeeded { [weak self] result in
				switch result {
				case .success:
                    if let prepareDish = self?.currentDiet?
                        .concreteDays[dayIndex]
                        .concreteMeals[mealIndex]
                        .concreteDishes[dish.id] {
                        prepareDish.eaten = eaten
                        self?.currentDiet?
                            .concreteDays[dayIndex]
                            .concreteMeals[mealIndex]
                            .concreteDishes[dish.id] = prepareDish
                    }
                    
                    if let meal = self?.currentDiet?.concreteDays[dayIndex].concreteMeals[mealIndex] {
                        self?.subject.send(meal)
                    }
				case let .failure(error):
					assertionFailure(error.localizedDescription)
				}
                
                DispatchQueue.main.async {
                    completion(result)
                }
			}
		}
	}
	
	func removeAll() {
		defaults.removeObject(forKey: Constants.currentDietIdKey)
		defaults.synchronize()
		dietIds.removeAll()
		backgroundContext.perform { [weak self] in
			let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DietModel")
			let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
			do {
				try self?.backgroundContext.execute(deleteRequest)
				try self?.backgroundContext.save()
				self?.currentDiet = nil
				self?.allDiets = []
				self?.allDietModels = nil
				self?.currentDietModel = nil
			} catch {
				assertionFailure(error.localizedDescription)
			}
		}
	}
}

private extension DietProtocol {
	
	func dietModel(context: NSManagedObjectContext, id: Int16) -> DietModel? {
		guard let model = NSEntityDescription.insertNewObject(forEntityName: "DietModel",
															  into: context) as? DietModel else {
			assertionFailure("Object must be a \(DietModel.self) type")
			return nil
		}
		model.id = id
		model.name = name
		model.startDate = startedDate
		model.status = status.value
		model.addToDays(NSOrderedSet(array: days.compactMap { $0.makeDayModel(context: context)}))
		return model
	}
}

private extension DietDayProtocol {
	
	func makeDayModel(context: NSManagedObjectContext) -> DayModel? {
		guard let model = NSEntityDescription.insertNewObject(forEntityName: "DayModel",
															  into: context) as? DayModel else {
			assertionFailure("Object must be a \(DayModel.self) type")
			return nil
		}
		model.id = Int16(id)
		model.addToMeals(NSOrderedSet(array: meals.compactMap { $0.makeMealModel(context: context) } ))
		return model
	}
}

private extension DietMealProtocol {
	
	func makeMealModel(context: NSManagedObjectContext) -> MealModel? {
		guard let model = NSEntityDescription.insertNewObject(forEntityName: "MealModel",
															  into: context) as? MealModel else {
			assertionFailure("Object must be a \(MealModel.self) type")
			return nil
		}
		model.id = Int16(id)
		model.name = name
		model.addToDishes(NSOrderedSet(array: dishes.compactMap{ $0.makeDishModel(context: context) }))
		return model
	}
}

private extension DietDishProtocol {
	
	func makeDishModel(context: NSManagedObjectContext) -> DishModel? {
		guard let model = NSEntityDescription.insertNewObject(forEntityName: "DishModel",
															  into: context) as? DishModel else {
			assertionFailure("Object must be a \(DishModel.self) type")
			return nil
		}
        
        model.id = Int16(id)
        model.dishId = dishId
        model.eaten = eaten
        model.dishWeight = weightInGrams
		return model
	}
}

private extension ProductProtocol {
	
	func makeProduct(context: NSManagedObjectContext) -> ProductModel? {
		guard let model = NSEntityDescription.insertNewObject(forEntityName: "ProductModel",
															  into: context) as? ProductModel else {
			assertionFailure("Object must be a \(ProductModel.self) type")
			return nil
		}
		model.name = name
		model.quantityType = quantity.typeValueForCoreData()
		model.quantityValue = quantity.valueForCoreData()
		return model
	}
}
