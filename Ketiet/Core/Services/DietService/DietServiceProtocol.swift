//
//  DietService.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation
import Combine

extension Notification.Name {
    static let didRemoveCurrentDietNotification = Notification.Name("didRemoveCurrentDietNotification")
    static let didSetCurrentDietNotification = Notification.Name("didSetCurrentDietNotification")
    static let didFinishCurrentDietNotification = Notification.Name("didFinishCurrentDiet")
}

protocol DietServiceProtocol {
    var publisher: AnyPublisher<DietMeal, Never> { get }
	var notificationCenter: NotificationCenter { get }
	func requestCurrentDiet(completion: @escaping(DietProtocol?) -> Void)
	func setCurrentDiet(_ diet: Diet,
						completion: @escaping(Result<Void, Error>) -> Void)
	func removeDiet(_ diet: DietProtocol, completion: @escaping(Result<Void, Error>) -> Void)
	func requestAllDiets(compeltion: @escaping([DietProtocol]?) -> Void)
	func addDiet(diet: Diet,
				 completion: @escaping(Result<Void, Error>) -> Void)
	func getCurrentDiet() -> DietProtocol?
    func getDiet(withId id: Int16,
                 completion: @escaping(DietProtocol?) -> Void)
	func getCurrentDay() -> DietDayProtocol?
	func hasCurrentDiet() -> Bool
	func getCurrentDayIndex() -> Int?
	func dateByDayIndex(_ dayIndex: Int) -> Date?
	func setStatus(_ status: DietStatus,
				   for diet: DietProtocol,
				   completion: @escaping(Result<Void, Error>) -> Void)
	func setCurrentDietDishStatus(_ eaten: Bool,
								  for dish: DietDishProtocol,
								  inMeal mealIndex: Int,
								  inDay dayIndex: Int,
								  completion: @escaping(Result<Void, Error>) -> Void)
    func setNewDish(dish: DietDish,
                    dayIndex: Int,
                    mealIndex: Int,
                    completion: @escaping(Result<Void, Error>) -> Void)
	func removeAll()
}
