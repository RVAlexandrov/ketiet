//
//  Diet.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class Diet: DietProtocol {
	
    let id: Int16
	let name: String
	var days: [DietDayProtocol] {
		concreteDays
	}
	let concreteDays: [DietDay]
	var startedDate: Date?
	var status: DietStatus
	
    init(id: Int16,
         name: String,
         days: [DietDay],
         startedDate: Date?,
         status: DietStatus) {
        self.id = id
		self.name = name
		self.concreteDays = days
		self.startedDate = startedDate
		self.status = status
	}
	
	func newDietCopy() -> Diet {
        return Diet(id: id + 1,
                    name: name,
                    days: concreteDays,
                    startedDate: nil,
                    status: .notStarted)
	}
}
