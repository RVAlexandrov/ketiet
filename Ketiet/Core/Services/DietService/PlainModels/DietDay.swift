//
//  Day.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class DietDay: DietDayProtocol {
	let id: Int
	var meals: [DietMealProtocol] {
		concreteMeals
	}
	let concreteMeals: [DietMeal]
	
	init(id: Int, meals: [DietMeal]) {
		self.id = id
		self.concreteMeals = meals
	}
}
