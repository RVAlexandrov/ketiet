//
//  DietDayProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol DietDayProtocol {
	var id: Int { get }
	var meals: [DietMealProtocol] { get }
}

extension DietDayProtocol {
    var isEaten: Bool {
        meals.filter{ !$0.isEaten }.isEmpty
    }
    
    func getDayCalories() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.calories }
    }
    
    func getDayFats() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.fats }
    }
    
    func getDayCarbohydrates() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.carbohydrates }
    }
    
    func getDayProteins() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.proteins }
    }
    
    func getAlreadyCalories() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.eatenCalories }
    }
    
    func getAlreadyFats() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.eatenFats }
    }
    
    func getAlreadyCarbohydrates() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.eatenCarbohydrates }
    }
    
    func getAlreadyProteins() -> Float {
		meals.reduce(into: Float(0)) { $0 += $1.eatenProteins }
    }
}
