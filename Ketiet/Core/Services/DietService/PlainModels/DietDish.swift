//
//  DietDish.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class DietDish: DietDishProtocol {    
	let imageURL: URL?
	let id: Int
    let dishId: String
	var eaten: Bool
	let name: String
	var weightInGrams: Float
	let products: [ProductProtocol]
	let dishRecipe: DishRecipe
	let nutriens: FoodNutrients
	
	init(id: Int,
         dishId: String,
		 eaten: Bool,
		 name: String,
		 weightInGrams: Float,
		 products: [ProductProtocol],
         dishRecipe: DishRecipe,
		 nutriens: FoodNutrients,
		 imageURL: URL?) {
		self.id = id
        self.dishId = dishId
		self.eaten = eaten
		self.name = name
		self.weightInGrams = weightInGrams
		self.products = products
		self.dishRecipe = dishRecipe
		self.nutriens = nutriens
		self.imageURL = imageURL
	}
	
	convenience init(id: Int, dish: DishProtocol, imageURL: URL?) {
        self.init(id: id,
                  dishId: dish.dishId,
				  eaten: false,
				  name: dish.name,
				  weightInGrams: dish.weightInGrams,
				  products: dish.products,
				  dishRecipe: dish.dishRecipe,
				  nutriens: dish.nutriens,
				  imageURL: imageURL)
	}
}
