//
//  DietDishProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol DietDishProtocol: DishProtocol {
	var id: Int { get }
	var eaten: Bool { get set }
}
