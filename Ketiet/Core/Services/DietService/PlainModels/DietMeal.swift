//
//  DietMeal.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class DietMeal: DietMealProtocol {
	let id: Int
	let name: String
	var dishes: [DietDishProtocol] {
		concreteDishes
	}
	var concreteDishes: [DietDish]
	
	init(id: Int,
		 name: String,
		 dishes: [DietDish]) {
		self.id = id
		self.name = name
		self.concreteDishes = dishes
	}
}
