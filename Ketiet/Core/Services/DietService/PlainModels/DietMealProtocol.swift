//
//  DietMealProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol DietMealProtocol {
	var id: Int { get }
	var name: String { get }
	var dishes: [DietDishProtocol] { get }
}

extension DietMealProtocol {
    var isEaten: Bool {
        dishes.filter{ !$0.eaten }.isEmpty
    }
    
	var calories: Float {
        dishes.reduce(into: 0) { $0 += $1.weightedNutrients.calories }
	}
    
    var fats: Float {
        dishes.reduce(into: 0) { $0 += $1.weightedNutrients.fat }
    }
    
    var carbohydrates: Float {
        dishes.reduce(into: 0) { $0 += $1.weightedNutrients.carbohydrates }
    }
    
    var proteins: Float {
        dishes.reduce(into: 0) { $0 += $1.weightedNutrients.proteins }
    }
    
    var eatenCalories: Float {
        dishes.filter{ $0.eaten }.reduce(into: 0) { $0 += $1.weightedNutrients.calories }
    }
    
    var eatenFats: Float {
        dishes.filter{ $0.eaten }.reduce(into: 0) { $0 += $1.weightedNutrients.fat }
    }
    
    var eatenCarbohydrates: Float {
        dishes.filter{ $0.eaten }.reduce(into: 0) { $0 += $1.weightedNutrients.carbohydrates }
    }
    
    var eatenProteins: Float {
        dishes.filter{ $0.eaten }.reduce(into: 0) { $0 += $1.weightedNutrients.proteins }
    }
}
