//
//  File.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol DietProtocol: AnyObject {
    var id: Int16 { get }
	var name: String { get }
	var days: [DietDayProtocol] { get }
	var startedDate: Date? { get }
	var status: DietStatus { get }
	func newDietCopy() -> Diet
}

extension DietProtocol {
	func calories() -> Float {
		days.reduce(into: Float(0)) { $0 += $1.getDayCalories() }
	}
	
	func fats() -> Float {
		days.reduce(into: Float(0)) { $0 += $1.getDayFats() }
	}
	
	func proteins() -> Float {
		days.reduce(into: Float(0)) { $0 += $1.getDayProteins() }
	}
	
	func carbohydrates() -> Float {
		days.reduce(into: Float(0)) { $0 += $1.getDayCarbohydrates() }
	}
}

extension DietProtocol {
    func dayCalories() -> Float {
        days.first?.getDayCalories() ?? 0
    }
    
    func dayFats() -> Float {
        days.first?.getDayFats() ?? 0
    }
    
    func dayProteins() -> Float {
        days.first?.getDayProteins() ?? 0
    }
    
    func dayCarbohydrates() -> Float {
        days.first?.getDayCarbohydrates() ?? 0
    }
}
