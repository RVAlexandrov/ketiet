//
//  DietStatus.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum DietStatus: Equatable {
	case notStarted
	case inProgress
	case cancelled
	case completed(success: Bool)
	
	init?(int: Int16) {
		switch int {
		case 0:
			self = .notStarted
		case 1:
			self = .inProgress
		case 2:
			self = .cancelled
		case 3:
			self = .completed(success: false)
		case 4:
			self = .completed(success: true)
		default:
			return nil
		}
	}
	
	var value: Int16 {
		switch self {
		case .notStarted:
			return 0
		case .inProgress:
			return 1
		case .cancelled:
			return 2
		case let .completed(success):
			return success ? 4 : 3
		}
	}
	
	var description: String {
		switch self {
		case .cancelled:
			return "CANCELLED".localized()
		case let .completed(success):
			return success ? "COMPLETED".localized() : "FAILED".localized()
		case .inProgress:
			return "IN_PROGRESS".localized()
		case .notStarted:
			return "NOT_STARTED".localized()
		}
	}
	
	var color: UIColor {
		switch self {
		case .notStarted:
			return .systemGray
		case .inProgress:
			return .accentColor
		case let .completed(success):
			return success ? .systemGreen : .systemRed
		case .cancelled:
			return .systemYellow
		}
	}
}
