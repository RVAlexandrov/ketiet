//
//  DishProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol DishProviderProtocol {
    var dishesCount: Int { get }
    var dishes: [SimpleDish] { get }
	func nextDish() -> DishProtocol?
}
