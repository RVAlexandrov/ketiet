//
//  SingleDishPerMealProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

/// Эта сущность для того случая, когда на каждый прием пищи приходится 1 блюдо
final class SingleDishPerMealProvider {
	
	private let inaccuracy: Float
	private let dishProvider: DishProviderProtocol
	private let meal: Meal
	
	init(inaccuracy: Float, dishProvider: DishProviderProtocol, meal: Meal) {
		self.inaccuracy = inaccuracy
		self.dishProvider = dishProvider
		self.meal = meal
	}
	
	func dish() -> DishProtocol? {
        var counter = dishProvider.dishesCount
		while let dish = dishProvider.nextDish(), counter > 0 {
            counter -= 1
			let targetWeight = meal.nutriens.calories * dish.weightInGrams / dish.nutriens.calories
			dish.weightInGrams = targetWeight
			if dish.nutriens.doesSatisfy(meal.nutriens, with: inaccuracy) {
				return dish
			}
		}
		return nil
	}
}
