//
//  ImageLoader.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ImageLoader {
	
	private lazy var cache: NSCache<NSString, UIImage> = {
		let cache = NSCache<NSString, UIImage>()
		cache.totalCostLimit = 20 * 1000 * 1000
		return cache
	}()
	private lazy var loadingQueue = DispatchQueue(label: "image loading queue")
}

extension ImageLoader: ImageLoaderProtocol {

	func loadImage(by url: URL, completion: @escaping (UIImage?) -> Void) {
		loadingQueue.async {
			if let image = self.cache.object(forKey: url.absoluteString as NSString) {
				DispatchQueue.main.async {
					completion(image)
				}
				return
			}
			guard let data = try? Data(contentsOf: url), let image = UIImage(data: data) else {
				DispatchQueue.main.async {
					completion(nil)
				}
				return
			}
			self.cache.setObject(image, forKey: url.absoluteString as NSString)
			DispatchQueue.main.async {
				completion(image)
			}
		}
	}
}

