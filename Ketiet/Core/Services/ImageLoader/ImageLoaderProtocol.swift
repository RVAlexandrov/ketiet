//
//  ImageLoaderProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol ImageLoaderProtocol {
	
	func loadImage(by url: URL, completion: @escaping(UIImage?) -> Void)
}
