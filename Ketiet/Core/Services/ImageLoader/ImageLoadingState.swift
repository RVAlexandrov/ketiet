//
//  ImageLoadingState.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum ImageLoadingState {
	case waitingForBegin
	case loading
	case loaded(UIImage)
	case loadingFailed(UIImage?)
}
