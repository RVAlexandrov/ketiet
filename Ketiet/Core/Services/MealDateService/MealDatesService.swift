//
//  MealDateService.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 04.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Combine
import Foundation

final class MealDatesService {
    
    private enum Constants {
        static let mealDates = "mealDates"
    }
    
    static let shared = MealDatesService()
    private let userDefaults = UserDefaults.standard
    private(set) lazy var currentDates = loadDates()
    private lazy var datesSubject = PassthroughSubject<[Date], Never>()
    private(set) lazy var updateMealDatesPublisher = datesSubject.eraseToAnyPublisher()
    
    private init() {}
    
    private func loadDates() -> [Date] {
        guard let data = userDefaults.data(forKey: Constants.mealDates),
              let dates = try? JSONDecoder().decode([Date].self, from: data) else {
            return []
        }
        return dates
    }
}

extension MealDatesService: MealDatesServiceProtocol {
    
    func save(dates: [Date]) {
        guard dates != currentDates else { return }
        guard let data = try? JSONEncoder().encode(dates) else {
            assertionFailure("Unable to make Data from \(dates)")
            return
        }
        currentDates = dates
        userDefaults.set(data, forKey: Constants.mealDates)
        datesSubject.send(dates)
    }
}
