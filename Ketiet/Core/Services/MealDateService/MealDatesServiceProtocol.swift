//
//  MealDatesServiceProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 23.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

protocol MealDatesServiceProtocol: MealDatesStorageProtocol, MealDatesUpdaterProtocol {}
