//
//  MealDateServiceProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 04.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

protocol MealDatesStorageProtocol {
    var currentDates: [Date] { get }
    func save(dates: [Date])
}
