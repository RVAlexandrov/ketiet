//
//  MealDatesUpdaterProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 23.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Combine
import Foundation

protocol MealDatesUpdaterProtocol {
    var updateMealDatesPublisher: AnyPublisher<[Date], Never> { get }
}
