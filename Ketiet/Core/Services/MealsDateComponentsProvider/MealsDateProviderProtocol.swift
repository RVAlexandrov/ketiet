//
//  MealsDateComponentsProviderProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol MealsDateProviderProtocol {
	func dateForMeal(at index: Int) -> Date
}
