//
//  File.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 15.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Combine
import UserNotifications
import UIKit

final class NotificationsService: NSObject {
	
	private struct Constants  {
		static let notificationsEnabled = "NotificationsService.notificationsEnabled"
		static let maxPendingRequestsCount = 64
		static let categoryId = "DietMealNotification"
		static let daysDifferenceToScheduleAgain = 2
	}
	
	static var shared = NotificationsService()
	var notificationsEnabled = UserDefaults.standard.bool(forKey: Constants.notificationsEnabled) {
		didSet {
			if !notificationsAllowed {
				return
			}
			if notificationsEnabled == oldValue {
				return
			}
			privateQueue.addOperation {
				self.userDefaults.set(self.notificationsEnabled,
									  forKey: Constants.notificationsEnabled)
				self.notificationsEnabled ? self.scheduleNotifications() : self.disableNotifications()
			}
		}
	}
	private(set) var onlyPossibleToGetAccessInSettings = false
	private(set) var notificationsAllowed = false {
		didSet {
			if oldValue != notificationsAllowed {
                NotificationCenter.default.post(name: .didChangeNotificationsAllowedNotification, object: nil)
			}
		}
	}
	private let userDefaults = UserDefaults.standard
	private let privateQueue = OperationQueue()
	private var observers = [NSObjectProtocol]()
	private lazy var dietService = DietService.shared
    private lazy var mealDatesService = MealDatesService.shared
	private var lastUpdatedDate = Date()
    private var mealDatesSubscription: AnyCancellable?
	
	override init() {
		super.init()
        UNUserNotificationCenter.current().delegate = self
		addNotificationObservers()
		updateNotificationsAllowed()
        addMealDatesSubscriber()
	}
	
	private func updateNotificationsAllowed() {
		UNUserNotificationCenter.current().getNotificationSettings { [weak self] settings in
			switch settings.authorizationStatus {
			case .authorized:
				 self?.notificationsAllowed = true
				self?.onlyPossibleToGetAccessInSettings = false
			case .denied, .ephemeral:
				self?.onlyPossibleToGetAccessInSettings = true
				self?.notificationsAllowed = false
			case .notDetermined, .provisional:
				self?.onlyPossibleToGetAccessInSettings = false
				self?.notificationsAllowed = false
			@unknown default:
				assertionFailure()
			}
		}
	}
	
	private func disableNotifications() {
		UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
	}
	
	private func scheduleNotifications() {
		if !notificationsAllowed || !notificationsEnabled {
			return
		}
		addCategoriesAndRequests()
	}
	
	private func addCategoriesAndRequests() {
		UNUserNotificationCenter.current().getNotificationCategories { [weak self] categories in
			if categories.isEmpty {
                self?.addCategory()
			}
            self?.addNotificationRequests()
		}
	}
    
    private func addCategory() {
        let category = UNNotificationCategory(
            identifier: Constants.categoryId,
            actions: [],
            intentIdentifiers: [],
            hiddenPreviewsBodyPlaceholder: ""
        )
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }
    
    private func addNotificationRequests() {
        dietService.requestCurrentDiet { diet in
            guard let diet = diet else { return }
            UNUserNotificationCenter.current().getPendingNotificationRequests { [weak self] requests in
                self?.scheduleRequests(for: diet, pendingRequests: requests)
            }
        }
    }
	
	private func scheduleRequests(for diet: DietProtocol, pendingRequests: [UNNotificationRequest]) {
		guard let maxNumberOfMealsPerDay = diet.days.map ({ $0.meals.count }).max(),
			var currentDayIndex = dietService.getCurrentDayIndex() else { return }
		let freePlaces = Constants.maxPendingRequestsCount - pendingRequests.count
		if freePlaces < maxNumberOfMealsPerDay {
			return
		}
		let freeDaysCount = freePlaces / maxNumberOfMealsPerDay
		if let lastRequest = pendingRequests.last,
			let lastDayIndexString = lastRequest.identifier.components(separatedBy: ":").first?.components(separatedBy: "_").last,
			let lastDayIndex = Int(lastDayIndexString)  {
			currentDayIndex = lastDayIndex + 1
		}
		if currentDayIndex >= diet.days.count - 1 {
			return
		}
		let maxIndex = min(currentDayIndex + freeDaysCount - 1, diet.days.count - 1)
		scheduleRequests(days: Array(diet.days[currentDayIndex...maxIndex]))
	}
	
	private func scheduleRequests(days: [DietDayProtocol]) {
		guard let currentDayIndex = dietService.getCurrentDayIndex() else {
			return
		}
		days.forEach { day in
			let dateComponents = Date().dateComponentsByAdding(DateComponents(day: day.id - currentDayIndex),
															   resultUnints: [.year, .month, .day])
            day.meals.enumerated().forEach { offset, meal in
				let content = UNMutableNotificationContent()
				content.categoryIdentifier = Constants.categoryId
				content.title = meal.name
				content.subtitle = "HAVE_A_BREAK".localized()
				
				var triggerComponents = dateComponents
                triggerComponents.hour = mealDatesService.currentDates[safe: offset]?.hour
				triggerComponents.minute = mealDatesService.currentDates[safe: offset]?.minute
				let trigger = UNCalendarNotificationTrigger(
                    dateMatching: triggerComponents,
                    repeats: false
                )
				
				let request = UNNotificationRequest(
                    identifier: makeRequestIdentifier(dayId: day.id, mealId: meal.id),
					content: content,
					trigger: trigger
                )
				UNUserNotificationCenter.current().add(request) { error in
					if let error = error {
						assertionFailure(error.localizedDescription)
					}
				}
			}
		}
		lastUpdatedDate = Date()
	}
	
	private func addNotificationObservers() {
        let center = NotificationCenter.default
        let removeDietObserver = center.addObserver(
            forName: .didRemoveCurrentDietNotification,
            object: nil,
            queue: privateQueue
        ) { [weak self] _ in
            self?.disableNotifications()
		}
        let setDietObserver = center.addObserver(
            forName: .didSetCurrentDietNotification,
            object: nil,
            queue: privateQueue
        ) { [weak self] _ in
            guard let self = self else { return }
            self.disableNotifications()
            if self.notificationsEnabled {
                self.scheduleNotifications()
            }
		}
		let enterForegroundObserver = center.addObserver(
            forName: UIApplication.willEnterForegroundNotification,
            object: nil,
            queue: privateQueue
        ) { [weak self] _ in
            self?.handleEnterForeground()
		}
		let didFinishLaunchingObserver = center.addObserver(
            forName: UIApplication.didFinishLaunchingNotification,
            object: nil,
            queue: privateQueue
        ) { [weak self] _ in
            self?.scheduleNotifications()
		}
		observers.append(contentsOf: [removeDietObserver,
                                      setDietObserver,
                                      enterForegroundObserver,
                                      didFinishLaunchingObserver])
	}
    
    private func addMealDatesSubscriber() {
        mealDatesSubscription = mealDatesService.updateMealDatesPublisher.sink { [weak self] _ in
            guard let self = self else { return }
            if !self.notificationsAllowed || !self.notificationsEnabled {
                return
            }
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            self.addNotificationRequests()
        }
    }
	
	private func handleEnterForeground() {
		updateNotificationsAllowed()
		if let days = Calendar.current.dateComponents([.day], from: lastUpdatedDate, to: Date()).day,
			days > Constants.daysDifferenceToScheduleAgain {
			scheduleNotifications()
		}
	}
    
    private func makeRequestIdentifier(
        dayId: Int,
        mealId: Int
    ) -> String {
        "day_\(dayId):meal_\(mealId)"
    }
}

extension NotificationsService: NotificationsServiceProtocol {
	
	func requestAccess() {
		UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert, .badge, .sound]
        ) { [weak self] granted, error in
			if let error = error {
				assertionFailure(error.localizedDescription)
				return
			}
			self?.notificationsAllowed = granted
			self?.onlyPossibleToGetAccessInSettings = !granted
		}
	}
}

extension NotificationsService: UNUserNotificationCenterDelegate {
	func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        completionHandler([.banner, .list, .badge, .sound])
	}

    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
		completionHandler()
	}
}
