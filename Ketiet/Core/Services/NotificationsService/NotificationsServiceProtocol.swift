//
//  NotificationsServiceProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 15.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol NotificationsServiceProtocol: AnyObject {
	
	var notificationsEnabled: Bool { get set }
	var notificationsAllowed: Bool { get }
	var onlyPossibleToGetAccessInSettings: Bool { get }
	func requestAccess()
}

extension Notification.Name {
    static let didChangeNotificationsAllowedNotification = Notification.Name("didChangeNotificationsAllowedNotification")
}
