//
//  ProfileModel+CoreDataClass.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import CoreData
import UIKit
import DietCore

@objc(ProfileModel)
public class ProfileModel: NSManagedObject {
    func makeProfile(
        tallMeasurementService: MeasurementServiceProtocol,
        weightMeasurementService: MeasurementServiceProtocol
    ) -> Profile? {
        guard let widgetTypesModels = widgetConfiguration?.array as? [WidgetTypeModel] else {
            assertionFailure("bad ProfileModel")
            return nil
        }
        
        var widgetTypes = widgetTypesModels.compactMap { WidgetType(rawValue: $0.name ?? "weight") }
        if widgetTypes.isEmpty {
			widgetTypes = WidgetType.allCases
        }
		var image: UIImage?
		if let data = self.image {
			image = UIImage(data: data as Data)
		} else {
			let assembly = UserProfileImageServiceAssembly()
			let randomImage = assembly.makeUserProfileImageService().userProfileImage()
			image = randomImage
			DispatchQueue.main.async {
				ProfileService.instance.setProfileImage(randomImage, completion: { _ in })
			}
		}
		
        return Profile(
            tall: Double(tallMeasurementService.convertToLocale(value: Float(initialTall))),
            weight: Double(weightMeasurementService.convertToLocale(value: Float(initialWeight))),
            gender: Gender(rawValue: gender ?? Gender.male.rawValue),
            goal: Goal(rawValue: initGoal ?? Goal.saveWeight.rawValue),
            name: name,
            surname: surname,
            age: Int(age),
            physicalActivity: PhysicalActivity(rawValue: initPhysicalActivity ?? PhysicalActivity.no.rawValue),
            widgetConfiguration: widgetTypes,
            image: image,
            caloriesCalculatorType: CaloriesCalculatorType(rawValue: caloriesCalculatorType ?? CaloriesCalculatorType.harrisBenedict.rawValue),
            dailyCalories: Float(dailyCalories)
        )
    }
}
