//
//  ProfileModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData


extension ProfileModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProfileModel> {
        return NSFetchRequest<ProfileModel>(entityName: "ProfileModel")
    }

    @NSManaged public var age: Int16
    @NSManaged public var gender: String?
    @NSManaged public var id: Int16
    @NSManaged public var initGoal: String?
    @NSManaged public var initialTall: Double
    @NSManaged public var initialWeight: Double
    @NSManaged public var initPhysicalActivity: String?
    @NSManaged public var name: String?
    @NSManaged public var surname: String?
    @NSManaged public var widgetConfiguration: NSOrderedSet?
	@NSManaged public var image: NSData?
    @NSManaged public var caloriesCalculatorType: String?
    @NSManaged public var dailyCalories: Double
}

// MARK: Generated accessors for widgetConfiguration
extension ProfileModel {

    @objc(insertObject:inWidgetConfigurationAtIndex:)
    @NSManaged public func insertIntoWidgetConfiguration(_ value: WidgetTypeModel, at idx: Int)

    @objc(removeObjectFromWidgetConfigurationAtIndex:)
    @NSManaged public func removeFromWidgetConfiguration(at idx: Int)

    @objc(insertWidgetConfiguration:atIndexes:)
    @NSManaged public func insertIntoWidgetConfiguration(_ values: [WidgetTypeModel], at indexes: NSIndexSet)

    @objc(removeWidgetConfigurationAtIndexes:)
    @NSManaged public func removeFromWidgetConfiguration(at indexes: NSIndexSet)

    @objc(replaceObjectInWidgetConfigurationAtIndex:withObject:)
    @NSManaged public func replaceWidgetConfiguration(at idx: Int, with value: WidgetTypeModel)

    @objc(replaceWidgetConfigurationAtIndexes:withWidgetConfiguration:)
    @NSManaged public func replaceWidgetConfiguration(at indexes: NSIndexSet, with values: [WidgetTypeModel])

    @objc(addWidgetConfigurationObject:)
    @NSManaged public func addToWidgetConfiguration(_ value: WidgetTypeModel)

    @objc(removeWidgetConfigurationObject:)
    @NSManaged public func removeFromWidgetConfiguration(_ value: WidgetTypeModel)

    @objc(addWidgetConfiguration:)
    @NSManaged public func addToWidgetConfiguration(_ values: NSOrderedSet)

    @objc(removeWidgetConfiguration:)
    @NSManaged public func removeFromWidgetConfiguration(_ values: NSOrderedSet)

}

extension ProfileModel : Identifiable {

}
