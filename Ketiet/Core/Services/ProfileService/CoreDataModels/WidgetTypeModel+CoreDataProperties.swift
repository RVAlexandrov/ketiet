//
//  WidgetTypeModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData


extension WidgetTypeModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WidgetTypeModel> {
        return NSFetchRequest<WidgetTypeModel>(entityName: "WidgetTypeModel")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var profile: ProfileModel?

}

extension WidgetTypeModel : Identifiable {

}
