//
//  Profile.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

final class Profile {
    var tall: Double?
    var weight: Double?
    var gender: Gender?
    var goal: Goal?
    var name: String?
    var surname: String?
    var age: Int?
    var physicalActivity: PhysicalActivity?
    var widgetConfiguration: [WidgetType]?
	var image: UIImage?
    var caloriesCalculatorType: CaloriesCalculatorType?
    var dailyCalories: Float?
    
    convenience init() {
        self.init(
            tall: nil,
            weight: nil,
            gender: nil,
            goal: nil,
            name: nil,
            surname: nil,
            age: nil,
            physicalActivity: nil,
            widgetConfiguration: nil,
            image: nil,
            caloriesCalculatorType: nil,
            dailyCalories: nil
        )
    }
    
    init(tall: Double?,
         weight: Double?,
         gender: Gender?,
         goal: Goal?,
         name: String?,
         surname: String?,
         age: Int?,
         physicalActivity: PhysicalActivity?,
         widgetConfiguration: [WidgetType]?,
		 image: UIImage?,
         caloriesCalculatorType: CaloriesCalculatorType?,
         dailyCalories: Float?) {
        self.tall = tall
        self.weight = weight
        self.gender = gender
        self.goal = goal
        self.name = name
        self.surname = surname
        self.age = age
        self.physicalActivity = physicalActivity
        self.widgetConfiguration = widgetConfiguration
		self.image = image
        self.caloriesCalculatorType = caloriesCalculatorType
        self.dailyCalories = dailyCalories
    }
}

extension Profile: ProfileProtocol {}
