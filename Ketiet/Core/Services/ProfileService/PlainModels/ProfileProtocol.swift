//
//  ProfileProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 06.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

protocol ProfileProtocol: AnyObject {
	var tall: Double? { get }
	var weight: Double?  { get }
	var gender: Gender?  { get }
	var goal: Goal?  { get }
	var name: String?  { get }
	var surname: String?  { get }
	var age: Int?  { get }
	var physicalActivity: PhysicalActivity?  { get }
	var image: UIImage?  { get }
    var caloriesCalculatorType: CaloriesCalculatorType? { get }
    var dailyCalories: Float? { get }
}
