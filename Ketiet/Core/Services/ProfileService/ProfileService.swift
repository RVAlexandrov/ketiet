//
//  ProfileService.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Combine
import CoreData
import UIKit

protocol ProfileServiceGetProtocol: AnyObject {
	var profileRegistered: Bool { get set }
	var imagePublisher: AnyPublisher<UIImage, Never> { get }
    var namePublisher: AnyPublisher<String, Never> { get }
    func getWeight(completion: @escaping (Double?) -> Void)
    func getGender(completion: @escaping (Gender?) -> Void)
    func getGoal(completion: @escaping (Goal?) -> Void)
    func getName(completion: @escaping (String?) -> Void)
    func getSurname(completion: @escaping (String?) -> Void)
    func getAge(completion: @escaping (Int?) -> Void)
    func getPhysicalActivity(completion: @escaping (PhysicalActivity?) -> Void)
    func getWidgetConfiguration(completion: @escaping ([WidgetType]?) -> Void)
	func getProfileImage(completion: @escaping(UIImage?) -> Void)
	func getProfile(completion: @escaping(ProfileProtocol?) -> Void)
    func getTargetWater(completion: @escaping (Double) -> Void)
    func getCaloriesCalculatorType(completion: @escaping (CaloriesCalculatorType?) -> Void)
    func getDailyCalories(completion: @escaping(Float?) -> Void)
}

protocol InitialProfileService {
    func setInitialTall(_ value: Double,
                        completion: @escaping (Result<Void, Error>) -> Void)
    func setInitialWeight(_ value: Double,
                          completion: @escaping (Result<Void, Error>) -> Void)
    func setInitialGoal(_ value: Goal,
                        completion: @escaping (Result<Void, Error>) -> Void)
    func setInitialPhysicalActivity(_ value: PhysicalActivity,
                                    completion: @escaping (Result<Void, Error>) -> Void)
    func setWidgetConfiguration(widgetTypes: [WidgetType],
                                completion: @escaping (Result<Void, Error>) -> Void)
}

protocol ProfileServiceProtocol: ProfileServiceGetProtocol, InitialProfileService {
    func setGender(_ value: Gender,
                   completion: @escaping (Result<Void, Error>) -> Void)
    func setAge(_ value: Int,
                completion: @escaping (Result<Void, Error>) -> Void)
    func setProfileImage(_ value: UIImage,
                         completion: @escaping (Result<Void, Error>) -> Void)
    func setName(_ value: String,
                 completion: @escaping (Result<Void, Error>) -> Void)
    func setCaloriesCalculatorType(_ value: CaloriesCalculatorType,
                                   completion: @escaping (Result<Void, Error>) -> Void)
    func setDailyCalories(_ value: Float,
                          completion: @escaping (Result<Void, Error>) -> Void)

	func removeProfile()
}

final class ProfileService {
	
	private enum Constants {
		static let profileRegisteredKey = "profileRegistered"
	}
	
    static let instance = ProfileService()
    private init() {
        dispatchGroup.enter()
        loadPersistentStores { [weak self] _ in
            self?.dispatchGroup.leave()
        }
    }
    
	private let imageSubject = PassthroughSubject<UIImage, Never>()
    private let nameSubject = PassthroughSubject<String, Never>()
    private lazy var persistentContainer = NSPersistentContainer(name: "ProfileModel")
    private lazy var backgroundContext = persistentContainer.newBackgroundContext()
    private lazy var dispatchGroup = DispatchGroup()
	private lazy var weightMeasurementService = MeasurementService(type: .weight)
	private lazy var tallMeasurementService = MeasurementService(type: .tall)
    
    private var profileModel: ProfileModel?
	private var profile: Profile?

	var imagePublisher: AnyPublisher<UIImage, Never> {
        imageSubject.eraseToAnyPublisher()
	}
    
    var namePublisher: AnyPublisher<String, Never> {
        nameSubject.eraseToAnyPublisher()
    }
}

extension ProfileService: ProfileServiceProtocol {
    func setDailyCalories(_ value: Float,
                          completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.dailyCalories = Double(value)
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.dailyCalories = value
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func getDailyCalories(completion: @escaping (Float?) -> Void) {
        dispatchGroup.wait()
        completion(profile?.dailyCalories)
    }
    
	var profileRegistered: Bool {
		set {
			UserDefaults.standard.set(newValue, forKey: Constants.profileRegisteredKey)
			UserDefaults.standard.synchronize()
		}
		get {
			UserDefaults.standard.bool(forKey: Constants.profileRegisteredKey)
		}
	}
    
    func setCaloriesCalculatorType(_ value: CaloriesCalculatorType,
                                   completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.caloriesCalculatorType = value.rawValue
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.caloriesCalculatorType = value
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func getCaloriesCalculatorType(completion: @escaping (CaloriesCalculatorType?) -> Void) {
        dispatchGroup.wait()
        completion(profile?.caloriesCalculatorType)
    }
	
    func getWidgetConfiguration(completion: @escaping ([WidgetType]?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.widgetConfiguration)
    }
    
    func setWidgetConfiguration(widgetTypes: [WidgetType],
                                completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        let set = NSOrderedSet(array: widgetTypes.enumerated().compactMap { makeWidgetTypeModel(context: backgroundContext,
                                                                                                id: $0.offset,
                                                                                                type: $0.element.rawValue) })
        profileModel?.widgetConfiguration = set
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.widgetConfiguration = widgetTypes
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func getTargetWater(completion: @escaping (Double) -> Void) {
        dispatchGroup.wait()
        let waterCalculator = DailyWaterConsumptionCalculator()
        let waterVolume = waterCalculator.targetWaterVolume(
            physicalActivity: profile?.physicalActivity,
            isMale: profile?.gender == .male,
            weight: profile?.weight
        )
        completion(waterVolume)
    }
    
	func getProfile(completion: @escaping (ProfileProtocol?) -> Void) {
		dispatchGroup.wait()
		completion(profile)
	}
	
	func getProfileImage(completion: @escaping (UIImage?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.image)
	}
    
    func getWeight(completion: @escaping (Double?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.weight)
    }
    
    func getGender(completion: @escaping (Gender?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.gender)
    }
    
	func getGoal(completion: @escaping (Goal?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.goal)
    }
    
    func getName(completion: @escaping (String?) -> Void) {
		dispatchGroup.wait()
		completion(profileModel?.name)
    }
    
    func getSurname(completion: @escaping (String?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.surname)
    }
    
    func getAge(completion: @escaping (Int?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.age)
    }
    
    func getPhysicalActivity(completion: @escaping (PhysicalActivity?) -> Void) {
		dispatchGroup.wait()
		completion(profile?.physicalActivity)
    }
    
    func setInitialTall(_ value: Double,
                        completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.initialTall = Double(tallMeasurementService.convertToStorage(value: Float(value)))
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.tall = value
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func setInitialWeight(_ value: Double,
                          completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.initialWeight = Double(weightMeasurementService.convertToStorage(value: Float(value)))
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.weight = value
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func setInitialGoal(_ value: Goal,
                        completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.initGoal = value.rawValue
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.goal = value
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func setInitialPhysicalActivity(_ value: PhysicalActivity,
                                    completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.initPhysicalActivity = value.rawValue
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.physicalActivity = value
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func setGender(_ value: Gender,
                   completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.gender = value.rawValue
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.gender = value
            case .failure:
                break
            }
            completion($0)
        }
    }
    
    func setAge(_ value: Int,
                completion: @escaping (Result<Void, Error>) -> Void) {
        dispatchGroup.wait()
        profileModel?.age = Int16(value)
        backgroundContext.saveIfNeeded {
            switch $0 {
            case .success:
                self.profile?.age = value
            case .failure:
                break
            }
            completion($0)
        }
    }
	
	func setName(_ value: String,
				 completion: @escaping (Result<Void, Error>) -> Void) {
		dispatchGroup.wait()
		profileModel?.name = value
		backgroundContext.saveIfNeeded { [weak self] result in
			switch result {
			case .success:
				self?.profile?.name = value
                self?.nameSubject.send(value)
			case .failure:
				break
			}
			completion(result)
		}
	}
	
	func setProfileImage(_ value: UIImage, completion: @escaping (Result<Void, Error>) -> Void) {
		dispatchGroup.wait()
		profileModel?.image = value.pngData() as NSData?
		backgroundContext.saveIfNeeded { [weak self] result in
			switch result {
			case .success:
				self?.profile?.image = value
				self?.imageSubject.send(value)
			case .failure:
				break
			}
			completion(result)
		}
	}
	
	func removeProfile() {
		backgroundContext.perform { [weak self] in
            let widgetFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WidgetTypeModel")
            let widgetDeleteRequest = NSBatchDeleteRequest(fetchRequest: widgetFetchRequest)
			let profileFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ProfileModel")
			let profileDeleteRequest = NSBatchDeleteRequest(fetchRequest: profileFetchRequest)
			do {
                try self?.backgroundContext.execute(widgetDeleteRequest)
				try self?.backgroundContext.execute(profileDeleteRequest)
				self?.profileRegistered = false
                self?.profileModel = nil
                self?.profile = nil
                self?.createNewProfileModel()
			} catch {
				assertionFailure(error.localizedDescription)
			}
		}
	}
}

// MARK: - Private methods
extension ProfileService {
    
    private func loadCurrentProfile(completion: @escaping() -> Void) {
        backgroundContext.perform { [weak self] in
            guard let self = self else { return }
            do {
                let profiles = try self.backgroundContext.fetch(ProfileModel.fetchRequest())
                self.profileModel = profiles.first
                self.profile = self.profileModel?.makeProfile(
                    tallMeasurementService: self.tallMeasurementService,
                    weightMeasurementService: self.weightMeasurementService
                )
            } catch {
                assertionFailure(error.localizedDescription)
            }
			completion()
        }
    }
    
    private func makeWidgetTypeModel(context: NSManagedObjectContext,
                                     id: Int,
                                     type: String) -> WidgetTypeModel? {
        guard let model = NSEntityDescription.insertNewObject(forEntityName: "WidgetTypeModel",
                                                              into: context) as? WidgetTypeModel else {
            assertionFailure("Object must be a \(WidgetTypeModel.self) type")
            return nil
        }
        model.id = Int16(id)
        model.name = type
        return model
    }
    
    private func loadPersistentStores(completion: @escaping (Result<Void, Error>) -> Void) {
        persistentContainer.loadPersistentStores { [weak self] _, error in
            if let error = error {
                
                assertionFailure(error.localizedDescription)
                completion(.failure(error))
                
            } else {
                
                // если в данный момент нету модели
                if self?.profileModel == nil {
                    
                    // пытаемся взять из CoreData
                    self?.loadCurrentProfile(completion: {
                        
                        // если её нету
						if self?.profile == nil {
                            // создаём новую
                            self?.createNewProfileModel(completion: completion)
                        // если есть, то говорим что готовы к работе
                        } else {
                            completion(.success(()))
                        }
                        
                    })
                    
                // если есть модель, то готовы к работе
                } else {
                    completion(.success(()))
                }
            }
        }
    }
    
    private func profileModel(context: NSManagedObjectContext) -> ProfileModel? {
        guard let model = NSEntityDescription.insertNewObject(forEntityName: "ProfileModel",
                                                              into: context) as? ProfileModel else {
            assertionFailure("Object must be a \(ProfileModel.self) type")
            return nil
        }
        model.id = 0
        return model
    }
    
    private func createNewProfileModel(completion: ((Result<Void, Error>) -> Void)? = nil) {
        backgroundContext.perform { [weak self] in
            guard let self = self else { return }
            self.profileModel = self.profileModel(context: self.backgroundContext)
            do {
                try self.backgroundContext.save()
                self.profile = self.profileModel?.makeProfile(
                    tallMeasurementService: self.tallMeasurementService,
                    weightMeasurementService: self.weightMeasurementService
                )
                completion?(.success(()))
            } catch {
                completion?(.failure(error))
            }
        }
    }
}
