//
//  DayProgressModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//
//

import Foundation
import CoreData


extension DayProgressModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DayProgressModel> {
        return NSFetchRequest<DayProgressModel>(entityName: "DayProgressModel")
    }

    @NSManaged public var dayIndex: Int16
    @NSManaged public var value: Float
    @NSManaged public var progress: ProgressModel?

}
