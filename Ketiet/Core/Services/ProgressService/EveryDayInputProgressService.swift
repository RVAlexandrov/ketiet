//
//  ProgressService.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Combine
import CoreData
import UIKit

final class EveryDayInputProgressService {
	
	static let waterProgressService = EveryDayInputProgressService(type: .water, dietService: DietService.shared)
	static let weightProgressService = EveryDayInputProgressService(type: .weight, dietService: DietService.shared)
	static let chestProgressService = EveryDayInputProgressService(type: .chest, dietService: DietService.shared)
	static let waistProgressService = EveryDayInputProgressService(type: .waist, dietService: DietService.shared)
	static let hipsProgressService = EveryDayInputProgressService(type: .hips, dietService: DietService.shared)
    
    static func generatePreviousProgressService(type: ProgressValueType,
                                                dietId: Int16?) -> EveryDayInputProgressService {
        EveryDayInputProgressService(type: type,
                                     dietService: DietService.shared,
                                     dietId: dietId)
    }
    
    private let subject = PassthroughSubject<Float, Never>()
    
    var publisher: AnyPublisher<Float, Never> {
        subject.eraseToAnyPublisher()
    }
    
	private static let container = NSPersistentContainer(name: "EveryDayInputProgress")
	private static var uglyFlag = false
	private static let uglyGroup = DispatchGroup()
	private static let initializingQueue = DispatchQueue(label: "ProgressSerivces init queue")
	
    let type: ProgressValueType
    private let measurementService: MeasurementServiceProtocol
    private let dietId: Int16?
	private let dietService: DietServiceProtocol
	private let persistentContainer = EveryDayInputProgressService.container
	private let group = EveryDayInputProgressService.uglyGroup
	private var currentProgress: ProgressModel?
	
	private init(type: ProgressValueType,
				 dietService: DietServiceProtocol,
                 dietId: Int16? = nil) {
		self.type = type
        self.dietService = dietService
        self.dietId = dietId
        measurementService = type.measurementService
        dietService.notificationCenter.addObserver(forName: .didSetCurrentDietNotification,
                                                   object: nil,
                                                   queue: .main) { [weak self] _ in
            self?.fetchProgress()
        }
        
		if EveryDayInputProgressService.uglyFlag {
			return
		}
		EveryDayInputProgressService.uglyFlag = true
		EveryDayInputProgressService.initializingQueue.async {
			self.group.enter()
			self.persistentContainer.loadPersistentStores { [weak self] _, error in
				if let error = error {
					assertionFailure(error.localizedDescription)
				}
				self?.group.leave()
			}
			self.group.enter()
			dietService.requestCurrentDiet { [weak self] _ in
				self?.group.leave()
			}
        }
	}
	
    private func fetchProgress() {
		do {
			let progresses = try persistentContainer.viewContext.fetch(ProgressModel.fetchRequest())
            let progressId = dietId ?? self.dietService.getCurrentDiet()?.id
            if let ourProgress = progresses.first(where: { $0.type == self.type.rawValue && $0.dietId == progressId }) {
				self.currentProgress = ourProgress
			} else {
				insertNewProgress()
			}
		} catch {
			assertionFailure(error.localizedDescription)
		}
	}
	
	private func insertNewProgress() {
		guard let diet = dietService.getCurrentDiet(),
			diet.days.count > 0,
			let entity = NSEntityDescription.entity(forEntityName: "ProgressModel", in: persistentContainer.viewContext) else {
			return
		}
		let model = ProgressModel(entity: entity, insertInto: persistentContainer.viewContext)
		model.type = type.rawValue
        model.dietId = diet.id
		let range = Range(uncheckedBounds: (lower: 0, upper: diet.days.count))
		let days: [DayProgressModel] = range.compactMap {
			guard let day = NSEntityDescription.insertNewObject(forEntityName: "DayProgressModel",
																into: persistentContainer.viewContext) as? DayProgressModel else {
				assertionFailure("day should be a \(DayProgressModel.self) type")
				return nil
			}
			day.dayIndex = Int16($0)
			return day
		}
		model.addToDays(NSOrderedSet(array: days))
		currentProgress = model
		do {
			try persistentContainer.viewContext.save()
		} catch {
			assertionFailure(error.localizedDescription)
		}
	}
}

extension EveryDayInputProgressService: ProgressServiceProtocol {
    func setValue(_ value: Float, forDay day: Int) {
        group.wait()
        if currentProgress == nil {
            fetchProgress()
        }
        let days = currentProgress?.days?.array as? [DayProgressModel]
        let storageValue = measurementService.convertToStorage(value: value)
        days?[day].value = storageValue
        persistentContainer.viewContext.saveIfNeeded()
        subject.send(storageValue)
    }

	func setCurrentDayValue(_ value: Float) {
		group.wait()
		guard let currentDayIndex = dietService.getCurrentDayIndex() else {
			return
		}
		if currentProgress == nil {
			fetchProgress()
		}
		let days = currentProgress?.days?.array as? [DayProgressModel]
        let storageValue = measurementService.convertToStorage(value: value)
		days?[currentDayIndex].value = storageValue
		persistentContainer.viewContext.saveIfNeeded()
        subject.send(storageValue)
	}
	
	func getCurrentDayValue() -> Float? {
		group.wait()
		guard let currentDayIndex = dietService.getCurrentDayIndex() else {
			return nil
		}
		if currentProgress == nil {
			fetchProgress()
		}
		let days = currentProgress?.days?.array as? [DayProgressModel]
        let value = days?[currentDayIndex].value
        return value.map { measurementService.convertToLocale(value: $0) }
	}
	
	func getAllValues() -> [Float]? {
		group.wait()
		if !dietService.hasCurrentDiet() {
			return nil
		}
		if currentProgress == nil {
			fetchProgress()
		}
		let days = currentProgress?.days?.array as? [DayProgressModel]
        return days?.map { measurementService.convertToLocale(value: $0.value) }
	}
    
    func getValuesDueToday() -> [Float]? {
        group.wait()
        if !dietService.hasCurrentDiet() {
            return nil
        }
        if currentProgress == nil {
            fetchProgress()
        }
        
        let days = currentProgress?.days?.array as? [DayProgressModel]
        
        let isCurentCurrentProgressIsActive = currentProgress?.dietId == dietService.getCurrentDiet()?.id
        
        if isCurentCurrentProgressIsActive {
            let todayIndex = dietService.getCurrentDayIndex()
            return days?[0...(todayIndex ?? 28)].map { measurementService.convertToLocale(value: $0.value) }
        } else {
            return days?.map { measurementService.convertToLocale(value: $0.value) }
        }
    }
}
