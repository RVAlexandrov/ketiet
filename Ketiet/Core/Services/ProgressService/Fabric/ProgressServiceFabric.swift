//
//  ProgressServiceFabric.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

final class ProgressServiceFabric { }

extension ProgressServiceFabric: ProgressServiceFabricProtocol {
    func makeProgressService(forType type: ProgressValueType) -> ProgressServiceProtocol {
        switch type {
        case .chest:
            return EveryDayInputProgressService.chestProgressService
        case .hips:
            return EveryDayInputProgressService.hipsProgressService
        case .waist:
            return EveryDayInputProgressService.waistProgressService
        case .water:
            return EveryDayInputProgressService.waterProgressService
        case .weight:
            return EveryDayInputProgressService.weightProgressService
        }
    }
    
    func makePreviousProgressService(forType type: ProgressValueType,
                                     dietId: Int16) -> EveryDayInputProgressService {
        EveryDayInputProgressService.generatePreviousProgressService(type: type,
                                                                     dietId: dietId)
    }
}
