//
//  ProgressService.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation
import Combine

protocol ProgressServiceProtocol {
	func setCurrentDayValue(_ value: Float)
	func getCurrentDayValue() -> Float?
	func getAllValues() -> [Float]?
    func getValuesDueToday() -> [Float]?
    func setValue(_ value: Float, forDay day: Int)
    
    var publisher: AnyPublisher<Float, Never> { get }
    var type: ProgressValueType { get }
}

extension ProgressServiceProtocol {
	func getLastNonNullValueIfPossible() -> Float? {
		if let index = getLastNonNullValueIndex() {
            return getAllValues()?.reversed()[index]
		}
		return nil
	}
	
	func getLastNonNullValueIndex() -> Int? {
		getAllValues()?.reversed().firstIndex { $0 > 0 }
	}
	
	func getFirstNonNullValueIndex() -> Int? {
		getAllValues()?.firstIndex { $0 > 0 }
	}
	
	func getFirstNonNullValue() -> Float? {
		if let index = getFirstNonNullValueIndex() {
			return getAllValues()?[index]
		}
		return nil
	}

	func diffBetweenStartAndEnd() -> Float {
		let start = getFirstNonNullValue() ?? 0
		let end = getLastNonNullValueIfPossible() ?? 0
		return end - start
	}
}
