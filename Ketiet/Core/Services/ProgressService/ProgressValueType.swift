//
//  ProgressValueType.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import DietCore
import UIKit

enum ProgressValueType: Int16, CaseIterable {
    case water
    case weight
    case chest
    case waist
    case hips
    
    var rawString: String {
        switch self {
        case .water:
            return "WATER"
        case .weight:
            return "WEIGHT"
        case .chest:
            return "CHEST"
        case .hips:
            return "HIPS"
        case .waist:
            return "WAIST"
        }
    }
    
    var titleString: String {
        switch self {
        case .water:
            return "WATER".localized()
        case .weight:
            return "WEIGHT".localized()
        case .chest:
            return "CHEST_VOLUME".localized()
        case .hips:
            return "HIPS_VOLUME".localized()
        case .waist:
            return "WAIST_VOLUME".localized()
        }
    }
    
    var imageString: String {
        switch self {
        case .water:
            return "drop.fill"
        case .weight:
            return "speedometer"
        case .chest:
            return "figure.wave"
        case .hips:
            return "figure.wave"
        case .waist:
            return "figure.wave"
        }
    }
    
    var imageColor: UIColor {
        switch self {
        case .water:
            return .systemBlue
        case .weight:
            return .systemOrange
        case .chest:
            return .systemCyan
        case .hips:
            return .systemPurple
        case .waist:
            return .systemPink
        }
    }
    
    var measurementService: MeasurementServiceProtocol {
        switch self {
        case .weight:
            return MeasurementService(type: .weight)
        case .water:
            return MeasurementService(type: .volume)
        case .chest:
            return MeasurementService(type: .lenght)
        case .waist:
            return MeasurementService(type: .lenght)
        case .hips:
            return MeasurementService(type: .lenght)
        }
    }
    
    var inputTitle: String {
        switch self {
        case .weight:
            return "INPUT_WEIGHT".localized()
        case .water:
            return "INPUT_WATER_TITLE".localized()
        case .chest:
            return "INPUT_CHEST_VOLUME".localized()
        case .waist:
            return "INPUT_WAIST_VOLUME".localized()
        case .hips:
            return "INPUT_HIPS_VOLUME".localized()
        }
    }
    
    init(from bodyMetricType: BodyMetricType) {
        switch bodyMetricType {
        case .chest:
            self = .chest
        case .hips:
            self = .hips
        case .waist:
            self = .waist
        }
    }
}
