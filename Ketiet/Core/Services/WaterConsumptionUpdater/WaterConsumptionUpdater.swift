//
//  WaterConsumptionUpdater.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 03.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import Combine

final class WaterConsumptionUpdater {

    private let storage = WaterConsumptionStorage()
    private var subsciber: AnyCancellable?
    
    init() {
        let progressService = EveryDayInputProgressService.waterProgressService
        let profileService = ProfileService.instance
        let measurementService = MeasurementService(type: .volume)
        subsciber = progressService.publisher.sink { [weak self] _ in
            profileService.getTargetWater { [weak progressService] targetWater in
                let currentWater = progressService?.getCurrentDayValue() ?? 0
                self?.storage.widgetData = WaterConsumptionMetrics(
                    currentWater: Double(measurementService.convertToLocale(value: currentWater)),
                    targetWater: targetWater
                )
            }
        }
    }
}
