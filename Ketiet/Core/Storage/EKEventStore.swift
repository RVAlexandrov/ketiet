//
//  EKEventStore.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 07.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import EventKit

extension EKEventStore {
	
	static let shared = EKEventStore()
}
