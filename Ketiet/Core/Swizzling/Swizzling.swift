//
//  Swizzling.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import ObjectiveC.runtime

func swizzle(klass: AnyClass, originalSelector: Selector, swizzledSelector: Selector) {
	guard let originalMethod = class_getInstanceMethod(klass, originalSelector),
		  let swizzledMethod = class_getInstanceMethod(klass, swizzledSelector) else {
		return
	}
	method_exchangeImplementations(originalMethod, swizzledMethod)
}
