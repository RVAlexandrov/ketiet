//
//  ValuesByDaysCoordinator.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit
import DietCore

final class ValuesByDaysViewControllerFabric {
    func defaultValuesByDaysViewController(type: ProgressValueType,
                                           diet: DietProtocol) -> UIViewController {
        
        ProgressDetailByDaysViewController(
            dataProvider: ProgressDetailByDaysProvider(
                measurementService: getDefaultMeasurementService(type: type),
                diet: diet,
                progressService: EveryDayInputProgressService.generatePreviousProgressService(type: type,
                                                                                              dietId: diet.id)),
            accentColor: getAccentColor(type: type))
    }
}
// MARK: - Private methods
extension ValuesByDaysViewControllerFabric {
    
    private func getDefaultMeasurementService(type: ProgressValueType) -> MeasurementServiceProtocol {
        switch type {
        case .chest, .hips, .waist:
            return MeasurementService(type: .lenght, unitStyle: .short)
        case .water:
            return MeasurementService(type: .volume, unitStyle: .short)
        case .weight:
            return MeasurementService(type: .weight, unitStyle: .short)
        }
    }
    
    private func getAccentColor(type: ProgressValueType) -> UIColor {
        switch type {
        case .chest, .hips, .waist:
            return .bodyMetricAccentColor
        case .water:
            return .waterAccentColor
        case .weight:
            return .weightAccentColor
        }
    }
}
