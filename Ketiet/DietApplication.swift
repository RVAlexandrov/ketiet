//
//  DietApplication.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 28.03.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

final class DietApplication: UIApplication {
    
    private var walkingAvocadoTask: DispatchWorkItem?
    private var interactionState = InteractionState.waiting
    
    override init() {
        super.init()
        scheduleWalkingAvocado(deadline: 7)
    }
    
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        walkingAvocadoTask?.cancel()
        guard interactionState == .waiting else { return }
        scheduleWalkingAvocado()
    }
    
    private func scheduleWalkingAvocado(deadline: TimeInterval = 5) {
        let task = DispatchWorkItem { [weak self] in
            self?.presentWalkingAvocado()
        }
        walkingAvocadoTask = task
        DispatchQueue.main.asyncAfter(deadline: .now() + deadline, execute: task)
    }
    
    private func presentFallingItemsViewController() {
        let controller = FallingItemsViewController(provider: FallingItemsProvider()) { [weak self] in
            guard let controller = self?.getKeyWindow()?.rootViewController else { return }
            RateAppAssembly().showRateAppController(from: controller)
        }
        getKeyWindow()?.rootViewController?.present(
            controller,
            animated: true
        )
    }
    
    private func presentWalkingAvocado() {
        interactionState = .inProgress
        let controller = WalkingAvocadoViewController { [weak self] completed in
            self?.interactionState = .finished
            if completed {
                self?.presentFallingItemsViewController()
            }
        }
        getKeyWindow()?.rootViewController?.present(
            controller,
            animated: true
        )
    }
}
