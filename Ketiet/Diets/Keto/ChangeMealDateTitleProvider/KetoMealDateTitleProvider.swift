//
//  KetoMealDateTitleProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct KetoMealDateTitleProvider {
    
    private let dateService: MealDatesStorageProtocol
    
    init(dateService: MealDatesStorageProtocol) {
        self.dateService = dateService
    }
}

extension KetoMealDateTitleProvider: ChangeMealDateTitleProviderProtocol {
    
    func titleAndDates() -> [ChangeMealDateTitleAndDate] {
        KetoDietMeals.allCases.enumerated().map { index, meal in
            return ChangeMealDateTitleAndDate(
                title: meal.changeDateTitle,
                date: dateService.currentDates[safe: index] ?? meal.defaultDate
            )
        }
    }
}

extension KetoDietMeals {
    
    var defaultDate: Date {
        
        var components = Calendar.current.dateComponents(
            [.year, .month, .day],
            from: Date()
        )
        switch self {
        case .breakfast:
            components.hour = 9
        case .firstBreak:
            components.hour = 11
        case .lunch:
            components.hour = 13
        case .secondBreak:
            components.hour = 15
        case .dinner:
            components.hour = 18
        }
        guard let date = Calendar.current.date(from: components) else {
            assertionFailure("Can not create date from componenets")
            return Date()
        }
        return date
    }
    
    var changeDateTitle: String {
        switch self {
        case .breakfast:
            return "BREAKFAST_TIME".localized()
        case .firstBreak:
            return "FIRST_BREAK_TIME".localized()
        case .lunch:
            return "LUNCH_TIME".localized()
        case .secondBreak:
            return "SECOND_BREAK_TIME".localized()
        case .dinner:
            return "DINNER_TIME".localized()
        }
    }
}
