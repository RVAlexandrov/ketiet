//
//  KetoDayDishes.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 01.06.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

final class KetoDayDishes {
    
    let breakfast, firstBreak, lunch, secondBreak, dinner: DishProtocol
    
    init(breakfast: DishProtocol,
         firstBreak: DishProtocol,
         lunch: DishProtocol,
         secondBreak: DishProtocol,
         dinner: DishProtocol) {
        self.breakfast = breakfast
        self.firstBreak = firstBreak
        self.lunch = lunch
        self.secondBreak = secondBreak
        self.dinner = dinner
    }
}
