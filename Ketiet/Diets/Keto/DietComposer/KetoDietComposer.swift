//
//  KetoDietComposer.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class KetoDietComposer {
	
	private let mealsProvider: CaloriesPerMealProviderProtocol
	private let numberOfDays: Int
	
	init(mealsProvider: CaloriesPerMealProviderProtocol,
		 numberOfDays: Int) {
		self.mealsProvider = mealsProvider
		self.numberOfDays = numberOfDays
	}
}

extension KetoDietComposer: DietComposerProtocol {
    func makeDiet(withId id: Int16) -> Diet {
		let range = Range(uncheckedBounds: (lower: 0, upper: numberOfDays))
		let days = range.map { index -> DietDay in
            self.mealsProvider.createDietDay()
			let meals = mealsProvider.meals().enumerated().map { mealPair -> DietMeal in
				let enumeratedDishes = mealsProvider.dishes(forMealAt: mealPair.offset).enumerated()
				let dishes = enumeratedDishes.map { DietDish(id: $0.offset,
															 dish: $0.element,
															 imageURL: $0.element.imageURL) }
				return DietMeal(id: mealPair.offset,
								name: mealPair.element.name,
								dishes: dishes)
			}
			return DietDay(id: index,
						   meals: meals)
		}
        return Diet(id: id,
                    name: "KETO_DIET".localized(),
                    days: days,
                    startedDate: nil,
                    status: .notStarted)
	}
}
