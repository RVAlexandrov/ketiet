//
//  BreakKetoDishesProvider.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

final class BreakKetoDishesProvider {
    
    private init() {}
    
    static let shared = BreakKetoDishesProvider()
    
    private lazy var queue = SimpleQueue(elements: dishes.shuffled())
    
    private lazy var idGenerator = DishIdGenerator()
    
    private(set) lazy var dishes: [SimpleDish] = [
        SimpleDish(
            dishId: idGenerator.mealId(
                for: .breakMeal,
                index: 0
            ),
            name: "CASHEW".mealLocalized(),
            dishRecipe: DishRecipe(
                weightInGrams: 0,
                cookingSteps: nil
            ),
            nutriens: FoodNutrients(
                proteins: 18,
                carbohydrates: 30,
                fat: 44
            ),
            products: [],
            imageURL: URL(string: "https://i.ibb.co/Q6rVCwP/depositphotos-4059470-stock-photo-ripe-cashew-nuts-with-leaves.jpg")
        ),
        SimpleDish(
            dishId: idGenerator.mealId(
                for: .breakMeal,
                index: 1
            ),
            name: "ALMOND".mealLocalized(),
            dishRecipe: DishRecipe(
                weightInGrams: 0,
                cookingSteps: nil),
                nutriens: FoodNutrients(
                    proteins: 18.6,
                    carbohydrates: 13,
                    fat: 53.7
                ),
            products: [],
            imageURL: URL(string: "https://i.ibb.co/S7qssHB/dfd4eda2073a0b1-730x475.jpg")
        ),
        SimpleDish(
            dishId: idGenerator.mealId(
                for: .breakMeal,
                index: 2
            ),
            name: "WALNUT".mealLocalized(),
            dishRecipe: DishRecipe(
                weightInGrams: 0,
                cookingSteps: nil
            ),
            nutriens: FoodNutrients(
                proteins: 16.2,
                carbohydrates: 11.1,
                fat: 60.8
            ),
            products: [],
            imageURL: URL(string: "https://i.ibb.co/qghdCQw/greckij-otbonyj.jpg")
        ),
        SimpleDish(
            dishId: idGenerator.mealId(
                for: .breakMeal,
                index: 3
            ),
            name: "HAZELNUT".mealLocalized(),
            dishRecipe: DishRecipe(
                weightInGrams: 0,
                cookingSteps: nil
            ),
            nutriens: FoodNutrients(
                proteins: 14.5,
                carbohydrates: 7.0,
                fat: 60.75
            ),
            products: [],
            imageURL: URL(string: "https://i.ibb.co/XtnCsVj/unnamed.jpg")
        ),
        SimpleDish(
            dishId: idGenerator.mealId(
                for: .breakMeal,
                index: 4
            ),
            name: "PARMEZANO_CHEESE".mealLocalized(),
            dishRecipe: DishRecipe(
                weightInGrams: 0,
                cookingSteps: nil
            ),
            nutriens: FoodNutrients(
                proteins: 35.8,
                carbohydrates: 3.22,
                fat: 25
            ),
            products: [],
            imageURL: URL(string: "https://i.ibb.co/wdsRfrz/parmesan-on-cheeseboard-452392.jpg")
        )
    ]
}

extension BreakKetoDishesProvider: DishProviderProtocol {
    
    var dishesCount: Int {
        dishes.count
    }
    
    func nextDish() -> DishProtocol? {
        queue.dequeue()
    }
}
