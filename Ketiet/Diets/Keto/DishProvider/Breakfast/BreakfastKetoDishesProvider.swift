//
//  BreakfastKetoDishesProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class BreakfastKetoDishesProvider {
    
    private init() {}
    
    static let shared = BreakfastKetoDishesProvider()
    
    private lazy var queue = SimpleQueue(elements: dishes.shuffled())
    
    private lazy var idGenerator = DishIdGenerator()
    
    private(set) lazy var dishes: [SimpleDish] = [
        CoconutCreamWithNutsBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 0)),
        SpinachOmeletWithBaconBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 1)),
        SpinachFetaBellPepperSaladBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 2)),
        EggsWithBaconAndTomatoesBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 3)),
        KetoShakshukaBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 4)),
        EggsWithCarbonadeAndCheeseBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 5)),
        KetoMacMaffinBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 6)),
        StuffedPeppersBuilder.makeMeal(id: idGenerator.mealId(for: .breakfast, index: 7))
    ]
}

extension BreakfastKetoDishesProvider: DishProviderProtocol {
    
    var dishesCount: Int {
        dishes.count
    }
    
    func nextDish() -> DishProtocol? {
        queue.dequeue()
    }
}
