//
//  CoconutCreamWithNuts.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct CoconutCreamWithNutsBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        SimpleDish(dishId: id,
                   name: "BREAKFAST_1".mealLocalized(),
                   weightInGrams: 100,
                   dishRecipe: DishRecipe(weightInGrams: 100,
                                          cookingSteps: nil),
                   nutriens: FoodNutrients(proteins: 6,
                                           carbohydrates: 9.7,
                                           fat: 30.4),
                   products: [Product(name: "BERRIES".mealLocalized(), quantity: .grams(19)),
                              Product(name: "ALMOND".mealLocalized(), quantity: .grams(16)),
                              Product(name: "WHIPPED_CREAM_OF_COCONUT_MILK".mealLocalized(), quantity: .grams(65))],
                   imageURL: URL(string: "https://i.ibb.co/2yGSYhL/br1.png"))
    }
}
