//
//  EggsWithBaconAndTomatoes.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 09.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct EggsWithBaconAndTomatoesBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 310
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "BACON_EGGS_TOMATO_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "BACON_EGGS_TOMATO_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "BACON_EGGS_TOMATO_STEP3".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "BACON_EGGS_TOMATO_STEP4".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "BACON_EGGS_TOMATO".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 8.64,
                                    carbohydrates: 1.1,
                                    fat: 15),
            products: [Product(name: "EGG".mealLocalized(),
                               quantity: .count(2)),
                       Product(name: "BACON".mealLocalized(),
                               quantity: .grams(100)),
                       Product(name: "CHERRY_TOMATO".mealLocalized(),
                               quantity: .grams(100))],
            imageURL: URL(string: "https://i.ibb.co/MkFwJ0H/appetizer-fried-egg-with-bacon-857549132-3567x2362-800x530-jpeg.webp"))
    }
}
