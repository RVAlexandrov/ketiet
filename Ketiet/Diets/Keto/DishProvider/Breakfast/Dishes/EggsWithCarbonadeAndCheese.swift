//
//  EggsWithCarbonadeAndCheese.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 10.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct EggsWithCarbonadeAndCheeseBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 245
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CARBONADE_EGGS_CHEESE_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CARBONADE_EGGS_CHEESE_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CARBONADE_EGGS_CHEESE_STEP3".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CARBONADE_EGGS_CHEESE_STEP4".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "CARBONADE_EGGS_CHEESE".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 16.5,
                                    carbohydrates: 1,
                                    fat: 14.2),
            products: [Product(name: "EGG".mealLocalized(),
                               quantity: .count(2)),
                       Product(name: "CARBONADE".mealLocalized(),
                               quantity: .grams(75)),
                       Product(name: "CHEESE".mealLocalized(),
                               quantity: .grams(50)),
                       Product(name: "BUTTER".mealLocalized(),
                               quantity: .grams(10)),
                       Product(name: "SALT".mealLocalized(),
                               quantity: .toTaste)],
            imageURL: URL(string: "https://i.ibb.co/YpzxsKM/2b8229.jpg"))
    }
}
