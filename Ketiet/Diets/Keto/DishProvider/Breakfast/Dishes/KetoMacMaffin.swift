//
//  KetoMacMaffin.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct KetoMacMaffinBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight = 530
        return SimpleDish(
            dishId: id,
            name: "KETO_MCMUFFIN".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: DishRecipe(
                weightInGrams: recipeWeight,
                cookingSteps: [
                    CookingStep(
                        title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                        description: "KETO_MCMUFFIN_1_STEP".mealLocalized()
                    ),
                    CookingStep(
                        title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                        description: "KETO_MCMUFFIN_2_STEP".mealLocalized()
                    ),
                    CookingStep(
                        title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                        description: "KETO_MCMUFFIN_3_STEP".mealLocalized()
                    ),
                    CookingStep(
                        title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                        description: "KETO_MCMUFFIN_4_STEP".mealLocalized()
                    )
                ]
            ),
            nutriens: FoodNutrients(
                proteins: 12.6,
                carbohydrates: 0.9,
                fat: 15.8
            ),
            products: [
                Product(
                    name: "EGG".mealLocalized(),
                    quantity: .count(4)
                ),
                Product(
                    name: "PARMEZANO_CHEESE".mealLocalized(),
                    quantity: .grams(30)
                ),
                Product(
                    name: "CREAM_CHEESE".mealLocalized(),
                    quantity: .grams(100)
                ),
                Product(
                    name: "SALT".mealLocalized(),
                    quantity: .toTaste
                ),
                Product(
                    name: "MINCED_MEAT".mealLocalized(),
                    quantity: .grams(150)
                ),
                Product(
                    name: "CHEESE_FOR_BURGERS".mealLocalized(),
                    quantity: .count(2)
                )
            ],
            imageURL: URL(string: "https://keto-recipes.ru/wp-content/uploads/2019/06/Keto-MakMaffin.jpg")
        )
    }
}
