//
//  KetoShakshuka.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 09.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct KetoShakshukaBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 345
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "KETO_SHAKSHUKA_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "KETO_SHAKSHUKA_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "KETO_SHAKSHUKA_STEP3".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "KETO_SHAKSHUKA".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 4.5,
                                    carbohydrates: 3.5,
                                    fat: 10.6),
            products: [Product(name: "EGG".mealLocalized(),
                               quantity: .count(2)),
                       Product(name: "ONION_POWDER".mealLocalized(),
                               quantity: .teaSpoon(1)),
                       Product(name: "GARLIC_POWDER".mealLocalized(),
                               quantity: .teaSpoon(1)),
                       Product(name: "TOMATO".mealLocalized(),
                               quantity: .count(1)),
                       Product(name: "SALT".mealLocalized(), quantity: .toTaste),
                       Product(name: "PEPPER".mealLocalized(), quantity: .toTaste),
                       Product(name: "CABBAGE".mealLocalized(), quantity: .grams(100)),
                       Product(name: "OLIVE_OIL".mealLocalized(), quantity: .tableSpoon(2))],
            imageURL: URL(string: "https://i.ibb.co/zF9kxqM/Ketoshakshukafor-One.jpg"))
    }
}
