//
//  SpinachFetaBellPepperSalad.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 11.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct SpinachFetaBellPepperSaladBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let weight: Int = 100
        return SimpleDish(dishId: id,
                          name: "SPINACH_BELL_PEPPER_SALAD".mealLocalized(),
                          weightInGrams: Float(weight),
                          dishRecipe: DishRecipe(weightInGrams: weight,
                                                 cookingSteps: nil),
                          nutriens: FoodNutrients(proteins: 8,
                                                  carbohydrates: 12,
                                                  fat: 20),
                          products: [Product(name: "OLIVES".mealLocalized(), quantity: .grams(15)),
                                     Product(name: "FETA_CHEESE".mealLocalized(), quantity: .grams(30)),
                                     Product(name: "SPINACH".mealLocalized(), quantity: .grams(60)),
                                     Product(name: "PINE_NUTS".mealLocalized(), quantity: .grams(10)),
                                     Product(name: "OLIVE_OIL".mealLocalized(), quantity: .teaSpoon(2)),
                                     Product(name: "LIMON_JUICE".mealLocalized(), quantity: .teaSpoon(2)),
                                     Product(name: "BELL_PEPPER".mealLocalized(), quantity: .grams(60))],
                          imageURL: URL(string: "https://i.ibb.co/T1M5tBR/nuts-Salad.png"))
    }
}

