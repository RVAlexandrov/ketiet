//
//  SpinachOmeletWithBacon.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 13.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct SpinachOmeletWithBaconBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let weight: Int = 240
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "SPINACH_BACON_OMELET_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "SPINACH_BACON_OMELET_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "SPINACH_BACON_OMELET_STEP3".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "SPINACH_BACON_OMELET_STEP4".mealLocalized(),
                imageWrapper: nil
            )
        ]
        return SimpleDish(
            dishId: id,
            name: "SPINACH_BACON_OMELET".mealLocalized(),
            weightInGrams: Float(weight),
            dishRecipe: DishRecipe(
                weightInGrams: weight,
                cookingSteps: cookingSteps
            ),
            nutriens: FoodNutrients(
                proteins: 9,
                carbohydrates: 1,
                fat: 18.7
            ),
            products: [
                Product(
                    name: "EGG".mealLocalized(),
                    quantity: .count(15)
                ),
                Product(
                    name: "BACON".mealLocalized(),
                    quantity: .grams(50)
                ),
                Product(
                    name: "SPINACH".mealLocalized(),
                    quantity: .grams(50)
                ),
                Product(name: "BUTTER".mealLocalized(),
                        quantity: .grams(10)
                       ),
                Product(
                    name: "CREAM_35".mealLocalized(),
                    quantity: .grams(50)
                )
            ],
            imageURL: URL(
                string: "https://i.ibb.co/SJnXw1k/omelet-with-spinach-and-green-beans-healthy-food-egg-and-milk-frittata-delicious-breakfast-in-black.webp"
            )
        )
    }
}
