//
//  StuffedPeppers.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct StuffedPeppersBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight = 700
        return SimpleDish(
            dishId: id,
            name: "STUFFED_PAPPERS".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: DishRecipe(
                weightInGrams: recipeWeight,
                cookingSteps: [
                    CookingStep(
                        title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                        description: "STUFFED_PAPPERS_1_STEP".mealLocalized()
                    ),
                    CookingStep(
                        title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                        description: "STUFFED_PAPPERS_2_STEP".mealLocalized()
                    ),
                    CookingStep(
                        title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                        description: "STUFFED_PAPPERS_3_STEP".mealLocalized()
                    ),
                ]
            ),
            nutriens: FoodNutrients(
                proteins: 13.4,
                carbohydrates: 2.5,
                fat: 11.9
            ),
            products: [
                Product(
                    name: "BELL_PEPPER".mealLocalized(),
                    quantity: .count(2)
                ),
                Product(
                    name: "EGG".mealLocalized(),
                    quantity: .count(4)
                ),
                Product(
                    name: "PARMEZANO_CHEESE".mealLocalized(),
                    quantity: .grams(100)
                ),
                Product(
                    name: "MOZZARELLA_CHEESE".mealLocalized(),
                    quantity: .grams(100)
                ),
                Product(
                    name: "RICOTTA_CHEESE".mealLocalized(),
                    quantity: .grams(100)
                ),
                Product(
                    name: "GARLIC_POWDER".mealLocalized(),
                    quantity: .teaSpoon(1)
                ),
                Product(
                    name: "SPINACH".mealLocalized(),
                    quantity: .grams(30)
                ),
            ],
            imageURL: URL(string: "https://keto-recipes.ru/wp-content/uploads/2018/09/Farshirovannye-pertsy-tri-syra-1.jpg")
        )
    }
}
