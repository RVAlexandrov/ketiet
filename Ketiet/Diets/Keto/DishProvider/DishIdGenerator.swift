//
//  DishIdGenerator.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

final class DishIdGenerator {
    
    func mealId(for mealType: MealType, index: Int) -> String {
        switch mealType {
        case .breakMeal:
            return "br\(index)"
        case .breakfast:
            return "bf\(index)"
        case .lunch:
            return "lu\(index)"
        case .dinner:
            return "di\(index)"
        }
    }
    
    func dishProvider(for mealType: MealType) -> DishProviderProtocol {
        switch mealType {
        case .breakMeal:
            return BreakKetoDishesProvider.shared
        case .breakfast:
            return BreakfastKetoDishesProvider.shared
        case .lunch:
            return LunchKetoDishesProvider.shared
        case .dinner:
            return DinnerKetoDishesProvider.shared
        }
    }
    
    func getDish(with id: String) -> DishProtocol {
                
        let mealType = MealType(prefixId: String(id.dropLast(id.count - 2)))
        
        let provider = dishProvider(for: mealType)
        
        let targetIndex = Int(id.dropFirst(2)) ?? 0
        
        if targetIndex <= (provider.dishesCount - 1) {
            return provider.dishes[targetIndex]
        } else {
            assertionFailure("Unable to get dish")
            return SimpleDish(
                dishId: "bloom",
                name: "UNAVAILABLE_DISH".localized(),
                nutriens: .zero
            )
        }
    }
    
    func dishProvider(for id: String) -> DishProviderProtocol {
        let mealType = MealType(prefixId: String(id.dropLast(id.count - 2)))
        
        let provider = dishProvider(for: mealType)
        
        return provider
    }
}

enum MealType {
    case breakfast
    case breakMeal
    case lunch
    case dinner
    
    init(prefixId: String) {
        if prefixId == "br" {
            self = .breakMeal
        } else if prefixId == "bf" {
            self = .breakfast
        } else if prefixId == "lu" {
            self = .lunch
        } else if prefixId == "di" {
            self = .dinner
        } else {
            self = .breakMeal
        }
    }
}
