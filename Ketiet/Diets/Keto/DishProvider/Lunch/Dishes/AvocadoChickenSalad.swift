//
//  AvocadoChickenSalad.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct AvocadoChickenSaladBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let dishWeight: Int = 1390
        let cookingSteps = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "AVOCADO_CHICKEN_SALAD_1_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "AVOCADO_CHICKEN_SALAD_2_STEP".mealLocalized(),
                imageWrapper: nil
            )
        ]
        return SimpleDish(
            dishId: id,
            name: "AVOCADO_CHICKEN_SALAD".mealLocalized(),
            weightInGrams: Float(dishWeight),
            dishRecipe: DishRecipe(
                weightInGrams: dishWeight,
                cookingSteps: cookingSteps
            ),
            nutriens: FoodNutrients(proteins: 8.94,
                                    carbohydrates: 2.62,
                                    fat: 18.13),
            products: [Product(name: "CHICKEN_BREAST".mealLocalized(), quantity: .grams(150)),
                       Product(name: "AVOCADO".mealLocalized(), quantity: .count(2)),
                       Product(name: "EGG".mealLocalized(), quantity: .count(6)),
                       Product(name: "BACON".mealLocalized(), quantity: .grams(290)),
                       Product(name: "GREEK_YOGURT".mealLocalized(), quantity: .tableSpoon(2)),
                       Product(name: "MAYO".mealLocalized(), quantity: .tableSpoon(2)),
                       Product(name: "LIMON_JUICE".mealLocalized(), quantity: .tableSpoon(1)),
                       Product(name: "CILANTRO".mealLocalized(), quantity: .grams(20)),
                       Product(name: "SALT".mealLocalized(), quantity: .toTaste),
                       Product(name: "PEPPER".mealLocalized(), quantity: .toTaste)
                      ],
            imageURL: URL(string: "https://i.ibb.co/KzqtXTV/1554654096-zelenyj-salat-s-kuricej-avokado-i-kukuruznymi-chipsami.jpg")
        )
    }
}
