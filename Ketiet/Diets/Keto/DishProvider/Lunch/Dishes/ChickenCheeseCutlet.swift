//
//  ChickenCheeseCutlet.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct ChickenCheeseCutletBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 1153
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CHICKEN_CHEESE_CUTLET_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CHICKEN_CHEESE_CUTLET_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CHICKEN_CHEESE_CUTLET_STEP3".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "CHICKEN_CHEESE_CUTLET_STEP4".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "CHICKEN_CHEESE_CUTLET".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 20.3,
                                    carbohydrates: 3,
                                    fat: 8),
            products: [Product(name: "CHICKEN_BREAST".mealLocalized(),
                               quantity: .grams(750)),
                       Product(name: "EGG".mealLocalized(),
                               quantity: .count(2)),
                       Product(name: "WHEAT_FLOUR".mealLocalized(),
                               quantity: .grams(30)),
                       Product(name: "CHEDDAR_CHEESE".mealLocalized(),
                               quantity: .grams(100)),
                       Product(name: "PARMEZANO_CHEESE".mealLocalized(),
                               quantity: .grams(50)),
                       Product(name: "SPRING_ONION".mealLocalized(),
                               quantity: .grams(15)),
                       Product(name: "GARLIC".mealLocalized(),
                               quantity: .grams(8)),
                       Product(name: "MAYO".mealLocalized(),
                               quantity: .grams(70)),
                       Product(name: "LIMON_JUICE".mealLocalized(),
                               quantity: .tableSpoon(1))
                      ],
            imageURL: URL(string: "https://i.ibb.co/kxtD5qY/izumitielnyie-kurinyie-kotlietki-s-syrom.webp"))
    }
}
