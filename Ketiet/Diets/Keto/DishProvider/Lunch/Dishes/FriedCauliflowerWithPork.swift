//
//  FriedCauliflowerWithPork.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation


struct FriedCauliflowerWithPorkBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let dishWeight: Int = 1000
        let cookingSteps = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "FRIED_CAULIFLOWER_WITH_PORK_1_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "FRIED_CAULIFLOWER_WITH_PORK_2_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "FRIED_CAULIFLOWER_WITH_PORK_3_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "FRIED_CAULIFLOWER_WITH_PORK_4_STEP".mealLocalized(),
                imageWrapper: nil
            )
        ]
        return SimpleDish(
            dishId: id,
            name: "FRIED_CAULIFLOWER_WITH_PORK".mealLocalized(),
            weightInGrams: Float(dishWeight),
            dishRecipe: DishRecipe(
                weightInGrams: dishWeight,
                cookingSteps: cookingSteps
            ),
            nutriens: FoodNutrients(proteins: 4.28,
                                    carbohydrates: 4.93,
                                    fat: 7.12),
            products: [Product(name: "CAULIFLOWER".mealLocalized(), quantity: .grams(500)),
                       Product(name: "EGG".mealLocalized(), quantity: .count(2)),
                       Product(name: "GARLIC".mealLocalized(), quantity: .grams(8)),
                       Product(name: "PORK_BELLY".mealLocalized(), quantity: .grams(100)),
                       Product(name: "BELL_PEPPER".mealLocalized(), quantity: .count(1)),
                       Product(name: "SPRING_ONION".mealLocalized(), quantity: .grams(100)),
                       Product(name: "SOY_SAUCE".mealLocalized(), quantity: .tableSpoon(1)),
                       Product(name: "PICKLED_GINGER".mealLocalized(), quantity: .grams(20)),
                       Product(name: "BLACK_SESAME".mealLocalized(), quantity: .teaSpoon(1)),
                      ],
            imageURL: URL(string: "https://i.ibb.co/7vfPFyk/keto-fried-rice-chahan-caulirice-1.jpg")
        )
    }
}
