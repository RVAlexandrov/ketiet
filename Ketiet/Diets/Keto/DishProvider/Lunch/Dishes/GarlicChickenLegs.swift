//
//  GarlicChickenLegs.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 29.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct GarlicChickenLegsBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 1050
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "GARLIC_CHICKEN_LEGS_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "GARLIC_CHICKEN_LEGS_STEP2".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "GARLIC_CHICKEN_LEGS".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 14.4,
                                    carbohydrates: 1.9,
                                    fat: 16.5),
            products: [Product(name: "CHICKEN_LEGS".mealLocalized(),
                               quantity: .grams(800)),
                       Product(name: "OLIVE_OIL".mealLocalized(),
                                          quantity: .grams(20)),
                       Product(name: "GARLIC".mealLocalized(),
                                          quantity: .grams(32)),
                       Product(name: "LIMON_JUICE".mealLocalized(),
                                          quantity: .grams(30)),
                       Product(name: "BUTTER".mealLocalized(),
                                          quantity: .grams(50)),
                       Product(name: "SALT".mealLocalized(),
                                          quantity: .toTaste),
                       Product(name: "PEPPER".mealLocalized(),
                                          quantity: .toTaste)
                      ],
            imageURL: URL(string: "https://i.ibb.co/z8wKZXg/ls45v144-orig.jpg"))
    }
}
