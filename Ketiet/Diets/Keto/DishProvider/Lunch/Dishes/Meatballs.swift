//
//  Meatballs.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct MeatballsBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 1100
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "MEATBALLS_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "MEATBALLS_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "MEATBALLS_STEP3".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "MEATBALLS".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 20.3,
                                    carbohydrates: 3,
                                    fat: 8),
            products: [Product(name: "OLIVE_OIL".mealLocalized(),
                               quantity: .tableSpoon(2)),
                       Product(name: "ONION".mealLocalized(),
                               quantity: .grams(37)),
                       Product(name: "ONION".mealLocalized(),
                               quantity: .grams(37)),
                       Product(name: "BEEF_MINCED".mealLocalized(),
                               quantity: .grams(200)),
                       Product(name: "PORK_MINCED".mealLocalized(),
                               quantity: .grams(200)),
                       Product(name: "WHEAT_FLOUR".mealLocalized(),
                               quantity: .grams(25)),
                       Product(name: "EGG".mealLocalized(),
                               quantity: .count(2)),
                       Product(name: "PEPPER".mealLocalized(),
                               quantity: .grams(20)),
                       Product(name: "SALT".mealLocalized(),
                               quantity: .grams(5)),
                       Product(name: "CREAM_35".mealLocalized(),
                               quantity: .grams(250)),
                       Product(name: "WATER".mealLocalized(),
                               quantity: .grams(250))
                      ],
            imageURL: URL(string: "https://i.ibb.co/gWvWPC4/tefteli-iz-farsha-s-risom-1.jpg"))
    }
}
