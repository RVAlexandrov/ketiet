//
//  RoastedPepperCreamSoup.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct RoastedPepperCreamSoupBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let dishWeight: Int = 824
        let cookingSteps = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "ROASTED_PEPPER_CREAM_SOUP_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "ROASTED_PEPPER_CREAM_SOUP_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "ROASTED_PEPPER_CREAM_SOUP_STEP3".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "ROASTED_PEPPER_CREAM_SOUP_STEP4".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "5 " + "STEP_LOWERCASE".mealLocalized(),
                description: "ROASTED_PEPPER_CREAM_SOUP_STEP5".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "6 " + "STEP_LOWERCASE".mealLocalized(),
                description: "ROASTED_PEPPER_CREAM_SOUP_STEP6".mealLocalized(),
                imageWrapper: nil
            )
        ]
        return SimpleDish(
            dishId: id,
            name: "ROASTED_PEPPER_CREAM_SOUP".mealLocalized(),
            weightInGrams: Float(dishWeight),
            dishRecipe: DishRecipe(
                weightInGrams: dishWeight,
                cookingSteps: cookingSteps
            ),
            nutriens: FoodNutrients(proteins: 4.52,
                                    carbohydrates: 4.49,
                                    fat: 10),
            products: [Product(name: "RED_BELL_PEPPER".mealLocalized(), quantity: .count(4)),
                       Product(name: "CREAM_35".mealLocalized(), quantity: .grams(100)),
                       Product(name: "SPRING_ONION".mealLocalized(), quantity: .grams(25)),
                       Product(name: "PARMEZANO_CHEESE".mealLocalized(), quantity: .grams(80)),
                       Product(name: "OLIVE_OIL".mealLocalized(), quantity: .grams(25)),
                       Product(name: "GARLIC".mealLocalized(), quantity: .grams(4)),
                       Product(name: "PEPPER_CHILLI".mealLocalized(), quantity: .grams(5)),
                       Product(name: "SALT".mealLocalized(), quantity: .toTaste),
                       Product(name: "PEPPER".mealLocalized(), quantity: .toTaste),
                       Product(name: "XANTHAN_GUM".mealLocalized(), quantity: .grams(5)),
                      ],
            imageURL: URL(string: "https://i.ibb.co/chj06q2/kr-s-per.jpg")
        )
    }
}
