//
//  SalmonCutlet.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct SalmonCutletBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 674
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "SULMON_CUTLET_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "SULMON_CUTLET_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "SULMON_CUTLET_STEP3".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "SULMON_CUTLET".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 6.8,
                                    carbohydrates: 3.85,
                                    fat: 15.6),
            products: [Product(name: "CANNED_SALMON".mealLocalized(),
                               quantity: .grams(150)),
                       Product(name: "EGG".mealLocalized(),
                               quantity: .count(1)),
                       Product(name: "MAYO".mealLocalized(),
                               quantity: .grams(40)),
                       Product(name: "GARLIC".mealLocalized(),
                               quantity: .grams(4)),
                       Product(name: "GINGER_POWDER".mealLocalized(),
                               quantity: .grams(2.5)),
                       Product(name: "OLIVE_OIL".mealLocalized(),
                               quantity: .tableSpoon(2)),
                       Product(name: "SALT".mealLocalized(),
                               quantity: .toTaste),
                       Product(name: "AVOCADO".mealLocalized(),
                               quantity: .grams(240)),
                       Product(name: "SOUR_CREAM".mealLocalized(),
                               quantity: .grams(80)),
                       Product(name: "CILANTRO".mealLocalized(),
                               quantity: .toTaste),
                       Product(name: "WATER".mealLocalized(),
                               quantity: .tableSpoon(2)),
                       Product(name: "LIMON_JUICE".mealLocalized(),
                               quantity: .tableSpoon(2))
                      ],
            imageURL: URL(string: "https://i.ibb.co/VtTvDvF/kotlety-iz-farsha-lososya-1616070491-22-max.jpg"))
    }
}
