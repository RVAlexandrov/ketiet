//
//  StuffedChickenMozzarella.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct StuffedChickenMozzarellaBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let recipeWeight: Int = 1970
        let cookingSteps: [CookingStep] = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STUFFED_CHICKEN_MOZZARELLA_STEP1".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STUFFED_CHICKEN_MOZZARELLA_STEP2".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STUFFED_CHICKEN_MOZZARELLA_STEP3".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STUFFED_CHICKEN_MOZZARELLA_STEP4".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "5 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STUFFED_CHICKEN_MOZZARELLA_STEP5".mealLocalized(),
                imageWrapper: nil
            )
        ]
        let dishRecipe = DishRecipe(weightInGrams: recipeWeight,
                                    cookingSteps: cookingSteps)
        return SimpleDish(
            dishId: id,
            name: "STUFFED_CHICKEN_MOZZARELLA".mealLocalized(),
            weightInGrams: Float(recipeWeight),
            dishRecipe: dishRecipe,
            nutriens: FoodNutrients(proteins: 18.3,
                                    carbohydrates: 1.74,
                                    fat: 4.6),
            products: [Product(name: "CREAM_CHEESE".mealLocalized(),
                               quantity: .grams(100)),
                       Product(name: "MOZZARELLA_CHEESE".mealLocalized(),
                                          quantity: .grams(100)),
                       Product(name: "SPINACH".mealLocalized(),
                                          quantity: .grams(300)),
                       Product(name: "CHICKEN_BREAST".mealLocalized(),
                                          quantity: .count(1)),
                       Product(name: "OLIVE_OIL".mealLocalized(),
                                          quantity: .tableSpoon(1)),
                       Product(name: "TOMATO_SAUCE".mealLocalized(),
                                          quantity: .grams(60)),
                       Product(name: "SALT".mealLocalized(),
                                          quantity: .toTaste),
                       Product(name: "PEPPER".mealLocalized(),
                                          quantity: .toTaste)
                      ],
            imageURL: URL(string: "https://i.ibb.co/r5cKSLD/otbivnaya-so-shpinatom-06.jpg"))
    }
}
