//
//  TurkeyStroganov.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct TurkeyStroganovBuilder {
    static func makeMeal(id: String) -> SimpleDish {
        let dishWeight: Int = 620
        let cookingSteps = [
            CookingStep(
                title: "1 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STROGANOV_1_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "2 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STROGANOV_2_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "3 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STROGANOV_3_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "4 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STROGANOV_4_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "5 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STROGANOV_5_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "6 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STROGANOV_6_STEP".mealLocalized(),
                imageWrapper: nil
            ),
            CookingStep(
                title: "7 " + "STEP_LOWERCASE".mealLocalized(),
                description: "STROGANOV_7_STEP".mealLocalized(),
                imageWrapper: nil
            )
        ]
        return SimpleDish(
            dishId: id,
            name: "STROGANOV_TURKEY".mealLocalized(),
            weightInGrams: Float(dishWeight),
            dishRecipe: DishRecipe(
                weightInGrams: dishWeight,
                cookingSteps: cookingSteps
            ),
            nutriens: FoodNutrients(proteins: 11.2,
                                    carbohydrates: 2.7,
                                    fat: 1.9),
            products: [Product(name: "TURKEY_CHEST".mealLocalized(), quantity: .grams(300)),
                       Product(name: "ONION".mealLocalized(), quantity: .grams(80)),
                       Product(name: "CHAMPIGNON".mealLocalized(), quantity: .grams(100)),
                       Product(name: "SOUR_CREAM".mealLocalized(), quantity: .grams(80)),
                       Product(name: "TOMATO_PASTE".mealLocalized(), quantity: .grams(30)),
                       Product(name: "VEGETABLE_OIL".mealLocalized(), quantity: .tableSpoon(1))],
            imageURL: URL(string: "https://i.ibb.co/fSjL5HY/1605463015-screenshot-4.jpg")
        )
    }
}
