//
//  LunchKetoDishesProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 17.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class LunchKetoDishesProvider {
    
    private init() {}
    
    static let shared = LunchKetoDishesProvider()
	
    private lazy var queue = SimpleQueue(elements: dishes.shuffled())
    
    private lazy var idGenerator = DishIdGenerator()
    
    private(set) lazy var dishes: [SimpleDish] = [
        
        TurkeyStroganovBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 0)),
        
        ChickenCheeseCutletBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 1)),
        
        BeefStroganovBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 2)),
        
        AvocadoChickenSaladBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 3)),
        
        FriedCauliflowerWithPorkBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 4)),
        
        SalmonCutletBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 5)),
        
        RoastedPepperCreamSoupBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 6)),
        
        MeatballsBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 7)),
        
        StuffedChickenMozzarellaBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 8)),
        
        GarlicChickenLegsBuilder
            .makeMeal(id: idGenerator.mealId(for: .lunch, index: 9))
    ]
}

extension LunchKetoDishesProvider: DishProviderProtocol {
    
    var dishesCount: Int {
        dishes.count
    }
    
    func nextDish() -> DishProtocol? {
        queue.dequeue()
    }
}
