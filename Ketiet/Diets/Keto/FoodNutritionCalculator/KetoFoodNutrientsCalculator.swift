//
//  KetoFoodNutrientsCalculator.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.05.2020.
//  Copyright © 2020 littDev. All rights reserved.

import Foundation
import DietCore

struct KetoFoodNutrientsCalculator {
    private struct Constants {
        static let proteinsPercent: Float = 0.20
        static let fatPercent: Float = 0.75
        static let carbohydratesPercent: Float = 0.05
    }
}

// MARK: - FoodNutrientsCalculatorProtocol
extension KetoFoodNutrientsCalculator: FoodNutrientsCalculatorProtocol {
    func calculateFoodNutrients(withCalories calories: Float) -> FoodNutrients {
        let carbohydratesCalories = calories * Constants.carbohydratesPercent
        let carbohydrates = carbohydratesCalories / CaloriesConstants.caloriesInCarbohydratesGram
        
        let proteinsCalories = calories * Constants.proteinsPercent
        let proteins = proteinsCalories / CaloriesConstants.caloriesInProteinGram
        
        let fatCalories = calories - carbohydratesCalories - proteinsCalories
        let fat = fatCalories / CaloriesConstants.caloriesInFatGram
        return FoodNutrients(proteins: proteins, carbohydrates: carbohydrates, fat: fat)
    }
}
