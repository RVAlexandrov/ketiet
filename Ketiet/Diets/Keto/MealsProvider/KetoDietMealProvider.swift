//
//  KetoDietMealProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 16.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class KetoDietMealProvider {
	
	private struct Constants {
		static let standardInaccuracy: Float = 0.20
	}
	
	private let dailyNutritiens: FoodNutrients
	private let dateProvider: MealsDateProviderProtocol
	private lazy var meals = KetoDietMeals.allCases.enumerated().map {
        $0.element.makeMeal(date: dateProvider.dateForMeal(at: $0.offset))
    }
    private lazy var breakfastDishProvider = BreakfastKetoDishesProvider.shared
	private lazy var lunchDishProvider = LunchKetoDishesProvider.shared
	private lazy var dinnerDishProvider = DinnerKetoDishesProvider.shared
    private lazy var breakDishProvider = BreakKetoDishesProvider.shared
    private lazy var ketoDay = makeDishesForDayAlternate()
	
	init(dailyNutritiens: FoodNutrients, dateProvider: MealsDateProviderProtocol) {
		self.dailyNutritiens = dailyNutritiens
		self.dateProvider = dateProvider
	}
    
    private func makeDishesForDayAlternate() -> KetoDayDishes {
        // 1. Рассчёт средней каллорийности на основной приём пищи (завтрак, обед и ужин)
        
        let countOfMainMeals: Float = 3 // основных приёмов пищи 3 (завтрак, обед и ужин)
        let numberOfBreakMeals: Float = 2 // количество перекусов
        
        // на основные блюда должно приходиться 75 процентов каллорийности
        let averageCalories = dailyNutritiens.calories / countOfMainMeals * 0.75
        
        guard let breakfastDish = breakfastDishProvider.nextDish(),
              let lunchDish = lunchDishProvider.nextDish(),
              let dinnerDish = dinnerDishProvider.nextDish(),
              let firstBreakDish = breakDishProvider.nextDish(),
              let secondBreakDish = breakDishProvider.nextDish() else {
            assertionFailure("Unable to satisfy daily nutrients requirements")
            return KetoDayDishes(breakfast: SimpleDish(dishId: "", name: "", nutriens: .zero),
                                 firstBreak: SimpleDish(dishId: "", name: "", nutriens: .zero),
                                 lunch: SimpleDish(dishId: "", name: "", nutriens: .zero),
                                 secondBreak: SimpleDish(dishId: "", name: "", nutriens: .zero),
                                 dinner: SimpleDish(dishId: "", name: "", nutriens: .zero))
        }
        
        let calculator = DishWeightCalculator()
        breakfastDish.weightInGrams = calculator.getWeightForDish(withTargerCalories: averageCalories,
                                                                  dishCaloriesPer100Gramm: breakfastDish.nutriens.calories,
                                                                  inaccuracy: 0.1)
        
        lunchDish.weightInGrams = calculator.getWeightForDish(withTargerCalories: averageCalories,
                                                              dishCaloriesPer100Gramm: lunchDish.nutriens.calories,
                                                              inaccuracy: 0.1)
        
        dinnerDish.weightInGrams = calculator.getWeightForDish(withTargerCalories: averageCalories,
                                                               dishCaloriesPer100Gramm: dinnerDish.nutriens.calories,
                                                               inaccuracy: 0.1)
        let dailyCalories = dailyNutritiens.calories
        
        let caloriesForAllBreaks = dailyCalories - breakfastDish.weightedNutrients.calories - lunchDish.weightedNutrients.calories - dinnerDish.weightedNutrients.calories
        
        let singleBreakCalories = caloriesForAllBreaks / numberOfBreakMeals
        
        firstBreakDish.weightInGrams = calculator.getWeightForDish(withTargerCalories: singleBreakCalories,
                                                                   dishCaloriesPer100Gramm: firstBreakDish.nutriens.calories,
                                                                   inaccuracy: 0.1)
        
        secondBreakDish.weightInGrams = calculator.getWeightForDish(withTargerCalories: singleBreakCalories,
                                                                    dishCaloriesPer100Gramm: secondBreakDish.nutriens.calories,
                                                                    inaccuracy: 0.1)
                
        return KetoDayDishes(breakfast: breakfastDish,
                             firstBreak: firstBreakDish,
                             lunch: lunchDish,
                             secondBreak: secondBreakDish,
                             dinner: dinnerDish)
    }
}

extension KetoDietMealProvider: CaloriesPerMealProviderProtocol {
    
    func createDietDay() {
        ketoDay = makeDishesForDayAlternate()
    }

	func numberOfMealsPerDay() -> Int {
		return KetoDietMeals.allCases.count
	}
	
	func meal(at index: Int) -> Meal {
		return meals[index]
	}
	
	func dishes(forMealAt index: Int) -> [DishProtocol] {
		switch KetoDietMeals.allCases[index] {
		case .breakfast:
            return [ketoDay.breakfast]
		case .lunch:
            return [ketoDay.lunch]
		case .dinner:
            return [ketoDay.dinner]
        case .firstBreak:
            return [ketoDay.firstBreak]
        case .secondBreak:
            return [ketoDay.secondBreak]
        }
	}
}
