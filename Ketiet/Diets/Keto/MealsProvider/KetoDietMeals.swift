//
//  KetoDietMeals.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 01.06.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

enum KetoDietMeals: CaseIterable {
    case breakfast
    case firstBreak
    case lunch
    case secondBreak
    case dinner
    
    func makeMeal(date: Date) -> Meal {
        switch self {
        case .breakfast:
            return Meal(name: "BREAKFAST".localized(),
                        date: date,
                        nutriens: .zero)
        case .firstBreak:
            return Meal(name: "FIRST_BREAK".localized(),
                        date: date,
                        nutriens: .zero)
        case .lunch:
            return Meal(name: "LUNCH".localized(),
                        date: date,
                        nutriens: .zero)
        case .secondBreak:
            return Meal(name: "SECOND_BREAK".localized(),
                        date: date,
                        nutriens: .zero)
        case .dinner:
            return Meal(name: "DINNER".localized(),
                        date: date,
                        nutriens: .zero)
        }
    }
}
