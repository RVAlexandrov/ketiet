//
//  InteractionState.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 28.03.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

enum InteractionState {
    case waiting
    case inProgress
    case finished
}
