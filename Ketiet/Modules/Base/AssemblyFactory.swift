//
//  AssemblyFactory.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

struct AssemblyFactory {
	
	func generateAssembly<Assembly: BaseAssembly>(moduleConfigurator: Assembly.ModuleConfigurator) -> Assembly {
        return Assembly(moduleConfigurator: moduleConfigurator)
	}
}
