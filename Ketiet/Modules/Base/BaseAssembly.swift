//
//  BaseAssembly.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol BaseAssembly {
	
	associatedtype ModuleConfigurator
	
	init(moduleConfigurator: ModuleConfigurator)
	
    func initialViewController() -> UIViewController
}
