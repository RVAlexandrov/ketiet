//
//  BodyMassIndexDetailsAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 13/12/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class BodyMassIndexDetailsAssembly {

    func makeViewController(
        weightProgressService: ProgressServiceProtocol,
        profile: ProfileProtocol,
        bmiCalculator: BodyMassIndexCalculatorProtocol
    ) -> UIViewController {
        let model = BodyMassIndexDetailsModel(
            weightProgressService: weightProgressService,
            profile: profile,
            bmiCalculator: bmiCalculator
        )
        let router = BodyMassIndexDetailsRouter()
        let controller = BodyMassIndexDetailsViewController(model: model, router: router)
        router.viewController = controller
        model.view = controller

        return controller
    }
}
