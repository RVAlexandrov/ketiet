//
//  BodyMassIndexDetailsModel.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 13/12/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation
import DietCore

final class BodyMassIndexDetailsModel {

    weak var view: BodyMassIndexDetailsViewProtocol?
    
    private let weightProgressService: ProgressServiceProtocol
    private let profile: ProfileProtocol
    private let bmiCalculator: BodyMassIndexCalculatorProtocol
    
    init(weightProgressService: ProgressServiceProtocol,
         profile: ProfileProtocol,
         bmiCalculator: BodyMassIndexCalculatorProtocol) {
        self.weightProgressService = weightProgressService
        self.profile = profile
        self.bmiCalculator = bmiCalculator
    }
}

// MARK: - BodyMassIndexDetailsModelProtocol
extension BodyMassIndexDetailsModel: BodyMassIndexDetailsModelProtocol {
    func viewDidLoad() {
        view?.setupView()
        configureModel()
    }
}

// MARK: - Private methods
extension BodyMassIndexDetailsModel {
    func configureModel() {
        guard let weight = profile.weight,
              let tall = profile.tall else { return }
        
        var sections: [LittTableViewSectionProtocol] = []
        
        let lastWeight = weightProgressService.getLastNonNullValueIfPossible() ?? Float(weight)
        let weightMeasurementService = MeasurementService(type: .weight)
        let tallMeasurementService = MeasurementService(type: .tall)
        let convertedTall = tallMeasurementService
            .convertToStorage(value: Float(tall))
            .convert(
                from: MeasurementType.tall.metricUnit,
                to: UnitLength.meters
            )
        let rawIndex = bmiCalculator.calculateRawBMI(withWeight: lastWeight, tall: convertedTall)
        let rawIndexString = String(rawIndex.round(to: 1))
        let index = bmiCalculator.calculateBMI(withWeight: lastWeight, tall: convertedTall)
        let currentIndexSection = LittTableViewSection(
            header: SystemHeaderItem(text: "BMI_TITLE".localized()),
            items: [
                IconDescriptionCellItem(
                    viewModel: IconDescriptionCell.ViewModel(
                        systemImageString: ImageStringType.system("person.fill"),
                        imageColor: index.associatedColor,
                        title: index.fullString,
                        description: "\("BMI_SHORT".localized()) = \(rawIndexString)"
                    )
                )
            ],
            footer: nil
        )
        
        sections.append(currentIndexSection)
        
        let nearestRawIndexType = bmiCalculator.nearestPositiveTendetionIndex(
            withWeight: lastWeight,
            tall: convertedTall
        )
        
        if let nearestType = nearestRawIndexType.type, let diffValue = nearestRawIndexType.value {
            let localValueString = String(diffValue.round(to: 1)) + " " + weightMeasurementService.measurement()
            let diffString = diffValue > 0 ? "+ \(localValueString)" : "\(localValueString)"
            let nearestNextLevelSection = LittTableViewSection(
                header: SystemHeaderItem(text: "BMI_NEAREST".localized()),
                items: [
                    IconDescriptionCellItem(
                        viewModel: IconDescriptionCell.ViewModel(
                            systemImageString: ImageStringType.system("target"),
                            imageColor: nearestType.associatedColor,
                            title: nearestType.fullString,
                            description: "\("BMI_ACHIVE_DESCRIPTION".localized()) \(diffString)"
                        )
                    )
                ],
                footer: nil
            )
            
            sections.append(nearestNextLevelSection)
        }
        
        let items: [InformationCellItem] = BodyMassIndex.allCases.map { makeInformationCellItem(fromBmiType: $0) }
        let tableSection = LittTableViewSection(
            header: SystemHeaderItem(text: "BMI_TABLE".localized()),
            items: items,
            footer: LinkFooterItem(
                text: "BMI_INFO_LINK".localized(),
                linkURLString: "https://www.euro.who.int/en/health-topics/disease-prevention/nutrition/a-healthy-lifestyle/body-mass-index-bmi"
            )
        )
        
        sections.append(tableSection)
        
        view?.setDataModels(sections: sections)
    }
}

// MARK: - Private methods
extension BodyMassIndexDetailsModel {
    func makeInformationCellItem(fromBmiType type: BodyMassIndex) -> InformationCellItem {
        InformationCellItem(title: type.fullString,
                            systemImageString: "circle.fill",
                            imageColor: type.associatedColor)
    }
}
