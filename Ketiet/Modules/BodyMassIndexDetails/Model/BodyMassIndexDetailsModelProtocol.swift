//
//  BodyMassIndexDetailsModelProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 13/12/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

protocol BodyMassIndexDetailsModelProtocol: AnyObject {
    func viewDidLoad()
}
