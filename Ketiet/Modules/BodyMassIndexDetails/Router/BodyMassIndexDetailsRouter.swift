//
//  BodyMassIndexDetailsRouter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 13/12/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class BodyMassIndexDetailsRouter {
    
    weak var viewController: UIViewController?
}

// MARK: - BodyMassIndexDetailsRouterProtocol
extension BodyMassIndexDetailsRouter: BodyMassIndexDetailsRouterProtocol {
}
