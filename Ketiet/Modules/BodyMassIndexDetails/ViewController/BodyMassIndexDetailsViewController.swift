//
//  BodyMassIndexDetailsViewController.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 13/12/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class BodyMassIndexDetailsViewController: UIViewController {
    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    private let model: BodyMassIndexDetailsModelProtocol
    private let router: BodyMassIndexDetailsRouterProtocol

    init(model: BodyMassIndexDetailsModelProtocol, router: BodyMassIndexDetailsRouterProtocol) {
        self.model = model
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds,
                                      style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.animations = []
        return tableView
    }()

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        model.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = view.frame
    }
}

// MARK: - BodyMassIndexDetailsViewProtocol
extension BodyMassIndexDetailsViewController: BodyMassIndexDetailsViewProtocol {
    func setupView() {
        title = "BMI_LONG".localized()
        view.backgroundColor = .littBackgroundColor
        view.addSubview(tableView)
    }
    
    func setDataModels(sections: [LittTableViewSectionProtocol]) {
        tableView.sections = sections
    }
}
