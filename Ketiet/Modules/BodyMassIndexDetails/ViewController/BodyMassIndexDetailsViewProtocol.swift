//
//  BodyMassIndexDetailsViewProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 13/12/2021.
//  Copyright © 2021 littDev. All rights reserved.
//


protocol BodyMassIndexDetailsViewProtocol: AnyObject {
    func setupView()
    func setDataModels(sections: [LittTableViewSectionProtocol])
}
