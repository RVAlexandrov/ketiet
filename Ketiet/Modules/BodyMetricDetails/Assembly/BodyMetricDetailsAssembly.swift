//
//  BodyMetricDetailsAssembly.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class BodyMetricDetailsAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: BodyMetricDetailsModuleConfiguratorProtocol) {
		let presenter = BodyMetricDetailsPresenter(bodyMetricType: moduleConfigurator.bodyMetricType,
												   numberFormatter: NumberFormatter.formatterForSignedIntegerValues())
        let progressService = EveryDayInputProgressService
            .generatePreviousProgressService(
                type: ProgressValueType(from: moduleConfigurator.bodyMetricType),
                dietId: moduleConfigurator.diet.id)
        
        let interactor = BodyMetricDetailsInteractor(
            presenter: presenter,
            profileService: ProfileService.instance,
            progressService: progressService,
            measurementService: MeasurementService(type: .lenght),
            diet: moduleConfigurator.diet
        )
        let view = BodyMetricDetailsViewController(presenter: presenter,
                                                   showCloseButton: moduleConfigurator.showCloseButton)
        let router = BodyMetricDetailsRouter(viewController: view)

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
