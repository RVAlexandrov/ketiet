//
//  BodyMetricDetailsConfigurator.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol BodyMetricDetailsModuleConfiguratorProtocol {
	var bodyMetricType: BodyMetricType { get }
    var showCloseButton: Bool { get }
    var diet: DietProtocol { get }
}

struct BodyMetricDetailsModuleConfigurator: BodyMetricDetailsModuleConfiguratorProtocol {
	let bodyMetricType: BodyMetricType
    let showCloseButton: Bool
    let diet: DietProtocol
}
