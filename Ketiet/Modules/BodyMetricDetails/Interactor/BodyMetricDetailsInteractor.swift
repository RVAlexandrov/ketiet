//
//  BodyMetricDetailsInteractor.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

final class BodyMetricDetailsInteractor {
	
    private weak var presenter: BodyMetricDetailsInteractorOutputProtocol?
	private let profileService: ProfileServiceProtocol
	let progressService: ProgressServiceProtocol
	let measurementService: MeasurementServiceProtocol
	let diet: DietProtocol
	
    init(presenter: BodyMetricDetailsInteractorOutputProtocol,
		 profileService: ProfileServiceProtocol,
		 progressService: ProgressServiceProtocol,
		 measurementService: MeasurementServiceProtocol,
		 diet: DietProtocol) {
        self.presenter = presenter
		self.profileService = profileService
		self.progressService = progressService
		self.measurementService = measurementService
		self.diet = diet
    }
}

// MARK: - BodyMetricDetailsInteractorInputProtocol
extension BodyMetricDetailsInteractor: BodyMetricDetailsInteractorInputProtocol {
	func requestProfile() {
		profileService.getProfile { [weak self] profile in
			guard let profile = profile else { return }
			self?.presenter?.didReceive(profile: profile)
		}
	}
}
