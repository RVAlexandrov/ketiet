//
//  BodyMetricDetailsInteractorInputProtocol.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol BodyMetricDetailsInteractorInputProtocol {
	
	var progressService: ProgressServiceProtocol { get }
	var measurementService: MeasurementServiceProtocol { get }
	var diet: DietProtocol { get }
	func requestProfile() 
}
