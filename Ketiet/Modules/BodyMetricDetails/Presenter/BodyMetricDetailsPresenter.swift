//
//  BodyMetricDetailsPresenter.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Foundation

final class BodyMetricDetailsPresenter {

	private let bodyMetricType: BodyMetricType
	private let numberFormatter: NumberFormatter
    weak var view: BodyMetricDetailsViewInputProtocol?
    var interactor: BodyMetricDetailsInteractorInputProtocol?
    var router: BodyMetricDetailsRouterInputProtocol?


	init(
        bodyMetricType: BodyMetricType,
        numberFormatter: NumberFormatter
    ) {
		self.bodyMetricType = bodyMetricType
		self.numberFormatter = numberFormatter
    }
}

// MARK: - BodyMetricDetailsViewOutputProtocol
extension BodyMetricDetailsPresenter: BodyMetricDetailsViewOutputProtocol {
    func requestItems() {
		interactor?.requestProfile()
    }
}

// MARK: - BodyMetricDetailsInteractorOutputProtocol
extension BodyMetricDetailsPresenter: BodyMetricDetailsInteractorOutputProtocol {
	func didReceive(profile: ProfileProtocol) {
		guard let interactor = interactor else { return }
		let gender = profile.gender ?? .female
		let measurementService = interactor.measurementService
		let headerItem = SingleLabelCellItem(text: bodyMetricType.title(), textAlignment: .left)
        let progressService = interactor.progressService
        let graphItem = ProgressGraphTableViewCellItem(
            service: progressService,
            accentColor: .bodyMetricAccentColor
        )
        let diet = interactor.diet
        
        let allValues = progressService.getAllValues()
        let startDate = diet.startedDate ?? Date()
        let daysBetween = Date.daysBetweenTwoDates(startDate,
                                                   secondDate: Date()) ?? 0
        let filteredAllValues = Array(allValues?.prefix(daysBetween + 1) ?? [])
        let firstBodyMetricValue = filteredAllValues.first
        let lastBodyMetricValue = filteredAllValues.last
        
        let diffString: String
        let devidedDiffTrend: Trend?
        let devidedDiffString: String?
        
        if let last = lastBodyMetricValue, last != 0,
           let first = firstBodyMetricValue, first != 0 {
            diffString = (last - first).cleanValue + " " + measurementService.measurement()
        } else if lastBodyMetricValue == 0 {
            diffString = "BODY_METRIC_DETAILS_INPUT_START_WEIGHT".localized()
        } else if firstBodyMetricValue == 0 {
            diffString = "BODY_METRIC_DETAILS_UNKNOWED_START_WEIGHT".localized()
        } else {
            diffString = "BODY_METRIC_DETAIL_POOR_DATA".localized()
        }
        
        if let last = lastBodyMetricValue, last != 0,
           let first = firstBodyMetricValue, first != 0 {
            let generalDiffDevided = Float(last) / Float(first)
            if generalDiffDevided <= 1 {
                devidedDiffString = "-" + ((1.0 - generalDiffDevided) * 100).round(to: 0).cleanValue + "%"
                devidedDiffTrend = .positive
            } else {
                devidedDiffString = "+" + ((generalDiffDevided - 1.0) * 100).round(to: 0).cleanValue + "%"
                devidedDiffTrend = .negative
            }
        } else {
            devidedDiffString = nil
            devidedDiffTrend = nil
        }
        
        let animationItem = AnimatedResultCellItem(metricChangeDirection: .decrease,
                                                   bodyPartImageName: bodyMetricType.bodyImageName(isMale: gender == .male),
                                                   isChangeInDesiredDirection: true)
		
        let changeItem = DetailOneItemCellItem(model: DetailViewModel(title: "CHANGE_OVER_TIME".localized(),
                                                                      value: diffString,
                                                                      valueDiff: devidedDiffString,
                                                                      valueDiffTrend: devidedDiffTrend))
        
        let valuesByDaysItem = ButtonTableViewCellItem(title: "SHOW_BY_DAYS".localized(),
                                                       color: .bodyMetricAccentColor) { [weak self] in
            guard let self = self, let diet = self.interactor?.diet else { return }
            self.router?.presentDetailScreen(
                type: ProgressValueType(from: self.bodyMetricType),
                diet: diet
            )
        }
                
		view?.showBodyMetricItems([headerItem,
                                   graphItem,
                                   changeItem,
                                   animationItem,
                                   valuesByDaysItem])
	}
}
