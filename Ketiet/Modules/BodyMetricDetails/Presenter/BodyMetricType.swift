//
//  BodyMetricType.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 14.11.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

enum BodyMetricType {
	case chest
	case waist
	case hips
	
	func title() -> String {
		switch self {
		case .chest:
			return "CHEST".localized()
		case .waist:
			return "WAIST".localized()
		case .hips:
			return "HIPS".localized()
		}
	}
	
	func bodyImageName(isMale: Bool) -> String {
		switch self {
		case .chest:
			return isMale ? "boyChest" : "girlChest"
		case .waist:
			return isMale ? "boyWaist" : "girlWaist"
		case .hips:
			return isMale ? "boyHips" : "girlHips"
		}
	}
	
	func recommendationHowToMeasure() -> String {
		switch self {
		case .chest:
			return "INPUT_CHEST_VOLUME_RECOMMENDATION".localized()
		case .waist:
			return "INPUT_WAIST_VOLUME_RECOMMENDATION".localized()
		case .hips:
			return "INPUT_HIPS_VOLUME_RECOMMENDATION".localized()
		}
	}
	
	func progressService() -> ProgressServiceProtocol {
		switch self {
		case .chest:
			return EveryDayInputProgressService.chestProgressService
		case .waist:
			return EveryDayInputProgressService.waistProgressService
		case .hips:
			return EveryDayInputProgressService.hipsProgressService
		}
	}
	
	func inputViewTitle() -> String {
		switch self {
		case .chest:
			return "INPUT_CHEST_VOLUME".localized()
		case .waist:
			return "INPUT_WAIST_VOLUME".localized()
		case .hips:
			return "INPUT_HIPS_VOLUME".localized()
		}
	}
}
