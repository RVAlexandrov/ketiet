//
//  BodyMetricDetailsRouter.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class BodyMetricDetailsRouter {
    
    private weak var viewController: UIViewController?

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

// MARK: - BodyMetricDetailsRouterInputProtocol
extension BodyMetricDetailsRouter: BodyMetricDetailsRouterInputProtocol {
    func presentDetailScreen(type: ProgressValueType,
                             diet: DietProtocol) {
        let fabric = ValuesByDaysViewControllerFabric()
        let vc = fabric.defaultValuesByDaysViewController(type: type,
                                                          diet: diet)
        viewController?.present(vc, animated: true)
    }
}
