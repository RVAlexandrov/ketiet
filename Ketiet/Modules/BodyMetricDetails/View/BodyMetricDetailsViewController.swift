//
//  BodyMetricDetailsViewController.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class BodyMetricDetailsViewController: DismissableDetailsViewController {

    private let presenter: BodyMetricDetailsViewOutputProtocol

	init(
        presenter: BodyMetricDetailsViewOutputProtocol,
        showCloseButton: Bool = true
    ) {
        self.presenter = presenter
		super.init(showCloseButton: showCloseButton)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.requestItems()
        view.backgroundColor = .littBackgroundColor
    }
}

// MARK: - BodyMetricDetailsViewInputProtocol
extension BodyMetricDetailsViewController: BodyMetricDetailsViewInputProtocol {

	func showBodyMetricItems(_ items: [LittTableViewCellItemProtocol]) {
		tableView.sections = [LittTableViewSection(items: items)]
	}
}
