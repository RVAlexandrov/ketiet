//
//  BodyMetricDetailsViewInputProtocol.swift
//  ketiet
//
//  Created by Alex on 04/11/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol BodyMetricDetailsViewInputProtocol: AnyObject {
	
	func showBodyMetricItems(_ items: [LittTableViewCellItemProtocol])
}
