//
//  AnimatedResultCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 04.11.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class AnimatedResultCell: UITableViewCell {
	
	private struct Constants {
		static let arrowImageViewSide: CGFloat = 35
		static let bodyImageHeight: CGFloat = ScreenMapper().map(iphoneSE: 70, iphone8: 75, iphone8Plus: 95, iphoneX: 100, iphoneXR: 100)
		static let animationDuration: TimeInterval = 2
		static let resultLabelAnimationDuration: TimeInterval = 1
		static let delayDuration: TimeInterval = 1
		static let turningBackDuration: TimeInterval = 1
		static let resultLabelFinalOffset: CGFloat = .largeLittMargin
	}
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .subTitle
        label.text = "BODY_METRIC_DETAIL_MEASURE_TITLE".localized()
        return label
    }()
    
    private let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .littSecondaryBackgroundColor
        view.layer.cornerRadius = 12.0
        return view
    }()
	
	private let bodyPartImageView: UIImageView = {
		let view = UIImageView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.contentMode = .scaleAspectFit
		return view
	}()
	
	let leadingArrowImageView: UIImageView = {
		let view = UIImageView()
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	let trailingArrowImageView: UIImageView = {
		let view = UIImageView()
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	let resultChangeLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
		label.alpha = 0
		return label
	}()
	
	let leadingTitleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
		label.textAlignment = .left
		return label
	}()
	
	let trailingTitleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
		label.textAlignment = .right
		label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
		return label
	}()
	
	var bodyMetricChangeDirecition = BodyMetricChangeDirection.increase
	private var isAnimating = false
	private var shouldStopAnimating = false
	private lazy var leadingArrowSpace = leadingArrowImageView.trailingAnchor.constraint(equalTo: bodyPartImageView.leadingAnchor)
	private lazy var trailingArrowSpace = trailingArrowImageView.leadingAnchor.constraint(equalTo: bodyPartImageView.trailingAnchor)
	private lazy var bodyImageViewWidthConstraint = bodyPartImageView.widthAnchor.constraint(equalToConstant: 0)
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification,
											   object: nil,
											   queue: .main) { [weak self] _ in
			self?.shouldStopAnimating = true
		}
		NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
											   object: nil,
											   queue: .main) { [weak self] _ in
			self?.shouldStopAnimating = false
			self?.animateCellIfNeeded()
		}
		setupSubviews()
	}
	
	required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
	func setBodyImage(_ image: UIImage?) {
		bodyPartImageView.image = image
		let imageSize = image?.size ?? CGSize(width: 1, height: 1)
		bodyImageViewWidthConstraint.constant = Constants.bodyImageHeight * imageSize.width / imageSize.height
	}
	
	func animateCellIfNeeded() {
		if isAnimating {
			return
		}
		animateCell()
	}
	
	private func animateCell() {
		if shouldStopAnimating {
			shouldStopAnimating = false
			isAnimating = false
			return
		}
		startAnimation()
	}
	
	private func startAnimation() {
		UIView.animate(withDuration: Constants.animationDuration,
					   delay: 0,
					   options: .curveEaseOut) {
			self.leadingArrowSpace.constant = -self.bodyMetricChangeDirecition.sideArrowsFinalSpacing()
			self.trailingArrowSpace.constant = self.bodyMetricChangeDirecition.sideArrowsFinalSpacing()
			self.leadingArrowImageView.alpha = 1
			self.trailingArrowImageView.alpha = 1
			self.layoutIfNeeded()
		} completion: { _ in
			self.layoutAndBackToInitalState()
		}
	}
	
	private func layoutAndBackToInitalState() {
		UIView.animate(withDuration: Constants.resultLabelAnimationDuration,
					   delay: 0,
					   options: .curveEaseOut) {
			self.layoutIfNeeded()
		} completion: { _ in
			self.moveViewsToInitialState()
		}
	}
	
    private func moveViewsToInitialState() {
        UIView.animate(withDuration: Constants.turningBackDuration) {
            self.leadingArrowImageView.alpha = 0
            self.trailingArrowImageView.alpha = 0
            self.leadingArrowSpace.constant -= self.bodyMetricChangeDirecition.sideArrowsInitialSpacing()
            self.trailingArrowSpace.constant += self.bodyMetricChangeDirecition.sideArrowsInitialSpacing()
            self.layoutIfNeeded()
        } completion: { _ in
            self.animateCell()
        }
    }
	
	override func didMoveToSuperview() {
		if superview == nil {
			shouldStopAnimating = true
		}
	}
	
	private func setupSubviews() {
        backgroundColor = .littBackgroundColor
        contentView.addSubview(containerView)
        containerView.addSubviews([leadingArrowImageView,
                                   bodyPartImageView,
                                   titleLabel,
                                   trailingArrowImageView])
        
		NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallLittMargin),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.smallLittMargin),
            
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .mediumLittMargin),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .mediumLittMargin),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.mediumLittMargin),

            
			bodyPartImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            bodyPartImageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .mediumLittMargin),
			bodyPartImageView.heightAnchor.constraint(equalToConstant: Constants.bodyImageHeight),
			bodyImageViewWidthConstraint,
			leadingArrowSpace,
			trailingArrowSpace,
			leadingArrowImageView.centerYAnchor.constraint(equalTo: bodyPartImageView.centerYAnchor),
			trailingArrowImageView.centerYAnchor.constraint(equalTo: bodyPartImageView.centerYAnchor),
            bodyPartImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
		])
	}
}

private extension BodyMetricChangeDirection {
	
	private struct Constants {
		static let arrowMovingLength: CGFloat = 32
	}
	
	func sideArrowsInitialSpacing() -> CGFloat {
		switch self {
		case .decrease, .saveCurrentValue:
			return Constants.arrowMovingLength
		case .increase:
			return 0
		}
	}
	
	func sideArrowsFinalSpacing() -> CGFloat {
		switch self {
		case .decrease, .saveCurrentValue:
			return 0
		case .increase:
			return Constants.arrowMovingLength
		}
	}
}
