//
//  AnimatedResultCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 04.11.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class AnimatedResultCellItem {
	
	private let metricChangeDirection: BodyMetricChangeDirection
	private let bodyPartImageName: String
	private let isChangeInDesiredDirection: Bool
	
	init(metricChangeDirection: BodyMetricChangeDirection,
		 bodyPartImageName: String,
		 isChangeInDesiredDirection: Bool) {
		self.metricChangeDirection = metricChangeDirection
		self.bodyPartImageName = bodyPartImageName
		self.isChangeInDesiredDirection = isChangeInDesiredDirection
	}
}

extension AnimatedResultCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? AnimatedResultCell else { return }
		cell.bodyMetricChangeDirecition = metricChangeDirection
		cell.setBodyImage(UIImage(named: bodyPartImageName))
		let isIncreasing = metricChangeDirection == .increase
		cell.leadingArrowImageView.image = UIImage(systemName: isIncreasing ? "arrow.left" : "arrow.right")
		cell.trailingArrowImageView.image = UIImage(systemName: isIncreasing ? "arrow.right" : "arrow.left")
		cell.leadingArrowImageView.tintColor = isChangeInDesiredDirection ? .littTurnedOnColor : .itemTurnedOffColor
		cell.trailingArrowImageView.tintColor = cell.leadingArrowImageView.tintColor
		cell.selectionStyle = .none
		DispatchQueue.main.async {
			cell.animateCellIfNeeded()
		}
	}
	
	func viewClass() -> AnyClass {
		AnimatedResultCell.self
	}
}
