//
//  BodyMetricChangeDirection.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 04.11.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

enum BodyMetricChangeDirection {
	case increase
	case decrease
	case saveCurrentValue
}
