//
//  CaloriesCalculatorChoose.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 11.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit
import DietCore

final class CaloriesCalculatorChooseAssembly {
    let profileService: ProfileServiceProtocol
    let currentGoal: Goal?
    let selectedPhysicalActivity: PhysicalActivity?
    let arrayItemTypes: [CaloriesCalculatorType] = [.harrisBenedict, .mifflinSanJeor, .custom]
    let mealDatesStorage: MealDatesStorageProtocol
    
    weak var chooseItemVc: UIViewController?
    
    init(profileService: ProfileServiceProtocol,
         mealDatesStorage: MealDatesStorageProtocol,
         currentGoal: Goal?,
         selectedPhysicalActivity: PhysicalActivity?) {
        self.profileService = profileService
        self.mealDatesStorage = mealDatesStorage
        self.currentGoal = currentGoal
        self.selectedPhysicalActivity = selectedPhysicalActivity
    }
    
    func makeViewController() -> UIViewController {
        let parameterVC = ChooseItemViewController(
            items: arrayItemTypes.map { configureItem(for: $0) },
            buttonTitle: "CONTINUE".localized()) { [weak self] (index, vc) in
                guard let self = self else { return }
                let choosenType = self.arrayItemTypes[index]
                self.profileService.setCaloriesCalculatorType(
                    choosenType,
                    completion: { _ in
                        switch choosenType {
                            
                        case .mifflinSanJeor, .harrisBenedict:
                            let assembly: GoalChooseAssembly = AssemblyFactory()
                                .generateAssembly(
                                    moduleConfigurator: GoalChooseModuleConfigurator(
                                        selectedGoal: self.currentGoal,
                                        selectedPhysicalActivity: self.selectedPhysicalActivity,
                                        profileService: self.profileService,
                                        mealDatesStorage: self.mealDatesStorage
                                    )
                                )
                            vc.navigationController?.pushViewController(assembly.initialViewController(), animated: true)
                            
                        case .custom:
                            let changeTargetCaloriesVC = ChangeTargetCaloriesViewController(
                                energyMeasurementService: MeasurementService(type: .energy),
                                continueButtonHandler: { [weak self] (calories, vc) in
                                    
                                    guard let self = self else { return }
                                    self.profileService.setDailyCalories(calories,
                                                                         completion: { _ in })
                                    let mealChangeDatesDataProvider = KetoMealDateTitleProvider(dateService: MealDatesService.shared)
                                    let assembly = ChangeMealDatesAssembly(
                                        title: "JUST_A_LITTLE_MORE".localized(),
                                        dataProvider: mealChangeDatesDataProvider,
                                        actionButtonTitle: "CONTINUE".localized(),
                                        mealDatesStorage: self.mealDatesStorage) { [weak self] in
                                            
                                            guard let self = self else { return }
                                            let configurator = WelcomeResumeModuleConfigurator(
                                                mealsDateService: self.mealDatesStorage,
                                                profileService: self.profileService,
                                                customTargetCalories: calories
                                            )
                                            let welcomeResumeAssembly = WelcomeResumeAssembly(moduleConfigurator: configurator)
                                            vc.navigationController?.pushViewController(
                                                welcomeResumeAssembly.initialViewController(),
                                                animated: true
                                            )
                                            
                                        }
                                    let controller = assembly.initialViewController()
                                    vc.navigationController?.pushViewController(
                                        controller,
                                        animated: true
                                    )
                                }
                            )
                            vc.navigationController?.pushViewController(
                                changeTargetCaloriesVC,
                                animated: true
                            )
                        }
                    }
                )
            }
        
        self.chooseItemVc = parameterVC
        return parameterVC
    }
    
    func showDietDetailVC() -> UIViewController {
        UIViewController()
    }
}

// MARK: - Private methods
extension CaloriesCalculatorChooseAssembly {
    private func configureItem(for type: CaloriesCalculatorType) -> SelectableCellItem {
        let builder = CaloriesCalculatorSelectableItemModelBuilder()
        switch type {
        case .harrisBenedict:
            return ExtendedChooseItem(
                title: builder.configureTitle(for: .harrisBenedict),
                description: builder.configureDescription(for: .harrisBenedict),
                buttonCompletion: {
                    let vc = DetailedInfoViewController(
                        detailText: "HARRIS_BENEDICT_SELECTABLE_DESCRIPTION_EXTENDED".localized(),
                        titleText: "HARRIS_BENEDICT_SELECTABLE_TITLE".localized(),
                        buttonModel: .init(
                            title: "READ_PAPER_ABOUT".localized(),
                            url: URL(string: "https://pubmed.ncbi.nlm.nih.gov/9550168/")
                        )
                    )
                    vc.setupStandardSheetPresentation()
                    self.chooseItemVc?.present(vc,
                                               animated: true)
                },
                buttonTitle: "SEE_DETAILS".localized(),
                isSelected: true
            )
        case .custom:
            return DefaultChooseItem(title: builder.configureTitle(for: .custom),
                                     description: builder.configureDescription(for: .custom),
                                     isSelected: false)
        case .mifflinSanJeor:
            return ExtendedChooseItem(
                title: builder.configureTitle(for: .mifflinSanJeor),
                description: builder.configureDescription(for: .mifflinSanJeor),
                buttonCompletion: {
                    let vc = DetailedInfoViewController(
                        detailText: "MIFFLIN_SAN_JEOR_SELECTABLE_DESCRIPTION_EXTENDED".localized(),
                        titleText: "MIFFLIN_SAN_JEOR_SELECTABLE_TITLE".localized(),
                        buttonModel: .init(
                            title: "READ_PAPER_ABOUT".localized(),
                            url: URL(string: "https://pubmed.ncbi.nlm.nih.gov/2305711/")
                        )
                    )
                    vc.setupStandardSheetPresentation()
                    self.chooseItemVc?.present(vc,
                                               animated: true)
                },
                buttonTitle: "SEE_DETAILS".localized(),
                isSelected: false)
        }
    }
}

