//
//  CaloriesTypeStringsBuilder.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 14.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

struct CaloriesCalculatorSelectableItemModelBuilder {
    func configureTitle(for type: CaloriesCalculatorType) -> String {
        switch type {
        case .harrisBenedict:
            return "HARRIS_BENEDICT_SELECTABLE_TITLE".localized()
        case .custom:
            return "CUSTOM_SELECTABLE_TITLE".localized()
        case .mifflinSanJeor:
            return "MIFFLIN_SAN_JEOR_SELECTABLE_TITLE".localized()
        }
    }
    
    func configureDescription(for type: CaloriesCalculatorType) -> String {
        switch type {
        case .harrisBenedict:
            return "HARRIS_BENEDICT_SELECTABLE_DESCRIPTION".localized()
        case .custom:
            return "CUSTOM_SELECTABLE_DESCRIPTION".localized()
        case .mifflinSanJeor:
            return "MIFFLIN_SAN_JEOR_SELECTABLE_DESCRIPTION".localized()
        }
    }
}
