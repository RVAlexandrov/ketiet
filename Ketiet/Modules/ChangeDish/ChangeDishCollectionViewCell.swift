//
//  ChangeDishCollectionViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 30.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class ChangeDishCollectionViewCell: UICollectionViewCell {
    
    private lazy var currentDishView: ChangeDishView = {
        let view = ChangeDishView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        currentDishView.imageView.image = nil
    }
    
    func configureCell(withDish dish: DishProtocol, targetSize: CGSize) {
        currentDishView.configureView(withDish: dish, targetSize: targetSize)
    }
}

// MARK: - Private methods
extension ChangeDishCollectionViewCell {
    private func configureCell() {
        contentView.addSubview(currentDishView)
        
        NSLayoutConstraint.activate([
            currentDishView.topAnchor.constraint(equalTo: contentView.topAnchor),
            currentDishView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            currentDishView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            currentDishView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
    }
}
