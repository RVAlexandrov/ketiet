//
//  ChangeDishDiffView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 31.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import UIKit

final class ChangeDishDiffView: UIView {
        
    private struct Settings {
        static let nameLabelWidth: CGFloat = UIScreen.main.bounds.width * 0.45
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .subTitle
        label.text = "CHANGES".localized()
        label.textColor = .label
        return label
    }()
    
    lazy var weightNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "WEIGHT".localized() + ":"
        label.textColor = .secondaryLabel
        return label
    }()
    
    lazy var weightValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        label.textColor = .label
        label.textAlignment = .left
        return label
    }()
    
    lazy var ccalNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "TOTAL_CCAL".localized() + ":"
        label.textColor = .secondaryLabel
        return label
    }()
    
    lazy var ccalValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        label.textColor = .label
        label.textAlignment = .left
        return label
    }()
    
    lazy var proteinNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "PROTEIN".localized() + ":"
        label.textColor = .secondaryLabel
        return label
    }()
    
    lazy var proteinValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        label.textColor = .label
        label.textAlignment = .left
        return label
    }()
    
    lazy var carbohydratesNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "CARBOHYDRATE".localized() + ":"
        label.textColor = .secondaryLabel
        return label
    }()
    
    lazy var carbohydratesValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        label.textColor = .label
        label.textAlignment = .left
        return label
    }()
    
    lazy var fatsNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "FAT".localized() + ":"
        label.textColor = .secondaryLabel
        return label
    }()
    
    lazy var fatsValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        label.textColor = .label
        label.textAlignment = .left
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func configureView(currentDish: DishProtocol, newDish: DishProtocol) {
        let weightMeasureService = MeasurementService(type: .smallWeight, unitStyle: .short, unitOptions: .providedUnit)
        let energyMeasureService = MeasurementService(type: .energy, unitStyle: .short, unitOptions: .providedUnit)
        
        let fatsDiff = newDish.weightedNutrients.fat - currentDish.weightedNutrients.fat
        let caloriesDiff = newDish.weightedNutrients.calories - currentDish.weightedNutrients.calories
        let carbohydratesDiff = newDish.weightedNutrients.carbohydrates - currentDish.weightedNutrients.carbohydrates
        let proteinsDiff = newDish.weightedNutrients.proteins - currentDish.weightedNutrients.proteins
        let weightDiff = newDish.weightInGrams - currentDish.weightInGrams
        
        func prepareStringForLabel(with value: Float, measureService: MeasurementServiceProtocol) -> (String, UIColor) {
            var color: UIColor = .label
            var finalString = ""
            
            let measure = measureService.measurement()
            let convertedValue = measureService.convertToLocale(value: value)
            
            if convertedValue < 0 {
                finalString = "-" + convertedValue.cleanValue + " " + measure
                finalString = String(finalString.dropFirst())
                color = .systemGreen
            } else if convertedValue > 0 {
                finalString = "+" + convertedValue.cleanValue + " " + measure
                color = .systemRed
            } else {
                finalString = convertedValue.cleanValue + " " + measure
                color = .label
            }
            
            return (finalString, color)
        }
        
        let fatsTuple = prepareStringForLabel(with: fatsDiff, measureService: weightMeasureService)
        let calorieTuple = prepareStringForLabel(with: caloriesDiff, measureService: energyMeasureService)
        let carbohydratesTuple = prepareStringForLabel(with: carbohydratesDiff, measureService: weightMeasureService)
        let proteinTuple = prepareStringForLabel(with: proteinsDiff, measureService: weightMeasureService)
        let weightTuple = prepareStringForLabel(with: weightDiff, measureService: weightMeasureService)

        fatsValueLabel.textColor = fatsTuple.1
        fatsValueLabel.text = fatsTuple.0
        
        proteinValueLabel.textColor = proteinTuple.1
        proteinValueLabel.text = proteinTuple.0
        
        carbohydratesValueLabel.textColor = carbohydratesTuple.1
        carbohydratesValueLabel.text = carbohydratesTuple.0
        
        weightValueLabel.textColor = weightTuple.1
        weightValueLabel.text = weightTuple.0
        
        ccalValueLabel.textColor = calorieTuple.1
        ccalValueLabel.text = calorieTuple.0
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private methods
extension ChangeDishDiffView {
    private func setupView() {

        backgroundColor = .littSecondaryBackgroundColor
        
        addSubviews([titleLabel,
                     weightNameLabel, weightValueLabel,
                     ccalNameLabel, ccalValueLabel,
                     proteinNameLabel, proteinValueLabel,
                     carbohydratesNameLabel, carbohydratesValueLabel,
                     fatsNameLabel, fatsValueLabel
                    ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: .largeLittMargin),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .logicBlockLittMargin),
            
            weightNameLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .largeLittMargin),
            weightNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .logicBlockLittMargin),
            weightNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            weightValueLabel.centerYAnchor.constraint(equalTo: weightNameLabel.centerYAnchor),
            weightValueLabel.leadingAnchor.constraint(equalTo: weightNameLabel.trailingAnchor, constant: .largeLittMargin),
            weightValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.logicBlockLittMargin),

            ccalNameLabel.topAnchor.constraint(equalTo: weightNameLabel.bottomAnchor, constant: .mediumLittMargin),
            ccalNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .logicBlockLittMargin),
            ccalNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            ccalValueLabel.centerYAnchor.constraint(equalTo: ccalNameLabel.centerYAnchor),
            ccalValueLabel.leadingAnchor.constraint(equalTo: ccalNameLabel.trailingAnchor, constant: .largeLittMargin),
            ccalValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.logicBlockLittMargin),
            
            proteinNameLabel.topAnchor.constraint(equalTo: ccalNameLabel.bottomAnchor, constant: .mediumLittMargin),
            proteinNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .logicBlockLittMargin),
            proteinNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            proteinValueLabel.centerYAnchor.constraint(equalTo: proteinNameLabel.centerYAnchor),
            proteinValueLabel.leadingAnchor.constraint(equalTo: proteinNameLabel.trailingAnchor, constant: .largeLittMargin),
            proteinValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.logicBlockLittMargin),
            
            carbohydratesNameLabel.topAnchor.constraint(equalTo: proteinNameLabel.bottomAnchor, constant: .mediumLittMargin),
            carbohydratesNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .logicBlockLittMargin),
            carbohydratesNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            carbohydratesValueLabel.centerYAnchor.constraint(equalTo: carbohydratesNameLabel.centerYAnchor),
            carbohydratesValueLabel.leadingAnchor.constraint(equalTo: carbohydratesNameLabel.trailingAnchor, constant: .largeLittMargin),
            carbohydratesValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.logicBlockLittMargin),
            
            fatsNameLabel.topAnchor.constraint(equalTo: carbohydratesNameLabel.bottomAnchor, constant: .mediumLittMargin),
            fatsNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .logicBlockLittMargin),
            fatsNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            fatsValueLabel.centerYAnchor.constraint(equalTo: fatsNameLabel.centerYAnchor),
            fatsValueLabel.leadingAnchor.constraint(equalTo: fatsNameLabel.trailingAnchor, constant: .largeLittMargin),
            fatsValueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.logicBlockLittMargin),
            fatsValueLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.logicBlockLittMargin)
        ])
    }
}
