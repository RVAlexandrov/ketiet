//
//  ChangeDishView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 30.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit
import AlamofireImage
import DietCore

final class ChangeDishView: UIView {
    
    private struct Settings {
        static let nameLabelWidth: CGFloat = 100
        static let imageViewHeight: CGFloat = 100
    }
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = .littBasicCornerRadius
        return imageView
    }()
    
    lazy var dishNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .bodySemibold
        label.numberOfLines = 2
        return label
    }()
    
    lazy var weightNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "WEIGHT".localized() + ":"
        label.textColor = .systemGray
        return label
    }()
    
    lazy var weightValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        return label
    }()
    
    lazy var ccalNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "TOTAL_CCAL".localized() + ":"
        label.textColor = .systemGray
        return label
    }()
    
    lazy var ccalValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        return label
    }()
    
    lazy var proteinNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "PROTEIN".localized() + ":"
        label.textColor = .systemGray
        return label
    }()
    
    lazy var proteinValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        return label
    }()
    
    lazy var carbohydratesNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "CARBOHYDRATE".localized() + ":"
        label.textColor = .systemGray
        return label
    }()
    
    lazy var carbohydratesValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        return label
    }()
    
    lazy var fatsNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.text = "FAT".localized() + ":"
        label.textColor = .systemGray
        return label
    }()
    
    lazy var fatsValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureView(withDish dish: DishProtocol,
                       targetSize: CGSize) {
        if let imageURL = dish.imageURL {
            let imageSize = CGSize(width: targetSize.width,
                                   height: Settings.imageViewHeight)
            imageView.showShimmer()
            imageView.af.setImage(withURL: imageURL,
                                  filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: imageSize,
                                                                                         radius: .littBasicCornerRadius),
                                  imageTransition: .crossDissolve(0.3),
                                  runImageTransitionIfCached: false,
                                  completion: { [weak self] _ in
                                    self?.imageView.hideShimmer()
                                  })
        }
        
        layer.addShadow(CGRect(origin: .zero,
                               size: targetSize))
        
        dishNameLabel.text = dish.name
        
        func prepareValue(value: Float,
                          measureService: MeasurementServiceProtocol) -> String {
            let measure = measureService.measurement()
            let convertedValue = measureService.convertToLocale(value: value)
            return convertedValue.cleanValue + " \(measure)"
        }
        
        let weightMesureService = MeasurementService(type: .smallWeight, unitStyle: .short, unitOptions: .providedUnit)
        let energyMeasureService = MeasurementService(type: .energy, unitStyle: .short, unitOptions: .providedUnit)
        
        weightValueLabel.text = prepareValue(value: dish.weightInGrams, measureService: weightMesureService)
        fatsValueLabel.text = prepareValue(value: dish.weightedNutrients.fat, measureService: weightMesureService)
        carbohydratesValueLabel.text = prepareValue(value: dish.weightedNutrients.carbohydrates, measureService: weightMesureService)
        proteinValueLabel.text = prepareValue(value: dish.weightedNutrients.proteins, measureService: weightMesureService)
        ccalValueLabel.text = prepareValue(value: dish.weightedNutrients.calories, measureService: energyMeasureService)
    }
}

// MARK: Private methods
extension ChangeDishView {
    private func setupView() {
        layer.cornerRadius = .littBasicCornerRadius

        backgroundColor = .littSecondaryBackgroundColor
                
        addSubviews([imageView, dishNameLabel,
                     weightNameLabel, weightValueLabel,
                     ccalNameLabel, ccalValueLabel,
                     proteinNameLabel, proteinValueLabel,
                     carbohydratesNameLabel, carbohydratesValueLabel,
                     fatsNameLabel, fatsValueLabel])
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            
            dishNameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: .smallLittMargin),
            dishNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            dishNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -.mediumLittMargin),
            
            weightNameLabel.topAnchor.constraint(equalTo: dishNameLabel.bottomAnchor, constant: .mediumLittMargin),
            weightNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            weightNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            weightValueLabel.centerYAnchor.constraint(equalTo: weightNameLabel.centerYAnchor),
            weightValueLabel.leadingAnchor.constraint(equalTo: weightNameLabel.trailingAnchor, constant: .largeLittMargin),

            ccalNameLabel.topAnchor.constraint(equalTo: weightNameLabel.bottomAnchor, constant: .smallLittMargin),
            ccalNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            ccalNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            ccalValueLabel.centerYAnchor.constraint(equalTo: ccalNameLabel.centerYAnchor),
            ccalValueLabel.leadingAnchor.constraint(equalTo: ccalNameLabel.trailingAnchor, constant: .largeLittMargin),
            
            proteinNameLabel.topAnchor.constraint(equalTo: ccalNameLabel.bottomAnchor, constant: .smallLittMargin),
            proteinNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            proteinNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            proteinValueLabel.centerYAnchor.constraint(equalTo: proteinNameLabel.centerYAnchor),
            proteinValueLabel.leadingAnchor.constraint(equalTo: proteinNameLabel.trailingAnchor, constant: .largeLittMargin),
            
            carbohydratesNameLabel.topAnchor.constraint(equalTo: proteinNameLabel.bottomAnchor, constant: .smallLittMargin),
            carbohydratesNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            carbohydratesNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            carbohydratesValueLabel.centerYAnchor.constraint(equalTo: carbohydratesNameLabel.centerYAnchor),
            carbohydratesValueLabel.leadingAnchor.constraint(equalTo: carbohydratesNameLabel.trailingAnchor, constant: .largeLittMargin),
            
            fatsNameLabel.topAnchor.constraint(equalTo: carbohydratesNameLabel.bottomAnchor, constant: .smallLittMargin),
            fatsNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .mediumLittMargin),
            fatsNameLabel.widthAnchor.constraint(equalToConstant: Settings.nameLabelWidth),
            
            fatsValueLabel.centerYAnchor.constraint(equalTo: fatsNameLabel.centerYAnchor),
            fatsValueLabel.leadingAnchor.constraint(equalTo: fatsNameLabel.trailingAnchor, constant: .largeLittMargin)
        ])
    }
}
