//
//  ChangeDishViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 30.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import UIKit

protocol ChangeDishViewControllerDelegate: AnyObject {
    func changeDishDidComplete()
}

final class ChangeDishViewController: UIViewController {
    
    private struct Settings {
        static let cellSize = CGSize(width: UIScreen.main.bounds.width * 0.8,
                                     height: 270)
        static let horizontalInset = (UIScreen.main.bounds.width - cellSize.width) / 2
    }
    
    weak var delegate: ChangeDishViewControllerDelegate?
    
    private let dietService: DietServiceProtocol
    private let dishProvider: DishProviderProtocol
    private let currentDish: DishProtocol
    private let anotherDishes: [DishProtocol]
    private let dayIndex: Int
    private let mealIndex: Int
    
    private var selectedDish: DishProtocol? {
        didSet {
            guard let selectedDish = selectedDish else { return }
            diffView.configureView(currentDish: currentDish,
                                   newDish: selectedDish)
        }
    }
    
    private let changeDishCollectionViewCellId = "changeDishCollectionViewCellId"
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .littBackgroundColor
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .littBackgroundColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var diffView: ChangeDishDiffView = {
        let view = ChangeDishDiffView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var navigationView: UIView = {
        let view = UIView()
        view.backgroundColor = .littBackgroundColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .title
        label.text = "CHANGE_DISH_TITLE".localized()
        return label
    }()
    
    private lazy var titleDescriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        label.textColor = .systemGray
        label.numberOfLines = 0
        label.text = "CHANGE_DISH_DESCRIPTION".localized()
        return label
    }()
    
    private lazy var currentDishView: ChangeDishView = {
        let view = ChangeDishView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var changeButton: UIButton = {
        let button = UIButton(frame: .zero)
        let conf = LittBasicButtonConfiguration.capsule
        button.translatesAutoresizingMaskIntoConstraints = false
        var configuration = conf.configuration
        configuration.image = UIImage(systemName: "arrow.up.and.down.circle.fill")
        configuration.attributedTitle = AttributedString(
            "REPLACE".localized().lowercased(),
            attributes: AttributeContainer([.font: UIFont.bodyBold])
        )
        configuration.imagePadding = .smallLittMargin
        button.configuration = configuration
        button.setNeedsUpdateConfiguration()
        button.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
        button.layer.addShadow(CGRect(origin: CGPoint.zero, size: CGSize(width: 134, height: 35)))
        return button
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.itemSize = Settings.cellSize
        collectionViewFlowLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.clipsToBounds = false
        collectionView.contentInset = UIEdgeInsets(top: 0,
                                                   left: Settings.horizontalInset,
                                                   bottom: 0,
                                                   right: Settings.horizontalInset)
        collectionView.register(ChangeDishCollectionViewCell.self,
                                forCellWithReuseIdentifier: changeDishCollectionViewCellId)
        return collectionView
    }()
    
    init(dietService: DietServiceProtocol,
         dishProvider: DishProviderProtocol,
         currentDish: DishProtocol,
         dayIndex: Int,
         mealIndex: Int) {
        self.dietService = dietService
        self.dishProvider = dishProvider
        self.currentDish = currentDish
        self.dayIndex = dayIndex
        self.mealIndex = mealIndex
        
        let dishToPrepare = dishProvider.dishes
            .filter { $0.dishId != currentDish.dishId }
        let weightCalculator = DishWeightCalculator()
        dishToPrepare.forEach {
            $0.weightInGrams = weightCalculator.getWeightForDish(withTargerCalories: currentDish.weightedNutrients.calories,
                                                                 dishCaloriesPer100Gramm: $0.nutriens.calories)
        }
        self.anotherDishes = dishToPrepare
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        prepareData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        let index: Int = collectionView.numberOfItems(inSection: 0) / 2
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0),
                                    at: .right,
                                    animated: false)
        if anotherDishes.count >= index + 1 {
            selectedDish = anotherDishes[index]
        }
    }
}

// MARK: - Private methods
extension ChangeDishViewController {
    private func prepareData() {
        currentDishView.configureView(withDish: currentDish,
                                      targetSize: CGSize(width: UIScreen.main.bounds.width - 2 * .largeLittMargin,
                                                         height: Settings.cellSize.height))
    }
    
    private func configureView() {
        view.backgroundColor = .littBackgroundColor
                        
        let leftButton = LittCapsuleButton(title: "CLOSE".localized().uppercased())
        leftButton.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        
        let rightButton = LittCapsuleButton(title: "REPLACE".localized().uppercased())
        rightButton.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
        
        view.addSubviews([navigationView, scrollView])
        navigationView.addSubviews([leftButton, rightButton])
        scrollView.addSubview(containerView)
        containerView.addSubviews([titleLabel, titleDescriptionLabel, currentDishView, changeButton, collectionView, diffView])
        
        NSLayoutConstraint.activate([
            navigationView.topAnchor.constraint(equalTo: view.topAnchor),
            navigationView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            navigationView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            navigationView.heightAnchor.constraint(equalToConstant: 56),

            scrollView.topAnchor.constraint(equalTo: navigationView.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            leftButton.centerYAnchor.constraint(equalTo: navigationView.centerYAnchor),
            leftButton.leadingAnchor.constraint(equalTo: navigationView.leadingAnchor, constant: .logicBlockLittMargin),
            
            rightButton.centerYAnchor.constraint(equalTo: navigationView.centerYAnchor),
            rightButton.trailingAnchor.constraint(equalTo: navigationView.trailingAnchor, constant: -.logicBlockLittMargin),
            
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.logicBlockLittMargin),
            
            titleDescriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .mediumLittMargin),
            titleDescriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            titleDescriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.logicBlockLittMargin),
            
            currentDishView.topAnchor.constraint(equalTo: titleDescriptionLabel.bottomAnchor, constant: .largeLittMargin),
            currentDishView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            currentDishView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.logicBlockLittMargin),
            currentDishView.heightAnchor.constraint(equalToConstant: Settings.cellSize.height),
            
            changeButton.topAnchor.constraint(equalTo: currentDishView.bottomAnchor, constant: .largeLittMargin),
            changeButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            
            collectionView.topAnchor.constraint(equalTo: changeButton.bottomAnchor, constant: .largeLittMargin),
            collectionView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: Settings.cellSize.height),
            
            diffView.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: .largeLittMargin),
            diffView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            diffView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            diffView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -.largeLittMargin)
        ])
    }
    
    private func performDishChange() {
        guard let selectedDish = selectedDish else { return }
        dietService.setNewDish(dish: DietDish(id: 0,
                                              dish: selectedDish,
                                              imageURL: selectedDish.imageURL),
                               dayIndex: dayIndex,
                               mealIndex: mealIndex,
                               completion: { result in
            self.delegate?.changeDishDidComplete()
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
            self.dismiss(animated: true)
        })
    }
    
    @objc
    private func didTapCloseButton() {
        dismiss(animated: true)
    }
    
    @objc
    private func didTapDoneButton() {
        performDishChange()
    }
    
    private func showDishDetail(selectedDish: DishProtocol) {
        let vc = DishDetailsViewController(measurementService: MeasurementService(type: .smallWeight, unitStyle: .short, unitOptions: .providedUnit),
                                           energyMeasurement: MeasurementService.init(type: .energy).measurement(), dish: selectedDish)
        present(vc, animated: true)
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension ChangeDishViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        anotherDishes.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: changeDishCollectionViewCellId,
                                                      for: indexPath) as! ChangeDishCollectionViewCell
        cell.configureCell(withDish: anotherDishes[indexPath.row], targetSize: Settings.cellSize)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showDishDetail(selectedDish: anotherDishes[indexPath.row])
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        let targetX = collectionView.contentOffset.x + velocity.x * 60.0
        var targetIndex: CGFloat
        if velocity.x > 0 {
            targetIndex = ceil(targetX / Settings.cellSize.width)
        } else if velocity.x == 0 {
            targetIndex = round(targetX / Settings.cellSize.width)
        } else {
            targetIndex = floor(targetX / Settings.cellSize.width)
        }
        targetIndex = min(CGFloat(collectionView.numberOfItems(inSection: 0)), max(0, targetIndex))
        targetContentOffset.pointee.x = (Settings.cellSize.width + layout.minimumLineSpacing) * targetIndex - collectionView.contentInset.left
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        collectionView.visibleCells.forEach {
            if $0.frame.contains(CGPoint(x: collectionView.contentOffset.x + UIScreen.main.bounds.width / 2,
                                         y: Settings.cellSize.height / 2)) {
                let indexPath = collectionView.indexPath(for: $0)
                if selectedDish?.dishId != anotherDishes[indexPath?.row ?? 0].dishId {
                    selectedDish = anotherDishes[indexPath?.row ?? 0]
                }
            }
        }
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred(intensity: 1.0)
    }
}
