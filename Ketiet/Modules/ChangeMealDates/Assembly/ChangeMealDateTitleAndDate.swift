//
//  ChangeMealDateTitleAndDate.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct ChangeMealDateTitleAndDate {
    let title: String
    let date: Date
}
