//
//  ChangeMealDatesAssembly.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class ChangeMealDatesAssembly {
    
    private let view: UIViewController
    
    init(
        title: String,
        dataProvider: ChangeMealDateTitleProviderProtocol,
        actionButtonTitle: String,
        mealDatesStorage: MealDatesStorageProtocol,
        completion: @escaping () -> Void
    ) {
        let presenter = ChangeMealDatesPresenter(
            datesService: mealDatesStorage,
            completion: completion
        )
        
        let parameters = dataProvider.titleAndDates().map {
            ParameterItem(
                title: $0.title,
                valueType: .time,
                defaultValue: .date($0.date)
            )
        }
        view = ParametersViewController(
            title: title,
            validatorFactory: ValidatorFactory(),
            parameters: parameters,
            buttonTitle: actionButtonTitle,
            presenter: presenter
        )
    }
    
    func initialViewController() -> UIViewController {
        view
    }
}
