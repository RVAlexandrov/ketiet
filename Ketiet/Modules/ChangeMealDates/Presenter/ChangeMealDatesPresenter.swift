//
//  ChangeMealDatesPresenter.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

struct ChangeMealDatesPresenter {
    
    private let datesService: MealDatesStorageProtocol
    private let completion: () -> Void
    
    init(
        datesService: MealDatesStorageProtocol,
        completion: @escaping () -> Void
    ) {
        self.datesService = datesService
        self.completion = completion
    }
}

extension ChangeMealDatesPresenter: ParametersPresenterProtocol {
    func didCompleteValuesInput(values: [LittValue?]) {
        let dates = values.map { $0?.date ?? Date() }
        datesService.save(dates: dates)
        completion()
    }
}
