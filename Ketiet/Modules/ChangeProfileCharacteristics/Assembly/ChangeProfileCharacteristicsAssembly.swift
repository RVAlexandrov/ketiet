//
//  ChangeProfileCharacteristicsAssembly.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import UIKit

final class ChangeProfileCharacteristicsAssembly {
    
    private let view: UIViewController
    
    init(
        profile: ProfileProtocol,
        completion: @escaping () -> Void
    ) {
        let presenter = ChangeProfileCharacteristicsPresenter(
            profileService: ProfileService.instance,
            completion: completion
        )
        let tallService = MeasurementService(type: .tall)
        let weightService = MeasurementService(type: .weight)
        
        var weigthString = ""
        
        if let weight = profile.weight {
            let floatWeight = Float(weight).round(to: 1)
            weigthString = String(floatWeight)
        }
        
        let parameters = [ParameterItem(title: "INPUT_NAME".localized(),
                                        valueType: .text,
                                        defaultValue: profile.name.map { LittValue.text($0) }),
                          ParameterItem(title: "INPUT_AGE".localized(),
                                        valueType: .int,
                                        defaultValue: profile.age.map { LittValue.text($0.description) }),
                          ParameterItem(title: "CHOOSE_YOUR_GENDER".localized(),
                                        valueType: .select(Gender.allCases.map { $0.title }),
                                        defaultValue: profile.gender.map { LittValue.text($0.title) }),
                          ParameterItem(title: "INPUT_TALL".localized() + ", " + tallService.measurement(),
                                        valueType: .float,
                                        defaultValue: profile.tall.map { LittValue.text($0.description) }),
                          ParameterItem(title: "INPUT_WEIGHT".localized() + ", " + weightService.measurement(),
                                        valueType: .float,
                                        defaultValue: LittValue.text(weigthString)),
                          ParameterItem(title: "CHOOSE_YOUR_GOAL".localized(),
                                        valueType: .select(Goal.allCases.map { $0.description }),
                                        defaultValue: profile.goal.map { LittValue.text($0.description) }),
                          ParameterItem(title: "PHYSICAL_ACTIVITY".localized(),
                                        valueType: .select(PhysicalActivity.allCases.map { $0.description }),
                                        defaultValue: profile.physicalActivity.map { LittValue.text($0.description) })]
        view = ParametersViewController(
            title: "CHARACTERISTICS".localized(),
            validatorFactory: ValidatorFactory(),
            parameters: parameters,
            buttonTitle: "SAVE".localized(),
            presenter: presenter
        )
    }
    
    func initialViewController() -> UIViewController {
        view
    }
}
