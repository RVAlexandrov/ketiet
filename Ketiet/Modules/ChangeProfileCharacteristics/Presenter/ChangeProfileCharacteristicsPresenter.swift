//
//  ChangeProfileCharacteristicsPresenter.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import Foundation

struct ChangeProfileCharacteristicsPresenter {
    
    private let profileService: ProfileServiceProtocol
    private let completion: () -> Void
    
    init(
        profileService: ProfileServiceProtocol,
        completion: @escaping () -> Void
    ) {
        self.profileService = profileService
        self.completion = completion
    }
}

extension ChangeProfileCharacteristicsPresenter: ParametersPresenterProtocol {
    
    func didCompleteValuesInput(values: [LittValue?]) {
        let name = values[safe: 0]??.text
        let age = values[safe: 1]??.int
        let gender = Gender(title: values[safe: 2]??.text ?? "")
        let tall = values[safe: 3]??.float
        let weight = values[safe: 4]??.float
        let goal = Goal(description: values[safe: 5]??.text ?? "")
        let physicalActivity = PhysicalActivity(description: values[safe: 6]??.text ?? "")
        let group = DispatchGroup()
        if let name = name {
            group.enter()
            profileService.setName(name, completion: { _ in group.leave() })
        }
        if let age = age {
            group.enter()
            profileService.setAge(age) { _ in group.leave() }
        }
        if let gender = gender {
            group.enter()
            profileService.setGender(gender) { _ in group.leave() }
        }
        if let tall = tall {
            group.enter()
            profileService.setInitialTall(Double(tall)) { _ in group.leave() }
        }
        if let weight = weight {
            group.enter()
            profileService.setInitialWeight(Double(weight)) { _ in group.leave() }
        }
        if let goal = goal {
            group.enter()
            profileService.setInitialGoal(goal) { _ in group.leave() }
        }
        if let physicalActivity = physicalActivity {
            group.enter()
            profileService.setInitialPhysicalActivity(physicalActivity) { _ in group.leave() }
        }
        group.notify(queue: .main, execute: completion)
    }
    
}
