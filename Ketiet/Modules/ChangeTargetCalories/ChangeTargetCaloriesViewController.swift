//
//  ChangeTargetCaloriesViewController.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 05.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation
import UIKit
import DietCore

class ChangeTargetCaloriesViewController: UIViewController {
    private let currentCalories: Float
    private let continueButtonHandler: (Float, ChangeTargetCaloriesViewController) -> Void
    private let energyMeasurementService: MeasurementServiceProtocol
    private let lowBoundCalories: Float
    private let hightBoundCalories: Float
    
    private var button: LittBasicButton?
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .littSecondaryBackgroundColor
        view.layer.cornerRadius = .littBasicCornerRadius
        return view
    }()
    
    private lazy var caloriesTextField: UITextField = {
        let textField = UITextField()
        textField.font = .giantDigits
        textField.keyboardType = .numberPad
        textField.tintColor = .accentColor
        textField.textColor = .accentColor
        textField.textAlignment = .center
        textField.text = currentCalories.cleanValue
        textField.addTarget(self, action: #selector(didChangeTextFieldValue), for: .editingChanged)
        textField.inputAccessoryView = LittTextFieldView.configureDoneAccessoryView()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private lazy var measurementLabel: UILabel = {
        let label = UILabel()
        label.font = .body
        label.textColor = .secondaryLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var slider: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.tintColor = .accentColor
        slider.addTarget(self, action: #selector(didChangeSliderValue), for: .valueChanged)
        return slider
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .secondaryLabel
        return label
    }()
    
    init(energyMeasurementService: MeasurementServiceProtocol,
         lowBoundCalories: Float = 1200,
         hightBoundCalories: Float = 5000,
         currentCalories: Float = 1800,
         continueButtonHandler: @escaping(Float, ChangeTargetCaloriesViewController) -> Void) {
        self.energyMeasurementService = energyMeasurementService
        self.lowBoundCalories = lowBoundCalories
        self.hightBoundCalories = hightBoundCalories
        self.currentCalories = currentCalories
        self.continueButtonHandler = continueButtonHandler
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setupSlider()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        littTabBarController?.setTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        littTabBarController?.setTabBarHidden(false)
    }
}

// MARK: - Private methods
extension ChangeTargetCaloriesViewController {
    private func configureView() {
        title = "CHOOSE_TARGET_CALORIES".localized()
        
        view.backgroundColor = .littBackgroundColor
        view.addSubviews([containerView, descriptionLabel])
        
        let buttonAndOffset = addBottomButtonWithBlur(with: "CONTINUE".localized())
        buttonAndOffset.0.setTapHandler { [weak self] in
            guard let self = self else { return }
            self.continueButtonHandler(self.slider.value, self)
        }
        
        button = buttonAndOffset.0
        
        containerView.addSubviews([caloriesTextField, measurementLabel, slider])
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .logicBlockLittMargin),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .largeLittMargin),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.largeLittMargin),
            
            descriptionLabel.topAnchor.constraint(equalTo: containerView.bottomAnchor, constant: .smallLittMargin),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .largeLittMargin),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.largeLittMargin),
            
            caloriesTextField.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .logicBlockLittMargin),
            caloriesTextField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            caloriesTextField.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.logicBlockLittMargin),
            
            measurementLabel.topAnchor.constraint(equalTo: caloriesTextField.bottomAnchor, constant: .smallLittMargin),
            measurementLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            
            slider.topAnchor.constraint(equalTo: measurementLabel.bottomAnchor, constant: .logicBlockLittMargin),
            slider.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .logicBlockLittMargin),
            slider.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.logicBlockLittMargin),
            slider.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -.logicBlockLittMargin)
        ])
    }
    
    private func setupSlider() {
        let lowBoundValueInLocalMeasurement = energyMeasurementService.convertToLocale(value: lowBoundCalories)
        let hightBoundValueInLocalMeasurement = energyMeasurementService.convertToLocale(value: hightBoundCalories)

        slider.minimumValue = lowBoundValueInLocalMeasurement
        slider.maximumValue = hightBoundValueInLocalMeasurement
        slider.value = currentCalories
        
        descriptionLabel.text = "CHOOSE_TARGET_CALORIES_DESCRIPTION".localized() + " " +  lowBoundValueInLocalMeasurement.cleanValue + " - " + hightBoundValueInLocalMeasurement.cleanValue + " " + energyMeasurementService.measurement()
        
        measurementLabel.text = energyMeasurementService.measurement()
    }
        
    @objc
    private func didChangeSliderValue() {
        navigationItem.rightBarButtonItem?.isEnabled = true
        caloriesTextField.text = slider.value.cleanValue
        let generator = UISelectionFeedbackGenerator()
        generator.selectionChanged()
    }
    
    @objc
    private func didChangeTextFieldValue() {
        if let currentFloatValue = Float(caloriesTextField.text ?? "0")  {
            slider.value = currentFloatValue
            if currentFloatValue >= lowBoundCalories && currentFloatValue <= hightBoundCalories {
                caloriesTextField.textColor = .accentColor
                if !(button?.isEnabled ?? true) {
                    UINotificationFeedbackGenerator().notificationOccurred(.success)
                }
                button?.isEnabled = true
            } else {
                caloriesTextField.textColor = .systemRed
                if button?.isEnabled ?? false {
                    UINotificationFeedbackGenerator().notificationOccurred(.error)
                }
                button?.isEnabled = false
            }
        } else {
            caloriesTextField.textColor = .systemRed
            if button?.isEnabled ?? false {
                UINotificationFeedbackGenerator().notificationOccurred(.error)
            }
            button?.isEnabled = false
        }
    }
}
