//
//  CookingStepsViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class CookingStepsViewController: BasicModalWithTableViewController {
    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private let steps: [CookingStep]
    private let mealName: String
    
    init(steps: [CookingStep],
         mealName: String) {
        self.steps = steps
        self.mealName = mealName
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        configureTableViewItems()
    }
    
    private func configureTableViewItems() {
        tableView.animations = []
        let headerTitle = SingleLabelCellItem(text: mealName,
                                              textAlignment: .left,
                                              font: .title)
        let afterTitleSpacer = SpaceCellItem(spaceHeight: .logicBlockLittMargin)
        
        var stepsItems = [LittTableViewCellItemProtocol]()
        steps.enumerated().forEach {
            let titleItem = SingleLabelCellItem(text: $0.element.title,
                                                 textAlignment: .left,
                                                 font: .subTitle)
            let afterTitleSpacer = SpaceCellItem(spaceHeight: .largeLittMargin)
            let descriptionItem = SingleLabelCellItem(text: $0.element.description,
                                                      textAlignment: .left,
                                                      font: .body)
            let afterDescriptionSpacer = SpaceCellItem(spaceHeight: .largeLittMargin)
            
            var itemsToAppend: [LittTableViewCellItemProtocol] = [titleItem,
                                                                  afterTitleSpacer,
                                                                  descriptionItem,
                                                                  afterDescriptionSpacer]
            
            if let imageWrapper = $0.element.imageWrapper {
                let imageItem = ImageCellItem(imageWrapper: imageWrapper)
                
                let afterImageSpacer = SpaceCellItem(spaceHeight: .logicBlockLittMargin)
                
                itemsToAppend.append(imageItem)
                itemsToAppend.append(afterImageSpacer)
            }
            
            stepsItems.append(contentsOf: itemsToAppend)
        }
        
        tableView.sections = [LittTableViewSection(items: [headerTitle,
                                                           afterTitleSpacer] + stepsItems)]
        tableView.reloadDataWithAnimation()
    }
}
