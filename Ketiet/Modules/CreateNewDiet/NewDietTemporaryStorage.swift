//
//  NewDietTemporaryStorage.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 08.12.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Combine
import UIKit
import DietCore

final class NewDietTemporaryStorage {
    var profileRegistered = false {
        didSet {
            if profileRegistered {
                saveProfileAndDates()
            }
        }
    }
    private var profile = Profile()
    private lazy var imageSubject = PassthroughSubject<UIImage, Never>()
    private lazy var nameSubject = PassthroughSubject<String, Never>()
    private(set) var currentDates = [Date]()
    
    private func saveProfileAndDates() {
        MealDatesService.shared.save(dates: currentDates)
        
        let profileService = ProfileService.instance
        if let gender = profile.gender {
            profileService.setGender(gender) { _ in }
        }
        if let goal = profile.goal {
            profileService.setInitialGoal(goal) { _ in }
        }
        if let weight = profile.weight {
            profileService.setInitialWeight(weight) { _ in }
        }
        if let image = profile.image {
            profileService.setProfileImage(image) { _ in }
        }
        if let tall = profile.tall {
            profileService.setInitialTall(tall) { _ in }
        }
        if let activity = profile.physicalActivity {
            profileService.setInitialPhysicalActivity(activity) { _ in }
        }
        if let age = profile.age {
            profileService.setAge(age) { _ in }
        }
        if let name = profile.name {
            profileService.setName(name) { _ in }
        }
    }
}

extension NewDietTemporaryStorage: ProfileServiceProtocol {
    func setDailyCalories(_ value: Float, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.dailyCalories = value
        completion(.success(()))
    }
    
    func getDailyCalories(completion: @escaping (Float?) -> Void) {
        completion(profile.dailyCalories)
    }
    
    func setCaloriesCalculatorType(_ value: CaloriesCalculatorType, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.caloriesCalculatorType = value
        completion(.success(()))
    }
    
    func getCaloriesCalculatorType(completion: @escaping (CaloriesCalculatorType?) -> Void) {
        completion(profile.caloriesCalculatorType)
    }
    
    func setGender(_ value: Gender, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.gender = value
        completion(.success(()))
    }
    
    func setAge(_ value: Int, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.age = value
        completion(.success(()))
    }
    
    func setProfileImage(_ value: UIImage, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.image = value
        completion(.success(()))
    }
    
    func setName(_ value: String, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.name = value
        completion(.success(()))
    }
    
    func removeProfile() {
        profile = Profile()
        profileRegistered = false
    }
    
    var imagePublisher: AnyPublisher<UIImage, Never> {
        imageSubject.eraseToAnyPublisher()
    }
    
    var namePublisher: AnyPublisher<String, Never> {
        nameSubject.eraseToAnyPublisher()
    }
    
    func getWeight(completion: @escaping (Double?) -> Void) {
        completion(profile.weight)
    }
    
    func getGender(completion: @escaping (Gender?) -> Void) {
        completion(profile.gender)
    }
    
    func getGoal(completion: @escaping (Goal?) -> Void) {
        completion(profile.goal)
    }
    
    func getName(completion: @escaping (String?) -> Void) {
        completion(profile.name)
    }
    
    func getSurname(completion: @escaping (String?) -> Void) {
        completion(profile.surname)
    }
    
    func getAge(completion: @escaping (Int?) -> Void) {
        completion(profile.age)
    }
    
    func getPhysicalActivity(completion: @escaping (PhysicalActivity?) -> Void) {
        completion(profile.physicalActivity)
    }
    
    func getWidgetConfiguration(completion: @escaping ([WidgetType]?) -> Void) {
        completion(profile.widgetConfiguration)
    }
    
    func getProfileImage(completion: @escaping (UIImage?) -> Void) {
        completion(profile.image)
    }
    
    func getProfile(completion: @escaping (ProfileProtocol?) -> Void) {
        completion(profile)
    }
    
    func getTargetWater(completion: @escaping (Double) -> Void) {
        let waterLevel = DailyWaterConsumptionCalculator().targetWaterVolume(
            physicalActivity: profile.physicalActivity,
            isMale: profile.gender == .male,
            weight: profile.weight
        )
        completion(waterLevel)
    }
    
    func setInitialTall(_ value: Double, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.tall = value
        completion(.success(()))
    }
    
    func setInitialWeight(_ value: Double, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.weight = value
        completion(.success(()))
    }
    
    func setInitialGoal(_ value: Goal, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.goal = value
        completion(.success(()))
    }
    
    func setInitialPhysicalActivity(_ value: PhysicalActivity, completion: @escaping (Result<Void, Error>) -> Void) {
        profile.physicalActivity = value
        completion(.success(()))
    }
    
    func setWidgetConfiguration(widgetTypes: [WidgetType], completion: @escaping (Result<Void, Error>) -> Void) {
        profile.widgetConfiguration = widgetTypes
        completion(.success(()))
    }
}

extension NewDietTemporaryStorage: MealDatesStorageProtocol {
    
    func save(dates: [Date]) {
        currentDates = dates
    }
}
