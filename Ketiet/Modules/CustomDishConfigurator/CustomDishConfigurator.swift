//
//  CustomDishConfigurator.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

class CustomDishViewController: UIViewController {
    
    let customDishesService: CustomDishesServiceProtocol
    let customDish: DishProtocol?
    
    init(customDishesService: CustomDishesServiceProtocol,
         customDish: DishProtocol?) {
        self.customDishesService = customDishesService
        self.customDish = customDish
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
}

// MARK: - private methods
extension CustomDishViewController {
    private func configureView() {
        view.addSubviews([
            
        ])
        
        NSLayoutConstraint.activate([
            
        ])
    }
}
