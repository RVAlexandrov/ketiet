//
//  DayMenuDisplayingAssembly.swift
//  ketiet
//
//  Created by Alex on 12/06/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import EventKit
import UIKit

final class DayMenuDisplayingAssembly {
    
	private let view: UIViewController
    
    init() {
		let presenter = DayMenuDisplayingPresenter()
		let interactor = DayMenuDisplayingInteractor(
            presenter: presenter,
            dietService: DietService.shared,
            mealDatesService: MealDatesService.shared
        )
        let progressManagerFabric = ProgressManagerFabric()
		let view = DayMenuDisplayingViewController(
            presenter: presenter,
            alertFactory: AlertControllerFactory(),
            weightProgressManager: progressManagerFabric.createManager(forType: .weight),
            waterProgressManager: progressManagerFabric.createManager(forType: .water),
            waistProgressManager: progressManagerFabric.createManager(forType: .waist),
            hipsProgressManager: progressManagerFabric.createManager(forType: .hips),
            chestProgressManager: progressManagerFabric.createManager(forType: .chest)
        )
		
		presenter.view = view
		presenter.interactor = interactor
		self.view = view
	}
	
	func initialViewController() -> UIViewController {
		return view
	}
}
