//
//  DayMenuDisplayingInteractor.swift
//  ketiet
//
//  Created by Alex on 12/06/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Combine
import Foundation

final class DayMenuDisplayingInteractor {
	
    private weak var presenter: DayMenuDisplayingInteractorOutputProtocol?
	private let dietService: DietServiceProtocol
	private var days: [DietDayModel]?
    private let mealDatesService: MealDatesStorageProtocol
    private var observer: NSObjectProtocol?
    private var mealDatesUpdateSubscription: AnyCancellable?
	
	init(
        presenter: DayMenuDisplayingInteractorOutputProtocol,
        dietService: DietServiceProtocol,
        mealDatesService: MealDatesServiceProtocol
    ) {
        self.presenter = presenter
		self.dietService = dietService
        self.mealDatesService = mealDatesService
        observer = NotificationCenter.default.addObserver(forName: .didSetCurrentDietNotification,
                                                          object: nil,
                                                          queue: .main) { [weak self] _ in
            self?.requestDietDays()
        }
        mealDatesUpdateSubscription = mealDatesService.updateMealDatesPublisher.sink { [weak self] _ in
            self?.requestDietDays()
        }
    }
}

// MARK: - DayMenuDisplayingInteractorInputProtocol
extension DayMenuDisplayingInteractor: DayMenuDisplayingInteractorInputProtocol {
	
	func requestDietDays() {
		dietService.requestCurrentDiet { [weak self] diet in
            guard let self = self else { return }
			guard let diet = diet,
				case .inProgress = diet.status,
				let dayIndex = self.dietService.getCurrentDayIndex() else {
				self.days = []
				self.presenter?.didFetch(days: [])
				return
			}
            let days = diet.makeDietDays(
                currentDayIndex: dayIndex,
                startingFromCurrentDay: false,
                mealDates: self.mealDatesService.currentDates
            )
			self.days = days
            self.presenter?.didFetch(days: days)
		}
	}
}

extension DietProtocol {
    func makeDietDays(
        currentDayIndex: Int,
        startingFromCurrentDay: Bool,
        mealDates: [Date]
    ) -> [DietDayModel] {
		guard case .inProgress = status else {
			return []
		}
		return days.enumerated().map {
			let components = Date().dateComponentsByAdding(DateComponents(day: $0.offset - currentDayIndex),
														   resultUnints: [.year, .month, .day])
            let meals: [DayMeal] = $0.element.meals.enumerated().compactMap {
                let (offset, meal) = $0
				var mealsComponents = components
                mealsComponents.hour = mealDates[offset].hour
                mealsComponents.minute = mealDates[offset].minute
				if !startingFromCurrentDay {
					return DayMeal(meal: meal, fullDateComponents: mealsComponents)
				}
				if let mealDate = Calendar.current.date(from: mealsComponents), mealDate >= Date() {
					return DayMeal(meal: meal, fullDateComponents: mealsComponents)
				}
				return nil
			}
			return DietDayModel(id: $0.element.id, dateComponents: components, meals: meals)
		}
	}
}
