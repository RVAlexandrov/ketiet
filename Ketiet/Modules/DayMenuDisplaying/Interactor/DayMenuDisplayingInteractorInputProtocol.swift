//
//  DayMenuDisplayingInteractorInputProtocol.swift
//  ketiet
//
//  Created by Alex on 12/06/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol DayMenuDisplayingInteractorInputProtocol {
	
    func requestDietDays()
}
