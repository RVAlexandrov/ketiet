//
//  DayMenuDisplayingInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 12/06/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol DayMenuDisplayingInteractorOutputProtocol: AnyObject {
	
	func didFetch(days: [DietDayModel])
}
