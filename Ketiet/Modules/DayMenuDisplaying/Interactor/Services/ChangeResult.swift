//
//  ChangeResult.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 28.09.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

enum ChangeResult {
    case changed
    case nothingHasBeenChange
    case accessDenied
    case error
}
