//
//  RemindersExporter.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 02.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Combine
import Foundation

final class RemindersExporter {
    
    static let shared = RemindersExporter()
    
    private var mealDateChangeSubscription: AnyCancellable?
    private lazy var remindersService = RemindersService(eventStore: .shared)
    
    private init() {}
    
    func startListeningMealDatesUpdates() {
        let updatePublisher = MealDatesService.shared.updateMealDatesPublisher
        mealDateChangeSubscription = updatePublisher.sink { [weak self] dates in
            self?.reexportReminders(dates: dates)
        }
    }
    
    private func reexportReminders(dates: [Date]) {
        remindersService.removeAllMealReminders { [weak self] result in
            guard result == .changed else { return }
            self?.exportReminders(dates: dates)
        }
    }
    
    private func exportReminders(dates: [Date]) {
        let dietService = DietService.shared
        dietService.requestCurrentDiet { [weak self, weak dietService] diet in
            guard let diet = diet,
                let currentDayIndex = dietService?.getCurrentDayIndex() else { return }
            let days = diet.makeDietDays(
                currentDayIndex: currentDayIndex,
                startingFromCurrentDay: true,
                mealDates: dates
            )
            let allMeals = days.reduce(into: [DayMeal]()) { $0.append(contentsOf: $1.meals) }
            self?.remindersService.addReminders(
                for: allMeals,
                completion: { _ in }
            )
        }
    }
}
