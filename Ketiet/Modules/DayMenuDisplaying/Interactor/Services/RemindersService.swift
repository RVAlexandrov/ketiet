//
//  RemindesService.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 07.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import EventKit
import UIKit

final class RemindersService {
	
	private struct Constants {
		static let calendarTitle = "Ketiet"
	}
	
	private let eventStore: EKEventStore
	private lazy var calendar = storedCalendar() ?? makeNewCalendar()
	private lazy var queue = DispatchQueue(label: "com.Ketiet.RemindersService.processQueue")
	
	init(eventStore: EKEventStore) {
		self.eventStore = eventStore
	}
    
    private func storedCalendar() -> EKCalendar? {
        eventStore.calendars(for: .reminder).first { $0.title == Constants.calendarTitle }
    }
	
	private func makeNewCalendar() -> EKCalendar? {
		guard accessGranted else { return nil }
		let calendar = EKCalendar(for: .reminder, eventStore: eventStore)
        calendar.cgColor = UIColor.accentColor.cgColor
		calendar.title = Constants.calendarTitle
        let firstSource = eventStore.sources.first { $0.sourceType == .calDAV && $0.title == "iCloud" }
        let secondSource = eventStore.sources.first { $0.sourceType == .local }
		calendar.source = firstSource ?? secondSource
		do {
			try eventStore.saveCalendar(calendar, commit: true)
		} catch {
			assertionFailure(error.localizedDescription)
		}
		return calendar
	}
	
    private func addReminder(meal: DayMeal, shouldSave: Bool = false) {
		guard accessGranted, calendar != nil else { return }
		let reminder = EKReminder(eventStore: eventStore)
		reminder.title = meal.meal.name
        reminder.notes = meal.meal.dishes.map(\.name).joined(separator: "\n")
		reminder.startDateComponents = meal.fullDateComponents
		if let date = Calendar.current.date(from: meal.fullDateComponents) {
			reminder.addAlarm(EKAlarm(absoluteDate: date))
		}
		reminder.calendar = calendar
		do {
			try eventStore.save(reminder, commit: shouldSave)
		} catch {
			assertionFailure(error.localizedDescription)
		}
	}
    
    private func remove(
        reminders: [EKReminder]?,
        completion: @escaping (ChangeResult) -> Void
    ) {
        guard let reminders = reminders, !reminders.isEmpty else {
            completion(.nothingHasBeenChange)
            return
        }
        do {
            try reminders.forEach { try eventStore.remove($0, commit: false) }
        } catch {
            assertionFailure(error.localizedDescription)
            completion(.error)
            return
        }
        do {
            try eventStore.commit()
            completion(.changed)
        } catch {
            assertionFailure(error.localizedDescription)
            completion(.error)
        }
    }
}

extension RemindersService: RemindersServiceProtocol {
    
    var shouldAskFirstTimeAccess: Bool {
        EKEventStore.authorizationStatus(for: .reminder) == .notDetermined
    }
    
    var accessGranted: Bool {
        EKEventStore.authorizationStatus(for: .reminder) == .authorized
    }
    
	func requestAccess(completion: @escaping (Bool) -> Void) {
		switch EKEventStore.authorizationStatus(for: .reminder) {
		case .authorized:
			completion(true)
		case .denied, .restricted:
			completion(false)
		case .notDetermined:
			eventStore.requestAccess(to: .reminder) { granted, error in
				if let error = error {
					assertionFailure(error.localizedDescription)
				}
				completion(granted)
			}
		@unknown default:
			assertionFailure("Unknown case")
			completion(false)
		}
	}
    
    func addReminders(
        for meals: [DayMeal],
        completion: @escaping (ChangeResult) -> Void
    ) {
        guard accessGranted else {
            completion(.accessDenied)
            return
        }
        guard !meals.isEmpty else {
            completion(.nothingHasBeenChange)
            return
        }
        queue.async {
            meals.forEach { self.addReminder(meal: $0) }
            do {
                try self.eventStore.commit()
                completion(.changed)
            } catch {
                assertionFailure(error.localizedDescription)
                completion(.error)
            }
        }
    }
    
    func removeAllMealReminders(completion: @escaping (ChangeResult) -> Void) {
        guard accessGranted, let calendar = self.calendar else {
            completion(.accessDenied)
            return
        }
        queue.async {
            let predicate = self.eventStore.predicateForReminders(in: [calendar])
            self.eventStore.fetchReminders(
                matching: predicate
            ) { [weak self] reminders in
                self?.remove(
                    reminders: reminders,
                    completion: completion
                )
            }
        }
        
    }
}
