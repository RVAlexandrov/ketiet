//
//  RemindesServiceProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 07.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol RemindersServiceProtocol {
    var shouldAskFirstTimeAccess: Bool { get }
	var accessGranted: Bool { get }
	func requestAccess(completion: @escaping(Bool) -> Void)
    func addReminders(
        for meals: [DayMeal],
        completion: @escaping (ChangeResult) -> Void
    )
    func removeAllMealReminders(completion: @escaping (ChangeResult) -> Void)
}
