//
//  DayMenuDisplayingPresenter.swift
//  ketiet
//
//  Created by Alex on 12/06/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class DayMenuDisplayingPresenter {

    weak var view: DayMenuDisplayingViewInputProtocol?
    var interactor: DayMenuDisplayingInteractorInputProtocol?
}

extension DayMenuDisplayingPresenter: DayMenuDisplayingViewOutputProtocol {

    func requestData() {
		interactor?.requestDietDays()
		view?.showLoading()
    }
}

extension DayMenuDisplayingPresenter: DayMenuDisplayingInteractorOutputProtocol {
	
	func didFetch(days: [DietDayModel]) {
		DispatchQueue.main.async {
			self.view?.hideLoading()
			self.view?.show(days: days)
		}
	}
}
