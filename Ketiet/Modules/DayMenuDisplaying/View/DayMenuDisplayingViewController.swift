//
//  DayMenuDisplayingViewController.swift
//  ketiet
//
//  Created by Alex on 12/06/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import FSCalendar
import ViewAnimator
import DietCore
import Combine

final class DayMenuDisplayingViewController: UIViewController {
	
	private struct Constants {
		static let interestedDateComponents: Set<Calendar.Component> = [.day, .month, .year]
	}
    
    private let presenter: DayMenuDisplayingViewOutputProtocol
	private var days: [DietDayModel]?
    private var currentDayModel: DietDayModel?
    private var currentDate: Date?
    
    private var cancellables = Set<AnyCancellable>()
	
	private let containerView: UIView = {
		let view = UIView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.backgroundColor = .littBackgroundColor
		return view
	}()
	
	private let mealsView: LittTableView = {
        let view = LittTableView(frame: .zero, style: .insetGrouped)
		view.translatesAutoresizingMaskIntoConstraints = false
		view.setContentInsetForControllersInTabbar()
		view.showsVerticalScrollIndicator = false
		view.backgroundColor = .littBackgroundColor
		view.clipsToBounds = true
		view.alwaysBounceVertical = false
        view.animations = []
		return view
	}()
	
	private lazy var calendarView: FSCalendar = {
		let view = FSCalendar()
		view.delegate = self
		view.select(Date())
		view.scope = .week
		view.translatesAutoresizingMaskIntoConstraints = false
        view.appearance.headerTitleFont = .bodySemibold
		view.appearance.titleFont = .subTitle
		view.appearance.weekdayFont = .bodySemibold
		view.appearance.weekdayTextColor = .label
		view.appearance.headerTitleColor = .label
		view.appearance.titleDefaultColor = .label
		view.appearance.subtitleTodayColor = .label
		view.appearance.selectionColor = .selectedDayColor
		view.firstWeekday = 2
		return view
	}()
	
	private lazy var scopeGestureRecognizer: UIPanGestureRecognizer = {
		let recognizer = UIPanGestureRecognizer(target: calendarView, action: #selector(calendarView.handleScopeGesture(_:)))
		recognizer.delegate = self
		return recognizer
	}()
	
	private lazy var calendarViewHeightConstraint = calendarView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
	
	private lazy var loadingView: LoadingIndicator = {
		let loadingView = LoadingIndicator(frame: UIScreen.main.bounds)
		view.addSubview(loadingView)
		loadingView.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			loadingView.widthAnchor.constraint(equalTo: view.widthAnchor),
			loadingView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height),
			loadingView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			loadingView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])
		return loadingView
	}()
    private let alertFactory: AlertControllerFactoryProtocol
    private let weightProgressManager: ProgressManagerProtocol
    private let waterProgressManager: ProgressManagerProtocol
    private let waistProgressManager: ProgressManagerProtocol
    private let hipsProgressManager: ProgressManagerProtocol
    private let chestProgressManager: ProgressManagerProtocol

    init(presenter: DayMenuDisplayingViewOutputProtocol,
         alertFactory: AlertControllerFactoryProtocol,
         weightProgressManager: ProgressManagerProtocol,
         waterProgressManager: ProgressManagerProtocol,
         waistProgressManager: ProgressManagerProtocol,
         hipsProgressManager: ProgressManagerProtocol,
         chestProgressManager: ProgressManagerProtocol) {
        self.presenter = presenter
        self.alertFactory = alertFactory
        self.weightProgressManager = weightProgressManager
        self.waterProgressManager = waterProgressManager
        self.waistProgressManager = waistProgressManager
        self.hipsProgressManager = hipsProgressManager
        self.chestProgressManager = chestProgressManager
        
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
		view.addGestureRecognizer(scopeGestureRecognizer)
		mealsView.panGestureRecognizer.require(toFail: scopeGestureRecognizer)
		addSubviews()
		presenter.requestData()
        
        NotificationCenter
            .default
            .publisher(for: UIApplication.didBecomeActiveNotification)
            .sink { [weak self] notification in
                self?.presenter.requestData()
            }
            .store(in: &cancellables)
    }
	
	override func loadView() {
		view = UIView()
		view.addSubview(containerView)
		containerView.pinToSuperview()
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        reloadDataSection()
    }
}

// MARK: - DayMenuDisplayingViewInputProtocol
extension DayMenuDisplayingViewController: DayMenuDisplayingViewInputProtocol {
	func showLoading() {
		loadingView.startAnimating()
	}
	
	func hideLoading() {
		loadingView.stopAnimating()
	}

	func show(days: [DietDayModel]) {
		self.days = days
		calendarView.reloadData()
		displayCurrentDayMealsIfPossible()
	}
}

// MARK: - Private methods
extension DayMenuDisplayingViewController {
    private func displayCurrentDayMealsIfPossible() {
        guard let day = day(from: Date()) else {
            displayEmptyState()
            currentDate = Date()
            return
        }
        currentDayModel = day
        currentDate = currentDayModel?.dateComponnets.date
        var arrayOfSections = [LittTableViewSectionProtocol]()
        if day.state == .completed || day.state == .incompleted {
            
            // прошедший или сегодняшний день (показываем данные)
            arrayOfSections = [makeMeasureSection(forDay: day.id)]
            arrayOfSections.append(contentsOf: mealsSections(from: day, isFutureDay: false))
            
        } else {
            
            // будущий день (не показываем данные)
            arrayOfSections.append(contentsOf: mealsSections(from: day, isFutureDay: true))
        }
        
        mealsView.sections = arrayOfSections
    }
    
    private func addSubviews() {
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = .systemGray2
        containerView.addSubviews([calendarView, separatorView, mealsView])
        NSLayoutConstraint.activate([
            calendarView.topAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.topAnchor),
            calendarView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            calendarView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            calendarViewHeightConstraint,
            calendarView.bottomAnchor.constraint(equalTo: mealsView.topAnchor),

            separatorView.bottomAnchor.constraint(equalTo: calendarView.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 0.5),
            separatorView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            
            mealsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            mealsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            mealsView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
        ])
    }
	
    private func dishItem(for dishIndex: Int,
                          mealIndex: Int,
                          day: DietDayModel) -> DayDishTableViewItem {
        DayDishTableViewItem(dish: day.meals[mealIndex].meal.dishes[dishIndex],
                                        mealIndex: mealIndex,
                                        dayIndex: day.id,
                                        dietService: DietService.shared,
                                        delegate: self)
	}
    
    private func mealsSections(from day: DietDayModel, isFutureDay: Bool) -> [LittTableViewSectionProtocol] {
        
        day.meals.enumerated().map { mealPair -> LittTableViewSectionProtocol in
            let items: [DayDishTableViewItem] = mealPair.element.meal.dishes.enumerated().map {
                dishItem(for: $0.offset,
                            mealIndex: mealPair.offset,
                            day: day)
            }
            
            let mealItem = DayMealItem(dayIndex: day.id,
                                       meal: mealPair.element,
                                       dietService: DietService.shared)
            return DayMealSection(dishes: items,
                                  sectionIndex: isFutureDay ? mealPair.offset : mealPair.offset + 1,
                                  header: mealItem,
                                  titleHeader: mealPair.offset == 0 ? PlainHeaderItem(text: "MEALS".localized()) : nil)
        }
        
    }
    
    private func makePastDataSectionForCurrentDay() -> LittTableViewSectionProtocol {
        generateMeasurementSection(weightString: weightProgressManager.getCurrentDayStringValue(),
                                   waterString: waterProgressManager.getCurrentDayStringValue(),
                                   hipsString: hipsProgressManager.getCurrentDayStringValue(),
                                   chestString: chestProgressManager.getCurrentDayStringValue(),
                                   waistString: waistProgressManager.getCurrentDayStringValue())
    }
    
    private func makeMeasureSection(forDay index: Int) -> LittTableViewSectionProtocol {
        generateMeasurementSection(weightString: weightProgressManager.getDayStringValue(forDayNumber: index),
                                   waterString: waterProgressManager.getDayStringValue(forDayNumber: index),
                                   hipsString: hipsProgressManager.getDayStringValue(forDayNumber: index),
                                   chestString: chestProgressManager.getDayStringValue(forDayNumber: index),
                                   waistString: waistProgressManager.getDayStringValue(forDayNumber: index))
    }
    
    private func generateMeasurementSection(weightString: String,
                                            waterString: String,
                                            hipsString: String,
                                            chestString: String,
                                            waistString: String) -> LittTableViewSectionProtocol {
        LittTableViewSection(
            header: PlainHeaderItem(text: "YOUR_DATA".localized()),
            items: [
                makeFastInputItem(type: .weight, valueString: weightString),
                makeFastInputItem(type: .water, valueString: waterString),
                makeFastInputItem(type: .waist, valueString: waistString),
                makeFastInputItem(type: .hips, valueString: hipsString),
                makeFastInputItem(type: .chest, valueString: chestString)
            ])
    }
    
    private func makeFastInputItem(type: ProgressValueType,
                                   valueString: String) -> LittTableViewCellItemProtocol {
        ValueDisplayingTableViewCellItem(title: type.titleString,
                                         value: valueString,
                                         style: .inputValue,
                                         imageSystemString: type.imageString,
                                         imageColor: type.imageColor,
                                         cellColor: .littSecondaryBackgroundColor,
                                         selectionHandler: { [weak self] in
            guard let dayNumber = self?.currentDayModel?.id else { return }
            let viewModel = SetNewMeasureViewModel(type: type,
                                                   dayNumber: dayNumber,
                                                   completion: { _,_ in
               self?.reloadDataSection()
           })
            let vc = FastMeasureInputViewController(viewModel: viewModel)
            viewModel.view = vc
            vc.setupStandardSheetPresentation()
            
            self?.present(vc, animated: true)
        })
    }
    
    private func day(from date: Date) -> DietDayModel? {
        let components = Calendar.current.dateComponents(Constants.interestedDateComponents, from: date)
        return days?.first { $0.dateComponnets == components }
    }
    
    private func displayEmptyState() {
        currentDayModel = nil
        mealsView.sections = [LittTableViewSection(items: [EmptyMealsTableViewItem(emoji: "🥺",
                                                                                   title: "NO_PLAN_FOR_THIS_DAY".localized())])]
    }
    
    private func reloadDataSection() {
        if mealsView.sections.count > 1, let currentModel = currentDayModel {
            var sections = mealsView.sections
            sections[0] = makeMeasureSection(forDay: currentModel.id)
            mealsView.sections = sections
        }
    }
}

// MARK: - DayDishTableViewItemDelegate
extension DayMenuDisplayingViewController: DayDishTableViewItemDelegate {
    func didSelectReplaceDish(dish: DietDishProtocol, mealIndex: Int, dayIndex: Int) {
        let idGenerator = DishIdGenerator()
        
        let vc = ChangeDishViewController(dietService: DietService.shared,
                                          dishProvider: idGenerator.dishProvider(for: dish.dishId),
                                          currentDish: dish,
                                          dayIndex: dayIndex,
                                          mealIndex: mealIndex)
        vc.delegate = self
        
        present(vc, animated: true)
        
    }
    
    func didTapOnCell(dish: DietDishProtocol) {
        let vc = DishDetailsViewController(measurementService: MeasurementService(type: .smallWeight,
                                                                                  unitStyle: .short,
                                                                                  unitOptions: .providedUnit),
                                           energyMeasurement: MeasurementService(type: .smallWeight,
                                                                                 unitStyle: .short,
                                                                                 unitOptions: .providedUnit)
                                            .measurement(),
                                           dish: dish)
        present(vc, animated: true)
    }
}

// MARK: - FSCalendarDelegateAppearance
extension DayMenuDisplayingViewController: FSCalendarDelegateAppearance, FSCalendarDelegate {

	func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
		calendarViewHeightConstraint.constant = bounds.height
		view.layoutIfNeeded()
	}

	func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        guard currentDayModel?.id != day(from: date)?.id else { return }
        
        let isDayAfter = currentDate ?? Date() < date
        
		if let day = day(from: date) {
            currentDayModel = day
            var arrayOfSections: [LittTableViewSectionProtocol] = []
            if day.state == .completed || day.state == .incompleted {
                
                // прошедший или сегодняшний день (показываем данные)
                arrayOfSections = [makeMeasureSection(forDay: day.id)]
                arrayOfSections.append(contentsOf: mealsSections(from: day, isFutureDay: false))
                
            } else {
                
                // будущий день (не показываем данные)
                arrayOfSections.append(contentsOf: mealsSections(from: day, isFutureDay: true))
            }
            
            makeTableTransition(isNeedRightTransition: isDayAfter, newSections: arrayOfSections)
		} else {
            currentDayModel = nil
            let array = [LittTableViewSection(items: [EmptyMealsTableViewItem(emoji: "🥺",
                                                                              title: "NO_PLAN_FOR_THIS_DAY".localized())])]
            makeTableTransition(isNeedRightTransition: isDayAfter, newSections: array)
		}
        currentDate = date
	}
    
    private func makeTableTransition(isNeedRightTransition: Bool, newSections: [LittTableViewSectionProtocol]) {
        let coef: CGFloat = isNeedRightTransition ? 1 : -1
        
        UIView.animate(withDuration: 0.25, animations: {
            let mealsView = self.mealsView
            mealsView.frame = CGRect(x: mealsView.frame.minX - coef * mealsView.frame.width, y: mealsView.frame.minY, width: mealsView.frame.width, height: mealsView.frame.height)
        }, completion: { _ in
            let mealsView = self.mealsView
            mealsView.sections = newSections
            mealsView.frame = CGRect(x: coef * mealsView.frame.width, y: mealsView.frame.minY, width: mealsView.frame.width, height: mealsView.frame.height)
            UIView.animate(withDuration: 0.25, animations: {
                let mealsView = self.mealsView
                mealsView.frame = CGRect(x: 0, y: mealsView.frame.minY, width: mealsView.frame.width, height: mealsView.frame.height)
            })
        })
    }
	
	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
		guard day(from: date) != nil else { return nil }
		return appearance.titleSelectionColor
	}
	
	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
		guard let day = day(from: date) else { return nil }
        if Calendar.current.isDateInToday(date) {
            return .selectedDayColor
        }
		switch day.state {
		case .notStarted:
			return .notStartedDayColor
		case .incompleted:
			return .incompletedDayColor
		case .completed:
			return .completedDayColor
		}
	}
}

// MARK: - ChangeDishViewControllerDelegate
extension DayMenuDisplayingViewController: ChangeDishViewControllerDelegate {
    func changeDishDidComplete() {
        reloadDataSection()
    }
}

extension DayMenuDisplayingViewController: UIGestureRecognizerDelegate {
	func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
		let shouldBegin = mealsView.contentOffset.y <= -mealsView.contentInset.top
        if shouldBegin {
            let velocity = scopeGestureRecognizer.velocity(in: self.view)
            switch calendarView.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
			@unknown default:
				break
			}
        }
        return shouldBegin
	}
}
