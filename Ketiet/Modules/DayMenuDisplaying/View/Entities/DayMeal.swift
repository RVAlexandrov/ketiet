//
//  DayMeal.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 12.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class DayMeal {

	let meal: DietMealProtocol
	let fullDateComponents: DateComponents
	
	init(
        meal: DietMealProtocol,
        fullDateComponents: DateComponents
    ) {
		self.meal = meal
		self.fullDateComponents = fullDateComponents
	}
}
