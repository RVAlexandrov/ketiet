//
//  DietComponentState.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

enum DietComponentState {
	case notStarted
	case incompleted
	case completed
}
