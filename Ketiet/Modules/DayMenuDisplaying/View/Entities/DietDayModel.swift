//
//  DietDayModel.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 12.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class DietDayModel {
	
	let dateComponnets: DateComponents
	let id: Int
	let meals: [DayMeal]
	var state: DietComponentState {
		if let date = Calendar.current.date(from: dateComponnets), date > Date() {
			return .notStarted
		}
		let dishes = meals.flatMap { $0.meal.dishes }
		if dishes.contains(where: { !$0.eaten } ) {
			return .incompleted
		}
		return .completed
	}
	
	init(id: Int,
		 dateComponents: DateComponents,
		 meals: [DayMeal]) {
		self.dateComponnets = dateComponents
		self.id = id
		self.meals = meals
	}
}
