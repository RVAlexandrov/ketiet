//
//  DayDishTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DayDishTableViewCell: UITableViewCell {
	
    struct Constants {
		static let contentViewInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
		static let circleSize = CGSize(width: 7, height: 7)
		static let smallCirceSize = CGSize(width: 10, height: 10)
		static let nutrientsSpacing: CGFloat = 4
		static let titleAndNutriensSpacing: CGFloat = 12
		static let horizontalSpacing: CGFloat = 12
		static let dataStackSpacing: CGFloat = 4
        static let imageViewSide: CGFloat = 69
	}
	
    weak var item: DayDishTableViewItem?
	
	let titleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .subTitle
		label.textAlignment = .left
		label.numberOfLines = 0
		label.lineBreakMode = .byWordWrapping
		return label
	}()
    
    let dishImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .systemGray6
        imageView.layer.cornerRadius = .littBasicCornerRadius
        
        return imageView
    }()
	
    let proteinsLabel = UILabel.nutrientLabel(horizontalHuggingPriority: .defaultLow, color: .accentColor)
    let carbohydratesLabel = UILabel.nutrientLabel(horizontalHuggingPriority: .defaultLow, color: .accentColor)
    let fatLabel = UILabel.nutrientLabel(horizontalHuggingPriority: .defaultLow, color: .accentColor)
    
    let weightLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        
        return label
    }()
    
    private(set) lazy var checkMarkView: CheckMarkView = {
        let view = CheckMarkView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        view.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        view.addAction(
            UIAction { [weak self] _ in
                guard let self = self else { return }
                self.item?.setCompletness(isComplete: !self.checkMarkView.isSelected)
            },
            for: .touchUpInside
        )
        return view
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        item?.cell = nil
        item = nil
        dishImageView.image = nil
    }
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews() {
		selectionStyle = .default
		let proteinsTitleLabel = UILabel.nutrientLabel(text: "PROTEIN".localized() + ":",
													   horizontalHuggingPriority: .defaultHigh)
		let carbohydratesTitleLabel = UILabel.nutrientLabel(text: "CARBOHYDRATE".localized() + ":",
															horizontalHuggingPriority: .defaultHigh)
		let fatTitleLabel = UILabel.nutrientLabel(text: "FAT".localized() + ":",
												  horizontalHuggingPriority: .defaultHigh)
        
		let titlesStack = UIStackView(arrangedSubviews: [proteinsTitleLabel,
                                                         carbohydratesTitleLabel,
                                                         fatTitleLabel])
		titlesStack.spacing = Constants.nutrientsSpacing
		titlesStack.axis = .vertical
		
		let valuesStack = UIStackView(arrangedSubviews: [proteinsLabel,
														 carbohydratesLabel,
														 fatLabel])
		valuesStack.spacing = Constants.nutrientsSpacing
		valuesStack.axis = .vertical
		
		let nutriensStack = UIStackView(arrangedSubviews: [titlesStack, valuesStack])
		nutriensStack.spacing = 3 * Constants.nutrientsSpacing
		nutriensStack.setContentHuggingPriority(.defaultLow, for: .horizontal)
        nutriensStack.translatesAutoresizingMaskIntoConstraints = false
        		
        contentView.addSubviews([checkMarkView, weightLabel, dishImageView, titleLabel, nutriensStack])
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: contentView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            checkMarkView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            checkMarkView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .littScreenEdgeMargin),
            checkMarkView.heightAnchor.constraint(equalToConstant: CheckMarkView.Constants.size.height),
            checkMarkView.widthAnchor.constraint(equalToConstant: CheckMarkView.Constants.size.width),
            
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.contentViewInsets.top),
            titleLabel.leadingAnchor.constraint(equalTo: checkMarkView.trailingAnchor, constant: .littScreenEdgeMargin),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.contentViewInsets.right),
            
            weightLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .smallLittMargin),
            weightLabel.leadingAnchor.constraint(equalTo: checkMarkView.trailingAnchor, constant: .littScreenEdgeMargin),
            weightLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.contentViewInsets.right),
            
            dishImageView.topAnchor.constraint(equalTo: weightLabel.bottomAnchor, constant: .mediumLittMargin),
            dishImageView.leadingAnchor.constraint(equalTo: checkMarkView.trailingAnchor, constant: .littScreenEdgeMargin),
            dishImageView.heightAnchor.constraint(equalToConstant: Constants.imageViewSide),
            dishImageView.widthAnchor.constraint(equalToConstant: Constants.imageViewSide),
            
            nutriensStack.topAnchor.constraint(equalTo: weightLabel.bottomAnchor, constant: .mediumLittMargin),
            nutriensStack.leadingAnchor.constraint(equalTo: dishImageView.trailingAnchor, constant: .littMediumWellMargin),
            nutriensStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.contentViewInsets.bottom),
            nutriensStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.contentViewInsets.right),
        ])
	}
}

extension UILabel {

    static func nutrientLabel(text: String? = nil,
                              horizontalHuggingPriority: UILayoutPriority,
                              color: UIColor = .systemGray) -> UILabel {
		let label = UILabel()
		label.font = .body
		label.text = text
        label.textColor = color
		label.translatesAutoresizingMaskIntoConstraints = false
		label.textAlignment = .left
		label.setContentHuggingPriority(horizontalHuggingPriority, for: .horizontal)
		label.setContentCompressionResistancePriority(.required, for: .horizontal)
		return label
	}
}
