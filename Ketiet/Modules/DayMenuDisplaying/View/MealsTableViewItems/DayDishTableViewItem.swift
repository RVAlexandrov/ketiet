//
//  DayDishTableViewItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit
import Combine
import AlamofireImage

protocol DayDishTableViewItemDelegate: AnyObject {
    func didTapOnCell(dish: DietDishProtocol)
    func didSelectReplaceDish(dish: DietDishProtocol, mealIndex: Int, dayIndex: Int)
}

final class DayDishTableViewItem: NSObject {
	
    weak var cell: UITableViewCell?
    private weak var delegate: DayDishTableViewItemDelegate?
    private let mealIndex: Int
    private let dayIndex: Int
    private let dietService: DietServiceProtocol
    private var dish: DietDishProtocol
    private var subscriber: AnyCancellable?
    
	init(dish: DietDishProtocol,
         mealIndex: Int,
         dayIndex: Int,
         dietService: DietServiceProtocol,
         delegate: DayDishTableViewItemDelegate?) {
		self.dish = dish
        self.mealIndex = mealIndex
        self.dayIndex = dayIndex
        self.dietService = dietService
        self.delegate = delegate
        super.init()
        subscriber = dietService.publisher
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] _ in
                self?.updateCell()
            })
	}
	
	func updateCell() {
		guard let cell = cell else { return }
		configure(view: cell)
	}
    
    func setCompletness(isComplete: Bool) {
        dietService.setCurrentDietDishStatus(isComplete,
                                             for: dish,
                                             inMeal: mealIndex,
                                             inDay: dayIndex,
                                             completion: { _ in })
    }
}

extension DayDishTableViewItem: LittTableViewCellItemProtocol {
    
    func viewClass() -> AnyClass {
        return DayDishTableViewCell.self
    }
    
    func configure(view: UITableViewCell) {
        guard let cell = view as? DayDishTableViewCell else { return }
        let newDish = dietService.getCurrentDiet()?.days[dayIndex].meals[mealIndex].dishes[dish.id]
        guard let dish = newDish else { return }
        self.dish = dish
        self.cell = cell
        cell.item = self
        cell.titleLabel.text = dish.name
        cell.proteinsLabel.text = dish.weightedNutrients.nutrient(for: .protein).stringValue
        cell.carbohydratesLabel.text = dish.weightedNutrients.nutrient(for: .carbohydrate).stringValue
        cell.fatLabel.text = dish.weightedNutrients.nutrient(for: .fat).stringValue
        
        let measurementService = MeasurementService(type: .smallWeight,
                                                    unitStyle: .short,
                                                    unitOptions: .providedUnit)
        let weightValueString = measurementService.convertToLocale(value: dish.weightInGrams).cleanValue
        cell.weightLabel.text = "WEIGHT_OF_PORTION".localized() + ":" + " " + weightValueString + " " + measurementService.measurement()
        
        cell.checkMarkView.isSelected = dish.eaten
        if let imageURL = dish.imageURL {
            let imageSize = CGSize(width: DayDishTableViewCell.Constants.imageViewSide,
                                   height: DayDishTableViewCell.Constants.imageViewSide)
            cell.dishImageView.showShimmer()
            cell.dishImageView.af.setImage(withURL: imageURL,
                                           filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: imageSize,
                                                                                                  radius: .littBasicCornerRadius),
                                           imageTransition: .crossDissolve(0.3),
                                           runImageTransitionIfCached: false) { _ in
                cell.dishImageView.hideShimmer()
            }
        }
        
        let interaction = UIContextMenuInteraction(delegate: self)
        cell.contentView.addInteraction(interaction)
    }
    
    func didSelect() {
        delegate?.didTapOnCell(dish: dish)
    }
}

// MARK: - UIContextMenuInteractionDelegate
extension DayDishTableViewItem: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction,
                                configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        let mealIndex = mealIndex
        let dish = dish
        let dayIndex = dayIndex
        let configuration = UIContextMenuConfiguration(
            identifier: nil,
            previewProvider: nil,
            actionProvider: { [weak self] _ in
                let save = UIAction(
                    title: "CHANGE_DISH_TITLE".localized(),
                    image: UIImage(systemName: "arrow.2.squarepath"),
                    identifier: UIAction.Identifier(rawValue: dish.dishId)
                ) { action in
                    guard action.identifier.rawValue == dish.dishId else { return }
                    self?.delegate?.didSelectReplaceDish(dish: dish,
                                                         mealIndex: mealIndex,
                                                         dayIndex: dayIndex)
                }
                
                let selectionState = UIAction(
                    title: dish.eaten ? "CANCEL_CHECKMARK".localized() : "MARK_EATEN".localized(),
                    image: UIImage(systemName: dish.eaten ? "xmark" : "checkmark")
                ) { action in
                    self?.setCompletness(isComplete: !dish.eaten)
                }
                
                return UIMenu(title: dish.name,
                              options: .displayInline,
                              children: [save, selectionState])
            }
        )
        return configuration
    }
}
