//
//  DayMealCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DayMealHeaderViewCell: UITableViewCell {
	
	private struct Constants {
		static let cornerRadius:  CGFloat = 15
		static let verticalLabelsSpacing: CGFloat = 10
		static let horizontalLabelsSpacing: CGFloat = 10
		static let roundedViewInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
		static let stackViewInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
	}
	
	weak var item: DayMealItem?
	
	var chevronExpanded = false {
		didSet {
			UIView.animate(withDuration: CATransaction.animationDuration(),
						   delay: 0,
						   options: [.curveEaseOut],
						   animations: {
				self.chevronImageView.transform = self.chevronExpanded ? CGAffineTransform(rotationAngle: CGFloat.pi/2) : .identity
			})
		}
	}
	
	let mealTitleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .subTitle
		label.setContentCompressionResistancePriority(.required, for: .vertical)
		return label
	}()
	
	let caloriesLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .smallBody
		label.setContentCompressionResistancePriority(.required, for: .vertical)
		return label
	}()
	
	private let chevronImageView: UIImageView = {
		let chevron = UIImageView(image: UIImage(systemName: "chevron.right")?.withRenderingMode(.alwaysTemplate))
		chevron.tintColor = .systemGray
		chevron.translatesAutoresizingMaskIntoConstraints = false
		chevron.setContentHuggingPriority(.defaultHigh, for: .horizontal)
		return chevron
	}()
	
	override init(reuseIdentifier: String?) {
		super.init(reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
		backgroundColor = .clear
		contentView.backgroundColor = .clear
		let titlesStackView = UIStackView(arrangedSubviews: [mealTitleLabel, caloriesLabel])
		titlesStackView.axis = .vertical
		titlesStackView.spacing = Constants.verticalLabelsSpacing
		titlesStackView.translatesAutoresizingMaskIntoConstraints = false
		
		let allLabelsStackView = UIStackView(arrangedSubviews: [titlesStackView, chevronImageView])
		allLabelsStackView.spacing = Constants.horizontalLabelsSpacing
		allLabelsStackView.axis = .horizontal
		allLabelsStackView.alignment = .center
		allLabelsStackView.translatesAutoresizingMaskIntoConstraints = false

		let roundedView = UIView()
		roundedView.layer.cornerRadius = Constants.cornerRadius
		roundedView.backgroundColor = .tertiarySystemBackground
		roundedView.translatesAutoresizingMaskIntoConstraints = false
		roundedView.clipsToBounds = true
		
		contentView.addSubview(roundedView)
		roundedView.addSubview(allLabelsStackView)
		
		allLabelsStackView.pinToSuperview(insets: Constants.stackViewInsets)
		roundedView.pinToSuperview(insets: Constants.roundedViewInsets)
		contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap)))
	}
	
	@objc private func didTap() {
		item?.didSelectView()
	}
}
