//
//  DayMealSection.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 22.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DayMealSection {
	
	private var dishes: [DayDishTableViewItem]
	private var expanded = false
	private let sectionIndex: Int
	private weak var tableView: LittTableView?
	private let pseudoHader: DayMealItem
    private var titleHeader: PlainHeaderItem?
	
	init(dishes: [DayDishTableViewItem],
		 sectionIndex: Int,
		 header: DayMealItem,
         titleHeader: PlainHeaderItem? = nil) {
		self.dishes = dishes
		self.sectionIndex = sectionIndex
		self.pseudoHader = header
        self.titleHeader = titleHeader
		header.didSelectClosure = { [weak self] in
			self?.toggleExpand()
		}
	}
	
	private func toggleExpand() {
		expanded.toggle()
		let indexPaths = dishes.enumerated().map { IndexPath(row: $0.offset + 1, section: sectionIndex) }
		if expanded {
            tableView?.performBatchUpdates({ tableView?.insertRows(at: indexPaths, with: .fade) })
		} else {
			tableView?.performBatchUpdates({ tableView?.deleteRows(at: indexPaths, with: .fade) })
		}
	}
	
	private func cellsUnderHeader() -> [UIView] {
		let indexPaths = tableView?.indexPathsForVisibleRows?.filter { $0.section == self.sectionIndex }
		return indexPaths?.dropFirst().compactMap { self.tableView?.cellForRow(at: $0) } ?? []
	}
}

extension DayMealSection: LittTableViewSectionProtocol {
	 
	var items: [LittTableViewCellItemProtocol] {
		set {
			if let dishes = newValue as? [DayDishTableViewItem] {
				self.dishes = dishes
			}
		}
		get {
			return [pseudoHader] + (expanded ? dishes : [])
		}
	}
    
    var header: LittTableViewHeaderFooterProtocol? {
        set {
            if let titleHeader = newValue as? PlainHeaderItem {
                self.titleHeader = titleHeader
            }
        }
        get {
            titleHeader
        }
    }
	
	func registerViews(to tableView: LittTableView) {
		self.tableView = tableView
		tableView.register(pseudoHader.viewClass(), forCellReuseIdentifier: pseudoHader.viewIdentifier())
		dishes.forEach { tableView.register($0.viewClass(), forCellReuseIdentifier: $0.viewIdentifier()) }
        
        if let titleHeader = titleHeader {
            tableView.register(titleHeader.viewClass(), forHeaderFooterViewReuseIdentifier: titleHeader.viewIdentifier())
        }
	}
}
