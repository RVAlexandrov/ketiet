//
//  DayMealItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import Combine

final class DayMealItem {
	
	var didSelectClosure: (() -> Void)?
    
    var subscriber = AnyCancellable({})
    
    var viewModel: DayMealTableViewCell.ViewModel?
    
    private let dietService: DietServiceProtocol
    private weak var view: DayMealTableViewCell?
    private let cellColor: UIColor
    private let meal: DayMeal
    private let dayIndex: Int
    
	private lazy var dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .none
		formatter.timeStyle = .short
		return formatter
	}()
	
    init(dayIndex: Int,
         meal: DayMeal,
         dietService: DietServiceProtocol,
         cellColor: UIColor = .littSecondaryBackgroundColor) {
        self.dayIndex = dayIndex
        self.meal = meal
        self.dietService = dietService
        self.cellColor = cellColor
        subscriber = dietService.publisher.sink(receiveValue: { [weak self] _ in
            DispatchQueue.main.async {
                if let cell = self?.view {
                    self?.configure(view: cell)
                }
            }
        })
	}
	
	func updateCell() {
		guard let cell = view else { return }
		configure(view: cell)
	}
}

extension DayMealItem: LittTableViewCellItemProtocol {

	func viewClass() -> AnyClass {
		return DayMealTableViewCell.self
	}
	
	func configure(view: UITableViewCell) {
		guard let cell = view as? DayMealTableViewCell else { return }
        self.view = cell
        
		let calories = Nutrient(type: . calories, value: meal.meal.calories)

        var timeString: String?
		if let date = Calendar.current.date(from: meal.fullDateComponents) {
			timeString = dateFormatter.string(from: date)
		} else {
            timeString = nil
		}
        
        let newViewModel = DayMealTableViewCell.ViewModel(
            caloriesText: calories.stringValue,
            caloriesFont: .body,
            mealTitleText: meal.meal.name,
            mealTitleFont: .subTitle,
            mealTimeString: timeString ?? "",
            mealTimeFont: .description,
            isSelected: meal.meal.isEaten,
            isExpanded: viewModel?.isExpanded ?? false
        )
        cell.viewModel = newViewModel
        cell.delegate = self
        self.viewModel = newViewModel
	}
	
	func didSelect() {
        let model = view?.viewModel
        let updatedModel = DayMealTableViewCell.ViewModel(
            caloriesText: model?.caloriesText ?? "",
            caloriesFont: model?.caloriesFont ?? .body,
            mealTitleText: model?.mealTitleText ?? "",
            mealTitleFont: model?.mealTitleFont ?? .subTitle,
            mealTimeString: model?.mealTimeString ?? "",
            mealTimeFont: model?.mealTimeFont ?? .description,
            isSelected: model?.isSelected ?? false,
            isExpanded: !(model?.isExpanded ?? true)
        )
        self.viewModel = updatedModel
        view?.isNeedPerformNextUpdateWithAnimation = true
        view?.viewModel = updatedModel
		didSelectClosure?()
	}
    
    func height(for boundingRect: CGRect) -> CGFloat {
        guard let viewModel = viewModel else { return 0 }
        let layout = DayMealTableViewCell.Layout(viewModel: viewModel,
                                                 width: boundingRect.width)
        return layout.height(itemRects: layout.itemRects)
    }
}

// MARK: - DayMealTableViewCellDelegate
extension DayMealItem: DayMealTableViewCellDelegate {
    func didTapCheckMark() {
        meal.meal.dishes.forEach {
            dietService.setCurrentDietDishStatus(!(viewModel?.isSelected ?? true),
                                                 for: $0,
                                                 inMeal: meal.meal.id,
                                                 inDay: dayIndex,
                                                 completion: { _ in })
        }
    }
    
    func didTapArrow() {
        didSelect()
    }
}
