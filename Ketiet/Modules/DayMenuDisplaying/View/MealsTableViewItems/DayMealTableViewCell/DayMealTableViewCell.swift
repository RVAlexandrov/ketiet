//
//  DayMealCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol DayMealTableViewCellDelegate: AnyObject {
    func didTapCheckMark()
    func didTapArrow()
}

final class DayMealTableViewCell: UITableViewCell {
    weak var delegate: DayMealTableViewCellDelegate?
    var isNeedPerformNextUpdateWithAnimation: Bool = false
    
    var viewModel: ViewModel? {
        didSet {
            guard let viewModel = viewModel,
                  viewModel != oldValue else { return }
            if isNeedPerformNextUpdateWithAnimation {
                UIView.animate(withDuration: CATransaction.animationDuration(),
                               delay: 0,
                               options: [.curveEaseOut],
                               animations: {
                    self.chevronButton.transform = viewModel.isExpanded ? CGAffineTransform(rotationAngle: CGFloat.pi/2) : .identity
                })
            } else {
                self.chevronButton.transform = viewModel.isExpanded ? CGAffineTransform(rotationAngle: CGFloat.pi/2) : .identity
            }
            checkMarkView.isSelected = viewModel.isSelected

            caloriesLabel.text = viewModel.caloriesText
            caloriesLabel.font = viewModel.caloriesFont
                        
            mealTitleLabel.text = viewModel.mealTitleText
            mealTitleLabel.font = viewModel.mealTitleFont
            
            mealtimeLabel.text = viewModel.mealTimeString
            mealtimeLabel.font = viewModel.mealTimeFont
            
            isNeedPerformNextUpdateWithAnimation = false
            
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let viewModel = viewModel else { return }

        let layout = Layout(viewModel: viewModel,
                            width: bounds.width)
        
        zip([mealtimeLabel, mealTitleLabel, caloriesLabel, dateContainerView, chevronButton, checkMarkView], layout.itemRects).forEach { (view, frame) in
            view.frame = frame
        }
    }
        
    private let dateContainerView: UIView = {
        let containerView = UIView()
        containerView.layer.cornerRadius = 10
        containerView.layer.borderWidth = 1.5
        containerView.layer.borderColor = UIColor.label.cgColor
        return containerView
    }()
	
    let mealtimeLabel: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		return label
	}()
	
    let mealTitleLabel = UILabel()
	
    let caloriesLabel: UILabel = {
		let label = UILabel()
        label.textColor = .accentColor
		return label
	}()
	
	private let chevronButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "chevron.right")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .systemGray2
        button.imageView?.contentMode = .center
		return button
	}()
    
    private lazy var checkMarkView: CheckMarkView = {
        let view = CheckMarkView()
        view.addAction(
            UIAction { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.didTapCheckMark()
            },
            for: .touchUpInside
        )
        return view
    }()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
		setupSubviews()
	}
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        if traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection) {
            dateContainerView.layer.borderColor = UIColor.label.resolvedColor(with: traitCollection).cgColor
        }
    }
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

// MARK: - Private methods
extension DayMealTableViewCell {
    private func setupSubviews() {
        chevronButton.addTarget(self,
                                action: #selector(didTapChevronButton),
                                for: .touchUpInside)
        
        dateContainerView.addSubview(mealtimeLabel)
        contentView.addSubviews([mealTitleLabel,
                                 checkMarkView,
                                 caloriesLabel,
                                 dateContainerView,
                                 chevronButton])
    }
    
    @objc
    private func didTapChevronButton() {
        delegate?.didTapArrow()
    }
}

// MARK: - ViewModel
extension DayMealTableViewCell {
    struct ViewModel: Equatable {
        let caloriesText: String
        let caloriesFont: UIFont
        let mealTitleText: String
        let mealTitleFont: UIFont
        let mealTimeString: String
        let mealTimeFont: UIFont
        let isSelected: Bool
        let isExpanded: Bool
    }
}

// MARK: - Layout
extension DayMealTableViewCell {
    struct Layout {
        let viewModel: ViewModel
        let width: CGFloat
                
        var itemRects: [CGRect] {
            var rects: [CGRect] = []
            
            let checkMarkMaxX = CGFloat.littMediumWellMargin + CheckMarkView.Constants.size.width
            
            let mealTimeContainerTitleInset: CGFloat = 5
            let arrowWidth: CGFloat = 24
            let arrowViewMinX: CGFloat = width - arrowWidth - CGFloat.littMediumWellMargin
            
            let mealTimeHeight = viewModel.mealTimeFont.lineHeight
            let mealTimeTitleRect = CGRect(
                x: mealTimeContainerTitleInset,
                y: mealTimeContainerTitleInset,
                width: viewModel
                    .mealTimeString
                    .width(withHeight: mealTimeHeight,
                           font: viewModel.mealTimeFont),
                height: mealTimeHeight
            )
            rects.append(mealTimeTitleRect)
            
            let mealTitleMinX = checkMarkMaxX + CGFloat.littMediumWellMargin
            let mealTitleWidth = arrowViewMinX - mealTitleMinX
            
            let mealTitleRect = CGRect(
                x: mealTitleMinX,
                y: CGFloat.largeLittMargin,
                width: mealTitleWidth,
                height: viewModel.mealTitleFont.lineHeight
            )
            rects.append(mealTitleRect)
            
            let caloriesLabelHeight: CGFloat = viewModel.caloriesFont.lineHeight
            let caloriesLabelRect = CGRect(
                x: mealTitleRect.minX,
                y: mealTitleRect.maxY + CGFloat.mediumLittMargin,
                width: viewModel
                    .caloriesText
                    .width(
                        withHeight: caloriesLabelHeight,
                        font: viewModel.caloriesFont
                    ),
                height: caloriesLabelHeight
            )
            rects.append(caloriesLabelRect)
            
            let mealTimeContainerHeight: CGFloat = mealTimeTitleRect.height + 2 * mealTimeContainerTitleInset
            let mealTimeContainerRect = CGRect(
                x: caloriesLabelRect.maxX + CGFloat.mediumLittMargin,
                y: caloriesLabelRect.minY + (caloriesLabelRect.height - mealTimeContainerHeight) / 2,
                width: mealTimeTitleRect.width + 2 * mealTimeContainerTitleInset,
                height: mealTimeContainerHeight
            )
            rects.append(mealTimeContainerRect)
                                    
            let maxY = max(caloriesLabelRect.maxY, mealTimeContainerRect.maxY)
            let arrowRect = CGRect(
                x: arrowViewMinX,
                y: (maxY + CGFloat.littScreenEdgeMargin) / 2 - arrowWidth / 2,
                width: arrowWidth,
                height: arrowWidth
            )
            rects.append(arrowRect)
            
            let checkMarkViewRect = CGRect(
                x: CGFloat.littMediumWellMargin,
                y: (maxY + CGFloat.littScreenEdgeMargin) / 2 - CheckMarkView.Constants.size.width / 2,
                width: CheckMarkView.Constants.size.width,
                height: CheckMarkView.Constants.size.height
            )
            rects.append(checkMarkViewRect)
            
            return rects
        }
        
        func height(itemRects: [CGRect]) -> CGFloat {
            guard itemRects.count > 5 else { return 0 }
            return max(itemRects[2].maxY, itemRects[3].maxY) + CGFloat.littScreenEdgeMargin
        }
    }
}
