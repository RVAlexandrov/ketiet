//
//  EmptyMealsTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class EmptyMealsTableViewCell: UITableViewCell {
	
	private struct Constants {
		static let cornerRadius: CGFloat = 15
		static let roundedViewInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
		static let verticalPadding: CGFloat = 10
		static let gapBetweenLabels: CGFloat = 8
		static let horizontalPading: CGFloat = 15
	}
	
	let emojiLabel: UILabel = {
		let label = UILabel()
		label.font = .systemFont(ofSize: 30)
		label.translatesAutoresizingMaskIntoConstraints = false
		label.setContentCompressionResistancePriority(.required, for: .vertical)
		return label
	}()
	
	let titleLabel: UILabel = {
		let label = UILabel()
		label.font = .subTitle
		label.translatesAutoresizingMaskIntoConstraints = false
		label.numberOfLines = 0
		label.lineBreakMode = .byWordWrapping
		label.setContentCompressionResistancePriority(.required, for: .vertical)
		label.textAlignment = .center
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		addSubviwews()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func addSubviwews() {
		contentView.backgroundColor = .clear
		backgroundColor = .clear
		let roundedView = UIView()
		roundedView.translatesAutoresizingMaskIntoConstraints = false
		roundedView.backgroundColor = .littSecondaryBackgroundColor
		contentView.addSubview(roundedView)
		roundedView.addSubview(emojiLabel)
		roundedView.addSubview(titleLabel)
		roundedView.clipsToBounds = true
		roundedView.layer.cornerRadius = Constants.cornerRadius
		roundedView.pinToSuperview(insets: Constants.roundedViewInsets)
		NSLayoutConstraint.activate([
			emojiLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
			emojiLabel.topAnchor.constraint(equalTo: roundedView.topAnchor, constant: Constants.verticalPadding),
			emojiLabel.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -Constants.gapBetweenLabels),
			titleLabel.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: Constants.horizontalPading),
			titleLabel.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -Constants.horizontalPading),
			titleLabel.bottomAnchor.constraint(equalTo: roundedView.bottomAnchor, constant: -Constants.verticalPadding)
		])
	}
}
