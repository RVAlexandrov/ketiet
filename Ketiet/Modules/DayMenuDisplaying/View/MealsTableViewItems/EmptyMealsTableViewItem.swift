//
//  EmptyMealsTableViewItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class EmptyMealsTableViewItem {
	
	let emoji: String
	let title: String
	
	init(emoji: String, title: String) {
		self.emoji = emoji
		self.title = title
	}
}

extension EmptyMealsTableViewItem: LittTableViewCellItemProtocol {

	func viewClass() -> AnyClass {
		return EmptyMealsTableViewCell.self
	}
	
	func configure(view: UITableViewCell) {
		guard let cell = view as? EmptyMealsTableViewCell else { return }
		cell.emojiLabel.text = emoji
		cell.titleLabel.text = title
		cell.selectionStyle = .none
	}
}
