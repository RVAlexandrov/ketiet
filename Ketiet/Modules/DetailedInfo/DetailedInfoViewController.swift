//
//  DetailedInfoViewController.swift
//  Ketiet
//
//  Created by Александров Роман Витальевич on 18.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

class DetailedInfoViewController: UIViewController {
    
    struct ButtonModel {
        let title: String
        let url: URL?
    }
    
    private let titleText: String
    private let detailText: String
    private let buttonModel: ButtonModel?
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .title
        label.numberOfLines = 0
        return label
    }()
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .close)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000),
                                                       for: .horizontal)
        button.addTarget(self, action: #selector(didTapCloseButton), for: .touchUpInside)
        return button
    }()
    
    private lazy var button: LittBasicButton = {
        var buttonConfiguration = UIButton.Configuration.gray()
        buttonConfiguration.cornerStyle = .large
        buttonConfiguration.titleAlignment = .leading
        let button = LittBasicButton(title: "", config: .custom(buttonConfiguration))
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    init(detailText: String,
         titleText: String,
         buttonModel: ButtonModel?) {
        self.detailText = detailText
        self.titleText = titleText
        self.buttonModel = buttonModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
}

// MARK: - Private methods
extension DetailedInfoViewController {
    private func configureView() {
        textLabel.text = detailText
        titleLabel.text = titleText
        
        if let buttonModel = buttonModel {
            button.isHidden = false
            button.configuration?.title = buttonModel.title
            button.setNeedsUpdateConfiguration()
        }
        
        view.backgroundColor = .littBackgroundColor
        view.addSubviews([closeButton, titleLabel, textLabel, button])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .logicBlockLittMargin),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .logicBlockLittMargin),
            titleLabel.trailingAnchor.constraint(equalTo: closeButton.leadingAnchor, constant: -.logicBlockLittMargin),
            
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .logicBlockLittMargin),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.logicBlockLittMargin),
            closeButton.heightAnchor.constraint(equalToConstant: 30),
            closeButton.widthAnchor.constraint(equalToConstant: 30),
            
            textLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .largeLittMargin),
            textLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .logicBlockLittMargin),
            textLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.logicBlockLittMargin),
            
            button.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: .logicBlockLittMargin),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .logicBlockLittMargin),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.logicBlockLittMargin),
            button.heightAnchor.constraint(equalToConstant: 44)
        ])
    }
    
    @objc
    private func didTapCloseButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func didTapButton() {
        guard let model = buttonModel,
              let url = model.url else { return }
        UIApplication.shared.open(url)
    }
}
