//
//  DietFinishAssembly.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 03.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

final class DietFinishAssembly {
	
	func makeFinishViewController(goal: Goal) -> UIViewController {
		let floaingPointValuesFormatter = NumberFormatter.formatterForSignedFloatingPointValues()
		let integerValuesFormatter = NumberFormatter.formatterForSignedIntegerValues()
		let unsignedIntegerValueFormatter = NumberFormatter.formatterForIntegerValues()
		let weightMeasurement = MeasurementService(type: .weight).measurement()
		let lengthMeasurement = MeasurementService(type: .lenght).measurement()
		let volumeMeasurement = MeasurementService(type: .volume).measurement()
		let metricValuesShouldDecrease = goal.metricsChangeDirection == .decrease
		
		let weightDiff = EveryDayInputProgressService.weightProgressService.diffBetweenStartAndEnd()
		let weightDiffString = floaingPointValuesFormatter.string(from: NSNumber(value: weightDiff)) ?? ""
		let weightItem = InfoViewControllerItem(icon: UIImage(systemName: "star.circle") ?? UIImage(),
												title: "WEIGHT".localized(),
												description: "CHANGE_OVER_TIME".localized() + ": " + weightDiffString + " " + weightMeasurement,
												tintColor: metricValuesShouldDecrease && weightDiff < 0 ? .littSuccessColor : .littFailureColor)
		
		let chestDiff = EveryDayInputProgressService.chestProgressService.diffBetweenStartAndEnd()
		let chestDiffString = integerValuesFormatter.string(from: NSNumber(value: chestDiff)) ?? ""
		let chestItem = InfoViewControllerItem(icon: UIImage(systemName: "hand.point.right") ?? UIImage(),
											   title: "CHEST".localized(),
											   description: "CHANGE_OVER_TIME".localized() + ": " + chestDiffString + " " + lengthMeasurement,
											   tintColor: metricValuesShouldDecrease && chestDiff < 0 ? .littSuccessColor : .littFailureColor)
		
		let waistDiff = EveryDayInputProgressService.waistProgressService.diffBetweenStartAndEnd()
		let waistDiffString = integerValuesFormatter.string(from: NSNumber(value: waistDiff)) ?? ""
		let waistItem = InfoViewControllerItem(icon: UIImage(systemName: "hand.point.right") ?? UIImage(),
											   title: "WAIST".localized(),
											   description: "CHANGE_OVER_TIME".localized() + ": " + waistDiffString + " " + lengthMeasurement,
											   tintColor: metricValuesShouldDecrease && waistDiff < 0 ? .littSuccessColor : .littFailureColor)
		
		let hipsDiff = EveryDayInputProgressService.hipsProgressService.diffBetweenStartAndEnd()
		let hipsDiffString = integerValuesFormatter.string(from: NSNumber(value: hipsDiff)) ?? ""
		let hipsItem = InfoViewControllerItem(icon: UIImage(systemName: "hand.point.right") ?? UIImage(),
											   title: "HIPS".localized(),
											   description: "CHANGE_OVER_TIME".localized() + ": " + hipsDiffString + " " + lengthMeasurement,
											   tintColor: metricValuesShouldDecrease && hipsDiff < 0 ? .littSuccessColor : .littFailureColor)
		
		let waterSum = EveryDayInputProgressService.waterProgressService.getAllValues()?.sum() ?? 0
		let waterSumString = unsignedIntegerValueFormatter.string(from: NSNumber(value: waterSum)) ?? ""
		let waterItem = InfoViewControllerItem(icon: UIImage(systemName: "star.circle") ?? UIImage(),
											   title: "WATER".localized(),
											   description: "QUANTITY_OF_WATER_DRUNK".localized() + ": " + waterSumString + " " + volumeMeasurement)
		
		return InfoViewController(headerTitle: "🥳 🎉 🎉 🥳 \n" + "CONGRATULATIONS".localized() + "\n" + "YOUR_DIET_IS_FINISHED".localized(),
											infoItems: [weightItem, chestItem, waistItem, hipsItem, waterItem],
											buttonTitle: "CONTINUE".localized()) { controller in
			RateAppAssembly().showRateAppController(from: controller) {
				controller.dismiss(animated: true)
			}
		}
	}
}
