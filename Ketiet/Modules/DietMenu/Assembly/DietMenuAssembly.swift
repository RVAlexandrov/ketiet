//
//  DietMenuAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/11/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class DietMenuAssembly {

    func makeViewController(diet: DietProtocol) -> UIViewController {
        let model = DietMenuModel(diet: diet)
        let router = DietMenuRouter()
        let controller = DietMenuViewController(model: model, router: router)
        router.viewController = controller
        model.view = controller

        return controller
    }
}
