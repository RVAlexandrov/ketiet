//
//  DietMenuModel.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/11/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

final class DietMenuModel {

    weak var view: DietMenuViewProtocol?
    
    private let diet: DietProtocol
    
    init(diet: DietProtocol) {
        self.diet = diet
    }
}

// MARK: - DietMenuModelProtocol
extension DietMenuModel: DietMenuModelProtocol {
    func viewDidLoad() {
        view?.setupView()
        configureData()
    }
}

// MARK: - Private methods
extension DietMenuModel {
    private func configureData() {
        guard let tableView = view?.tableView else { return }
        let daysSections = diet.days.enumerated().map { pair -> LittTableViewSectionProtocol in
            let firstItem = DietDayTableViewCellItem(dayNumber: pair.offset + 1)
            let mealsItems = pair.element.meals.map { [weak self] meal in
                DietMealTableViewCellItem(meal: meal) { dietMeal in
                    self?.view?.showMealDetail(withMeal: dietMeal)
                }
            }
            return LittTableViewExpandableSection(firstSectionItem: firstItem,
                                                  sectionIndex: pair.offset,
                                                  items: mealsItems,
                                                  tableView: tableView)
        }
        
        view?.showData(daysSections)
        
    }
    
}
