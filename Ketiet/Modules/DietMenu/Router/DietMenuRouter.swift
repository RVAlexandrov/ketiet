//
//  DietMenuRouter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/11/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class DietMenuRouter {
    
    weak var viewController: UIViewController?
}

// MARK: - DietMenuRouterProtocol
extension DietMenuRouter: DietMenuRouterProtocol {
    func showMealDetail(withMeal meal: DietMealProtocol) {
        let assembly: MealDetailAssembly = AssemblyFactory()
            .generateAssembly(
                moduleConfigurator: MealDetailModuleConfigurator(
                    mealIndex: 0,
                    meals: [meal],
                    showCloseButton: false)
            )
        viewController?.navigationController?.pushViewController(assembly.initialViewController(),
                                                                 animated: true)
    }
}
