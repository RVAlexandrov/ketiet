//
//  DietMenuRouterProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/11/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

protocol DietMenuRouterProtocol: AnyObject {
    func showMealDetail(withMeal meal: DietMealProtocol)
}
