//
//  DietMenuViewController.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/11/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class DietMenuViewController: UIViewController {
    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    private let model: DietMenuModelProtocol
    private let router: DietMenuRouterProtocol
    
    lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds,
                                      style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.animations = []
        return tableView
    }()

    init(model: DietMenuModelProtocol, router: DietMenuRouterProtocol) {
        self.model = model
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        model.viewDidLoad()
    }
}

// MARK: - DietMenuViewProtocol
extension DietMenuViewController: DietMenuViewProtocol {
    func setupView() {
        tableView.contentInset.bottom += (UIApplication.shared.getKeyWindow()?.safeAreaInsets.bottom ?? 0) + 50
        view.backgroundColor = .littBackgroundColor
        title = "DIET_MENU".localized()
        view.addSubview(tableView)
        tableView.pinToSuperview()
    }
    
    func showData(_ sections: [LittTableViewSectionProtocol]) {
        tableView.sections = sections
    }
    
    func showMealDetail(withMeal meal: DietMealProtocol) {
        router.showMealDetail(withMeal: meal)
    }
}
