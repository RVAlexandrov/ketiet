//
//  DietMenuViewProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/11/2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

protocol DietMenuViewProtocol: AnyObject {
    func setupView()
    func showData(_ sections: [LittTableViewSectionProtocol])
    func showMealDetail(withMeal meal: DietMealProtocol)
    
    var tableView: LittTableView { get }
}
