//
//  DietStatisticsAssembly.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

struct DietStatisticsAssembly {
    
    func makeViewController(
        diet: DietProtocol,
        progressServiceFabric: ProgressServiceFabricProtocol
    ) -> UIViewController {
        let model = DietStatisticsModel(diet: diet,
                                        progressServiceFabric: progressServiceFabric)
        let router = DietStatisticsRouter()
        
        let controller = DietStatisticsViewController(model: model, router: router)
        router.viewController = controller
        model.view = controller
        
        return controller
    }
}
