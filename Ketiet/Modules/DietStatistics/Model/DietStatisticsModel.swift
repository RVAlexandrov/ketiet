//
//  DietStatisticsModel.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

final class DietStatisticsModel {
    
    weak var view: DietStatisticsViewProtocol?
        
    private let diet: DietProtocol
    private let progressServiceFabric: ProgressServiceFabricProtocol
    
    private var statisticsItemsOrderDict: [Int: ProgressValueType] = [:]
    
    init(diet: DietProtocol,
         progressServiceFabric: ProgressServiceFabricProtocol) {
        self.diet = diet
        self.progressServiceFabric = progressServiceFabric
    }
}

// MARK: - DietStatisticsModelInputProtocol
extension DietStatisticsModel: DietStatisticsModelProtocol {
    func viewDidLoad() {
        view?.configureNavigationTitle(diet.name)
        view?.configureView()
        prepareData()
    }
}

// MARK: - Private methods
extension DietStatisticsModel {
    private func prepareData() {
        
        let statisticsSection = prepareProgressStatisticSection()
        
        let dishesSection = prepareDishesPerDaySection()
        
        view?.showDataSections([statisticsSection, dishesSection])
    }
    
    private func makeProgressTypeItem(type: ProgressValueType,
                                      orderNumber: Int) -> LittTableViewCellItemProtocol {
        ArrowedCellItem(
            title: type.titleString,
            imageSystemString: type.imageString,
            imageColor: type.imageColor,
            cellColor: .littSecondaryBackgroundColor,
            selectionHandler: { [weak self] in
                guard let self = self,
                      let selectedType = self.statisticsItemsOrderDict[orderNumber] else { return }
                self.view?.showDetailProgress(forType: selectedType,
                                              diet: self.diet)
            })
    }
    
    private func prepareProgressStatisticSection() -> LittTableViewSectionProtocol {
        var statisticsMeasurementItems: [LittTableViewCellItemProtocol] = []

        ProgressValueType.allCases.enumerated().forEach {
            statisticsMeasurementItems.append(makeProgressTypeItem(type: $0.element,
                                                                orderNumber: $0.offset))
            statisticsItemsOrderDict[$0.offset] = $0.element
        }
        return LittTableViewSection(
            header: PlainHeaderItem(text: "STATISTICS".localized()),
            items: statisticsMeasurementItems
        )
    }
    
    private func prepareDishesPerDaySection() -> LittTableViewSectionProtocol {
        LittTableViewSection(
            items: [
                ArrowedCellItem(
                    title: "MEALS".localized(),
                    imageSystemString: nil,
                    imageColor: nil,
                    cellColor: nil,
                    selectionHandler: { [weak self] in
                        guard let self = self else { return }
                        self.view?.showDietMenu(withDiet: self.diet)
                    }
                )
            ],
            footer: BasicFooterItem(
                text: "WATCH_DISH_DESCRIPTION".localized()
            )
        )
    }
}

