//
//  DietStatisticsModelInputProtocol.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

protocol DietStatisticsModelProtocol: AnyObject {
    func viewDidLoad()
}
