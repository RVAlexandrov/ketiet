//
//  DietStatisticsRouter.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class DietStatisticsRouter {
    weak var viewController: UIViewController?
}

extension DietStatisticsRouter: DietStatisticsRouterProtocol {
    func showBodyMetricDetailScreen(_ type: BodyMetricType,
                                    withDiet diet: DietProtocol) {
        let assembly: BodyMetricDetailsAssembly = AssemblyFactory()
            .generateAssembly(
                moduleConfigurator: BodyMetricDetailsModuleConfigurator(bodyMetricType: type,
                                                                        showCloseButton: false,
                                                                        diet: diet))
        
        viewController?.navigationController?.pushViewController(assembly.initialViewController(),
                                                                 animated: true)
    }
    
    func showWeightDetailScreen(withDiet diet: DietProtocol) {
        let vc = WeightDetailsAssembly(showCloseButton: false,
                                       diet: diet)
            .initialViewController()
        viewController?.navigationController?.pushViewController(vc ,animated: true)
    }
    
    func showWaterDetailScreen(withDiet diet: DietProtocol) {
        let assembly: WaterDetailsAssembly = AssemblyFactory()
            .generateAssembly(moduleConfigurator: WaterDetailsModuleConfigurator(showCloseButton: false,
                                                                                 diet: diet))
        let vc = assembly.initialViewController()
        viewController?.navigationController?.pushViewController(vc,
                                                                 animated: true)
    }
    
    func showDietMenu(withDiet diet: DietProtocol) {
        let vc = DietMenuAssembly().makeViewController(diet: diet)
        viewController?.navigationController?.pushViewController(vc,
                                                                 animated: true)
    }
}
