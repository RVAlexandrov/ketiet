//
//  DietStatisticsRouterInputProtocol.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

protocol DietStatisticsRouterProtocol {
    func showBodyMetricDetailScreen(_ type: BodyMetricType,
                                    withDiet diet: DietProtocol)
    func showWeightDetailScreen(withDiet diet: DietProtocol)
    func showWaterDetailScreen(withDiet diet: DietProtocol)
    func showDietMenu(withDiet diet: DietProtocol)
}
