//
//  DietStatisticsViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class DietStatisticsViewController: UIViewController {
    
    private let model: DietStatisticsModelProtocol
    private let router: DietStatisticsRouterProtocol
    
    private lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .littBackgroundColor
        tableView.showsVerticalScrollIndicator = false
        tableView.animations = []
        tableView.sectionHeaderTopPadding = 2 * CGFloat.logicBlockLittMargin
        tableView.contentInset.top = -(2 * CGFloat.logicBlockLittMargin)
        return tableView
    }()
    
    init(model: DietStatisticsModelProtocol,
         router: DietStatisticsRouterProtocol) {
        self.model = model
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model.viewDidLoad()
    }
}

// MARK: - DietStatisticsModelOutputProtocol
extension DietStatisticsViewController: DietStatisticsViewProtocol {
    func configureView() {
        view.backgroundColor = .littBackgroundColor
        view.addSubview(tableView)
        
        tableView.pinToSuperview(insets: .zero, useSafeArea: true)
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    func configureNavigationTitle(_ title: String) {
        self.title = title
    }
    
    func showDataSections(_ sections: [LittTableViewSectionProtocol]) {
        tableView.sections = sections
    }
    
    func showDetailProgress(forType type: ProgressValueType, diet: DietProtocol) {
        switch type {
        case .water:
            router.showWaterDetailScreen(withDiet: diet)
        case .weight:
            router.showWeightDetailScreen(withDiet: diet)
        case .chest:
            router.showBodyMetricDetailScreen(.chest, withDiet: diet)
        case .waist:
            router.showBodyMetricDetailScreen(.waist, withDiet: diet)
        case .hips:
            router.showBodyMetricDetailScreen(.hips, withDiet: diet)
        }
    }
    
    func showDietMenu(withDiet diet: DietProtocol) {
        router.showDietMenu(withDiet: diet)
    }
}
