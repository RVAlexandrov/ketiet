//
//  DietStatisticsModelOutputProtocol.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 04.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import Foundation

protocol DietStatisticsViewProtocol: AnyObject {
    func configureView()
    func configureNavigationTitle(_ title: String)
    func showDataSections(_ sections: [LittTableViewSectionProtocol])
    func showDetailProgress(forType type: ProgressValueType,
                            diet: DietProtocol)
    func showDietMenu(withDiet diet: DietProtocol)
}
