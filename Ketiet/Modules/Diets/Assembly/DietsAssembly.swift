//
//  DietsViewControllerAssembly.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietsAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: DietsModuleConfiguratorProtocol) {
        let presenter = DietsPresenter(didUpdateDiets: moduleConfigurator.didUpdateDiets)
        let interactor = DietsInteractor(presenter: presenter,
                                         dietService: DietService.shared,
                                         getProfileService: ProfileService.instance)
        let view = DietsViewController(presenter: presenter)
        let router = DietsRouter(viewController: view,
                                 alertFactory: AlertControllerFactory())
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
