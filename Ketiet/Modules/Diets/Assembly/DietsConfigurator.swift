//
//  DietsViewControllerConfigurator.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol DietsModuleConfiguratorProtocol {
	var didUpdateDiets: () -> Void { get }
}

final class DietsModuleConfigurator: DietsModuleConfiguratorProtocol {
	let didUpdateDiets: () -> Void

    init(didUpdateDiets: @escaping() -> Void) {
		self.didUpdateDiets = didUpdateDiets
    }
}
