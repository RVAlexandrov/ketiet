//
//  DietsViewControllerInteractor.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

final class DietsInteractor {
	
    private weak var presenter: DietsInteractorOutputProtocol?
	private let dietService: DietServiceProtocol
	private var diets: [DietProtocol]?
    private let getProfileService: ProfileServiceGetProtocol
	
    init(
        presenter: DietsInteractorOutputProtocol,
        dietService: DietServiceProtocol,
        getProfileService: ProfileServiceGetProtocol
    ) {
        self.presenter = presenter
		self.dietService = dietService
        self.getProfileService = getProfileService
    }
}

// MARK: - DietsInteractorInputProtocol
extension DietsInteractor: DietsInteractorInputProtocol {
    func getProfile() {
        getProfileService.getProfile() { [weak self] profile in
            if let profile = profile,
               let self = self {
                self.presenter?.didGetProfile(profile)
            }
        }
    }
    
	func requestDiets() {
		dietService.requestAllDiets { [weak self] diets in
			self?.diets = diets
			self?.presenter?.didReceiveDiets(diets: diets ?? [])
		}
	}
	
	func start(diet: Diet) {
		dietService.setCurrentDiet(diet) { [weak self] result in
			switch result {
			case  .success:
				self?.presenter?.didStartDiet()
			case let .failure(error):
				assertionFailure(error.localizedDescription)
			}
		}
	}
	
	func copy(diet: DietProtocol) {
		let dietCopy = diet.newDietCopy()
		dietService.addDiet(diet: dietCopy) { [weak self] result in
			switch result {
			case .success:
				self?.presenter?.didCreateDietCopy(copy: dietCopy)
			case let .failure(error):
				assertionFailure(error.localizedDescription)
			}
		}
	}
    
    func cancelCurrentDiet() {
        guard let currentDiet = dietService.getCurrentDiet() else { return }
        dietService.setStatus(.cancelled,
                              for: currentDiet,
                              completion: { _ in })
    }

	func cancel(diet: DietProtocol) {
		dietService.setStatus(.cancelled,
							  for: diet,
							  completion: { [weak self] result in
			switch result {
			case .success:
				self?.presenter?.didCancelDiet()
			case let .failure(error):
				assertionFailure(error.localizedDescription)
			}
		})
	}
	
	func remove(diet: DietProtocol) {
		dietService.removeDiet(diet) { [weak self] result in
			switch result {
			case .success:
				self?.presenter?.didRemoveCurrentSelectedDiet()
			case let .failure(error):
				assertionFailure(error.localizedDescription)
			}
		}
	}
}
