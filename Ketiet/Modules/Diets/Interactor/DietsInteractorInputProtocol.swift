//
//  DietsViewControllerInteractorInputProtocol.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol DietsInteractorInputProtocol {
	
	func requestDiets()
	func start(diet: Diet)
	func copy(diet: DietProtocol)
	func cancel(diet: DietProtocol)
	func remove(diet: DietProtocol)
    func getProfile()
    func cancelCurrentDiet()
}
