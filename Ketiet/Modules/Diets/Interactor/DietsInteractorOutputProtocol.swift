//
//  DietsViewControllerInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol DietsInteractorOutputProtocol: AnyObject {
	
	func didReceiveDiets(diets: [DietProtocol])
	func didStartDiet()
	func didCancelDiet()
	func didCreateDietCopy(copy: DietProtocol)
	func didRemoveCurrentSelectedDiet()
    func didGetProfile(_ profile: ProfileProtocol)
}
