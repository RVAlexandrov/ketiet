//
//  DietsViewControllerPresenter.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietsPresenter {

    weak var view: DietsViewControllerInputProtocol?
    var interactor: DietsInteractorInputProtocol?
    var router: DietsRouterInputProtocol?
    
	private var currentSelectedItem: DietTableViewCellItem?
	private var items = [DietTableViewCellItem]()
	private var hasChanges = false
	private let didUpdateDiets: () -> Void

    init(didUpdateDiets: @escaping() -> Void) {
		self.didUpdateDiets = didUpdateDiets
    }
	
	private func didSelectDietItem(_ dietItem: DietTableViewCellItem) {
		currentSelectedItem = dietItem
//		var actions = [AlertAction]()
		switch dietItem.diet.status {
		case .cancelled, .completed:
            router?.showDietStatistics(diet: dietItem.diet)
//			actions.append(AlertAction(
//                title: "COPY".localized(),
//                style: .normal
//            ) { [weak self] in
//				guard let self = self,
//                        let item = self.currentSelectedItem else { return }
//				self.interactor?.copy(diet: item.diet)
//			})
//			actions.append(removeDietAtIndexAction())
		case .notStarted:
            break
//			actions.append(AlertAction(
//                title: "START_DIET".localized(),
//                style: .normal
//            ) { [weak self] in
//				guard let self = self,
//                        let item = self.currentSelectedItem,
//                        let diet = item.diet as? Diet  else { return }
//				self.hasChanges = true
//				self.interactor?.start(diet: diet)
//			})
//			actions.append(removeDietAtIndexAction())
		case .inProgress:
            break
//			actions.append(AlertAction(
//                title: "CANCEL_DIET".localized(),
//                style: .normal
//            ) { [weak self] in
//				guard let self = self,
//                        let item = self.currentSelectedItem else { return }
//				self.hasChanges = true
//				self.interactor?.cancel(diet: item.diet)
//			})
		}
//		actions.append(AlertAction(
//            title: "CANCEL".localized(),
//            style: .delete
//        ))
//		router?.showSctionSheet(
//            title: "SELECT_ACTION".localized(),
//            message: "",
//            actions: actions
//        )
	}
	
	private func removeDietAtIndexAction() -> AlertAction {
		AlertAction(
            title: "REMOVE_DIET".localized(),
            style: .delete
        ) { [weak self] in
			guard let self = self, let item = self.currentSelectedItem else { return }
			self.interactor?.remove(diet: item.diet)
		}
	}
}

// MARK: - DietsViewControllerViewOutputProtocol
extension DietsPresenter: DietsViewControllerViewOutputProtocol {
    func viewDidLoad() {
		interactor?.requestDiets()
    }
	
	func didTapBackButton() {
		if hasChanges {
			didUpdateDiets()
		}
	}
    
    func didTapBarButton() {
        interactor?.getProfile()
    }
}

// MARK: - DietsInteractorOutputProtocol
extension DietsPresenter: DietsInteractorOutputProtocol {
    func didGetProfile(_ profile: ProfileProtocol) {
        router?.showNewDietInputParameters(profile: profile)
    }
    
	func didReceiveDiets(diets: [DietProtocol]) {
        let items = diets.map { DietTableViewCellItem(diet: $0) { [weak self] item in self?.didSelectDietItem(item) } }
		self.items.append(contentsOf: items)
        
        var cells: [LittTableViewCellItemProtocol] = []
        cells.append(contentsOf: items)
        let createItem = CreateDietCellItem {
            self.interactor?.getProfile()
//            self.interactor?.cancelCurrentDiet()
        }
        cells.append(createItem)
		view?.displayDietItems(items: cells)
	}
	
	func didStartDiet() {
		items.forEach { $0.updateCell() }
		currentSelectedItem = nil
	}
	
	func didCancelDiet() {
		currentSelectedItem?.updateCell()
		currentSelectedItem = nil
	}
	
	func didCreateDietCopy(copy: DietProtocol) {
		let item = DietTableViewCellItem(diet: copy) { [weak self] item in
            self?.didSelectDietItem(item)
		}
		items.append(item)
		view?.appendDietItemAndScroll(item: item)
	}

	func didRemoveCurrentSelectedDiet() {
		guard let index = items.firstIndex(where: { $0 === self.currentSelectedItem }) else { return }
		self.items.remove(at: index)
		view?.removeDiet(at: index)
	}
}
