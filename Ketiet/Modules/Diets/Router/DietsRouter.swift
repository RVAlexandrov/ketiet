//
//  DietsViewControllerRouter.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietsRouter {

    private(set) weak var viewController: UIViewController?
    private let alertFactory: AlertControllerFactoryProtocol

    init(viewController: UIViewController,
         alertFactory: AlertControllerFactoryProtocol) {
        self.viewController = viewController
        self.alertFactory = alertFactory
    }
}

// MARK: - DietsRouterInputProtocol
extension DietsRouter: DietsRouterInputProtocol {
	
	func showSctionSheet(
        title: String,
        message: String,
        actions: [AlertAction]
    ) {
        let controller = alertFactory.makeAlert(
            title: title,
            description: message,
            actions: actions
        )
		viewController?.present(controller, animated: true)
	}
    
    func showDietStatistics(diet: DietProtocol) {
        let assembly = DietStatisticsAssembly()
        let vc = assembly.makeViewController(diet: diet, progressServiceFabric: ProgressServiceFabric())
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNewDietInputParameters(profile: ProfileProtocol) {
        let storage = NewDietTemporaryStorage()
        let assembly = WelcomeParametersAssembly(
            profile: profile,
            profileService: storage,
            mealDatesStorage: storage
        )
        viewController?.navigationController?.pushViewController(
            assembly.initialViewController(),
            animated: true
        )
    }
}
