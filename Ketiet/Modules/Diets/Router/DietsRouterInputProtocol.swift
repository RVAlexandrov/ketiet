//
//  DietsViewControllerRouterInputProtocol.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol DietsRouterInputProtocol {
	func showSctionSheet(
        title: String,
        message: String,
        actions: [AlertAction]
    )
    func showDietStatistics(diet: DietProtocol)
    func showNewDietInputParameters(profile: ProfileProtocol)
}
