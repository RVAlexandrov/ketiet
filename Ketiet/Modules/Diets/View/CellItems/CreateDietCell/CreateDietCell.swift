//
//  CreateDietCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.05.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class CreateDietCell: UITableViewCell {
    
    private lazy var backgroundImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "loseWeightImage")
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var blurView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        return view
    }()
    
    private(set) lazy var button = LittBasicButton(
        title: "START_NEW_DIET".localized(),
        config: .filledLarge()
    )
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CreateDietCell {
    private func configureView() {
        contentView.addSubviews([backgroundImageView, blurView])
        blurView.contentView.addSubview(button)
                
        NSLayoutConstraint.activate([
            backgroundImageView.heightAnchor.constraint(equalToConstant: 116),
            backgroundImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            blurView.topAnchor.constraint(equalTo: contentView.topAnchor),
            blurView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            blurView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            blurView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            button.centerYAnchor.constraint(equalTo: blurView.contentView.centerYAnchor),
            button.centerXAnchor.constraint(equalTo: blurView.contentView.centerXAnchor),
            button.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.75)
        ])
    }
}
