//
//  CreateDietCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.05.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

final class CreateDietCellItem {
    private weak var cell: CreateDietCell?
    private let didSelectHandler: (() -> Void)
    
    init(didSelectHandler: @escaping (() -> Void)) {
        self.didSelectHandler = didSelectHandler
    }
    
    func updateCell() {
        DispatchQueue.main.async {
            if let cell = self.cell {
                self.configure(view: cell)
            }
        }
    }
}

extension CreateDietCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? CreateDietCell else { return }
        self.cell = cell
        cell.button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    func viewClass() -> AnyClass {
        CreateDietCell.self
    }
    
    func didSelect() {
       didSelectHandler()
    }
    
    @objc
    private func didTapButton() {
        didSelectHandler()
    }
}
