//
//  DietCollectionViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 08.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietTableViewCell: UITableViewCell {
	
	let titleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.textAlignment = .left
		label.numberOfLines = 2
		label.font = .subTitle
		label.lineBreakMode = .byWordWrapping
		label.setContentCompressionResistancePriority(.required, for: .vertical)
		label.setContentHuggingPriority(.defaultHigh, for: .vertical)
		return label
	}()
	
	let startDateLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
		label.textAlignment = .right
		label.setContentHuggingPriority(.defaultLow, for: .vertical)
		return label
	}()
	
	let statusLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
		label.textAlignment = .right
		label.setContentCompressionResistancePriority(.required, for: .vertical)
		label.setContentHuggingPriority(.defaultHigh, for: .vertical)
		return label
	}()
    
    let startDateTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        label.text = "START_DATE".localized() + ":"
        return label
    }()
    
    let statusTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "STATUS".localized() + ":"
        label.font = .body
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setBorderColor(color: UIColor) {
        contentView.layer.borderColor = color.cgColor
	}
	
	private func setupView() {
        contentView.layer.borderWidth = 1
		contentView.layer.cornerRadius = .littBasicCornerRadius
		contentView.addSubviews([titleLabel, startDateTitleLabel, startDateLabel, statusTitleLabel, statusLabel])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: .littMediumWellMargin
            ),
            titleLabel.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: .littMediumWellMargin
            ),
            titleLabel.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: -.littMediumWellMargin
            ),
            
            startDateTitleLabel.topAnchor.constraint(
                equalTo: titleLabel.bottomAnchor,
                constant: .littMediumWellMargin
            ),
            startDateTitleLabel.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: .littMediumWellMargin
            ),
            
            startDateLabel.topAnchor.constraint(
                equalTo: titleLabel.bottomAnchor,
                constant: .littMediumWellMargin
            ),
            startDateLabel.leadingAnchor.constraint(
                equalTo: startDateTitleLabel.trailingAnchor,
                constant: .littMediumWellMargin
            ),
            startDateLabel.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: -.littMediumWellMargin
            ),
            
            statusTitleLabel.topAnchor.constraint(
                equalTo: startDateLabel.bottomAnchor,
                constant: .littMediumWellMargin
            ),
            statusTitleLabel.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: .littMediumWellMargin
            ),
            
            statusLabel.topAnchor.constraint(
                equalTo: startDateLabel.bottomAnchor,
                constant: .littMediumWellMargin
            ),
            statusLabel.leadingAnchor.constraint(
                equalTo: statusTitleLabel.trailingAnchor,
                constant: .littMediumWellMargin
            ),
            statusLabel.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: -.littMediumWellMargin
            ),
            
            statusLabel.bottomAnchor.constraint(
                equalTo: contentView.bottomAnchor,
                constant: -.littScreenEdgeMargin
            )
        ])
	}
}
