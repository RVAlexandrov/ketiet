//
//  DietCollectionViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 08.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietTableViewCellItem {
	
	let diet: DietProtocol
	private weak var cell: DietTableViewCell?
	private lazy var dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .medium
		return formatter
	}()
	private let didSelectHandler: (DietTableViewCellItem) -> Void
	
	init(
        diet: DietProtocol,
        didSelectHandler: @escaping(DietTableViewCellItem) -> Void
    ) {
		self.diet = diet
		self.didSelectHandler = didSelectHandler
	}
	
	func updateCell() {
		DispatchQueue.main.async {
			if let cell = self.cell {
				self.configure(view: cell)
			}
		}
	}
}

extension DietTableViewCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? DietTableViewCell else { return }
		self.cell = cell
		cell.titleLabel.text = diet.name
		cell.statusLabel.text = diet.status.description
        cell.setBorderColor(color: diet.status.color)
		if let date = diet.startedDate {
			cell.startDateLabel.text = dateFormatter.string(from: date)
		} else {
			cell.startDateLabel.text = nil
		}
	}
	
	func viewClass() -> AnyClass {
		DietTableViewCell.self
	}
	
	func didSelect() {
		didSelectHandler(self)
	}
}
