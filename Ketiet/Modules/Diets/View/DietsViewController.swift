//
//  DietsViewControllerViewController.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietsViewController: UIViewController {

    private lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .littBackgroundColor
        tableView.setContentInsetForControllersInTabbar()
        tableView.animations = []
        tableView.forwardingDelegate = self
        return tableView
    }()
    
    private lazy var barButton = LittCapsuleButton(title: "START_NEW_DIET".localized())
    
    private lazy var rightBarButtonItems = navigationItem.makeNavigationBarLittItems(barButton: barButton)
    
    private let isInitialSetButton = false

    private let presenter: DietsViewControllerViewOutputProtocol

    init(presenter: DietsViewControllerViewOutputProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
		navigationItem.title = "DIETS".localized()
        presenter.viewDidLoad()
        barButton.addTarget(self, action: #selector(didTapBarButton), for: .touchUpInside)
    }
	
	override func loadView() {
		view = UIView()
		view.addSubview(tableView)
        tableView.pinToSuperview()
	}
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)
        guard parent == nil else { return }
        presenter.didTapBackButton()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if tableView.frame.height != 0,
           !isInitialSetButton {
            showButtonIfNeeded()
        }
    }
}

// MARK: - DietsViewControllerViewInputProtocol
extension DietsViewController: DietsViewControllerInputProtocol {
	func displayDietItems(items: [LittTableViewCellItemProtocol]) {
        tableView.sections = items.map { LittTableViewSection(items: [$0]) }
        showButtonIfNeeded()
	}
	
	func removeDiet(at index: Int) {
		tableView.sections.remove(at: index)
		let indexPath = IndexPath(row: 0, section: index)
        tableView.deleteRows(at: [indexPath], with: .automatic)
	}
	
	func appendDietItemAndScroll(item: LittTableViewCellItemProtocol) {
        let indexPath = IndexPath(item: 0, section: tableView.numberOfSections)
        tableView.sections.append(LittTableViewSection(items: [item]))
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
	}
}

// MARK: - UITableViewDelegate
extension DietsViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        showButtonIfNeeded()
    }
}

// MARK: - Private methods
extension DietsViewController {
    private func showButtonIfNeeded() {
        guard tableView.numberOfSections > 0 else { return }
        
        let offsetDelta = tableView.contentOffset.y + tableView.contentInset.top + view.safeAreaInsets.top
        let visibleHeight = view.frame.size.height - tableView.contentInset.bottom - view.safeAreaInsets.top - tableView.contentInset.top
        let collectionViewHeight = tableView.contentSize.height
        let visibleDiff = collectionViewHeight - visibleHeight
                        
        if offsetDelta < visibleDiff {
            navigationItem.rightBarButtonItems = rightBarButtonItems
        } else {
            navigationItem.rightBarButtonItems = []
        }
    }
    
    @objc
    private func didTapBarButton() {
        presenter.didTapBarButton()
    }
}
