//
//  DietsViewControllerViewInputProtocol.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol DietsViewControllerInputProtocol: AnyObject {
	
	func removeDiet(at index: Int)
	func displayDietItems(items: [LittTableViewCellItemProtocol])
	func appendDietItemAndScroll(item: LittTableViewCellItemProtocol)
}
