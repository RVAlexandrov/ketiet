//
//  DietsViewControllerViewController.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietsViewController: UIViewController, BaseViewController {
	
	private struct Constants {
		static let itemsInRow: CGFloat = 2
	}

    typealias Presenter = DietsViewControllerViewOutputProtocol

	private let collectionView: LittCollectionView = {
		let layout = UICollectionViewFlowLayout()
		layout.minimumLineSpacing = .largeLittMargin
		layout.minimumInteritemSpacing = .largeLittMargin
		let itemSide = (UIScreen.main.bounds.width - (Constants.itemsInRow) * .largeLittMargin) / Constants.itemsInRow
		layout.sectionInset = UIEdgeInsets(top: 0, left: .largeLittMargin, bottom: 0, right: .largeLittMargin)
		layout.itemSize = CGSize(width: itemSide, height: itemSide)
		let collectionView = LittCollectionView(frame: .zero, collectionViewLayout: layout)
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		return collectionView
	}()

    let presenter: Presenter

    init(presenter: Presenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
	
	override func loadView() {
		view = UIView()
		view.addSubview(collectionView)
		collectionView.pinToSuperview()
	}
}

// MARK: - DietsViewControllerViewInputProtocol
extension DietsViewControllerViewController: DietsViewControllerViewInputProtocol {
	func displayDietItems(items: [LittCollectionViewCellItemProtocol]) {
		collectionView.sections = [LittCollectionViewSection(items: items)]
	}
	
	func appendDietItemAndScroll(item: LittCollectionViewCellItemProtocol) {
		collectionView.sections.first?.items.append(item)
		let indexPath = IndexPath(item: collectionView.numberOfItems(inSection: 0), section: 0)
		collectionView.insertItems(at: [indexPath])
		collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
	}
}
