//
//  DietsViewControllerViewOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 08/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol DietsViewControllerViewOutputProtocol {
    func viewDidLoad()
	func didTapBackButton()
    func didTapBarButton()
}
