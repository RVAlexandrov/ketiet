//
//  DishDetailsViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 31.08.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import UIKit

final class DishDetailsViewController: UIViewController {
    
    private struct Constants {
        static let topInset = CGFloat(40)
    }
    
    private let measurementService: MeasurementServiceProtocol
    private let energyMeasurement: String
    private let dish: DishProtocol
    
    private(set) lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.showsVerticalScrollIndicator = false
        tableView.contentInset.top += .logicBlockLittMargin
        tableView.animations = []
        return tableView
    }()
    
    init(measurementService: MeasurementServiceProtocol,
         energyMeasurement: String,
         dish: DishProtocol) {
        self.measurementService = measurementService
        self.energyMeasurement = energyMeasurement
        self.dish = dish
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private methods
extension DishDetailsViewController {
    private func configureView() {
        view.addSubview(tableView)
        view.backgroundColor = .littBackgroundColor
        
        tableView.pinToSuperview(insets: UIEdgeInsets(top: view.safeAreaInsets.top,
                                                      left: 0,
                                                      bottom: 0,
                                                      right: 0))
        
        var sections: [LittTableViewSectionProtocol] = []
                
        let currentDish = dish
        
        let headerItem = DishHeaderCellItem(mealString: dish.name,
                                            dishString: nil,
                                            weightString: "WEIGHT_OF_PORTION".localized() + " " + currentDish.weightInGrams.cleanValue + " " +  measurementService.measurement())
        let headerSection = LittTableViewSection(items: [headerItem])
        
        sections.append(headerSection)
        
        let badgeTypes: [OutlineBadgeType] = currentDish.categories.map { OutlineBadgeType(with: $0) }
        if !badgeTypes.isEmpty {
            let badgeItem = BadgesCellItem(badgesType: badgeTypes)
            let badgeSection = LittTableViewSection(items: [badgeItem])
            sections.append(badgeSection)
        }
        
        let imageItem = ImageCellItem(imageWrapper: .url(currentDish.imageURL?.absoluteString ?? ""))
        
        let imageSection = LittTableViewSection(items: [imageItem])
        sections.append(imageSection)
        
        let detailSections = currentDish.detailsSections(measurement: measurementService.measurement(),
                                                         energyMeasure: self.energyMeasurement)
        
        sections.append(contentsOf: detailSections)
        
        if let cookingSteps = currentDish.dishRecipe.cookingSteps,
           !cookingSteps.isEmpty {
            let prepareSection = currentDish.preparationSection(presenterController: self)
            sections.append(prepareSection)
        }

        tableView.sections = sections
        tableView.reloadDataWithAnimation()
    }
}

extension DishProtocol {
    func detailsSections(measurement: String, energyMeasure: String) -> [LittTableViewSectionProtocol] {
        
        var sections: [LittTableViewSectionProtocol] = []
        
        let nutrientsItems = [
            ValueDisplayingTableViewCellItem(title: NutrientType.protein.fullString + ", " + measurement,
                                             value: String(nutriens.proteins.round(to: 1)),
                                             valueColor: .accentColor),
            ValueDisplayingTableViewCellItem(title: NutrientType.carbohydrate.fullString + ", " + measurement,
                                             value: String(nutriens.carbohydrates.round(to: 1)),
                                             valueColor: .accentColor),
            ValueDisplayingTableViewCellItem(title: NutrientType.fat.fullString  + ", " + measurement,
                                             value: String(nutriens.fat.round(to: 1)),
                                             valueColor: .accentColor),
            ValueDisplayingTableViewCellItem(title: "TOTAL".localized() + ", " + energyMeasure,
                                             value: String(nutriens.calories.round(to: 1)),
                                             valueColor: .accentColor)
        ]
        
        let energyProfitSection = LittTableViewSection(header: SegmentHeaderItem(text: "ENERGY_PROFIT".localized(),
                                                                                 itemTitles: [
                                                                                    "ENERGY_PROFIT_PER_100_GRAMM".localized(),
                                                                                    "ENERGY_PROFIT_PER_PORTION".localized()
                                                                                 ],
                                                                                 didChangeValueHandler: { [weak self] _ , index in
            guard let self = self else { return }
            
            if index == 0 {
                nutrientsItems[0].reloadValue(newValue: String(self.nutriens.proteins.round(to: 1)))
                nutrientsItems[1].reloadValue(newValue: String(self.nutriens.carbohydrates.round(to: 1)))
                nutrientsItems[2].reloadValue(newValue: String(self.nutriens.fat.round(to: 1)))
                nutrientsItems[3].reloadValue(newValue: String(self.nutriens.calories.round(to: 1)))
            } else {
                nutrientsItems[0].reloadValue(newValue: String(self.weightedNutrients.proteins.round(to: 1)))
                nutrientsItems[1].reloadValue(newValue: String(self.weightedNutrients.carbohydrates.round(to: 1)))
                nutrientsItems[2].reloadValue(newValue: String(self.weightedNutrients.fat.round(to: 1)))
                nutrientsItems[3].reloadValue(newValue: String(self.weightedNutrients.calories.round(to: 1)))
            }
            
        }),
                                                       items: nutrientsItems)
        sections.append(energyProfitSection)
        
        if !products.isEmpty {
            let productItems: [ValueDisplayingTableViewCellItem] = products.map {
                ValueDisplayingTableViewCellItem(title: $0.name,
                                                 value: $0.quantity.description,
                                                 valueColor: .accentColor)
            }
            
            let productsSection = LittTableViewSection(
                header: PlainHeaderItem(text: "PRODUCTS".localized()),
                items: productItems,
                footer: BasicFooterItem(
                    text: "\("PRODUCT_COUNT_PER_DISH_DESCRIPTION".localized()) \(dishRecipe.weightInGrams) \(measurement)"))
            sections.append(productsSection)
        }
        return sections
    }
    
    func preparationSection(presenterController: UIViewController) -> LittTableViewSectionProtocol {
        let preparationMethodValue = HiddenElementsLinkTableViewCellItem(
            dishRecipe: dishRecipe,
            buttonModel: ButtonModel(
                title: "VIEW_THE_RECIPE".localized() + " \u{203A}",
                action: { [weak presenterController] in
                    let controller = CookingStepsViewController(
                        steps: self.dishRecipe.cookingSteps ?? [],
                        mealName: self.name
                    )
                    presenterController?.present(
                        controller,
                        animated: true
                    )
                }
            )
        )
        
        return LittTableViewSection(header: PlainHeaderItem(text: "PREPARATION_METHOD".localized()),
                                    items: [preparationMethodValue])
    }
}
