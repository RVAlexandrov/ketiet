//
//  Apple.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 16/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class Apple: ElipseColisionImageView {
    
    init() {
        super.init(image: #imageLiteral(resourceName: "apple"))
        layer.cornerRadius = 35
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 35
    }
}
