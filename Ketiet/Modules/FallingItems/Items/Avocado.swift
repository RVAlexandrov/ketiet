//
//  Avocado.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 26/10/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

final class Avocado : UIImageView {
    init() {
        super.init(image:#imageLiteral(resourceName: "Avocado"))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 6.5 - wDelta, y: 25.5 - hDelta))
        path.addLine(to: CGPoint(x:21.5 - wDelta, y:14.5 - hDelta))
        path.addLine(to: CGPoint(x:39.5 - wDelta, y:3.5 - hDelta))
        path.addLine(to: CGPoint(x:55.5 - wDelta, y:0.5 - hDelta))
        path.addLine(to: CGPoint(x:66.5 - wDelta, y:2.5 - hDelta))
        path.addLine(to: CGPoint(x:69.5 - wDelta, y:15.5 - hDelta))
        path.addLine(to: CGPoint(x:66.5 - wDelta, y:26.5 - hDelta))
        path.addLine(to: CGPoint(x:61.5 - wDelta, y:42.5 - hDelta))
        path.addLine(to: CGPoint(x:51.5 - wDelta, y:60.5 - hDelta))
        path.addLine(to: CGPoint(x:38.5 - wDelta, y:67.5 - hDelta))
        path.addLine(to: CGPoint(x:28.5 - wDelta, y:69.5 - hDelta))
        path.addLine(to: CGPoint(x:15.5 - wDelta, y:66.5 - hDelta))
        path.addLine(to: CGPoint(x:6.5 - wDelta, y:60.5 - hDelta))
        path.addLine(to: CGPoint(x:1.5 - wDelta, y:51.5 - hDelta))
        path.addLine(to: CGPoint(x:1.5 - wDelta, y:38.5 - hDelta))
        path.addLine(to: CGPoint(x:6.5 - wDelta, y:25.5 - hDelta))
        path.close()
        return path
    }
}
