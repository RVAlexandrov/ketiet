//
//  Carrot.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Carrot: UIImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "carrot"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 1.5 - wDelta, y: 12.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 40.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 11.5 - wDelta, y: 86.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 17.5 - wDelta, y: 89.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 23.5 - wDelta, y: 86.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 33.5 - wDelta, y: 45.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 33.5 - wDelta, y: 9.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 23.5 - wDelta, y: 2.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 10.5 - wDelta, y: 0.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 1.5 - wDelta, y: 12.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
