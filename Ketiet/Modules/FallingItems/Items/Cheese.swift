//
//  Cheese.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Cheese: UIImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "cheese"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
