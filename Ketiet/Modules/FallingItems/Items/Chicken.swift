//
//  Chicken.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Chicken: UIImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "smiling_chicken"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0.5 - wDelta, y: 22.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 9.5 - wDelta, y: 7.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 24.5 - wDelta, y: 1.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 42.5 - wDelta, y: 6.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 50.5 - wDelta, y: 22.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 48.5 - wDelta, y: 44.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 44.5 - wDelta, y: 81.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 40.5 - wDelta, y: 111.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 38.5 - wDelta, y: 118.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 23.5 - wDelta, y: 119.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 20.5 - wDelta, y: 111.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 14.5 - wDelta, y: 90.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 38.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 22.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
