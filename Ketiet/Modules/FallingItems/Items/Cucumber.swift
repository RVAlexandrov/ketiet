//
//  Cucumber.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Cucumber: UIImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "cucumber"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0.5 - wDelta, y: 69.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 14.5 - wDelta, y: 26.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 22.5 - wDelta, y: 10.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 34.5 - wDelta, y: 0.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 43.5 - wDelta, y: 15.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 49.5 - wDelta, y: 26.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 49.5 - wDelta, y: 59.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 42.5 - wDelta, y: 75.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 27.5 - wDelta, y: 88.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 14.5 - wDelta, y: 88.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 4.5 - wDelta, y: 85.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 69.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
