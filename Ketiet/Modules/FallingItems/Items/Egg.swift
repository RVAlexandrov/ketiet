//
//  Egg.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Egg: UIImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "smiling_egg"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 2.5 - wDelta, y: 25.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 5.5 - wDelta, y: 16.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 10.5 - wDelta, y: 8.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 17.5 - wDelta, y: 2.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 22.5 - wDelta, y: 0.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 28.5 - wDelta, y: 2.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 35.5 - wDelta, y: 6.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 39.5 - wDelta, y: 11.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 42.5 - wDelta, y: 17.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 45.5 - wDelta, y: 25.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 47.5 - wDelta, y: 33.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 46.5 - wDelta, y: 43.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 41.5 - wDelta, y: 52.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 35.5 - wDelta, y: 57.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 28.5 - wDelta, y: 59.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 22.5 - wDelta, y: 59.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 16.5 - wDelta, y: 58.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 10.5 - wDelta, y: 55.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 6.5 - wDelta, y: 52.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 3.5 - wDelta, y: 48.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 41.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 32.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 2.5 - wDelta, y: 25.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
