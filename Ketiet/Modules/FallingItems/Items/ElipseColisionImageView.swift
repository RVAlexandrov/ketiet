//
//  ElipseColisionImageView.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 16/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class ElipseColisionImageView : UIImageView {
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .ellipse
    }
}
