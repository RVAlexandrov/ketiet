//
//  Fish.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 16/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Fish : UIImageView {
    
    init() {
        super.init(image: #imageLiteral(resourceName: "fish"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let bezierPath = UIBezierPath()
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        bezierPath.move(to: CGPoint(x: 0.5 - wDelta, y: 51.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 10.5 - wDelta, y: 29.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 29.5 - wDelta, y: 8.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 41.5 - wDelta, y: 1.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 59.5 - wDelta, y: 13.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 71.5 - wDelta, y: 23.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 76.5 - wDelta, y: 37.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 79.5 - wDelta, y: 48.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 77.5 - wDelta, y: 58.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 71.5 - wDelta, y: 69.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 52.5 - wDelta, y: 76.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 40.5 - wDelta, y: 80.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 10.5 - wDelta, y: 80.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 74.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 51.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
