//
//  Meat.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Meat: UIImageView {
    
    init() {
        super.init(image: #imageLiteral(resourceName: "meat"))
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0.5 - wDelta, y: 78.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 12.5 - wDelta, y: 18.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 28.5 - wDelta, y: 2.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 38.5 - wDelta, y: 2.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 49.5 - wDelta, y: 2.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 64.5 - wDelta, y: 18.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 81.5 - wDelta, y: 50.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 81.5 - wDelta, y: 85.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 54.5 - wDelta, y: 98.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 21.5 - wDelta, y: 98.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 1.5 - wDelta, y: 90.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 78.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
