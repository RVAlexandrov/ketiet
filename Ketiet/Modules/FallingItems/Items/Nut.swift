//
//  Nut.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 26/10/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Nut : UIImageView {
    
    init() {
        super.init(image: #imageLiteral(resourceName: "Nut"))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 2.5 - wDelta, y: 7.5 - hDelta))
        path.addLine(to: CGPoint(x:13.5 - wDelta, y:0.5 - hDelta))
        path.addLine(to: CGPoint(x:25.5 - wDelta, y:2.5 - hDelta))
        path.addLine(to: CGPoint(x:32.5 - wDelta, y:8.5 - hDelta))
        path.addLine(to: CGPoint(x:34.5 - wDelta, y:18.5 - hDelta))
        path.addLine(to: CGPoint(x:32.5 - wDelta, y:32.5 - hDelta))
        path.addLine(to: CGPoint(x:24.5 - wDelta, y:38.5 - hDelta))
        path.addLine(to: CGPoint(x:3.5 - wDelta, y:30.5 - hDelta))
        path.addLine(to: CGPoint(x:0.5 - wDelta, y:17.5 - hDelta))
        path.addLine(to: CGPoint(x:2.5 - wDelta, y:7.5 - hDelta))
        path.close()
        return path
    }
}
