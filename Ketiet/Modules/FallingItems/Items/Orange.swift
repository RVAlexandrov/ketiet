//
//  Orange.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Orange: ElipseColisionImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "smiling_orange"))
        layer.cornerRadius = 30
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        layer.cornerRadius = 30
    }
}
