//
//  Pear.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Pear: UIImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "pear"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0.5 - wDelta, y: 43.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 13.5 - wDelta, y: 13.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 23.5 - wDelta, y: 0.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 33.5 - wDelta, y: 0.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 41.5 - wDelta, y: 14.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 53.5 - wDelta, y: 43.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 53.5 - wDelta, y: 53.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 41.5 - wDelta, y: 67.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 24.5 - wDelta, y: 69.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 9.5 - wDelta, y: 66.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 2.5 - wDelta, y: 59.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 43.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
