//
//  Salad.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 17/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Salad: UIImageView {

    init() {
        super.init(image: #imageLiteral(resourceName: "salad"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0.5 - wDelta, y: 33.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 8.5 - wDelta, y: 9.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 25.5 - wDelta, y: -0.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 45.5 - wDelta, y: 9.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 49.5 - wDelta, y: 20.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 45.5 - wDelta, y: 85.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 40.5 - wDelta, y: 104.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 26.5 - wDelta, y: 109.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 11.5 - wDelta, y: 105.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 8.5 - wDelta, y: 96.5 - hDelta))
        bezierPath.addLine(to: CGPoint(x: 0.5 - wDelta, y: 33.5 - hDelta))
        bezierPath.close()
        return bezierPath
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
}
