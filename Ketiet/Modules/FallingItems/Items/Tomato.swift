//
//  Tomato.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 16/08/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Tomato : ElipseColisionImageView {
    
    init() {
        super.init(image: #imageLiteral(resourceName: "tomato"))
        layer.cornerRadius = 28
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 28
    }
}

