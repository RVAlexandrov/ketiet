//
//  Yougurt.swift
//  EggDiet
//
//  Created by Kovalev Alexander on 27/10/2019.
//  Copyright © 2019 litDev. All rights reserved.
//

import UIKit

class Yougurt : UIImageView {
    init () {
        super.init(image: #imageLiteral(resourceName: "Yougurt"))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .path
    }
    
    override var collisionBoundingPath: UIBezierPath {
        let wDelta = bounds.size.width/2
        let hDelta = bounds.size.height/2
        let path = UIBezierPath()
        path.move(to: CGPoint(x:0.5 - wDelta, y:7.5 - hDelta))
        path.addLine(to: CGPoint(x:10.5 - wDelta, y:1.5 - hDelta))
        path.addLine(to: CGPoint(x:28.5 - wDelta, y:0.5 - hDelta))
        path.addLine(to: CGPoint(x:46.5 - wDelta, y:1.5 - hDelta))
        path.addLine(to: CGPoint(x:59.5 - wDelta, y:7.5 - hDelta))
        path.addLine(to: CGPoint(x:50.5 - wDelta, y:63.5 - hDelta))
        path.addLine(to: CGPoint(x:38.5 - wDelta, y:69.5 - hDelta))
        path.addLine(to: CGPoint(x:28.5 - wDelta, y:69.5 - hDelta))
        path.addLine(to: CGPoint(x:18.5 - wDelta, y:69.5 - hDelta))
        path.addLine(to: CGPoint(x:6.5 - wDelta, y:62.5 - hDelta))
        path.addLine(to: CGPoint(x:0.5 - wDelta, y:7.5 - hDelta))
        path.close()
        return path
    }
}
