//
//  FallingItemsProvider.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.03.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

final class FallingItemsProvider: FallingItemsProviderProtocol {
    private lazy var items = [
        Apple(),
        Avocado(),
        Carrot(),
        Cheese(),
        Chicken(),
        Cucumber(),
        Egg(),
        Fish(),
        Meat(),
        Nut(),
        Orange(),
        Pear(),
        Salad(),
        Toast(),
        Tomato(),
        Yougurt()
    ].map { view -> UIImageView in
        view.isUserInteractionEnabled = true
        return view
    }.shuffled()
    
    func nextItem() -> UIView? {
        guard !items.isEmpty else { return nil }
        return items.removeFirst()
    }
}
