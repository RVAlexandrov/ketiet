//
//  FallingItemsProviderProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 24.03.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

protocol FallingItemsProviderProtocol {
    func nextItem() -> UIView?
}
