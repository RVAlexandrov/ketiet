//
//  FallingItemsViewController.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 24.03.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit
import CoreMotion

final class FallingItemsViewController: UIViewController {

    private enum Configuration {
        static let mainBoundryId = NSString(string: "mainBoundyId")
    }
    
    private let provider: FallingItemsProviderProtocol
    private lazy var animator = UIDynamicAnimator(referenceView: view)
    private lazy var collisionBehavior: UICollisionBehavior = {
        let bahavior = UICollisionBehavior()
        let boundry = UIBezierPath()
        boundry.move(to: .zero)
        let screenSize = UIScreen.main.bounds.size
        boundry.addLine(to: CGPoint(x: 0, y: screenSize.height))
        boundry.addLine(to: CGPoint(x: screenSize.width, y: screenSize.height))
        boundry.addLine(to: CGPoint(x: screenSize.width, y: 0))
        bahavior.addBoundary(withIdentifier: Configuration.mainBoundryId, for: boundry)
        animator.addBehavior(bahavior)
        bahavior.collisionDelegate = self
        return bahavior
    }()
    private lazy var gravityBehavior: UIGravityBehavior = {
        let behavior = UIGravityBehavior()
        behavior.gravityDirection = CGVector(dx: 0, dy: 1)
        animator.addBehavior(behavior)
        return behavior
    }()
    private lazy var itemBehavior: UIDynamicItemBehavior = {
        let itemBehavior = UIDynamicItemBehavior()
        itemBehavior.friction = 0.01
        itemBehavior.elasticity = 0.1
        itemBehavior.density = 0.7
        animator.addBehavior(itemBehavior)
        return itemBehavior
    }()
    private let fallingInterval: TimeInterval
    private lazy var impactGenerator = UIImpactFeedbackGenerator(style: .light)
    private let completion: () -> Void
    private var firstCollisionSet = Set<UIView>()
    private lazy var motionManager = CMMotionManager()
    private var attachmentBehavior: UIAttachmentBehavior?
    private lazy var pushBehavior: UIPushBehavior = {
        let behavior = UIPushBehavior()
        behavior.magnitude = 1
        animator.addBehavior(behavior)
        return behavior
    }()
    
    init(
        provider: FallingItemsProviderProtocol,
        fallingInterval: TimeInterval = 1,
        completion: @escaping () -> Void
    ) {
        self.provider = provider
        self.fallingInterval = fallingInterval
        self.completion = completion
        super.init(nibName: nil, bundle: nil)
        transitioningDelegate = self
        modalPresentationStyle = .custom
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view?.backgroundColor = .clear
        addAccelerometerUpdatesHandler()
        view.addGestureRecognizer(UITapGestureRecognizer(
            target: self,
            action: #selector(scheduleItemDismiss)
        ))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scheduleItemDropIfNeeded()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        motionManager.stopDeviceMotionUpdates()
    }
    
    private func addAccelerometerUpdatesHandler() {
        guard motionManager.isDeviceMotionAvailable else { return }
        motionManager.deviceMotionUpdateInterval = 0.1
        motionManager.startDeviceMotionUpdates(to: .main) { [weak self] motion, error in
            guard let motion = motion else { return }
            self?.gravityBehavior.gravityDirection = CGVector(dx: motion.gravity.x, dy: -motion.gravity.y)
        }
    }
    
    private func scheduleItemDropIfNeeded() {
        guard let item = provider.nextItem() else { return }
        Timer.scheduledTimer(
            withTimeInterval: fallingInterval,
            repeats: false
        ) { [weak self] _ in
            self?.dropItem(item)
            self?.scheduleItemDropIfNeeded()
        }
    }
    
    private func dropItem(_ item: UIView) {
        let lowerBound = Int(item.bounds.size.width/2 + 1)
        let upperBound = Int(view.bounds.size.width) - lowerBound
        let xPosition = Int.random(in: lowerBound...upperBound)
        item.frame.origin = CGPoint(x: CGFloat(xPosition), y: -item.bounds.size.height/3)
        view.addSubview(item)
        item.addGestureRecognizer(UIPanGestureRecognizer(
            target: self,
            action: #selector(handlePanGestureRecognizer(recognizer:))
        ))
        collisionBehavior.addItem(item)
        gravityBehavior.addItem(item)
        itemBehavior.addItem(item)
    }
    
    @objc private func scheduleItemDismiss() {
        guard let subview = view.subviews.first else {
            dismiss(animated: true, completion: completion)
            return
        }
        Timer.scheduledTimer(
            withTimeInterval: 0.1,
            repeats: false
        ) { [weak self] _ in
            self?.gravityBehavior.removeItem(subview)
            self?.collisionBehavior.removeItem(subview)
            self?.itemBehavior.removeItem(subview)
            subview.removeFromSuperview()
            self?.scheduleItemDismiss()
        }
    }
    
    private func makeInteraction() {
        impactGenerator.impactOccurred()
    }
    
    @objc private func handlePanGestureRecognizer(recognizer: UIPanGestureRecognizer) {
        guard let view = recognizer.view else { return }
        switch recognizer.state {
        case .began:
            let attachmentBehavior = UIAttachmentBehavior(
                item: view,
                attachedToAnchor: recognizer.location(in: self.view)
            )
            self.attachmentBehavior = attachmentBehavior
            animator.addBehavior(attachmentBehavior)
        case .changed:
            attachmentBehavior?.anchorPoint = recognizer.location(in: self.view)
        case .ended, .failed, .cancelled:
            guard let behavior = attachmentBehavior else { return }
            animator.removeBehavior(behavior)
            attachmentBehavior = nil
        case .possible:
            break
        @unknown default:
            assertionFailure("Unexpected state")
        }
    }
}

extension FallingItemsViewController : UIViewControllerTransitioningDelegate {
    
    func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
        DimmingBackgroundPresentationController(
            presentedViewController: presented,
            presenting: presenting
        )
    }
}

extension FallingItemsViewController: UICollisionBehaviorDelegate {
    
    func collisionBehavior(
        _ behavior: UICollisionBehavior,
        endedContactFor item1: UIDynamicItem,
        with item2: UIDynamicItem
    ) {
        makeInteraction()
    }
    
    func collisionBehavior(
        _ behavior: UICollisionBehavior,
        endedContactFor item: UIDynamicItem,
        withBoundaryIdentifier identifier: NSCopying?
    ) {
        guard let view = item as? UIView else {
            assertionFailure("We expect UIView here")
            return
        }
        if firstCollisionSet.contains(view) {
            return
        }
        firstCollisionSet.insert(view)
        makeInteraction()
    }
}
