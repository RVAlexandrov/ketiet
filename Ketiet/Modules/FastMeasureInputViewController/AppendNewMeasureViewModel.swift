//
//  AppendNewMeasureViewModel.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

final class AppendNewMeasureViewModel {
    
    weak var view: DismissableViewProtocol?
    private lazy var inputItem = ValueInputCellItem(placeholder: type.measurementService.measurement())
    private let type: ProgressValueType
    private let dayNumber: Int
    private let description: String?
    private let completion: ((Float?, Int?) -> Void)?
    private lazy var progressService: ProgressServiceProtocol = {
        switch type {
        case .weight:
            return EveryDayInputProgressService.weightProgressService
        case .water:
            return EveryDayInputProgressService.waterProgressService
        case .chest:
            return EveryDayInputProgressService.chestProgressService
        case .waist:
            return EveryDayInputProgressService.waistProgressService
        case .hips:
            return EveryDayInputProgressService.hipsProgressService
        }
    }()

    init(
        type: ProgressValueType,
        dayNumber: Int,
        description: String? = nil,
        completion: ((Float?, Int?) -> Void)? = nil
    ) {
        self.type = type
        self.dayNumber = dayNumber
        self.description = description
        self.completion = completion
    }
}

extension AppendNewMeasureViewModel: FastMeasureInputViewModelProtocol {
    
    func makeItems() -> [LittTableViewCellItemProtocol] {
        [
            SingleLabelCellItem(text: type.inputTitle, textAlignment: .left),
            SpaceCellItem(spaceHeight: 16),
            inputItem,
            ButtonTableViewCellItem(title: "SAVE".localized(),
                                    color: .accentColor) { [weak self] in
                                        guard let self = self,
                                              let value = self.inputItem.value else { return }
                                        let allValues = self.progressService.getAllValues()
                                        let currentValue = allValues?[self.dayNumber] ?? 0
                                        self.progressService.setValue(
                                            currentValue + value,
                                            forDay: self.dayNumber
                                        )
                                        self.inputItem.showCompleteSign()
                                        self.view?.dismiss(animated: true) { [weak self] in
                                            guard let self = self else { return }
                                            self.completion?(value, self.dayNumber)
                                        }
                                    },
            makeDescriptionItem(description: description)
        ].compactMap { $0 }
    }
}
