//
//  DismissableViewProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

protocol DismissableViewProtocol: AnyObject {

    func dismiss(
        animated flag: Bool,
        completion: @escaping () -> Void
    )
}
