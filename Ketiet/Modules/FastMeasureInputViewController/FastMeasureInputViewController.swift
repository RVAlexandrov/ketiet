//
//  FastMeasureInputViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 19.07.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit
import DietCore

final class FastMeasureInputViewController: UIViewController {
    
    private let tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.alwaysBounceVertical = false
        tableView.animations = []
        tableView.insetsChangeHelper = ScrollViewContentInsetsHelper(scrollView: tableView)
        return tableView
    }()
    private let viewModel: FastMeasureInputViewModelProtocol

    init(viewModel: FastMeasureInputViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.sections = [LittTableViewSection(items: viewModel.makeItems())]
    }
    
    override func loadView() {
        view = UIView()
        view.addSubview(tableView)
        view.backgroundColor = .littOnSurfacePrimary
        tableView.pinToSuperview(insets: UIEdgeInsets(top: .largeLittScreenEdgeMargin, left: 0, bottom: 0, right: 0))
    }
}

extension FastMeasureInputViewController: DismissableViewProtocol {

    func dismiss(
        animated flag: Bool,
        completion: @escaping () -> Void
    ) {
        super.dismiss(
            animated: true,
            completion: completion
        )
    }
}
