//
//  FastMeasureInputViewModelProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

protocol FastMeasureInputViewModelProtocol {
    
    func makeItems() -> [LittTableViewCellItemProtocol]
}

extension FastMeasureInputViewModelProtocol {
    func makeDescriptionItem(description: String?) -> LittTableViewCellItemProtocol? {
        guard let description = description else { return nil }
        return SingleLabelCellItem(
            text: description,
            font: .body,
            textColor: .lightGray
        )
    }
}
