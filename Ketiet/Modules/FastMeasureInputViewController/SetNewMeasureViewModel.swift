//
//  SetNewMeasureViewModel.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 20.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Foundation

final class SetNewMeasureViewModel {
    
    weak var view: DismissableViewProtocol?
    private lazy var inputItem = createInputItem()
    private let type: ProgressValueType
    private let dayNumber: Int
    private let description: String?
    private let completion: ((Float?, Int?) -> Void)?
    private lazy var progressService: ProgressServiceProtocol = {
        switch type {
        case .weight:
            return EveryDayInputProgressService.weightProgressService
        case .water:
            return EveryDayInputProgressService.waterProgressService
        case .chest:
            return EveryDayInputProgressService.chestProgressService
        case .waist:
            return EveryDayInputProgressService.waistProgressService
        case .hips:
            return EveryDayInputProgressService.hipsProgressService
        }
    }()

    init(
        type: ProgressValueType,
        dayNumber: Int,
        description: String? = nil,
        completion: ((Float?, Int?) -> Void)? = nil
    ) {
        self.type = type
        self.dayNumber = dayNumber
        self.description = description
        self.completion = completion
    }
    
    private func createInputItem() -> LittTableViewCellItemProtocol {
        let inputItem = ValueInputCellItem(placeholder: type.measurementService.measurement())
        let allValues = progressService.getAllValues()
        if let strongAllValues = allValues, strongAllValues.count - 1 >= dayNumber {
            inputItem.value = strongAllValues[dayNumber]
        } else {
            inputItem.value = nil
        }
        return inputItem
    }
}

extension SetNewMeasureViewModel: FastMeasureInputViewModelProtocol {

    func makeItems() -> [LittTableViewCellItemProtocol] {
        [
            SingleLabelCellItem(text: type.inputTitle, textAlignment: .left),
            SpaceCellItem(spaceHeight: 16),
            inputItem,
            ButtonTableViewCellItem(title: "SAVE".localized(),
                                    color: .accentColor) { [weak self] in
                                        guard let self = self,
                                              let item = self.inputItem as? ValueInputCellItem,
                                              let value = item.value else { return }
                                        self.progressService.setValue(value, forDay: self.dayNumber)
                                        item.showCompleteSign()
                                        self.view?.dismiss(animated: true) { [weak self] in
                                            guard let self = self else { return }
                                            self.completion?(value, self.dayNumber)
                                        }
                                    },
            makeDescriptionItem(description: description)
        ].compactMap { $0 }
    }
}
