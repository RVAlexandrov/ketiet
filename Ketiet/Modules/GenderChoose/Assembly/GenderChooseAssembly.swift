//
//  GenderChooseAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class GenderChooseAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: GenderChooseModuleConfiguratorProtocol) {
        let presenter = GenderChoosePresenter(gender: moduleConfigurator.gender)
        let interactor = GenderChooseInteractor(profileService: moduleConfigurator.profileService)
        let view = ChooseItemViewController(items: Gender.allCases.map { DefaultChooseItem(title: $0.title,
                                                                                    description: nil,
                                                                                           isSelected: $0 == moduleConfigurator.gender) }, buttonTitle: "CONTINUE".localized()) { (index, _)  in
            presenter.didTapContinueButton(gender: Gender.allCases[index],
                                           currentGoal: moduleConfigurator.goal,
                                           currentPhysicalActivity: moduleConfigurator.physicalActivity)
		}
		view.navigationItem.title = "CHOOSE_YOUR_GENDER".localized()
        let router = GenderChooseRouter(
            viewController: view,
            profileService: moduleConfigurator.profileService,
            mealDatesStorage: moduleConfigurator.mealDatesStorage
        )

        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
