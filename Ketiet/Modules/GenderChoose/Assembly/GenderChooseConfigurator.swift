//
//  GenderChooseConfigurator.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

protocol GenderChooseModuleConfiguratorProtocol {
    var parentModule: GenderChooseModuleOutputProtocol? { get }
    var gender: Gender? { get}
    var goal: Goal? { get }
    var physicalActivity: PhysicalActivity? { get }
    var profileService: ProfileServiceProtocol { get }
    var mealDatesStorage: MealDatesStorageProtocol { get }
}

final class GenderChooseModuleConfigurator: GenderChooseModuleConfiguratorProtocol {
    let parentModule: GenderChooseModuleOutputProtocol?
    let gender: Gender?
    let goal: Goal?
    let physicalActivity: PhysicalActivity?
    let profileService: ProfileServiceProtocol
    let mealDatesStorage: MealDatesStorageProtocol
    
    init(
        parentModule: GenderChooseModuleOutputProtocol?,
        gender: Gender?,
        goal: Goal?,
        physicalActivity: PhysicalActivity?,
        profileService: ProfileServiceProtocol,
        mealDatesStorage: MealDatesStorageProtocol
    ) {
        self.parentModule = parentModule
        self.gender = gender
        self.goal = goal
        self.physicalActivity = physicalActivity
        self.profileService = profileService
        self.mealDatesStorage = mealDatesStorage
    }
}
