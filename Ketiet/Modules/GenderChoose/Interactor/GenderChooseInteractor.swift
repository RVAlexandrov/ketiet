//
//  GenderChooseInteractor.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

final class GenderChooseInteractor {
    
    private let profileService: ProfileServiceProtocol
    
    init(profileService: ProfileServiceProtocol) {
        self.profileService = profileService
    }
}

// MARK: - GenderChooseInteractorInputProtocol
extension GenderChooseInteractor: GenderChooseInteractorInputProtocol {
    func saveGender(_ gender: Gender) {
        profileService.setGender(gender) { _ in }
    }
}
