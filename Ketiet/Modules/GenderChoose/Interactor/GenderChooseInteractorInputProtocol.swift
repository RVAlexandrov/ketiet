//
//  GenderChooseInteractorInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol GenderChooseInteractorInputProtocol {
    func saveGender(_ gender: Gender)
}
