//
//  GenderChoosePresenter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

final class GenderChoosePresenter {

    var interactor: GenderChooseInteractorInputProtocol?
    var router: GenderChooseRouterInputProtocol?
    
    private let gender: Gender?

    init(gender: Gender?) {
        self.gender = gender
    }
}

// MARK: - GenderChooseViewOutputProtocol
extension GenderChoosePresenter: GenderChooseViewOutputProtocol {
    func didTapContinueButton(gender: Gender,
                              currentGoal: Goal?,
                              currentPhysicalActivity: PhysicalActivity?) {
        interactor?.saveGender(gender)
        router?.showCaloriesCalculatorChooseModule(currentGoal: currentGoal,
                                                   selectedPhysicalActivity: currentPhysicalActivity)
    }
}
