//
//  GenderChooseRouter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

final class GenderChooseRouter {
    private weak var viewController: UIViewController?
    private let profileService: ProfileServiceProtocol
    private let mealDatesStorage: MealDatesStorageProtocol
    private var caloriesCalculatorChooseAssembly: CaloriesCalculatorChooseAssembly?

    init(viewController: UIViewController,
         profileService: ProfileServiceProtocol,
         mealDatesStorage: MealDatesStorageProtocol) {
        self.viewController = viewController
        self.profileService = profileService
        self.mealDatesStorage = mealDatesStorage
    }
}

// MARK: - GenderChooseRouterInputProtocol
extension GenderChooseRouter: GenderChooseRouterInputProtocol {
    func showCaloriesCalculatorChooseModule(currentGoal: Goal?,
                                            selectedPhysicalActivity: PhysicalActivity?) {
        let assembly = CaloriesCalculatorChooseAssembly(
            profileService: profileService,
            mealDatesStorage: mealDatesStorage,
            currentGoal: currentGoal,
            selectedPhysicalActivity: selectedPhysicalActivity
        )
        self.caloriesCalculatorChooseAssembly = assembly
        
        viewController?.navigationController?.pushViewController(
            assembly.makeViewController(),
            animated: true
        )
    }
}
