//
//  GenderChooseRouterInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol GenderChooseRouterInputProtocol {
    func showCaloriesCalculatorChooseModule(currentGoal: Goal?, selectedPhysicalActivity: PhysicalActivity?)
}
