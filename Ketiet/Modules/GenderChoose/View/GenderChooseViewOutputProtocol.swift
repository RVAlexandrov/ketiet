//
//  GenderChooseViewOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 04/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol GenderChooseViewOutputProtocol {
    func didTapContinueButton(gender: Gender, currentGoal: Goal?, currentPhysicalActivity: PhysicalActivity?)
}
