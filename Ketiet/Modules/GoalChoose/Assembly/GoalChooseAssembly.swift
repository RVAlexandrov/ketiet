//
//  GoalChooseAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class GoalChooseAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: GoalChooseModuleConfiguratorProtocol) {
        let presenter = GoalChoosePresenter()
        let interactor = GoalChooseInteractor(profileService: moduleConfigurator.profileService)
        let view = ChooseItemViewController(items: Goal.allCases.map { DefaultChooseItem(title: $0.description,
                                                                                         description: $0.descriptionString,
                                                                                         isSelected: $0 == moduleConfigurator.selectedGoal) },
                                            buttonTitle: "CONTINUE".localized()) { (index, _) in
            presenter.didTapContinueButton(index,
                                           selectedPhysicalActivity: moduleConfigurator.selectedPhysicalActivity)
		}
		view.navigationItem.title = "CHOOSE_YOUR_GOAL".localized()

        presenter.interactor = interactor
        presenter.router = GoalChooseRouter(
            viewController: view,
            profileService: moduleConfigurator.profileService,
            mealDatesStorage: moduleConfigurator.mealDatesStorage
        )
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
