//
//  GoalChooseConfigurator.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

protocol GoalChooseModuleConfiguratorProtocol {
    var selectedGoal: Goal? { get }
    var selectedPhysicalActivity: PhysicalActivity? { get }
    var profileService: ProfileServiceProtocol { get }
    var mealDatesStorage: MealDatesStorageProtocol { get }
}

final class GoalChooseModuleConfigurator: GoalChooseModuleConfiguratorProtocol {
    let selectedGoal: Goal?
    let selectedPhysicalActivity: PhysicalActivity?
    let profileService: ProfileServiceProtocol
    let mealDatesStorage: MealDatesStorageProtocol

    init(selectedGoal: Goal?,
         selectedPhysicalActivity: PhysicalActivity?,
         profileService: ProfileServiceProtocol,
         mealDatesStorage: MealDatesStorageProtocol) {
        self.selectedGoal = selectedGoal
        self.selectedPhysicalActivity = selectedPhysicalActivity
        self.profileService = profileService
        self.mealDatesStorage = mealDatesStorage
    }
}
