//
//  GoalChooseInteractor.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

struct GoalChooseInteractor {
    
    let profileService: ProfileServiceProtocol
}

// MARK: - GoalChooseInteractorInputProtocol
extension GoalChooseInteractor: GoalChooseInteractorInputProtocol {
    func saveGoal(_ goal: Goal) {
		profileService.setInitialGoal(goal, completion: {_ in})
    }
}
