//
//  GoalChooseInteractorInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol GoalChooseInteractorInputProtocol {
    func saveGoal(_ goal: Goal)
}
