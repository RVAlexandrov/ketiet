//
//  GoalChoosePresenter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

final class GoalChoosePresenter {

    var interactor: GoalChooseInteractorInputProtocol?
    var router: GoalChooseRouterInputProtocol?
}

// MARK: - GoalChooseViewOutputProtocol
extension GoalChoosePresenter: GoalChooseViewOutputProtocol {
    
    func didTapContinueButton(_ index: Int,
                              selectedPhysicalActivity: PhysicalActivity?) {
		interactor?.saveGoal(Goal.allCases[index])
        router?.showPhysicalActivityModule(currentPhysicalActivity: selectedPhysicalActivity)
    }
}
