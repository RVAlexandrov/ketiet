//
//  GoalChooseRouter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

final class GoalChooseRouter {

    private weak var viewController: UIViewController?
    private let profileService: ProfileServiceProtocol
    private let mealDatesStorage: MealDatesStorageProtocol

    init(
        viewController: UIViewController,
        profileService: ProfileServiceProtocol,
        mealDatesStorage: MealDatesStorageProtocol
    ) {
        self.viewController = viewController
        self.profileService = profileService
        self.mealDatesStorage = mealDatesStorage
    }
}

// MARK: - GoalChooseRouterInputProtocol
extension GoalChooseRouter: GoalChooseRouterInputProtocol {
    func showPhysicalActivityModule(currentPhysicalActivity: PhysicalActivity?) {
		let assembly: PhysicalActivityChooseAssembly = AssemblyFactory()
            .generateAssembly(
                moduleConfigurator: PhysicalActivityChooseModuleConfigurator(
                    selectedPhysicaActivity: currentPhysicalActivity,
                    profileService: profileService,
                    mealDatesStorage: mealDatesStorage
                )
            )
        viewController?.navigationController?.pushViewController(assembly.initialViewController(),
																 animated: true)
    }
}
