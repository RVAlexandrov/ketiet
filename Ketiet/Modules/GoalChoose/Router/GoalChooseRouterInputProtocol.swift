//
//  GoalChooseRouterInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol GoalChooseRouterInputProtocol {
    func showPhysicalActivityModule(currentPhysicalActivity: PhysicalActivity?)
}
