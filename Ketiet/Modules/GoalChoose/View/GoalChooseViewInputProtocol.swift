//
//  GoalChooseViewInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol GoalChooseViewInputProtocol: AnyObject {
    func reloadRows(_ indexes: [Int])
}
