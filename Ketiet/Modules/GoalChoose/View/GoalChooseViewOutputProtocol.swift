//
//  GoalChooseViewOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 07/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol GoalChooseViewOutputProtocol {
    func didTapContinueButton(_ index: Int,
                              selectedPhysicalActivity: PhysicalActivity?)
}
