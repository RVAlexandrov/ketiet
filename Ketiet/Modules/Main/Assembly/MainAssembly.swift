//
//  MainAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

struct MainAssembly {

    private let view: UIViewController
    
    init() {
		let presenter = MainPresenter(dietService: DietService.shared,
									  profileService: ProfileService.instance)
		let interactor = MainInteractor(presenter: presenter,
                                        dietService: DietService.shared,
                                        profileService: ProfileService.instance)
        let view = MainViewController(presenter: presenter)
        let router = MainRouter(presenter: presenter, viewController: view)

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
