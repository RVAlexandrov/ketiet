//
//  MainInteractor.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Combine

final class MainInteractor {
	
    private weak var presenter: MainInteractorOutputProtocol?
	private let dietService: DietServiceProtocol
    private let profileService: ProfileServiceProtocol
	
	init(
        presenter: MainInteractorOutputProtocol,
        dietService: DietServiceProtocol,
        profileService: ProfileServiceProtocol
    ) {
        self.presenter = presenter
		self.dietService = dietService
        self.profileService = profileService
    }
}

// MARK: - MainInteractorInputProtocol
extension MainInteractor: MainInteractorInputProtocol {    
    func getDataForWidgets(_ widgets: [WidgetType], currentWidgets: [WidgetData]) {
        profileService.setWidgetConfiguration(widgetTypes: widgets, completion: { _ in })
        var widgetsData: [WidgetData] = []
        for widget in widgets {
            if let matchingData = currentWidgets.first(where: { $0.type == widget }) {
                widgetsData.append(matchingData)
            } else {
                widgetsData.append(getDataForWidget(widget))
            }
        }
        
        var orderedWidgets = [WidgetData]()
        for widgetData in widgetsData.enumerated() {
            var orderedWidget = widgetData.element
            orderedWidget.position = widgetData.offset
            orderedWidgets.append(orderedWidget)
        }
        
        presenter?.didGetCurentWidgets(orderedWidgets)
    }
    
    func getCurrentWidgets() {
		if !dietService.hasCurrentDiet() {
			presenter?.didGetCurentWidgets([])
			return
		}
        profileService.getWidgetConfiguration(completion: { [weak self] types in
            guard let self = self, let types = types else { return }
            let widgets = types.map { self.getDataForWidget($0)}
            var orderedWidgets = [WidgetData]()
            for widgetData in widgets.enumerated() {
                var orderedWidget = widgetData.element
                orderedWidget.position = widgetData.offset
                orderedWidgets.append(orderedWidget)
            }
            self.presenter?.didGetCurentWidgets(orderedWidgets)
        })
    }
}

// MARK: - Private methods
extension MainInteractor {
    private func getDataForWidget(_ widget: WidgetType) -> WidgetData {
        switch widget {
        case .water:
            return WaterWidgetData(
                profileService: profileService,
                firstFastInputValue: Locale.current.usesMetricSystem ? 200 : 6,
                progressService: EveryDayInputProgressService.waterProgressService,
                measurementService: MeasurementService(
                    type: .volume,
                    unitOptions: [.providedUnit]
                ),
                inputValueHandler: { [weak self] _ in
                    self?.appendMetric(
                        type: .water,
                        description: "INPUT_WATER_DESCRIPTION".localized()
                    )
                })
        case .weight:
            let progressService = EveryDayInputProgressService.weightProgressService
            
            if dietService.getCurrentDay()?.id == 0 {
                profileService.getWeight(completion: { value in
                    if let value = value,
                       progressService.getCurrentDayValue() == nil || progressService.getCurrentDayValue() == 0 {
                        progressService.setCurrentDayValue(Float(value))
                    }
                })
            }
            return WeightWidgetData(
                weightProgressService: progressService,
                dietService: dietService,
                measurementService: MeasurementService(type: .weight),
                inputValueHandler: { [weak self] _ in
                    self?.updateMetric(
                        type: .weight,
                        description: "INPUT_WATER_DESCRIPTION".localized()
                    )
            })
            
        case .bodyMetrics:
            return BodyMetricsWidgetData(
                dietService: dietService,
                breastProgressService: EveryDayInputProgressService.chestProgressService,
                waistProgressService: EveryDayInputProgressService.waistProgressService,
                hipsProgressService: EveryDayInputProgressService.hipsProgressService,
                measurementService: MeasurementService(type: .lenght),
                updateBodyMetricVolumeHandler: { [weak self] _, type in
                    switch type {
                    case .chest:
                        self?.updateMetric(
                            type: .chest,
                            description: nil
                        )
                    case .waist:
                        self?.updateMetric(
                            type: .waist,
                            description: nil
                        )
                    case .hips:
                        self?.updateMetric(
                            type: .hips,
                            description: nil
                        )
                    }
                })
        case .nearestMeal:
            return NearestMealWidgetData(dietService: dietService)
        case .nutritionMetrics:
            return NutritionMetricsWidgetData(dietService: dietService)
        case .feelings:
            return NearestMealWidgetData(dietService: dietService)
        case .training:
            return NearestMealWidgetData(dietService: dietService)
        }
    }
    
    private func updateMetric(
        type: ProgressValueType,
        description: String?
    ) {
        guard let dayIndex = dietService.getCurrentDayIndex() else {
            assertionFailure("Unable to get current day index")
            return
        }
        presenter?.setCurrentMetricValue(
            type: type,
            dayIndex: dayIndex,
            description: description
        )
    }
    
    private func appendMetric(
        type: ProgressValueType,
        description: String?
    ) {
        guard let dayIndex = dietService.getCurrentDayIndex() else {
            assertionFailure("Unable to get current day index")
            return
        }
        presenter?.appendNewMetricValue(
            type: type,
            dayIndex: dayIndex,
            description: description
        )
    }
}
