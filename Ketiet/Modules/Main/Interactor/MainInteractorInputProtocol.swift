//
//  MainInteractorInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol MainInteractorInputProtocol {
    func getCurrentWidgets()
    func getDataForWidgets(_ widgets: [WidgetType], currentWidgets: [WidgetData])
}
