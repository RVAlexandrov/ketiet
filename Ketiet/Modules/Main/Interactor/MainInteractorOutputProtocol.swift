//
//  MainInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol MainInteractorOutputProtocol: AnyObject {
    func didGetCurentWidgets(_ widgets: [WidgetData])
	func setCurrentMetricValue(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    )
    func appendNewMetricValue(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    )
}
