//
//  MainPresenter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class MainPresenter {

    weak var view: MainViewInputProtocol?
    var interactor: MainInteractorInputProtocol?
    var router: MainRouterInputProtocol?
    
    private(set) var widgets: [WidgetData] = []
	let header: HeaderWidgetData
	
	init(dietService: DietServiceProtocol,
         profileService: ProfileServiceProtocol) {
		header = HeaderWidgetData(profileService: profileService, dietService: dietService)
        dietService.notificationCenter.addObserver(forName: .didFinishCurrentDietNotification,
                                                   object: nil,
                                                   queue: .main) { [weak self] _ in
            self?.clearWidgets()
        }
        dietService.notificationCenter.addObserver(forName: .didRemoveCurrentDietNotification,
                                                   object: nil,
                                                   queue: .main) { [weak self] _ in
            self?.clearWidgets()
        }
        dietService.notificationCenter.addObserver(forName: .didSetCurrentDietNotification,
                                                   object: nil,
                                                   queue: .main) { [weak self] _ in
			DispatchQueue.main.async {
				self?.interactor?.getCurrentWidgets()
			}
		}
	}
	
	private func clearWidgets() {
		let oldCount = widgets.count
		if oldCount == 0 {
			return
		}
		widgets = []
		view?.removeAll(in: IndexSet(integer: oldCount - 1))
	}
}

// MARK: - MainViewOutputProtocol
extension MainPresenter: MainViewOutputProtocol {
    func didTapOnWidgetConfigurationButton() {
        router?.showWidgetConfigurationModule(currentWidgets: widgets.map { $0.type })
    }
    
    func viewDidLoad() {
        interactor?.getCurrentWidgets()
    }
}

// MARK: - MainInteractorOutputProtocol
extension MainPresenter: MainInteractorOutputProtocol {

    func setCurrentMetricValue(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    ) {
        router?.showValueInputController(
            type: type,
            dayIndex: dayIndex,
            description: description
        )
	}
    
    func appendNewMetricValue(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    ) {
        router?.showAppendValueInputController(
            type: type,
            dayIndex: dayIndex,
            description: description
        )
    }
	
    func didGetCurentWidgets(_ widgets: [WidgetData]) {
        self.widgets = widgets
        view?.reloadData()
    }
}

// MARK: - WidgetsConfigurationModuleOutputProtocol
extension MainPresenter: WidgetsConfigurationModuleOutputProtocol {
    func didTapCloseButton() {
        router?.closeWidgetConfigurationModule()
    }
    
    func didChangeWidgetsOrder(with widgets: [WidgetType]) {
        interactor?.getDataForWidgets(widgets, currentWidgets: self.widgets)
    }
	
	func didSelectWidget(view: UIView,
						 fromCardFrame: CGRect,
						 fromCardFrameWithoutTransform: CGRect,
						 detailsController: UIViewController) {
		router?.showWidgetDetailsController(from: view,
											fromCardFrame: fromCardFrame,
											fromCardFrameWithoutTransform: fromCardFrameWithoutTransform,
											detailsController: detailsController)
	}
}
