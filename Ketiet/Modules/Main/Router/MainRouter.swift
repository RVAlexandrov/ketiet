//
//  MainRouter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class MainRouter {
    private weak var presenter: WidgetsConfigurationModuleOutputProtocol?
    private weak var viewController: UIViewController?
	private var transition: CardTransition?

    init(
        presenter: WidgetsConfigurationModuleOutputProtocol,
        viewController: UIViewController
    ) {
        self.presenter = presenter
        self.viewController = viewController
    }
}

// MARK: - MainRouterInputProtocol
extension MainRouter: MainRouterInputProtocol {
    func showValueInputController(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    ) {
        let viewModel = SetNewMeasureViewModel(
            type: type,
            dayNumber: dayIndex,
            description: description
        )
        let controller = FastMeasureInputViewController(viewModel: viewModel)
        controller.setupStandardSheetPresentation()
        viewModel.view = controller
        viewController?.present(controller, animated: true)
	}
    
    func showAppendValueInputController(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    ) {
        let viewModel = AppendNewMeasureViewModel(
            type: type,
            dayNumber: dayIndex,
            description: description
        )
        let controller = FastMeasureInputViewController(viewModel: viewModel)
        controller.setupStandardSheetPresentation()
        viewModel.view = controller
        viewController?.present(controller, animated: true)
    }
	
    func showWidgetConfigurationModule(currentWidgets: [WidgetType]) {
        let assembly: WidgetsConfigurationAssembly = AssemblyFactory().generateAssembly(moduleConfigurator: WidgetsConfigurationModuleConfigurator(parentModule: presenter, currentWidgets: currentWidgets))
        
        let vc = assembly.initialViewController()
        
        if let sheet = vc.sheetPresentationController {
            sheet.detents = [ .medium(), .large()]
            sheet.prefersGrabberVisible = true
            sheet.preferredCornerRadius = 30
        }
        
        viewController?.present(vc,
                                animated: true,
                                completion: nil)
    }
    
    func closeWidgetConfigurationModule() {
        viewController?.dismiss(animated: true, completion: nil)
    }
	
	func showWidgetDetailsController(from view: UIView,
									 fromCardFrame: CGRect,
									 fromCardFrameWithoutTransform: CGRect,
									 detailsController: UIViewController) {
		let params = CardTransition.Params(fromCardFrame: fromCardFrame,
										   fromCardFrameWithoutTransform: fromCardFrameWithoutTransform,
										   fromView: view)
		transition = CardTransition(params: params)
		detailsController.transitioningDelegate = transition
		detailsController.modalPresentationCapturesStatusBarAppearance = true
		detailsController.modalPresentationStyle = .custom
		viewController?.present(detailsController, animated: true)
	}
}
