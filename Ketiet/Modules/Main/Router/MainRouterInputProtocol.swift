//
//  MainRouterInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

protocol MainRouterInputProtocol {
    func showWidgetConfigurationModule(currentWidgets: [WidgetType])
    func closeWidgetConfigurationModule()
    func showValueInputController(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    )
    func showAppendValueInputController(
        type: ProgressValueType,
        dayIndex: Int,
        description: String?
    )
	func showWidgetDetailsController(
        from view: UIView,
        fromCardFrame: CGRect,
        fromCardFrameWithoutTransform: CGRect,
        detailsController: UIViewController
    )
}
