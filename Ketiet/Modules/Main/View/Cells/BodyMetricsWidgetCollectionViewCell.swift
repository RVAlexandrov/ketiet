//
//  BodyMetricsWidgetCollectionViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 06.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class BodyMetricsWidgetCollectionViewCell: UICollectionViewCell {
    static let height: CGFloat = 180
    
    private weak var data: BodyMetricsWidgetData?
    
    let widthOfBodyMetricView: CGFloat = 250
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentInset.left = .littScreenEdgeMargin
        scrollView.contentInset.right = .littScreenEdgeMargin
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
	private lazy var breastMetric = BodyMetricView(metricName: "CHEST_VOLUME".localized(),
												   currentValue: nil,
												   diffValue: nil,
												   color: .systemPurple)
	private lazy var waistMetric = BodyMetricView(metricName: "WAIST_VOLUME".localized(),
												  currentValue: nil,
												  diffValue: nil,
                                                  color: .systemBlue)
	private lazy var hipsMetric = BodyMetricView(metricName: "HIPS_VOLUME".localized(),
												 currentValue: nil,
												 diffValue: nil,
												 color: .systemPink)
	var showDetailsHandler: ((BodyMetricType, UIView) -> Void)?
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - WidgetCellProtocol
extension BodyMetricsWidgetCollectionViewCell: WidgetCellProtocol {
    func configureCell(_ data: WidgetData) {
        self.data = data as? BodyMetricsWidgetData
        self.data?.cell = self
        updateView()
    }
    
    func updateView() {
		let suffixWithMeasurement = " " + (data?.measurementService.measurement() ?? "")
        let currentBreast = data?.currentBreastVolume != 0 && data?.currentBreastVolume != nil ? String(data?.currentBreastVolume ?? 0) + suffixWithMeasurement : "🙈"
        let diffBreast = data?.diffBreastVolume != nil ? String(data?.diffBreastVolume ?? 0) + suffixWithMeasurement : nil
        breastMetric.setCurrentMetric(currentBreast, diff: diffBreast)
        
        let currentWaist = data?.currentWaistVolume != 0 && data?.currentWaistVolume != nil ? String(data?.currentWaistVolume ?? 0) + suffixWithMeasurement : "🙉"
        let diffWaist = data?.diffWaistVolume != nil ? String(data?.diffWaistVolume ?? 0) + suffixWithMeasurement : nil
        waistMetric.setCurrentMetric(currentWaist, diff: diffWaist)
        
        let currentHips = data?.currentHipsVolume != 0 && data?.currentHipsVolume != nil ? String(data?.currentHipsVolume ?? 0) + suffixWithMeasurement : "🙊"
        let diffHips = data?.diffHipsVolume != nil ? String(data?.diffHipsVolume ?? 0) + suffixWithMeasurement : nil
        hipsMetric.setCurrentMetric(currentHips, diff: diffHips)
    }
}

// MARK: - Private methods
extension BodyMetricsWidgetCollectionViewCell {
    private func configureView() {

        let stackView = UIStackView(arrangedSubviews: [breastMetric, waistMetric, hipsMetric])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
                
        contentView.addSubview(scrollView)
        
        scrollView.addSubview(containerView)
        containerView.addSubview(stackView)
        
		breastMetric.inputButton.addTarget(self, action: #selector(updateChestValueDidTap), for: .touchUpInside)
		waistMetric.inputButton.addTarget(self, action: #selector(updateWaistValueDidTap), for: .touchUpInside)
		hipsMetric.inputButton.addTarget(self, action: #selector(updateHipsValueDidTap), for: .touchUpInside)
		breastMetric.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showDetailsForChest)))
		waistMetric.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showDetailsForWaist)))
		hipsMetric.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showDetailsForHips)))
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: contentView.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            scrollView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.heightAnchor.constraint(equalToConstant: BodyMetricsWidgetCollectionViewCell.height),
            
            stackView.topAnchor.constraint(equalTo: containerView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            
            breastMetric.widthAnchor.constraint(equalToConstant: widthOfBodyMetricView),
            waistMetric.widthAnchor.constraint(equalToConstant: widthOfBodyMetricView),
            hipsMetric.widthAnchor.constraint(equalToConstant: widthOfBodyMetricView),
        ])
    }
	
	@objc private func showDetailsForChest() {
		showDetailsHandler?(.chest, breastMetric)
	}
	
	@objc private func showDetailsForWaist() {
		showDetailsHandler?(.waist, waistMetric)
	}
	
	@objc private func showDetailsForHips() {
		showDetailsHandler?(.hips, hipsMetric)
	}
	
	@objc private func updateChestValueDidTap() {
		guard let data = data else { return }
		data.updateBodyMetricVolumeHandler(data, .chest)
	}
	
	@objc private func updateWaistValueDidTap() {
		guard let data = data else { return }
		data.updateBodyMetricVolumeHandler(data, .waist)
	}
	
	@objc private func updateHipsValueDidTap() {
		guard let data = data else { return }
		data.updateBodyMetricVolumeHandler(data, .hips)
	}
}
