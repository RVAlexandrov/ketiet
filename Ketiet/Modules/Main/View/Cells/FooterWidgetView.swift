//
//  FooterWidgetView.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 03.02.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import Lottie
import UIKit

protocol FooterWidgetViewDelegate: AnyObject {
    func didTapFooterWidgetView()
}

final class FooterWidgetView: UICollectionReusableView {
    
    weak var delegate: FooterWidgetViewDelegate?
    
    private lazy var animationView: AnimationView = {
        let view = AnimationView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.loopMode = .loop
        view.animation = Animation.named("walking_avocado")
        return view
    }()
    
    private lazy var button: UIButton = {
        
        var conf = UIButton.Configuration.filled()
        conf.baseBackgroundColor = .accentColor
        conf.title = "CHANGE_WIDGETS".localized()
        conf.baseForegroundColor = .white
        conf.cornerStyle = .capsule
        conf.buttonSize = .small
        
        let button = UIButton(configuration: conf,
                              primaryAction: UIAction() { [weak self] _ in
            self?.primaryButtonAction()
        })
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func playAnimationIfNeeded() {
        if !animationView.isAnimationPlaying {
            animationView.play()
        }
    }
}

// MARK: - Private methods
extension FooterWidgetView {
    private func setupView() {
        addSubviews([animationView,
                     button])
        
        NSLayoutConstraint.activate([
            animationView.topAnchor.constraint(equalTo: topAnchor, constant: -.largeLittMargin),
            animationView.centerXAnchor.constraint(equalTo: centerXAnchor),
            animationView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.33),
            animationView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.33),
            
            button.topAnchor.constraint(equalTo: animationView.bottomAnchor, constant: -.largeLittMargin),
            button.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        animationView.play()
    }
    
    private func primaryButtonAction() {
        delegate?.didTapFooterWidgetView()
    }
}
