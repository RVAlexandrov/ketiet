//
//  WidgetCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 12.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class WidgetCell: UICollectionViewCell {
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.font = .subTitle
        label.numberOfLines = 1
		label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()
    
    private lazy var iconView: SquareIconView = {
        let view = SquareIconView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .littSecondaryBackgroundColor
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
		prepareCell()
    }
    
    private func prepareCell() {
        layer.cornerRadius = .littMediumCornerRadius
        clipsToBounds = true
        contentView.backgroundColor = .littSecondaryBackgroundColor
        contentView.addSubviews([titleLabel, iconView, containerView])
        
        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .largeLittMargin),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            
            iconView.heightAnchor.constraint(equalToConstant: 24),
            iconView.widthAnchor.constraint(equalToConstant: 24),
            
            titleLabel.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: .mediumLittMargin),
            
            containerView.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: .largeLittMargin),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.mediumLittMargin),
        ])
    }
    
    func setTitle(_ title: String,
                  iconString: String,
                  backgroundColor: UIColor = .systemBlue) {
        titleLabel.text = title
        iconView.iconImageType = .system(iconString)
        iconView.backgroundColor = backgroundColor
    }
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        animate(isHighlighted: true)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        animate(isHighlighted: false)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        animate(isHighlighted: false)
    }

	private func animate(isHighlighted: Bool) {
		UIView.animate(withDuration: 0.5,
					   delay: 0,
					   usingSpringWithDamping: 1,
					   initialSpringVelocity: 0,
					   options: .allowUserInteraction,
					   animations: {
							self.transform = isHighlighted ? .init(scaleX: 0.96, y: 0.96) : .identity
					   })
	}
}
