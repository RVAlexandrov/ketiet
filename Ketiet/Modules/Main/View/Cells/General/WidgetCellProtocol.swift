//
//  WidgetCellProtocol.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 10.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol WidgetCellProtocol: AnyObject {
    func configureCell(_ data: WidgetData)
    func updateView()
}
