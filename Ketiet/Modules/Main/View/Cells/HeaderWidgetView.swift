//
//  HeaderWidgetCollectionViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 09.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class HeaderWidgetView: UICollectionReusableView {
	
	private struct Constants {
		static let imageViewSide: CGFloat = 50
	}
	
	private weak var data: HeaderWidgetData?
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .littSecondaryBackgroundColor
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = .littMediumCornerRadius
        return view
    }()
	
	private lazy var dayTitleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .body
		label.lineBreakMode = .byWordWrapping
		label.text = "DAY".localized() + ":"
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
		label.numberOfLines = 0
		return label
	}()
	
	private lazy var currentDayLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .body
		label.textColor = .accentColor
		label.lineBreakMode = .byWordWrapping
        label.textAlignment = .left
		label.numberOfLines = 0
		return label
	}()
	
	private lazy var profileImageView: UIImageView = {
		let view = UIImageView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.clipsToBounds = true
		view.layer.cornerRadius = Constants.imageViewSide / 2
		return view
	}()
	
	private lazy var nameLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .subTitle
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 1
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func configure(_ data: HeaderWidgetData) {
		self.data = data
		data.cell = self
		updateView()
	}
	
	func updateView() {
		nameLabel.text = data?.profile?.name ?? "NAME_IS_NOT_SET".localized()
		profileImageView.image = data?.profile?.image
		if let diet = data?.diet, let currentDayIndex = data?.currentDayIndex {
			currentDayLabel.text = "\(currentDayIndex + 1) / \(diet.days.count)"
		} else {
			currentDayLabel.text = "NO_CURRENT_DIET".localized()
		}
	}
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        animate(isHighlighted: true)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        animate(isHighlighted: false)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        animate(isHighlighted: false)
    }
}

// MARK: - Private methods
extension HeaderWidgetView {
    private func setupSubviews() {
        addSubview(containerView)
        containerView.addSubviews([
            nameLabel,
            currentDayLabel,
            dayTitleLabel,
            profileImageView
        ])
        
        NSLayoutConstraint.activate(
            makeContainerViewConstraints() +
            makeNameLabelConstraints() +
            makeProfileImageViewConstraints() +
            makeDayTitleLabelConstraints() +
            makeCurrentDayLabelConstraints()
        )
    }
    
    private func makeContainerViewConstraints() -> [NSLayoutConstraint] {
        [containerView.topAnchor.constraint(
            equalTo: topAnchor,
            constant: .largeLittMargin
        ),
        containerView.leadingAnchor.constraint(
            equalTo: leadingAnchor,
            constant: .largeLittMargin
        ),
        containerView.bottomAnchor.constraint(
            equalTo: bottomAnchor,
            constant: -.largeLittMargin
        ),
        containerView.trailingAnchor.constraint(
            equalTo: trailingAnchor,
            constant: -.largeLittMargin
        )]
    }
    
    private func makeNameLabelConstraints() -> [NSLayoutConstraint] {
        [nameLabel.topAnchor.constraint(
            equalTo: containerView.topAnchor,
            constant: .logicBlockLittMargin
        ),
        nameLabel.leadingAnchor.constraint(
            equalTo: containerView.leadingAnchor,
            constant: .logicBlockLittMargin
        ),
        nameLabel.trailingAnchor.constraint(
            equalTo: profileImageView.leadingAnchor,
            constant: -.mediumLittMargin
        ),
        nameLabel.bottomAnchor.constraint(
            equalTo: dayTitleLabel.topAnchor,
            constant: -.smallLittMargin
        )]
    }
    
    private func makeProfileImageViewConstraints() -> [NSLayoutConstraint] {
        [profileImageView.trailingAnchor.constraint(
            equalTo: containerView.trailingAnchor,
            constant: -.logicBlockLittMargin
        ),
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
        profileImageView.widthAnchor.constraint(equalToConstant: Constants.imageViewSide),
        profileImageView.heightAnchor.constraint(equalToConstant: Constants.imageViewSide)]
    }
    
    private func makeDayTitleLabelConstraints() -> [NSLayoutConstraint] {
        [dayTitleLabel.leadingAnchor.constraint(
            equalTo: containerView.leadingAnchor,
            constant: .logicBlockLittMargin
        ),
        dayTitleLabel.bottomAnchor.constraint(
            equalTo: containerView.bottomAnchor,
            constant: -.logicBlockLittMargin
        ),
        dayTitleLabel.trailingAnchor.constraint(
            equalTo: currentDayLabel.leadingAnchor,
            constant: -.mediumLittMargin
        )]
    }
    
    private func makeCurrentDayLabelConstraints() -> [NSLayoutConstraint] {
        [currentDayLabel.centerYAnchor.constraint(equalTo: dayTitleLabel.centerYAnchor),
         currentDayLabel.bottomAnchor.constraint(
             equalTo: containerView.bottomAnchor,
             constant: -.logicBlockLittMargin
         )]
    }
    
    private func animate(isHighlighted: Bool) {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .allowUserInteraction,
                       animations: {
                            self.transform = isHighlighted ? .init(scaleX: 0.96, y: 0.96) : .identity
                       })
    }
}
