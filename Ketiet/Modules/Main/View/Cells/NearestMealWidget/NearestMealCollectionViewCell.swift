//
//  NearestMealWidgetCollectionViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 17.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol NearestMealCollectionViewCellDelegate: AnyObject {
    func didTapDoneMeal(at index: Int, newValue: Bool)
    func didTapChangeDishButton(at index: Int)
}

final class NearestMealCollectionViewCell: UICollectionViewCell {
    private weak var data: NearestMeal?
    weak var delegate: NearestMealCollectionViewCellDelegate?
    
    private lazy var mealNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.numberOfLines = 1
        label.font = .subTitle
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var iconView: SquareIconView = {
        let view = SquareIconView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var dishNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .label
        label.numberOfLines = 2
        label.font = .body
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var checkMarkView: CheckMarkView = {
        let view = CheckMarkView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addAction(
            UIAction { [weak self] _ in
                guard let self = self, let index = self.data?.index else { return }
                self.delegate?.didTapDoneMeal(at: index, newValue: !self.checkMarkView.isSelected)
            },
            for: .touchUpInside
        )
        return view
    }()
    
    private lazy var portionWeightLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var allCaloriesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var changeDishButton: UIButton = {
        let button = LittBasicButton(
            title: "CHANGE_DISH".localized(),
            config: .capsule
        )
        button.addTarget(self, action: #selector(didTapChangeDishButton), for: .touchUpInside)
        return button
    }()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        animate(isHighlighted: true)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        animate(isHighlighted: false)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        animate(isHighlighted: false)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = .littMediumCornerRadius
        clipsToBounds = true
        contentView.backgroundColor = .littSecondaryBackgroundColor
        
        iconView.iconImageType = .system("takeoutbag.and.cup.and.straw.fill")
        iconView.backgroundColor = .systemGreen
                                        
        contentView.addSubviews([
            iconView,
            mealNameLabel,
            checkMarkView,
            portionWeightLabel,
            allCaloriesLabel,
            changeDishButton,
            portionWeightLabel,
            dishNameLabel
        ])
                
        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .largeLittMargin),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            iconView.widthAnchor.constraint(equalToConstant: 24),
            iconView.heightAnchor.constraint(equalToConstant: 24),
            
            mealNameLabel.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            mealNameLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: .mediumLittMargin),
            mealNameLabel.trailingAnchor.constraint(equalTo: checkMarkView.leadingAnchor, constant: -.mediumLittMargin),
            
            dishNameLabel.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: .largeLittMargin),
            dishNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            dishNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
            
            portionWeightLabel.topAnchor.constraint(equalTo: dishNameLabel.bottomAnchor, constant: .mediumLittMargin),
            portionWeightLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            portionWeightLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
            
            allCaloriesLabel.topAnchor.constraint(equalTo: portionWeightLabel.bottomAnchor, constant: .mediumLittMargin),
            allCaloriesLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            allCaloriesLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
            
            checkMarkView.centerYAnchor.constraint(equalTo: mealNameLabel.centerYAnchor),
            checkMarkView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
            checkMarkView.heightAnchor.constraint(equalToConstant: CheckMarkView.Constants.size.height),
            checkMarkView.widthAnchor.constraint(equalToConstant: CheckMarkView.Constants.size.height),
            
            changeDishButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.largeLittMargin),
            changeDishButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            changeDishButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public API
extension NearestMealCollectionViewCell {
    func configureCell(_ data: NearestMeal) {
        self.data = data
        initialConfigureCell()
    }
}

// MARK: - Private methods
extension NearestMealCollectionViewCell {
    private func initialConfigureCell() {
        
        mealNameLabel.text = data?.mealName
        
        checkMarkView.isSelected = data?.isEaten ?? false

        if data?.dishNames != nil {
            dishNameLabel.text = data?.dishNames.first ?? ""
        }

        if let kcalValue = data?.kcal {
            allCaloriesLabel.text = "TOTAL".localized() + ":" + " " + kcalValue.cleanValue + " " +  (data?.energyMeasurement ?? "")
            allCaloriesLabel.isHidden = false
        } else {
            allCaloriesLabel.isHidden = true
        }
        
        if let weight = data?.dishWeights[0] {
            portionWeightLabel.text = "WEIGHT_OF_PORTION".localized() + ":" + " " + weight.cleanValue + " " +  (data?.nutriensMeasurement ?? "")
            portionWeightLabel.isHidden = false
        } else {
            portionWeightLabel.isHidden = true
        }
    }
    
    @objc
    private func didTapChangeDishButton() {
        guard let index = data?.index else { return }
        delegate?.didTapChangeDishButton(at: index)
    }

    private func animate(isHighlighted: Bool) {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .allowUserInteraction,
                       animations: {
                            self.transform = isHighlighted ? .init(scaleX: 0.96, y: 0.96) : .identity
                       })
    }
}
