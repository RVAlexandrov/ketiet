//
//  NearestMealWidgetCollectionViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 23.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class NearestMealWidgetCollectionViewCell: UICollectionViewCell {
	
	private struct Constants {
		static let itemSize = CGSize(width: UIScreen.main.bounds.width * 0.8, height: 208)
		static let cellId = "cellId"
	}
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private weak var data: NearestMealWidgetData?
	
	var didSelectMealCellHandler: ((Int, UICollectionViewCell) -> Void)?
    var didTapChangeDishButtonHandler: ((Int) -> Void)?
    var didCompleteThirdMealHandler: (() -> Void)?
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
		layout.itemSize = Constants.itemSize
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.collectionViewLayout = layout
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.layer.masksToBounds = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .littBackgroundColor
        collectionView.translatesAutoresizingMaskIntoConstraints = false
		collectionView.register(NearestMealCollectionViewCell.self,
								forCellWithReuseIdentifier: Constants.cellId)
        collectionView.contentInset = UIEdgeInsets(top: 0,
                                                   left: .littScreenEdgeMargin,
                                                   bottom: 0,
                                                   right: .littScreenEdgeMargin)
        return collectionView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubviews([collectionView])
		collectionView.pinToSuperview()
    }
    
}

// MARK: - UICollectionViewDataSource implementation
extension NearestMealWidgetCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data?.nearestMeals?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellId, for: indexPath)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension NearestMealWidgetCollectionViewCell: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		guard let data = data?.nearestMeals?[indexPath.row],
			  let cell = cell as? NearestMealCollectionViewCell else { return }
		cell.configureCell(data)
		cell.delegate = self
	}
	
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
		let targetX = collectionView.contentOffset.x + velocity.x * 60.0
		var targetIndex: CGFloat
		if velocity.x > 0 {
			targetIndex = ceil(targetX / Constants.itemSize.width)
		} else if velocity.x == 0 {
			targetIndex = round(targetX / Constants.itemSize.width)
		} else {
			targetIndex = floor(targetX / Constants.itemSize.width)
		}
		targetIndex = min(CGFloat(collectionView.numberOfItems(inSection: 0)), max(0, targetIndex))
		targetContentOffset.pointee.x = (Constants.itemSize.width + layout.minimumLineSpacing) * targetIndex - collectionView.contentInset.left
    }
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let cell = collectionView.cellForItem(at: indexPath) else { return }
		didSelectMealCellHandler?(indexPath.item, cell)
	}
}

// MARK: - NearestMealCollectionViewCellDelegate
extension NearestMealWidgetCollectionViewCell: NearestMealCollectionViewCellDelegate {
    func didTapChangeDishButton(at index: Int) {
        didTapChangeDishButtonHandler?(index)
    }
    
    func didTapDoneMeal(at index: Int, newValue: Bool) {
        data?.setDoneForMeal(at: index,
                             newValue: newValue)
        if index == 2 {
            didCompleteThirdMealHandler?()
        }
    }
}

// MARK: - WidgetCellProtocol implementation
extension NearestMealWidgetCollectionViewCell: WidgetCellProtocol {
    func updateView() {
		collectionView.indexPathsForVisibleItems.forEach {
			guard let cell = collectionView.cellForItem(at: $0) as? NearestMealCollectionViewCell,
				  let data = data?.nearestMeals?[$0.row] else { return }
			cell.configureCell(data)
		}
    }
    
    func configureCell(_ data: WidgetData) {
        guard let data = data as? NearestMealWidgetData else {
            assertionFailure("Wrong widget data")
            return
        }
        self.data = data
        data.cell = self
		collectionView.reloadData()
    }
}
