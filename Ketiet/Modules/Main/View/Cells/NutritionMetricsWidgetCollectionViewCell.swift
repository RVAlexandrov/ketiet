//
//  NutritionMetricsWidgetCollectionViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 29.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class NutritionMetricsWidgetCollectionViewCell: WidgetCell {
    
    private weak var data: NutritionMetricsWidgetData?
    
    private lazy var currentValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .nutrientAccentColor
        label.font = .largeTitle
        label.numberOfLines = 0
        label.textAlignment = .center
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var targetValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .nutrientAccentColor
        label.font = .largeTitle
        label.numberOfLines = 0
        label.textAlignment = .center
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
	private lazy var fatsBarView = HorizontalPercentBarView(title: "FAT".localized(),
                                                            color: .littFatColor)
    
	private lazy var proteinsBarView = HorizontalPercentBarView(title: "PROTEIN".localized(),
                                                                color: .littProteinColor)
    
	private lazy var carbohydratesBarView = HorizontalPercentBarView(title: "CARBOHYDRATE".localized(),
                                                                    color: .littCarbohydratesColor)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
}

// MARK: - WidgetCellProtocol
extension NutritionMetricsWidgetCollectionViewCell: WidgetCellProtocol {
    func configureCell(_ data: WidgetData) {
        self.data = data as? NutritionMetricsWidgetData
        self.data?.cell = self
        setData(animated: false)
    }
    
    func updateView() {
        setData(animated: true)
    }
}

// MARK: - Private methods
extension NutritionMetricsWidgetCollectionViewCell {
    
    private func makeTitleGeneralLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
        label.text = "TOTAL_CCAL".localized()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }
    
    private func makeInHelperStringLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
        label.text = "FROM".localized()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }

    private func setData(animated: Bool) {
        proteinsBarView.setTargetValue(Int(data?.nutrients.targetProteins ?? 1))
        fatsBarView.setTargetValue(Int(data?.nutrients.targetFats ?? 1))
        carbohydratesBarView.setTargetValue(Int(data?.nutrients.targetCarbohydrates ?? 1))
        
        proteinsBarView.setCurrentPercentValue(
            data?.nutrients.proteinsPercent() ?? 0,
            rawValue: Int(data?.nutrients.currentProteins ?? 0),
            animate: animated
        )
        carbohydratesBarView.setCurrentPercentValue(
            data?.nutrients.carbohydratesPercent() ?? 0,
            rawValue: Int(data?.nutrients.currentCarbohydrates ?? 0),
            animate: animated
        )
        fatsBarView.setCurrentPercentValue(
            data?.nutrients.fatsPercent() ?? 0,
            rawValue: Int(data?.nutrients.currentFats ?? 0),
            animate: animated
        )
        currentValueLabel.text = String(Int(data?.nutrients.currentCalories ?? 0))
        targetValueLabel.text = String(Int(data?.nutrients.targetCalories ?? 0))
    }
    
    private func configureView() {        
        setTitle("PROTEINS_FATS_CARBOHYDRATES_SHORT".localized(),
                 iconString: "chart.bar.xaxis",
                 backgroundColor: .nutrientAccentColor)
        let titleGeneralLabel = makeTitleGeneralLabel()
        let inHelperStringLabel = makeInHelperStringLabel()
        
        containerView.addSubviews([
            titleGeneralLabel,
            currentValueLabel,
            inHelperStringLabel,
            targetValueLabel,
            proteinsBarView,
            fatsBarView,
            carbohydratesBarView
        ])

        NSLayoutConstraint.activate(
            makeTitleGeneralConstraints(label: titleGeneralLabel) +
            makeProteinsBarViewConstraints() +
            makeCarbohydratesBarViewConstraints() +
            makeFatsBarViewConstraints() +
            makeInHelperStringLabelConstraints(label: inHelperStringLabel) +
            makeCurrentValueLabelConstraints(inHelperStringLabel: inHelperStringLabel) +
            makeTargetValueLabelConstraints(inHelperStringLabel: inHelperStringLabel)
        )
    }
    
    private func makeCurrentValueLabelConstraints(inHelperStringLabel: UILabel) -> [NSLayoutConstraint] {
        [currentValueLabel.leadingAnchor.constraint(
            equalTo: proteinsBarView.trailingAnchor,
            constant: .mediumLittMargin
        ),
         currentValueLabel.trailingAnchor.constraint(
            equalTo: containerView.trailingAnchor,
            constant: -.mediumLittMargin
         ),
         currentValueLabel.bottomAnchor.constraint(
            equalTo: inHelperStringLabel.topAnchor,
            constant: -.smallLittMargin
         )]
    }
    
    private func makeTargetValueLabelConstraints(inHelperStringLabel: UILabel) -> [NSLayoutConstraint] {
        [targetValueLabel.leadingAnchor.constraint(
            equalTo: fatsBarView.trailingAnchor,
            constant: .mediumLittMargin
        ),
         targetValueLabel.trailingAnchor.constraint(
            equalTo: containerView.trailingAnchor,
            constant: -.mediumLittMargin
         ),
         targetValueLabel.topAnchor.constraint(
            equalTo: inHelperStringLabel.bottomAnchor,
            constant: .smallLittMargin
         )]
    }
    
    private func makeInHelperStringLabelConstraints(label: UILabel) -> [NSLayoutConstraint] {
        [label.centerYAnchor.constraint(equalTo: carbohydratesBarView.centerYAnchor),
         label.leadingAnchor.constraint(
            equalTo: carbohydratesBarView.trailingAnchor,
            constant: .mediumLittMargin
         ),
         label.trailingAnchor.constraint(
            equalTo: containerView.trailingAnchor,
            constant: -.mediumLittMargin
         )]
    }
    
    private func makeTitleGeneralConstraints(label: UILabel) -> [NSLayoutConstraint] {
        [label.topAnchor.constraint(equalTo: containerView.topAnchor),
         label.leadingAnchor.constraint(
            equalTo: proteinsBarView.trailingAnchor,
            constant: .smallLittMargin
         ),
         label.trailingAnchor.constraint(
            equalTo: containerView.trailingAnchor,
            constant: -.smallLittMargin
         )]
    }
    
    private func makeProteinsBarViewConstraints() -> [NSLayoutConstraint] {
        [proteinsBarView.topAnchor.constraint(equalTo: containerView.topAnchor),
         proteinsBarView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
         proteinsBarView.widthAnchor.constraint(
            equalTo: containerView.widthAnchor,
            multiplier: 0.65
         ),
         proteinsBarView.bottomAnchor.constraint(
            equalTo: carbohydratesBarView.topAnchor,
            constant: -.mediumLittMargin
         )]
    }
    
    private func makeCarbohydratesBarViewConstraints() -> [NSLayoutConstraint] {
        [carbohydratesBarView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
         carbohydratesBarView.widthAnchor.constraint(
            equalTo: containerView.widthAnchor,
            multiplier: 0.65
         ),
         carbohydratesBarView.bottomAnchor.constraint(
            equalTo: fatsBarView.topAnchor,
            constant: -.mediumLittMargin
         )]
    }
    
    private func makeFatsBarViewConstraints() -> [NSLayoutConstraint] {
        [fatsBarView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
         fatsBarView.widthAnchor.constraint(
            equalTo: containerView.widthAnchor,
            multiplier: 0.65
         ),
         fatsBarView.bottomAnchor.constraint(
            equalTo: containerView.bottomAnchor,
            constant: -.mediumLittMargin
         )]
    }
}
