//
//  WaterWidgetCollectionViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 27.05.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WaterWidgetCollectionViewCell: WidgetCell {
    
    private var data: WaterWidgetData?
    private var displayLink: CADisplayLink?
    
    private lazy var waterInputsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 5
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var labelsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var currentValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .waterAccentColor
        label.font = .largeTitle
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var firstMeasurementLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var firstLineHStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .lastBaseline
        stackView.spacing = 3
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var inHelperStringLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
		label.text = "FROM".localized()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var targetValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .waterAccentColor
        label.font = .largeTitle
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var secondMeasurementLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var secondLineHStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .lastBaseline
        stackView.spacing = 3
        stackView.axis = .horizontal
        return stackView
    }()
    
    var confettiHandler: (() -> Void)?
	
    private lazy var firstAddVolumeButton = LittBasicButton(config: .capsule)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitle("WATER".localized(),
                 iconString: "drop.fill",
                 backgroundColor: .waterAccentColor)
		
		let customButton = LittBasicButton(title: "OTHER".localized(), config: .capsule)
        
        firstAddVolumeButton.addTarget(self, action: #selector(didTapAddVolumeButton), for: .touchUpInside)
        firstAddVolumeButton.setBackgroundColor(.waterAccentColor)
        customButton.addTarget(self, action: #selector(didTapAnotherWaterButton), for: .touchUpInside)
        customButton.setBackgroundColor(.waterAccentColor)
        
        waterInputsStackView.addArrangedSubviews([firstAddVolumeButton, customButton])
        
        firstLineHStack.addArrangedSubviews([currentValueLabel, firstMeasurementLabel])
        secondLineHStack.addArrangedSubviews([targetValueLabel, secondMeasurementLabel])
        labelsStackView.addArrangedSubviews([firstLineHStack, inHelperStringLabel, secondLineHStack])
        
        containerView.addSubviews([labelsStackView, waterInputsStackView])
                
        NSLayoutConstraint.activate([
            labelsStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            labelsStackView.topAnchor.constraint(equalTo: containerView.topAnchor),
            
            waterInputsStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            waterInputsStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            waterInputsStackView.topAnchor.constraint(equalTo: labelsStackView.bottomAnchor, constant: .largeLittMargin),
            waterInputsStackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -.smallLittMargin)
        ])
        
    }
}

// MARK: - WidgetCellProtocol
extension WaterWidgetCollectionViewCell: WidgetCellProtocol {
    func configureCell(_ data: WidgetData) {
		guard let data = data as? WaterWidgetData else {
			assertionFailure("Wrong data type")
			return
		}
        self.data = data
        self.data?.cell = self
        guard let targetWater = self.data?.targetWater, let currentWater = self.data?.currentWater else { return }
        targetValueLabel.text = Float(targetWater).measurementRound().cleanValue
        currentValueLabel.text = Float(currentWater).measurementRound().cleanValue
        firstMeasurementLabel.text = data.measurementService.measurement()
        secondMeasurementLabel.text = data.measurementService.measurement()
        firstAddVolumeButton.setTitle("+ " + String(data.firstFastInputValue))
    }
    
    func updateView() {
        guard let targetWater = data?.targetWater else { return }
        targetValueLabel.text = Float(targetWater).measurementRound().cleanValue
        displayLink = CADisplayLink(target: self, selector: #selector(handleUpdate))
        displayLink?.add(to: .main, forMode: .common)
    }
}

// MARK: - Private methods
extension WaterWidgetCollectionViewCell {
    @objc
    private func didTapAddVolumeButton() {
		guard let currentWater = data?.currentWater, let volume = data?.firstFastInputValue else {
            assertionFailure("Incorrect currentWater")
            return
        }
        data?.didSetCurrentWater(currentWater + Double(volume))
    }
    
    @objc
    private func didTapAnotherWaterButton() {
        guard let data = data else {
            assertionFailure("Incorrect water data")
            return
        }
        data.inputValueHandler(data)
    }
    
    @objc
    private func handleUpdate() {
        guard var currentWater = Double(currentValueLabel.text ?? "0"), let targetWater = data?.currentWater else { return }
        let waterBefore = currentWater
        currentWater += 10
        
        if currentWater > targetWater {
            currentWater = targetWater
        }
        
        currentValueLabel.text = Float(currentWater).measurementRound().cleanValue
                
        if let globalTarget = data?.targetWater, currentWater >= globalTarget && waterBefore < globalTarget {
            letsParty()
            UINotificationFeedbackGenerator().notificationOccurred(.success)
        }
        
        if currentWater == targetWater {
            displayLink?.invalidate()
        }
    }
    
    private func letsParty() {
        ConfettiGenerator(rect: contentView.bounds).letsParty()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.confettiHandler?()
        })
    }
    
}
