//
//  WeightWidgetCollectionViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 09.06.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WeightWidgetCollectionViewCell: WidgetCell {
    
    private weak var data: WeightWidgetData?
    
    private lazy var todayWeightInputStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .leading
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var todayWeightInputTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
		label.text = "TODAY".localized() + ":"
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var todayWeightHStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 3
        stackView.alignment = .lastBaseline
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var todayWeightValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.textColor = .weightAccentColor
        label.font = .largeTitle
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var todayMeasurementLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var yesterdayWeightInputStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .leading
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        return stackView
    }()
    
    private lazy var yesterdayWeightInputTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
        label.text = "YESTERDAY".localized() + ":"
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var yesterdayWeightHStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 3
        stackView.alignment = .lastBaseline
        stackView.axis = .horizontal
        return stackView
    }()
    
    private lazy var yesterdayWeightValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .right
        label.textColor = .weightAccentColor
        label.font = .largeTitle
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    private lazy var yesterdayMeasurementLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .secondaryLabel
        label.font = .body
        label.backgroundColor = .littSecondaryBackgroundColor
        return label
    }()
    
    let weightInputButton = LittBasicButton(
        title: "INPUT_WEIGHT".localized(),
        config: .capsule
    )

    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitle("WEIGHT".localized(),
                 iconString: "lines.measurement.horizontal",
                 backgroundColor: .weightAccentColor)
        	
        todayWeightHStack.addArrangedSubviews([todayWeightValueLabel, todayMeasurementLabel])
        todayWeightInputStackView.addArrangedSubviews([todayWeightInputTitleLabel, todayWeightHStack])
        todayWeightInputStackView.setCustomSpacing(.smallLittMargin, after: todayWeightInputTitleLabel)
        
        yesterdayWeightHStack.addArrangedSubviews([yesterdayWeightValueLabel, yesterdayMeasurementLabel])
        yesterdayWeightInputStackView.addArrangedSubviews([yesterdayWeightInputTitleLabel, yesterdayWeightHStack])
        yesterdayWeightInputStackView.setCustomSpacing(.smallLittMargin, after: yesterdayWeightInputTitleLabel)

        weightInputButton.addTarget(self, action: #selector(didTapEnterWeightButton), for: .touchUpInside)
        
        weightInputButton.setBackgroundColor(.weightAccentColor)

        containerView.addSubviews([todayWeightInputStackView, yesterdayWeightInputStackView, weightInputButton])

        NSLayoutConstraint.activate([
            todayWeightInputStackView.topAnchor.constraint(equalTo: containerView.topAnchor),
            todayWeightInputStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            
            yesterdayWeightInputStackView.topAnchor.constraint(equalTo: containerView.topAnchor),
            yesterdayWeightInputStackView.leadingAnchor.constraint(equalTo: todayWeightInputStackView.trailingAnchor, constant: 2 * .littScreenEdgeMargin),
            
            weightInputButton.topAnchor.constraint(equalTo: todayWeightInputStackView.bottomAnchor, constant: .littMediumWellMargin),
            weightInputButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            weightInputButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
        ])
    }
}

// MARK: - WidgetCellProtocol
extension WeightWidgetCollectionViewCell: WidgetCellProtocol {
    func configureCell(_ data: WidgetData) {
		guard let data = data as? WeightWidgetData else {
			assertionFailure("Wrong widget data")
			return
		}
        self.data = data
        data.cell = self
		todayMeasurementLabel.text = data.measurementService.measurement()
        yesterdayMeasurementLabel.text = data.measurementService.measurement()
        guard let currentWeight = data.currentWeight else { return }
        let isTodayValueCorrect = currentWeight != 0
        todayWeightValueLabel.text = isTodayValueCorrect ? String(currentWeight) : "-"
        todayMeasurementLabel.isHidden = !isTodayValueCorrect
        
        guard let yesterdayWeight = data.lastInputWeight, let lastInputDate = data.lastInputDate else {
            yesterdayWeightInputStackView.isHidden = true
            return
        }
        yesterdayWeightInputStackView.isHidden = false
        yesterdayWeightInputTitleLabel.text = lastInputDate
        yesterdayWeightValueLabel.text = String(yesterdayWeight)
    }
    
    func updateView() {
		if let data = data {
			configureCell(data)
		}
    }
}

// MARK: - Private methods
extension WeightWidgetCollectionViewCell {
    @objc
    private func didTapEnterWeightButton() {
		guard let data = data else {
			assertionFailure("Where is data?!")
			return
		}
		data.inputValueHandler(data)
    }
    
    private func configureDiff(lastWeight: Float, currentWeight: Float) -> String {
        let modalDiff = currentWeight - lastWeight
        return modalDiff < 0 ? String(modalDiff) : "+\(modalDiff)"
    }
}
