//
//  LittWidgetLayout.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 31/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

struct LittWidgetLayoutConstants {
    struct Cell {
        static let standardHeight: CGFloat = 159.5
        static let defaultMargin: CGFloat = 16.0
        static let topMargin: CGFloat = 20.0
        static let smallHeight: CGFloat = 44.5
    }
}

// MARK: Properties and Variables
class LittWidgetLayout: UICollectionViewFlowLayout {
    
    // The amount the user needs to scroll before the featured cell changes
    let dragOffset: CGFloat = LittWidgetLayoutConstants.Cell.standardHeight + LittWidgetLayoutConstants.Cell.topMargin
    
    var cache: [UICollectionViewLayoutAttributes] = []
    
    var featuredItemIndex: Int {
        // Use max to make sure the featureItemIndex is never < 0
        return max(0, Int(collectionView!.contentOffset.y / dragOffset))
    }
    
    var width: CGFloat {
        return collectionView!.bounds.width - 2 * LittWidgetLayoutConstants.Cell.defaultMargin
    }
    
    var height: CGFloat {
        return collectionView!.bounds.height
    }
    
    var numberOfItems: Int {
        return collectionView!.numberOfItems(inSection: 0)
    }
}

// MARK: UICollectionViewLayout
extension LittWidgetLayout {
    override var collectionViewContentSize : CGSize {
        let contentHeight = CGFloat(numberOfItems) * (LittWidgetLayoutConstants.Cell.standardHeight + LittWidgetLayoutConstants.Cell.topMargin) + LittWidgetLayoutConstants.Cell.topMargin
        return CGSize(width: width,
                      height: contentHeight)
    }
    
    override func prepare() {
        cache.removeAll(keepingCapacity: false)
        guard let collectionView = collectionView else { return }
        
        let standardHeight = LittWidgetLayoutConstants.Cell.standardHeight
        let xdefaultMargin = LittWidgetLayoutConstants.Cell.defaultMargin
        let yDefalutMargin = LittWidgetLayoutConstants.Cell.topMargin
        let smallHeight = LittWidgetLayoutConstants.Cell.smallHeight
        
        var frame = CGRect.zero
        var y: CGFloat = yDefalutMargin
        
        for item in 0..<numberOfItems {
            let indexPath = IndexPath(item: item, section: 0)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            var height = standardHeight
            var transform: CGAffineTransform = .identity
            var alpha: CGFloat = 1
            if indexPath.item == featuredItemIndex {
                if y + standardHeight - (collectionView.contentOffset.y + yDefalutMargin) <= standardHeight {
                    attributes.zIndex = -item
                    var currentOffset = (y + standardHeight) - (collectionView.contentOffset.y + yDefalutMargin)
                    if currentOffset <= smallHeight {
                        alpha = (1 / smallHeight) * currentOffset
                        transform = CGAffineTransform(scaleX: 1 - (smallHeight - currentOffset) * 0.005, y: 1 - (smallHeight - currentOffset) * 0.005)
                        currentOffset = smallHeight
                    }
                    height = currentOffset
                    y = collectionView.contentOffset.y + yDefalutMargin
                }
            } else if indexPath.item == (featuredItemIndex + 1) && indexPath.item != numberOfItems {
                let helpDiff = (CGFloat(featuredItemIndex + 1) * (standardHeight + yDefalutMargin)) - (collectionView.contentOffset.y) - yDefalutMargin
                if helpDiff <= smallHeight {
                    y = (CGFloat(featuredItemIndex + 1) * (standardHeight + yDefalutMargin)) + yDefalutMargin
                }
            }
            frame = CGRect(x: xdefaultMargin,
                           y: y,
                           width: width,
                           height: height)
            attributes.frame = frame
            attributes.alpha = alpha
            attributes.transform = transform
            cache.append(attributes)
            y = frame.maxY + yDefalutMargin
        }
    }
    
    // Return all attributes in the cache whose frame intersects with the rect passed to the method
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes: [UICollectionViewLayoutAttributes] = []
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
    
    // Return true so that the layout is continuously invalidated as the user scrolls
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
