//
//  MainViewController.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import Combine

final class MainViewController: UIViewController {
	
	private struct Constants {
		static let headerSize = CGSize(width: UIScreen.main.bounds.width, height: 120)
        static let footerSize = CGSize(
            width: UIScreen.main.bounds.width,
            height: UIScreen.main.bounds.width * 0.33 + 28
        )
        static let basicHeaderViewHeight: CGFloat = UIScreen.main.bounds.height * 150 / 926 > 64 ? UIScreen.main.bounds.height * 150 / 926 : 64
        static let nameLogoWidthToHeight: CGFloat = 366 / 140
	}
    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    private let presenter: MainViewOutputProtocol
    private let waterWidgetCollectionViewCellId = "waterWidgetCollectionViewCellId"
    private let weightWidgetCollectionViewCellId = "weightWidgetCollectionViewCellId"
    private let nearestWidgetCollectionViewCellId = "nearestWidgetCollectionViewCellId"
    private let nutritionMetricsCollectionViewCellId = "nutritionMetricsCollectionViewCellId"
    private let bodyMetricsCollecionViewCellId = "bodyMetricsCollecionViewCellId"
	private let headerWidgetView = "headerWidgetCell"
    private let footerWidgetViewId = "footerWidgetView"
    
    private var isConfettiDisplayed: Bool = false
    
    private var animationFooterView: FooterWidgetView?
    
    private var cancellables = Set<AnyCancellable>()
    
    private lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 12
        flowLayout.headerReferenceSize = Constants.headerSize
        flowLayout.footerReferenceSize = Constants.footerSize
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.alwaysBounceVertical = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.setContentInsetForControllersInTabbar()
        collectionView.contentInset.bottom = collectionView.contentInset.bottom + 34
        collectionView.contentInset.top = Constants.basicHeaderViewHeight
        return collectionView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "MainLogo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var visualEffectView: UIVisualEffectView = {
        let visuallEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        visuallEffectView.translatesAutoresizingMaskIntoConstraints = false
        return visuallEffectView
    }()
    
    private lazy var navBarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var changeWidgetsButton: UIButton = {
        var configuration = UIButton.Configuration.gray()
        configuration.image = UIImage(systemName: "rectangle.3.offgrid.fill",
                                      withConfiguration: UIImage.SymbolConfiguration(scale: .medium))
        configuration.baseForegroundColor = .accentColor
        configuration.cornerStyle = .capsule
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 7,
                                                              leading: 7,
                                                              bottom: 7,
                                                              trailing: 7)
        let button = UIButton(configuration: configuration,
                             primaryAction: UIAction() { [weak self] _ in
            self?.presenter.didTapOnWidgetConfigurationButton()
        })
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var imageViewHeightConstraint = NSLayoutConstraint(item: imageView,
                                                                    attribute: .height,
                                                                    relatedBy: .equal,
                                                                    toItem: nil,
                                                                    attribute: .notAnAttribute,
                                                                    multiplier: 1,
                                                                    constant: Constants.basicHeaderViewHeight)
    
    private lazy var navBarViewHeightConstraint = NSLayoutConstraint(item: navBarView,
                                                                     attribute: .height,
                                                                     relatedBy: .equal,
                                                                     toItem: nil,
                                                                     attribute: .notAnAttribute,
                                                                     multiplier: 1,
                                                                     constant: 1)

    init(presenter: MainViewOutputProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        configureView()
        view.backgroundColor = .littBackgroundColor
        
        NotificationCenter
            .default
            .publisher(for: UIApplication.didBecomeActiveNotification)
            .dropFirst()
            .sink { [weak self] notification in
                self?.presenter.viewDidLoad()
            }
            .store(in: &cancellables)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animationFooterView?.playAnimationIfNeeded()
    }
}

// MARK: - MainViewInputProtocol
extension MainViewController: MainViewInputProtocol {
    func reloadData() {
        collectionView.reloadData()
    }
	
	func removeAll(in indexSet: IndexSet) {
		collectionView.performBatchUpdates {
			self.collectionView.deleteItems(at: indexSet.map { IndexPath(item: $0, section: 0) })
		}
	}
}

// MARK: - UICollectionViewDataSource
extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.widgets.count
    }
        
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch presenter.widgets[indexPath.row].type {
        case .water:
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: waterWidgetCollectionViewCellId,
                for: indexPath
            ) as! WaterWidgetCollectionViewCell
            cell.configureCell(presenter.widgets[indexPath.row])
            cell.confettiHandler = { [weak self] in
                self?.showFiveStarsPopUp()
            }
            return cell
        case .nearestMeal:
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: nearestWidgetCollectionViewCellId,
                for: indexPath
            ) as! NearestMealWidgetCollectionViewCell
            let data = presenter.widgets[indexPath.row]
            cell.configureCell(data)
			if cell.didSelectMealCellHandler != nil { return cell }
			cell.didSelectMealCellHandler = { [weak self] index, mealCell in
				let assembly: MealDetailAssembly = AssemblyFactory().generateAssembly(
                    moduleConfigurator: MealDetailModuleConfigurator(
                        mealIndex: index,
                        meals: DietService.shared.getCurrentDay()?.meals ?? [],
                    showCloseButton: true
                    )
                )
				let mealCellRect = mealCell.superview!.convert(mealCell.frame, to: nil)
				self?.presenter.didSelectWidget(
                    view: mealCell,
                    fromCardFrame: mealCellRect,
                    fromCardFrameWithoutTransform: mealCellRect,
                    detailsController: assembly.initialViewController()
                )
			}
            
            cell.didCompleteThirdMealHandler = { [weak self] in
                self?.showFiveStarsPopUp()
            }
            
            cell.didTapChangeDishButtonHandler = { [weak self] index in
                guard let data = data as? NearestMealWidgetData,
                      let dishId = data.nearestMeals?[index].dishIds.first else { return }
                let idGenerator = DishIdGenerator()
                let dish = idGenerator.getDish(with: dishId)
                dish.weightInGrams = data.nearestMeals?[index].dishWeights.first ?? 100.0
                let vc = ChangeDishViewController(dietService: DietService.shared,
                                                  dishProvider: idGenerator.dishProvider(for: dishId),
                                                  currentDish: dish,
                                                  dayIndex: DietService.shared.getCurrentDay()?.id ?? 0,
                                                  mealIndex: index)
                self?.present(vc, animated: true)
            }
            return cell
        case .weight:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: weightWidgetCollectionViewCellId, for: indexPath) as! WeightWidgetCollectionViewCell
            cell.configureCell(presenter.widgets[indexPath.row])
            return cell
        case .nutritionMetrics:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nutritionMetricsCollectionViewCellId, for: indexPath) as! NutritionMetricsWidgetCollectionViewCell
            cell.configureCell(presenter.widgets[indexPath.row])
            return cell
        case .bodyMetrics:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: bodyMetricsCollecionViewCellId, for: indexPath) as! BodyMetricsWidgetCollectionViewCell
            cell.configureCell(presenter.widgets[indexPath.row])
			if cell.showDetailsHandler != nil { return cell }
            guard let diet = DietService.shared.getCurrentDiet() else { return cell }
			cell.showDetailsHandler = { [weak self] type, view in
                
                let assembly: BodyMetricDetailsAssembly = AssemblyFactory()
                    .generateAssembly(
                        moduleConfigurator: BodyMetricDetailsModuleConfigurator(bodyMetricType: type,
                                                                                showCloseButton: true,
                                                                                diet: diet))
                
				let viewRect = view.convert(view.frame, to: nil)
				self?.presenter.didSelectWidget(view: view,
												fromCardFrame: viewRect,
												fromCardFrameWithoutTransform: viewRect,
												detailsController: assembly.initialViewController())
			}
            return cell
        case .feelings:
            return UICollectionViewCell()
        case .training:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch presenter.widgets[indexPath.row].type {
        case .nearestMeal:
            return CGSize(width: UIScreen.main.bounds.width,
                          height: 208)
        case .weight:
            return CGSize(width: UIScreen.main.bounds.width - 2 * .littScreenEdgeMargin,
                          height: 170)
        case .water:
            return CGSize(width: UIScreen.main.bounds.width - 2 * .littScreenEdgeMargin,
                          height: 196.5)
        case .nutritionMetrics:
            return CGSize(width: UIScreen.main.bounds.width - 2 * .littScreenEdgeMargin,
                          height: 200)
        case .bodyMetrics:
            return CGSize(width: UIScreen.main.bounds.width,
                          height: BodyMetricsWidgetCollectionViewCell.height)
        default:
            return CGSize(width: UIScreen.main.bounds.width - 2 * .littScreenEdgeMargin,
                          height: 200)
        }
    }
	
	func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
		guard let cell = collectionView.cellForItem(at: indexPath) else { return }
		switch presenter.widgets[indexPath.row].type {
		case .water:
            guard let diet = DietService.shared.getCurrentDiet() else { return }
            let assembly: WaterDetailsAssembly = AssemblyFactory()
                .generateAssembly(moduleConfigurator: WaterDetailsModuleConfigurator(showCloseButton: true,
                                                                                     diet: diet))
            presenter.didSelectWidget(view: cell,
                                      fromCardFrame: cell.fromCardFrame(),
                                      fromCardFrameWithoutTransform: cell.fromCardFrameWithoutTransform(),
                                      detailsController: assembly.initialViewController())
		case .nearestMeal:
			break
        case .weight:
            guard let diet = DietService.shared.getCurrentDiet() else { return }
			presenter.didSelectWidget(view: cell,
									  fromCardFrame: cell.fromCardFrame(),
									  fromCardFrameWithoutTransform: cell.fromCardFrameWithoutTransform(),
                                      detailsController: WeightDetailsAssembly(diet: diet)
                                        .initialViewController())
		default:
			break
		}
	}
	
	func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                       withReuseIdentifier: headerWidgetView,
                                                                       for: indexPath)
            if let headerView = view as? HeaderWidgetView {
                headerView.configure(presenter.header)
            }
            return view
        } else {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                       withReuseIdentifier: footerWidgetViewId,
                                                                       for: indexPath)
            if let footerView = view as? FooterWidgetView {
                footerView.delegate = self
                animationFooterView = footerView
                footerView.playAnimationIfNeeded()
            }
            return view
        }
	}
        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var navBarHeight = -collectionView.contentOffset.y
        var imageViewHeight = -collectionView.contentOffset.y - view.safeAreaInsets.top
        
        if -collectionView.contentOffset.y <= view.safeAreaInsets.top + 44 {
            navBarHeight = view.safeAreaInsets.top + 44
            imageViewHeight = 44
            UIView.animate(withDuration: 0.25) {
                self.changeWidgetsButton.alpha = 1
            }
            self.visualEffectView.alpha = 1

        } else {
            UIView.animate(withDuration: 0.25) {
                self.changeWidgetsButton.alpha = 0
            }
            self.visualEffectView.alpha = 0
        }
        
        imageViewHeightConstraint.constant = imageViewHeight
        navBarViewHeightConstraint.constant = navBarHeight
        if collectionView.contentOffset.y <= -350 {
            guard !isConfettiDisplayed else { return }
            isConfettiDisplayed = true
            let confettiGenerator = ConfettiGenerator(rect: view.frame,
                                                      confettiEntity: .avocado)
            confettiGenerator.letsParty()
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.isConfettiDisplayed = false
            }
        }
    }
}

// MARK: - FooterWidgetViewDelegate
extension MainViewController: FooterWidgetViewDelegate {
    func didTapFooterWidgetView() {
        presenter.didTapOnWidgetConfigurationButton()
    }
}

// MARK: - Private methods
extension MainViewController {
    private func configureView() {
        collectionView.contentInsetAdjustmentBehavior = .never

        navBarView.addSubview(visualEffectView)
        
        view.addSubviews([collectionView, navBarView, imageView, changeWidgetsButton])
        
        configureCollectionView()
        
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: navBarView.centerXAnchor),
            imageView.bottomAnchor.constraint(equalTo: navBarView.bottomAnchor, constant: -5),
            imageViewHeightConstraint,
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: Constants.nameLogoWidthToHeight),
            
            navBarView.topAnchor.constraint(equalTo: view.topAnchor),
            navBarView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            navBarView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            navBarViewHeightConstraint,
            
            visualEffectView.topAnchor.constraint(equalTo: navBarView.topAnchor),
            visualEffectView.leadingAnchor.constraint(equalTo: navBarView.leadingAnchor),
            visualEffectView.bottomAnchor.constraint(equalTo: navBarView.bottomAnchor),
            visualEffectView.trailingAnchor.constraint(equalTo: navBarView.trailingAnchor),
            
            changeWidgetsButton.bottomAnchor.constraint(equalTo: navBarView.bottomAnchor, constant: -.mediumLittMargin),
            changeWidgetsButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.mediumLittMargin),
            
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func configureCollectionView() {
        collectionView.register(WaterWidgetCollectionViewCell.self,
								forCellWithReuseIdentifier: waterWidgetCollectionViewCellId)
        collectionView.register(WeightWidgetCollectionViewCell.self,
								forCellWithReuseIdentifier: weightWidgetCollectionViewCellId)
        collectionView.register(NearestMealWidgetCollectionViewCell.self,
								forCellWithReuseIdentifier: nearestWidgetCollectionViewCellId)
        collectionView.register(NutritionMetricsWidgetCollectionViewCell.self,
								forCellWithReuseIdentifier: nutritionMetricsCollectionViewCellId)
        collectionView.register(BodyMetricsWidgetCollectionViewCell.self,
								forCellWithReuseIdentifier: bodyMetricsCollecionViewCellId)
		collectionView.register(HeaderWidgetView.self,
								forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
								withReuseIdentifier: headerWidgetView)
        collectionView.register(FooterWidgetView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
                                withReuseIdentifier: footerWidgetViewId)
    }
    
    private func showFiveStarsPopUp() {
        RateAppAssembly().showRateAppController(from: self)
    }
}

extension UIView {
	func fromCardFrame() -> CGRect {
		let currentCellFrame = layer.presentation()!.frame
		return superview!.convert(currentCellFrame, to: nil)
	}
	
	func fromCardFrameWithoutTransform() -> CGRect {
		let r = CGRect(origin: CGPoint(x: center.x - bounds.size.width / 2,
									   y: center.y - bounds.size.height / 2),
					   size: bounds.size)
		return superview!.convert(r, to: nil)
	}
}
