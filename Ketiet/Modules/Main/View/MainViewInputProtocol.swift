//
//  MainViewInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol MainViewInputProtocol: AnyObject {
    func reloadData()
	func removeAll(in indexSet: IndexSet)
}
