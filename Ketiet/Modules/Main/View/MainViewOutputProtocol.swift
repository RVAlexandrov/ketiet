//
//  MainViewOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 26/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol MainViewOutputProtocol {
    var widgets: [WidgetData] { get }
	var header: HeaderWidgetData { get }
    
    func viewDidLoad()
    func didTapOnWidgetConfigurationButton()
	func didSelectWidget(view: UIView,
						 fromCardFrame: CGRect,
						 fromCardFrameWithoutTransform: CGRect,
						 detailsController: UIViewController)
}
