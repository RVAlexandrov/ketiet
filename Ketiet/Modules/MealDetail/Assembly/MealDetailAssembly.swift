//
//  MealDetailAssembly.swift
//  ketiet
//
//  Created by Alex on 27/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class MealDetailAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: MealDetailModuleConfiguratorProtocol) {
		let allMeals = moduleConfigurator.meals
		var visibleDishIndex: Int
		if moduleConfigurator.mealIndex == 0 {
			visibleDishIndex = 0
		} else {
			visibleDishIndex = allMeals[..<moduleConfigurator.mealIndex].reduce(into: 0) { $0 += $1.dishes.count }
		}
		let dishes = allMeals.reduce(into: [DishDetailsItem]()) { dishes, meal in
			dishes.append(contentsOf: meal.dishes.map { DishDetailsItem(dish: $0, mealName: meal.name) })
		}
		let presenter = MealDetailPresenter(visibleDishIndex: visibleDishIndex)
		let interactor = MealDetailInteractor(presenter: presenter,
												   dishes: dishes)
		let view = MealDetailViewController(presenter: presenter,
												 measurementService: MeasurementService(type: .smallWeight),
												 energyMeasurement: MeasurementService(type: .energy).measurement(),
												 showCloseButton: moduleConfigurator.showCloseButton)

        presenter.view = view
        presenter.interactor = interactor
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
