//
//  MealDetailConfigurator.swift
//  ketiet
//
//  Created by Alex on 27/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol MealDetailModuleConfiguratorProtocol {
	var mealIndex: Int { get }
	var meals: [DietMealProtocol] { get }
	var showCloseButton: Bool { get }
}

final class MealDetailModuleConfigurator: MealDetailModuleConfiguratorProtocol {
	let mealIndex: Int
	let meals: [DietMealProtocol]
	let showCloseButton: Bool

	init(mealIndex: Int,
		 meals: [DietMealProtocol],
		 showCloseButton: Bool) {
		self.mealIndex = mealIndex
		self.meals = meals
		self.showCloseButton = showCloseButton
    }
}
