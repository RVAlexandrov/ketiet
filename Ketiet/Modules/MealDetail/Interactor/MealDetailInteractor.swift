//
//  MealDetailInteractor.swift
//  ketiet
//
//  Created by Alex on 27/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class MealDetailInteractor {
	
    private weak var presenter: MealDetailInteractorOutputProtocol?
	private let dishes: [DishDetailsItem]
	
	init(
        presenter: MealDetailInteractorOutputProtocol,
        dishes: [DishDetailsItem]
    ) {
        self.presenter = presenter
		self.dishes = dishes
    }
}

// MARK: - MealDetailInteractorInputProtocol
extension MealDetailInteractor: MealDetailInteractorInputProtocol {
	func requestDishes() {
		presenter?.didReceiveDishes(dishes)
	}
}
