//
//  MealDetailInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 27/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

protocol MealDetailInteractorOutputProtocol: AnyObject {
	
	func didReceiveDishes(_ dishes: [DishDetailsItem])
}
