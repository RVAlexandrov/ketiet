//
//  DishDetailsItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 01.11.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class DishDetailsItem {
	let dish: DishProtocol
	let mealName: String
	
	init(dish: DishProtocol, mealName: String) {
		self.dish = dish
		self.mealName = mealName
	}
}
