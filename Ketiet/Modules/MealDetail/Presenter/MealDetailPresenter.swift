//
//  MealDetailPresenter.swift
//  ketiet
//
//  Created by Alex on 27/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class MealDetailPresenter {

    weak var view: MealDetailViewInputProtocol?
    var interactor: MealDetailInteractorInputProtocol?
	private let visibleDishIndex: Int

    init(visibleDishIndex: Int) {
		self.visibleDishIndex = visibleDishIndex
    }
}

// MARK: - MealDetailViewOutputProtocol
extension MealDetailPresenter: MealDetailViewOutputProtocol {
	
    func requestItems() {
		interactor?.requestDishes()
    }
}

// MARK: - MealDetailInteractorOutputProtocol
extension MealDetailPresenter: MealDetailInteractorOutputProtocol {
	func didReceiveDishes(_ dishes: [DishDetailsItem]) {
		let loader = ImageLoader()
		let galleryItems = dishes.map { DishGalleryItem(dishItem: $0, imageLoader: loader) }
		view?.showDishItems(galleryItems, visibleDishIndex: visibleDishIndex)
	}
}
