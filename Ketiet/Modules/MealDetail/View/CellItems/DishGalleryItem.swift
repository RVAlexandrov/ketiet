//
//  MealGalleryItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 27.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DishGalleryItem {
	
	private let imageView: UIImageView = {
		let view = UIImageView()
		view.clipsToBounds = true
		view.layer.cornerRadius = .littMediumCornerRadius
		view.contentMode = .scaleAspectFill
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	private let imageLoader: ImageLoaderProtocol
	private var imageLoadingState: ImageLoadingState = .waitingForBegin
	let dishItem: DishDetailsItem
	
	init(dishItem: DishDetailsItem, imageLoader: ImageLoaderProtocol) {
		self.dishItem = dishItem
		self.imageLoader = imageLoader
		if dishItem.dish.imageURL == nil {
			imageLoadingState = .loadingFailed(nil)
		}
	}
}

extension DishGalleryItem: GalleryViewItem {
	var view: UIView {
		switch imageLoadingState {
		case .waitingForBegin:
			guard let imageURL = dishItem.dish.imageURL else { return imageView }
			imageLoader.loadImage(by: imageURL) { [weak self] image in
				self?.imageView.hideShimmer()
				if let image = image {
					self?.imageLoadingState = .loaded(image)
					self?.imageView.image = image
				} else {
					self?.imageLoadingState = .loadingFailed(nil)
				}
			}
			self.imageLoadingState = .loading
			imageView.showShimmer()
		case .loading, .loadingFailed:
			break
		case .loaded(let image):
			imageView.hideShimmer()
			imageView.image = image
		}
		return imageView
	}
	
	var title: String? {
		dishItem.dish.name
	}
}
