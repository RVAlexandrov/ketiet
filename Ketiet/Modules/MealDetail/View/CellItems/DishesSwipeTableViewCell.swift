//
//  MealsSwipeView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 27.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DishesSwipeTableViewCell: UITableViewCell {
	
	weak var item: DishesSwipeTableViewCellItem?
	
	private(set) lazy var galleryView: GalleryView = {
		let view = GalleryView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		view.delegate = self
		return view
	}()
	
	let mealNameLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .title
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	@available(*, unavailable)
	required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
	private func setupSubviews() {
        backgroundColor = .littBackgroundColor
		contentView.addSubview(galleryView)
		contentView.addSubview(mealNameLabel)
		NSLayoutConstraint.activate([mealNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor,
																		constant: .smallLittMargin),
									 mealNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,
																			constant: .littScreenEdgeMargin),
									 mealNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,
																			 constant: -.littScreenEdgeMargin),
									 galleryView.topAnchor.constraint(equalTo: mealNameLabel.bottomAnchor),
									 galleryView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
									 galleryView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
									 galleryView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)])
	}
}

extension DishesSwipeTableViewCell: GalleryViewDelegate {

	func galleryView(_ view: GalleryView, didChangePage index: Int) {
		guard let item = item else { return }
		item.selectedDishIndex = index
		mealNameLabel.text = item.galleryItems[index].dishItem.mealName
		item.didUpdateHandler(item.galleryItems[index])
	}
}
