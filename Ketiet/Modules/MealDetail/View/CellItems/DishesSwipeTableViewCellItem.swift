//
//  DishesSwipeTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 31.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DishesSwipeTableViewCellItem {
	
	var selectedDishIndex: Int
	let galleryItems: [DishGalleryItem]
	let didUpdateHandler: (DishGalleryItem) -> Void
	
	init(dishItems: [DishGalleryItem],
		 selectedDishIndex: Int,
		 didUpdateHandler: @escaping(DishGalleryItem) -> Void) {
		self.galleryItems = dishItems
		self.selectedDishIndex = selectedDishIndex
		self.didUpdateHandler = didUpdateHandler
	}
}

extension DishesSwipeTableViewCellItem: LittTableViewCellItemProtocol {

	func configure(view: UITableViewCell) {
		guard let cell = view as? DishesSwipeTableViewCell else { return }
		cell.item = self
		cell.galleryView.content = galleryItems
		cell.galleryView.setVisibleItemIndex(selectedDishIndex)
		cell.mealNameLabel.text = galleryItems[selectedDishIndex].dishItem.mealName
		cell.selectionStyle = .none
	}
	
	func viewClass() -> AnyClass {
		DishesSwipeTableViewCell.self
	}
}
