//
//  MealGalleryItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 27.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DishGalleryItem {
	
	private let imageView: UIImageView = {
		let view = UIImageView()
		view.clipsToBounds = true
		view.layer.cornerRadius = .littMediumCornerRadius
		view.contentMode = .scaleAspectFill
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	init(image: UIImage) {
		imageView.image = image
	}
}

extension DishGalleryItem: GalleryViewItem {
	var view: UIView {
		imageView
	}
	
	var title: String? {
		nil
	}
}
