//
//  MealsSwipeView.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 27.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DishesSwipeView: UIView {
	
	private let mealItems: [GalleryViewItem]
	private let didUpdateHandler: (Int) -> Void
	
	private let titleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		label.font = .subTitle
		return label
	}()
	
	private lazy var galleryView: GalleryView = {
		let view = GalleryView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.delegate = self
		return view
	}()
	
	init(mealItems: [GalleryViewItem], didUpdateHandler: @escaping(Int) -> Void) {
		self.mealItems = mealItems
		self.didUpdateHandler = didUpdateHandler
		super.init(frame: .zero)
	}
	
	@available(*, unavailable)
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension DishesSwipeView: GalleryViewDelegate {

	func galleryView(_ view: GalleryView, didChangePage index: Int) {
		didUpdateHandler(index)
	}
}
