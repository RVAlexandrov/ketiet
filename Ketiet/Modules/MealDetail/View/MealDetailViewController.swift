//
//  MealDetailViewController.swift
//  ketiet
//
//  Created by Alex on 27/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class MealDetailViewController: DismissableDetailsViewController {

    private let presenter: MealDetailViewOutputProtocol
	private let measurementService: MeasurementServiceProtocol
	private let energyMeasurement: String

	init(presenter: MealDetailViewOutputProtocol,
		 measurementService: MeasurementServiceProtocol,
		 energyMeasurement: String,
		 showCloseButton: Bool) {
        self.presenter = presenter
		self.measurementService = measurementService
		self.energyMeasurement = energyMeasurement
        super.init(showCloseButton: showCloseButton)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.shouldAutoReload = false
        navigationController?.navigationBar.prefersLargeTitles = false
        tableView.separatorStyle = .singleLine
        view.backgroundColor = .littBackgroundColor
        tableView.backgroundColor = .littBackgroundColor
        if !(parent is UINavigationController) {
            tableView.contentInset.bottom += 20
        }
        presenter.requestItems()
	}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        littTabBarController?.setTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        littTabBarController?.setTabBarHidden(false)
    }
}

// MARK: - MealDetailViewInputProtocol
extension MealDetailViewController: MealDetailViewInputProtocol {
	
	func showDishItems(_ items: [DishGalleryItem], visibleDishIndex: Int) {
        
        var sections: [LittTableViewSectionProtocol] = []
                
        let currentDish = items[visibleDishIndex].dishItem.dish

        let headerItem = DishHeaderCellItem(mealString: items[visibleDishIndex].dishItem.mealName,
                                            dishString: currentDish.name,
                                            weightString: "WEIGHT_OF_PORTION".localized() + " " + currentDish.weightInGrams.cleanValue + " " +  measurementService.measurement())
        let headerSection = LittTableViewSection(items: [headerItem])
        sections.append(headerSection)
        
        let badgeTypes: [OutlineBadgeType] = currentDish.categories.map { OutlineBadgeType(with: $0) }
        if !badgeTypes.isEmpty {
            let badgeItem = BadgesCellItem(badgesType: badgeTypes)
            let badgeSection = LittTableViewSection(items: [badgeItem])
            sections.append(badgeSection)
        }
        
        let imageItem = ImageCellItem(imageWrapper: .url(currentDish.imageURL?.absoluteString ?? ""))
        let imageSection = LittTableViewSection(items: [imageItem])
        sections.append(imageSection)
        
        let detailSections = currentDish.detailsSections(measurement: measurementService.measurement(),
                                                         energyMeasure: self.energyMeasurement)
        sections.append(contentsOf: detailSections)
        
        if let cookingSteps = currentDish.dishRecipe.cookingSteps,
           !cookingSteps.isEmpty {
            let prepareSection = currentDish.preparationSection(presenterController: self)
            sections.append(prepareSection)
        }

        tableView.sections = sections
        tableView.reloadDataWithAnimation()
    }
}
