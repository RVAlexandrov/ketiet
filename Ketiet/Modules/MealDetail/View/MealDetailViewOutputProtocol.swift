//
//  MealDetailViewOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 27/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol MealDetailViewOutputProtocol {
    func requestItems()
}
