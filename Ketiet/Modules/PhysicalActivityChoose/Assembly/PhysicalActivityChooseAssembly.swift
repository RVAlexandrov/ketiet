//
//  PhysicalActivityChooseAssembly.swift
//  ketiet
//
//  Created by Alex on 12/12/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

final class PhysicalActivityChooseAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: PhysicalActivityChooseModuleConfiguratorProtocol) {
        let presenter = PhysicalActivityChoosePresenter()
        let interactor = PhysicalActivityChooseInteractor(profileService: moduleConfigurator.profileService)
        let view = ChooseItemViewController(items: PhysicalActivity.allCases.map { DefaultChooseItem(title: $0.description,
                                                                                                     description: $0.descriptionString,
                                                                                                     isSelected: $0 == moduleConfigurator.selectedPhysicaActivity) } ,
											buttonTitle: "CONTINUE".localized()) { (index, _) in
			presenter.didTapContinueButton(index)
		}
		view.navigationItem.title = "YOUR_ACTIVITY".localized()
			
        presenter.interactor = interactor
        presenter.router = PhysicalActivityChooseRouter(
            viewController: view,
            profileService: moduleConfigurator.profileService,
            mealDatesStorage: moduleConfigurator.mealDatesStorage
        )
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
