//
//  PhysicalActivityChooseConfigurator.swift
//  ketiet
//
//  Created by Alex on 12/12/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import DietCore

protocol PhysicalActivityChooseModuleConfiguratorProtocol {
    var selectedPhysicaActivity: PhysicalActivity? { get }
    var profileService: ProfileServiceProtocol { get }
    var mealDatesStorage: MealDatesStorageProtocol { get }
}

struct PhysicalActivityChooseModuleConfigurator: PhysicalActivityChooseModuleConfiguratorProtocol {
    let selectedPhysicaActivity: PhysicalActivity?
    let profileService: ProfileServiceProtocol
    let mealDatesStorage: MealDatesStorageProtocol
}
