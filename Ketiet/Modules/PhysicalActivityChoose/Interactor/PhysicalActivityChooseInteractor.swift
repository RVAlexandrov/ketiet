//
//  PhysicalActivityChooseInteractor.swift
//  ketiet
//
//  Created by Alex on 12/12/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

struct PhysicalActivityChooseInteractor {

	private let profileService: ProfileServiceProtocol
	
	init(profileService: ProfileServiceProtocol) {
		self.profileService = profileService
    }
}

// MARK: - PhysicalActivityChooseInteractorInputProtocol
extension PhysicalActivityChooseInteractor: PhysicalActivityChooseInteractorInputProtocol {
	func savePhysicalActivity(_ activity: PhysicalActivity) {
		profileService.setInitialPhysicalActivity(activity, completion: { _ in })
	}
}
