//
//  PhysicalActivityChooseInteractorInputProtocol.swift
//  ketiet
//
//  Created by Alex on 12/12/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol PhysicalActivityChooseInteractorInputProtocol {
	func savePhysicalActivity(_ activity: PhysicalActivity)
}
