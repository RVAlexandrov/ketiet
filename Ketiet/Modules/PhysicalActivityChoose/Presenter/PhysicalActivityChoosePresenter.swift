//
//  PhysicalActivityChoosePresenter.swift
//  ketiet
//
//  Created by Alex on 12/12/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

final class PhysicalActivityChoosePresenter {

    var interactor: PhysicalActivityChooseInteractorInputProtocol?
    var router: PhysicalActivityChooseRouterInputProtocol?
}


extension PhysicalActivityChoosePresenter: PhysicalActivityChooseViewOutputProtocol {
	func didTapContinueButton(_ index: Int) {
		interactor?.savePhysicalActivity(PhysicalActivity.allCases[index])
		router?.showMealDatesInput()
    }
}
