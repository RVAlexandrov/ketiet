//
//  PhysicalActivityChooseRouter.swift
//  ketiet
//
//  Created by Alex on 12/12/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class PhysicalActivityChooseRouter {

    private weak var viewController: UIViewController?
    private let profileService: ProfileServiceProtocol
    private let mealDatesStorage: MealDatesStorageProtocol

    init(
        viewController: UIViewController,
        profileService: ProfileServiceProtocol,
        mealDatesStorage: MealDatesStorageProtocol
    ) {
        self.viewController = viewController
        self.profileService = profileService
        self.mealDatesStorage = mealDatesStorage
    }
    
    private func showFinalScreen() {
        let configurator = WelcomeResumeModuleConfigurator(
            mealsDateService: mealDatesStorage,
            profileService: profileService,
            customTargetCalories: nil
        )
        let assembly: WelcomeResumeAssembly = AssemblyFactory().generateAssembly(moduleConfigurator: configurator)
        viewController?.navigationController?.pushViewController(
            assembly.initialViewController(),
            animated: true
        )
    }
}

// MARK: - PhysicalActivityChooseRouterInputProtocol
extension PhysicalActivityChooseRouter: PhysicalActivityChooseRouterInputProtocol {
	
	func showMealDatesInput() {
        let mealChangeDatesDataProvider = KetoMealDateTitleProvider(dateService: MealDatesService.shared)
        let assembly = ChangeMealDatesAssembly(
            title: "JUST_A_LITTLE_MORE".localized(),
            dataProvider: mealChangeDatesDataProvider,
            actionButtonTitle: "CONTINUE".localized(),
            mealDatesStorage: mealDatesStorage) { [weak self] in
                self?.showFinalScreen()
        }
        let controller = assembly.initialViewController()
		viewController?.navigationController?.pushViewController(controller, animated: true)
	}
}
