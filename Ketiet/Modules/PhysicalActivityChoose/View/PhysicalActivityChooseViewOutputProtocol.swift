//
//  PhysicalActivityChooseViewOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 12/12/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol PhysicalActivityChooseViewOutputProtocol {
	func didTapContinueButton(_ index: Int)
}
