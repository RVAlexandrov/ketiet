//
//  ProfileAssembly.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProfileAssembly {

    private let view: UIViewController
    
    init() {
		let assembly = UserProfileImageServiceAssembly()
        let progresServiceFabric = ProgressServiceFabric()
        let presenter = ProfilePresenter(
            profileService: ProfileService.instance,
            dietService: DietService.shared,
            mealDatesDataProvider: KetoMealDateTitleProvider(dateService: MealDatesService.shared),
            weightProgressService: progresServiceFabric.makeProgressService(forType: .weight)
        )
		let interactor = ProfileInteractor(presenter: presenter,
										   profileService: ProfileService.instance,
										   dietService: DietService.shared)
        let view = ProfileViewController(presenter: presenter)
		let router = ProfileRouter(presenter: presenter,
								   viewController: view,
								   imageSize: assembly.imageSize(),
								   emojies: assembly.emojis(),
                                   imageFont: assembly.font(), alertFactory: AlertControllerFactory())

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
