//
//  ProfileInteractor.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProfileInteractor {

    private weak var presenter: ProfileInteractorOutputProtocol?
	private let profileService: ProfileServiceProtocol
	private let dietService: DietServiceProtocol
	
    init(presenter: ProfileInteractorOutputProtocol,
		 profileService: ProfileServiceProtocol,
		 dietService: DietServiceProtocol) {
        self.presenter = presenter
		self.profileService = profileService
		self.dietService = dietService
    }
}

// MARK: - ProfileInteractorInputProtocol
extension ProfileInteractor: ProfileInteractorInputProtocol {
	func getProfileService() -> ProfileServiceProtocol {
		profileService
	}
	
	func getDietService() -> DietServiceProtocol {
		dietService
	}
	
	func saveNewImage(image: UIImage) {
		profileService.setProfileImage(image, completion: { [weak self] result in
			switch result {
			case .success:
				self?.presenter?.updateHeaderItem()
			case let .failure(error):
				assertionFailure(error.localizedDescription)
			}
		})
	}
}
