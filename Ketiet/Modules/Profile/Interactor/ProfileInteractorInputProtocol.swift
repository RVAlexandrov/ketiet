//
//  ProfileInteractorInputProtocol.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol ProfileInteractorInputProtocol {
	
	func saveNewImage(image: UIImage)
}
