//
//  UserProfileImageService.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class UserProfileImageService {
	
	private let imageSize: CGSize
	private let font: UIFont
	private let availableEmojis: [String]
	
	init(imageSize: CGSize, availableEmojis: [String], font: UIFont) {
		self.imageSize = imageSize
		self.availableEmojis = availableEmojis
		self.font = font
	}
}

extension UserProfileImageService: UserProfileImageServiceProtocol {
	func userProfileImage() -> UIImage {
		if let random = availableEmojis.randomElement(),
			let image = random.image(font: font, size: imageSize) {
			return image
		}
		return UIImage()
	}
}
