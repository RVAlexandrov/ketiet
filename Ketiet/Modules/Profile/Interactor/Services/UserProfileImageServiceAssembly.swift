//
//  UserProfileImageServiceAssembly.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 14.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class UserProfileImageServiceAssembly {
	
	func makeUserProfileImageService() -> UserProfileImageServiceProtocol {
		UserProfileImageService(imageSize: imageSize(), availableEmojis: emojis().map { $0.emoji }, font: font())
	}
	
	func emojis() -> [EmojiWithDescription] {
		[EmojiWithDescription(emoji: "🐶", description: "DOG".localized()),
					   EmojiWithDescription(emoji: "🐱", description: "CAT".localized()),
					   EmojiWithDescription(emoji: "🐰", description: "RABBIT".localized()),
					   EmojiWithDescription(emoji: "🦊", description: "FOX".localized()),
					   EmojiWithDescription(emoji: "🐻", description: "BEAR".localized()),
					   EmojiWithDescription(emoji: "🐼", description: "PANDA".localized()),
					   EmojiWithDescription(emoji: "🐯", description: "TIGER".localized()),
					   EmojiWithDescription(emoji: "🦁", description: "LION".localized()),]
	}
	
	func font() -> UIFont {
		UIFont.systemFont(ofSize: 75)
	}
	
	func imageSize() -> CGSize {
		CGSize(width: 80, height: 80)
	}
}
