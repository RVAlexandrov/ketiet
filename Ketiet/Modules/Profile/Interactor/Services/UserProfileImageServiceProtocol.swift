//
//  UserProfileImageServiceProtocol.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol UserProfileImageServiceProtocol {

	func userProfileImage() -> UIImage
}

