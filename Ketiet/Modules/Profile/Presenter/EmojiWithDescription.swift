//
//  EmojiWithDescription.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

struct EmojiWithDescription {
	let emoji: String
	let description: String
}
