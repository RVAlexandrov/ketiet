//
//  ProfilePresenter.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import CoreGraphics
import DietCore
import Combine

final class ProfilePresenter {

    weak var view: ProfileViewInputProtocol?
    var interactor: ProfileInteractorInputProtocol?
    var router: ProfileRouterInputProtocol?
    
	private var headerItem: ProfileHeaderCellItem?
	private lazy var dietItem = ProgressCellItem(dietService: dietService)
    private var weightItem: ValueDisplayingTableViewCellItem?
    private var tallItem: ValueDisplayingTableViewCellItem?
    private var physicalActivityItem: ValueDisplayingTableViewCellItem?
    private var goalItem: ValueDisplayingTableViewCellItem?
    private var bmiItem: IconDescriptionCellItem?
    private lazy var mealDateItems = makeMealDatesItems()
    private var profile: ProfileProtocol?
    private var diet: DietProtocol?
    private let privateProfileService: ProfileServiceProtocol
    private let dietService: DietServiceProtocol
    private let mealDatesDataProvider: ChangeMealDateTitleProviderProtocol
    private let weightProgressService: ProgressServiceProtocol
    private let weightMeasurementService = MeasurementService(type: .weight)
    private let tallMeasurementService = MeasurementService(
        type: .tall,
        unitStyle: .short,
        unitOptions: [.providedUnit]
    )
    private var subscriber: AnyCancellable?
    
    init(
        profileService: ProfileServiceProtocol,
        dietService: DietServiceProtocol,
        mealDatesDataProvider: ChangeMealDateTitleProviderProtocol,
        weightProgressService: ProgressServiceProtocol
    ) {
        self.privateProfileService = profileService
        self.dietService = dietService
        self.mealDatesDataProvider = mealDatesDataProvider
        self.weightProgressService = weightProgressService
    }
}

// MARK: - ProfileViewOutputProtocol
extension ProfilePresenter: ProfileViewOutputProtocol {
	func viewDidLoad() {
        privateProfileService.getProfile { [weak self] profile in
            self?.profile = profile
            self?.prepareData()
        }
        diet = dietService.getCurrentDiet()
        subscriber = weightProgressService.publisher.sink(receiveValue: { [weak self] _ in
            DispatchQueue.main.async {
                guard let self = self else { return }
                let model = self.configureBmiModel()
                self.bmiItem?.viewModel = model
            }
        })
	}
}

// MARK: - ProfileInteractorOutputProtocol
extension ProfilePresenter: ProfileInteractorOutputProtocol {
	
	func updateHeaderItem() {
		DispatchQueue.main.async {
			self.headerItem?.updateCell()
		}
	}
}

// MARK: - ProfileRouterOutputProtocol
extension ProfilePresenter: ProfileRouterOutputProtocol {
    
    var profileService: ProfileServiceGetProtocol? {
        privateProfileService
    }
    
    func didChangeMealDates() {
        let titlesAndDates = mealDatesDataProvider.titleAndDates()
        guard titlesAndDates.count == mealDateItems.count else {
            assertionFailure("Something went wrong, count should be equal")
            return
        }
        mealDateItems.enumerated().forEach { index, item in
            guard let dateString = titlesAndDates[safe: index]?.date.shortTimeString() else { return }
            item.reloadValue(newValue: dateString)
        }
    }
	
	func removeWholeAccount() {
		dietService.removeAll()
		RateAppAssembly().deleteAskedRating()
        privateProfileService.removeProfile()
	}
}

// MARK: - Private methods
extension ProfilePresenter {
    private func prepareData() {
        guard let profile = profile else { return }
    
        let headerItem = makeHeaderItem(profile: profile)
        self.headerItem = headerItem
        
        var sections: [LittTableViewSectionProtocol] = []
        sections.append(LittTableViewSection(items: [headerItem]))
        
        let currentDietTitleItem = HeaderViewItem(text: diet?.name ?? "CURRENT_DIET".localized())
        let seeAllDietsItem = ValueDisplayingTableViewCellItem(
            title: "SEE_ALL_DIETS".localized(),
            titleColor: .systemBlue,
            value: "",
            selectionHandler: { [weak self] in
                self?.router?.showDietsViewController {
                    self?.dietItem.updateCell()
                }
            }
        )
        sections.append(LittTableViewSection(header: currentDietTitleItem,
                                             items: [dietItem, seeAllDietsItem]))
        
        let charsHeaderViewItem = HeaderViewItem(text: "CHARACTERISTICS".localized())
        sections.append(LittTableViewSection(header: charsHeaderViewItem, items: configureCharsSectionItems()))
        
        
        if let bmiModel = configureBmiModel() {
            let bmiItem = IconDescriptionCellItem(
                viewModel: bmiModel
            )
            bmiItem.delegate = self
            self.bmiItem = bmiItem
            
            sections.append(
                LittTableViewSection(
                    header: HeaderViewItem(text: "BMI_SHORT".localized()),
                    items: [bmiItem]
                )
            )
        }
        
        sections.append(
            LittTableViewSection(
                header: HeaderViewItem(text: "MEAL_DATES".localized()),
                items: mealDateItems
            )
        )
        
        sections.append(LittTableViewSection(items: [makeDeleteProfileItem()]))

        view?.displayItems(sections: sections)
    }
    
    private func configureBmiModel() -> IconDescriptionCell.ViewModel? {
        guard let profile = profile,
              let weight = profile.weight,
              let tall = profile.tall else { return nil }
        let lastWeight = weightProgressService.getLastNonNullValueIfPossible() ?? Float(weight)
        let tallMeasurementService = MeasurementService(type: .tall)
        let convertedTall = tallMeasurementService
            .convertToStorage(value: Float(tall))
            .convert(
                from: MeasurementType.tall.metricUnit,
                to: UnitLength.meters
            )
        
        let bmiCalculator = BodyMassIndexCalculator()
        let indexType = bmiCalculator.calculateBMI(withWeight: lastWeight,
                                                   tall: convertedTall)
        let bmiModel = IconDescriptionCell.ViewModel(
            systemImageString: .system("person.fill"),
            imageColor: .systemBlue,
            title: "BMI_TITLE".localized() + " - " + indexType.fullString,
            description: "BMI_DESCRIPTION".localized()
        )
        return bmiModel
    }
    
    private func makeHeaderItem(profile: ProfileProtocol) -> ProfileHeaderCellItem {
        let handler: (ProfileHeaderCellItem) -> Void = { [weak self] item in
            self?.router?.showChangeProfileImage { image in
                self?.interactor?.saveNewImage(image: image)
                self?.router?.showRateAppController()
            }
        }
        return ProfileHeaderCellItem(
            profile: profile,
            updateProfileImageHandler: handler
        )
    }
    
    private func configureCharsSectionItems() -> [LittTableViewCellItemProtocol] {
        guard let profile = profile else { return [] }
        
        let selectionHandler: () -> Void = { [weak self] in
            self?.router?.showChangeProfileCharacteristics {
                self?.headerItem?.updateCell()
                self?.reloadChangableItems()
            }
        }
        
        let weightDataString = weightMeasurementService.format(value: Float(profile.weight ?? 0))
        let weightItem = ValueDisplayingTableViewCellItem(
            title: "WEIGHT".localized(),
            value: weightDataString,
            selectionHandler: selectionHandler
        )
        self.weightItem = weightItem
        
        let tallDataString = tallMeasurementService.format(value: Float(profile.tall ?? 0))
        let tallItem = ValueDisplayingTableViewCellItem(
            title: "TALL".localized(),
            value: tallDataString,
            selectionHandler: selectionHandler
        )
        self.tallItem = tallItem
        
        let physicalActivityItem = ValueDisplayingTableViewCellItem(
            title: "PHYSICAL_ACTIVITY".localized(),
            value: profile.physicalActivity?.description ?? "--",
            selectionHandler: selectionHandler
        )
        self.physicalActivityItem = physicalActivityItem
        
        let goalItem = ValueDisplayingTableViewCellItem(
            title: "GOAL".localized(),
            value: profile.goal?.description ?? "--",
            selectionHandler: selectionHandler
        )
        self.goalItem = goalItem
        
        return [weightItem, tallItem, physicalActivityItem, goalItem]
    }
    
    private func makeMealDatesItems() -> [ValueDisplayingTableViewCellItem] {
        let titlesAndDates = mealDatesDataProvider.titleAndDates()
        let items = titlesAndDates.enumerated().map { index, data in
            ValueDisplayingTableViewCellItem(
                title: data.title,
                value: data.date.shortTimeString()) { [weak self] in
                    guard let self = self else { return }
                    self.router?.showMealDatesChange(mealDatesDataProvider: self.mealDatesDataProvider)
                }
        }
        return items
    }
    
    private func reloadChangableItems() {
        guard let profile = profile else { return }

        let weightDataString = weightMeasurementService.format(value: Float(profile.weight ?? 0))
        weightItem?.reloadValue(newValue: weightDataString)
        
        let tallDataString = tallMeasurementService.format(value: Float(profile.tall ?? 0))
        tallItem?.reloadValue(newValue: tallDataString)
        
        physicalActivityItem?.reloadValue(newValue: profile.physicalActivity?.description ?? "--")

        goalItem?.reloadValue(newValue: profile.goal?.description ?? "--")
    }
    
    private func makeDeleteProfileItem() -> LittTableViewCellItemProtocol {
        ButtonTableViewCellItem(
            title: "DELETE_ACCOUNT".localized(),
            color: .littFailureColor
        ) { [weak self] in
            self?.router?.showDeleteAccountScreen()
        }
    }
}

// MARK: - IconDecriptionCellItemDelegate
extension ProfilePresenter: IconDecriptionCellItemDelegate {
    func didSelectIconDescriptionCellItem(item: IconDescriptionCellItem) {
        guard let profile = profile else { return }
        router?.showBmiDetailsModule(
            weightProgressService: weightProgressService,
            profile: profile,
            bmiCalculator: BodyMassIndexCalculator()
        )
    }
}
