//
//  ProfileRouter.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProfileRouter: NSObject {

    private weak var presenter: ProfileRouterOutputProtocol?
    private(set) weak var viewController: UIViewController?
	private let imageSize: CGSize
	private let imageFont: UIFont
	private let emojies: [EmojiWithDescription]
    private let alertFactory: AlertControllerFactoryProtocol
	private var completion: ((UIImage) -> Void)?

	init(presenter: ProfileRouterOutputProtocol,
		 viewController: UIViewController,
		 imageSize: CGSize,
		 emojies: [EmojiWithDescription],
		 imageFont: UIFont,
         alertFactory: AlertControllerFactoryProtocol) {
        self.presenter = presenter
        self.viewController = viewController
		self.imageSize = imageSize
		self.emojies = emojies
		self.imageFont = imageFont
        self.alertFactory = alertFactory
    }
	
	private func showImagePicker() {
		let picker = UIImagePickerController()
		picker.allowsEditing = true
		picker.delegate = self
		picker.sourceType = .photoLibrary
		viewController?.present(picker, animated: true)
	}
}

// MARK: - ProfileRouterInputProtocol
extension ProfileRouter: ProfileRouterInputProtocol {
	func showChangeProfileImage(completion: @escaping (UIImage) -> Void) {
		self.completion = completion
        var actions = emojies.map { emoji in
            AlertAction(title: "\(emoji.emoji) - \(emoji.description)",
                        style: .normal,
                        handler: { [weak self] in
                            guard let self = self,
                                  let image = emoji.emoji.image(font: self.imageFont,
                                                                size: self.imageSize) else { return }
                            self.completion?(image)
                        })
        }
		if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            actions.append(AlertAction(title: "SELECT_FROM_GALLERY".localized(),
                                       style: .normal,
                                       handler: { [weak self] in
                self?.showImagePicker()
            }))
		}
        actions.append(AlertAction(title: "CANCEL".localized(), style: .approve))
        let controller = alertFactory.makeActionSheet(title: nil, actions: actions)
		viewController?.present(controller, animated: true)
	}
	
	func showChangeProfileCharacteristics(completion: @escaping () -> Void) {
		presenter?.profileService?.getProfile(completion: { [weak self] profile in
			guard let self = self,
				let profile = profile else {
				assertionFailure("Unable to get profile")
				return
			}
            let changeProfileAssebly = ChangeProfileCharacteristicsAssembly(
                profile: profile,
                completion: { [weak self] in
                    completion()
                    self?.viewController?.navigationController?.popViewController(animated: true)
                }
            )
			self.viewController?.navigationController?.pushViewController(
                changeProfileAssebly.initialViewController(),
                animated: true
            )
		})
	}
	
	func showDietsViewController(completion: @escaping () -> Void) {
		let assembly: DietsAssembly = AssemblyFactory().generateAssembly(
            moduleConfigurator: DietsModuleConfigurator(didUpdateDiets: completion)
        )
		viewController?.navigationController?.pushViewController(assembly.initialViewController(), animated: true)
	}
	
	func showRateAppController() {
		guard let controller = viewController else { return }
		RateAppAssembly().showRateAppController(from: controller)
	}
    
    func showMealDatesChange(mealDatesDataProvider: ChangeMealDateTitleProviderProtocol) {
        let assembly = ChangeMealDatesAssembly(
            title: "MEALS".localized(),
            dataProvider: mealDatesDataProvider,
            actionButtonTitle: "SAVE".localized(),
            mealDatesStorage: MealDatesService.shared
        ) { [weak self] in
            self?.viewController?.navigationController?.popViewController(animated: true)
            self?.presenter?.didChangeMealDates()
        }
        viewController?.navigationController?.pushViewController(
            assembly.initialViewController(),
            animated: true
        )
    }
    
    func showDeleteAccountScreen() {
        let actions = [
            AlertAction(
                title: "YES".localized(),
                style: .delete
            ) { [weak self] in
                self?.presenter?.removeWholeAccount()
                UIApplication.shared.showWelcomeViewController()
            },
            AlertAction(
                title: "CANCEL".localized(),
                style: .normal
            )
        ]
        let actionSheet = alertFactory.makeAlert(
            title: "DELETE_ACCOUNT".localized(),
            description: "DELETE_ACCOUNT?".localized(),
            actions: actions
        )
        viewController?.navigationController?.visibleViewController?.present(
            actionSheet,
            animated: true
        )
    }
    
    func showBmiDetailsModule(weightProgressService: ProgressServiceProtocol,
                              profile: ProfileProtocol,
                              bmiCalculator: BodyMassIndexCalculatorProtocol) {
        let assembly = BodyMassIndexDetailsAssembly()
        let nc = UINavigationController(
            rootViewController: assembly.makeViewController(
                weightProgressService: weightProgressService,
                profile: profile,
                bmiCalculator: bmiCalculator
            )
        )
        viewController?.present(nc, animated: true)
    }
}

extension ProfileRouter: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
		guard let photo = (info[.editedImage] ?? info[.originalImage]) as? UIImage else { return }
		completion?(photo)
		picker.dismiss(animated: true)
	}

	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true)
	}
}
