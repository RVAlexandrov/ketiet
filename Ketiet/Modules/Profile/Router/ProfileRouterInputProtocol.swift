//
//  ProfileRouterInputProtocol.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol ProfileRouterInputProtocol {
	
	func showChangeProfileImage(completion: @escaping(UIImage) -> Void)
	func showRateAppController()
	func showChangeProfileCharacteristics(completion: @escaping() -> Void)
	func showDietsViewController(completion: @escaping() -> Void)
    func showMealDatesChange(mealDatesDataProvider: ChangeMealDateTitleProviderProtocol)
    func showDeleteAccountScreen()
    func showBmiDetailsModule(weightProgressService: ProgressServiceProtocol,
                              profile: ProfileProtocol,
                              bmiCalculator: BodyMassIndexCalculatorProtocol)
}
