//
//  ProfileRouterOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol ProfileRouterOutputProtocol: AnyObject {
	
	var profileService: ProfileServiceGetProtocol? { get }
	
	func removeWholeAccount()
    func didChangeMealDates()
}
