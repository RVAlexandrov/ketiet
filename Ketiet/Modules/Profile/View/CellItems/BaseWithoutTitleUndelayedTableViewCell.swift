//
//  BaseWithoutTitleUndelayedTableViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 21.03.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import UIKit

class BaseWithoutTitleUndelayedTableViewCell: UITableViewCell {
    let separatorView: UIView = {
        let separatorView = UIView()
        separatorView.backgroundColor = .systemGray
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        return separatorView
    }()

    var isSeparatorHidden: Bool = true {
        didSet {
            separatorView.isHidden = isSeparatorHidden
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupSubviews() {
        contentView.addSubviews([separatorView])

        NSLayoutConstraint.activate([
            separatorView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 0.33),
            separatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
    }
}
