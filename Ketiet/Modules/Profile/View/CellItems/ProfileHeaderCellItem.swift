//
//  ProfileHeaderCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProfileHeaderCellItem {
	
	private weak var cell: ProfileHeaderTableViewCell?
	private let profile: ProfileProtocol
	private let updateProfileImageHandler: (ProfileHeaderCellItem) -> Void
	
	init(profile: ProfileProtocol,
		 updateProfileImageHandler: @escaping(ProfileHeaderCellItem) -> Void) {
		self.updateProfileImageHandler = updateProfileImageHandler
        self.profile = profile
	}
	
	func updateCell() {
		guard let cell = cell else { return }
		configure(view: cell)
	}
}

extension ProfileHeaderCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? ProfileHeaderTableViewCell else {
			return
		}
		self.cell = cell
		cell.nameLabel.text = profile.name
		cell.profileImageView.image = profile.image
	}
	
	func viewClass() -> AnyClass {
		ProfileHeaderTableViewCell.self
	}
	
	func didSelect() {
		updateProfileImageHandler(self)
	}
}
