//
//  ProfileTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProfileHeaderTableViewCell: UITableViewCell {
	
	private struct Constants {
		static let imageViewSide: CGFloat = UIScreen.main.bounds.height * 0.1
	}
	
	let profileImageView: UIImageView = {
		let view = UIImageView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.clipsToBounds = true
        view.layer.cornerRadius = UIScreen.main.bounds.height * 0.1 / 2
		return view
	}()
	
	let nameLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .subTitle
        label.backgroundColor = .littBackgroundColor
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
        selectionStyle = .none
        backgroundColor = .clear
        
		let stack = UIStackView(arrangedSubviews: [profileImageView, nameLabel])
		stack.translatesAutoresizingMaskIntoConstraints = false
		stack.spacing = .mediumLittMargin
		stack.axis = .vertical
		stack.alignment = .center
        
        contentView.addSubview(stack)

		NSLayoutConstraint.activate([
			profileImageView.widthAnchor.constraint(equalToConstant: Constants.imageViewSide),
			profileImageView.heightAnchor.constraint(equalToConstant: Constants.imageViewSide)
		])
		stack.pinToSuperview(insets: UIEdgeInsets(top: .mediumLittMargin,
												  left: .largeLittMargin,
												  bottom: .mediumLittMargin,
												  right: .largeLittMargin))
	}
}
