//
//  ProfileTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProfileHeaderTableViewCell: BaseProfileTableViewCell {
	
	private struct Constants {
		static let imageViewSide: CGFloat = 60
	}
	
	let profileImageView: UIImageView = {
		let view = UIImageView()
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	let nameLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .subTitle
		return label
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
		let stack = UIStackView(arrangedSubviews: [profileImageView, nameLabel])
		stack.translatesAutoresizingMaskIntoConstraints = false
		stack.spacing = .mediumLittMargin
		stack.axis = .horizontal
		underlayingView.addSubview(stack)
		stack.pinToSuperview(insets: UIEdgeInsets(top: .littScreenEdgeMargin,
												  left: .smallLittMargin,
												  bottom: .smallLittMargin,
												  right: .smallLittMargin))
	}
}
