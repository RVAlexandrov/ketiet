//
//  ProfileViewController.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ProfileViewController: UIViewController {

    private let presenter: ProfileViewOutputProtocol
	private lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.separatorStyle = .none
        tableView.separatorStyle = .singleLine
		tableView.backgroundColor = .littBackgroundColor
		tableView.setContentInsetForControllersInTabbar()
        tableView.animations = []
		return tableView
	}()

    init(presenter: ProfileViewOutputProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
		view.addSubview(tableView)
		tableView.pinToSuperview()
		presenter.viewDidLoad()
		navigationItem.title = "PROFILE".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}

// MARK: - ProfileViewInputProtocol
extension ProfileViewController: ProfileViewInputProtocol {
	func displayItems(sections: [LittTableViewSectionProtocol]) {
		tableView.sections = sections
	}
}
