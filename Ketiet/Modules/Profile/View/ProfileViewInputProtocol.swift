//
//  ProfileViewInputProtocol.swift
//  ketiet
//
//  Created by Alex on 03/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol ProfileViewInputProtocol: AnyObject {
	
	func displayItems(sections: [LittTableViewSectionProtocol])
}
