//
//  ProgressDetailByDaysProvider.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Foundation

protocol ProgressDetailByDaysProviderProtocol {
    func prepareData(with dayValues: [Float]) -> [(dayString: String, valueString: String)]
    func getStartDate() -> Date
    func getMeasurementString() -> String
    func getTitleString() -> String
    func getDescriptionString() -> String
    func getRawData() -> [Float]?
}

final class ProgressDetailByDaysProvider {
    
    private let measurementService: MeasurementServiceProtocol
    private let diet: DietProtocol
    private let progressService: ProgressServiceProtocol
    
    init(measurementService: MeasurementServiceProtocol,
         diet: DietProtocol,
         progressService: ProgressServiceProtocol) {
        self.measurementService = measurementService
        self.diet = diet
        self.progressService = progressService
    }
}
 
// MARK: - ProgressDetailByDaysProviderProtocol
extension ProgressDetailByDaysProvider: ProgressDetailByDaysProviderProtocol {
    
    func prepareData(with dayValues: [Float]) -> [(dayString: String, valueString: String)] {
        let startDate = diet.startedDate ?? Date()
        let measurementString = measurementService.measurement()
        
        let daysBetween = Date.daysBetweenTwoDates(startDate,
                                                   secondDate: Date()) ?? 0
        let values: [(dayString: String, valueString: String)] = dayValues.prefix(daysBetween + 1).enumerated().map {
            let lastInputDateComponents = startDate.dateComponentsByAdding(DateComponents(day: $0.offset),
                                                                           resultUnints: [.month, .day, .year])
            guard let date = Calendar.current.date(from: lastInputDateComponents) else { return (String($0.offset), "") }
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeStyle = .none
            let dateString = formatter.string(from: date)
            
            let value = $0.element != 0 ? String($0.element) + " " + measurementString : "-"
            
            return (dateString, value)
        }
        
        return values
    }
    
    func getRawData() -> [Float]? {
        var values = progressService.getAllValues()
        let startDate = diet.startedDate ?? Date()
        let daysBetween = Date.daysBetweenTwoDates(startDate,
                                                   secondDate: Date()) ?? 0
        values = Array(values?.prefix(daysBetween + 1) ?? [])
        return values
    }
    
    func getStartDate() -> Date {
        diet.startedDate ?? Date()
    }
    
    func getMeasurementString() -> String {
        measurementService.measurement()
    }
    
    func getTitleString() -> String {
        "HISTORY_OF_CHANGING_\(progressService.type.rawString)".localized()
    }
    
    func getDescriptionString() -> String {
        "HISTORY_OF_CHANGING_\(progressService.type.rawString)_DESCRIPTION".localized()
    }
}
