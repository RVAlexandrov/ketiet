//
//  ProgressDetailByDaysViewController.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 22.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

class ProgressDetailByDaysViewController: UIViewController {
    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private let tableViewCellId = "tableViewCellId"
    private let titleTableViewCellId = "titleTableViewCellId"
    private var preparedDayValues: [(dayString: String, valueString: String)] = []
    
    private var dayValues: [Float]?
    private let dataProvider: ProgressDetailByDaysProviderProtocol
    private let accentColor: UIColor
    
    init(dayValues: [Float]? = nil,
         dataProvider: ProgressDetailByDaysProviderProtocol,
         accentColor: UIColor) {
        self.dayValues = dayValues
        self.dataProvider = dataProvider
        self.accentColor = accentColor
        super.init(nibName: nil, bundle: nil)
    }
    
    private lazy var closeButton: UIButton = {
        let button = UIButton.makeCloseButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addAction(UIAction { [weak self] _ in
            self?.dismiss(animated: true)
        }, for: .touchUpInside)
        return button
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .littBackgroundColor
        tableView.estimatedRowHeight = 41.0
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UITableViewCell.self,
                           forCellReuseIdentifier: tableViewCellId)
        tableView.register(UITableViewHeaderFooterView.self,
                           forHeaderFooterViewReuseIdentifier: titleTableViewCellId)
        tableView.separatorStyle = .singleLine
        return tableView
    }()
    

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if dayValues == nil {
            dayValues = dataProvider.getRawData()
        }
        
        if let values = dayValues, !values.isEmpty {
            preparedDayValues = dataProvider.prepareData(with: values)
        } else {
            preparedDayValues = []
        }
        configureView()
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension ProgressDetailByDaysViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        preparedDayValues.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView()
        var configuration = UIListContentConfiguration.prominentInsetGroupedHeader()
        configuration.text = dataProvider.getTitleString()
        configuration.textProperties.font = .largeTitle
            
        view.contentConfiguration = configuration
        return view
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        dataProvider.getDescriptionString()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellId) else { return UITableViewCell() }
        var conf = UIListContentConfiguration.valueCell()
        conf.secondaryText = preparedDayValues[indexPath.row].valueString
        conf.text = preparedDayValues[indexPath.row].dayString
        conf.secondaryTextProperties.color = accentColor
        
        cell.contentConfiguration = conf
        cell.selectionStyle = .none
        
        return cell
    }
}

// MARK: - Private methods
extension ProgressDetailByDaysViewController {
    private func configureView() {
        view.backgroundColor = .littBackgroundColor
        view.addSubviews([closeButton, tableView])
        
        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: .largeLittMargin),
            closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.littScreenEdgeMargin),

            tableView.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: .mediumLittMargin),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ] + closeButton.closeButtonConstrains())
    }
}
