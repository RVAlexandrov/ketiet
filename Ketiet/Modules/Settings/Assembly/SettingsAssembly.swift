//
//  SettingsAssembly.swift
//  ketiet
//
//  Created by Alex on 15/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import EventKit
import UIKit

final class SettingsAssembly {

    private let view: UIViewController
    
    init() {
        let presenter = SettingsPresenter()
		let interactor = SettingsInteractor(
            presenter: presenter,
            notificationsService: NotificationsService.shared,
            dietService: DietService.shared,
            remindersService: RemindersService(eventStore: EKEventStore.shared),
            mealDatesService: MealDatesService.shared
        )
        let view = SettingsViewController(presenter: presenter)

        presenter.view = view
        presenter.interactor = interactor
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
