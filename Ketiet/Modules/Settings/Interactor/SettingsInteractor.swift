//
//  SettingsInteractor.swift
//  ketiet
//
//  Created by Alex on 15/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation
import UIKit

final class SettingsInteractor {
	
    let remindersService: RemindersServiceProtocol
    private weak var presenter: SettingsInteractorOutputProtocol?
	private let notificationsService: NotificationsServiceProtocol
	private let dietService: DietServiceProtocol
    private let mealDatesService: MealDatesStorageProtocol
	private var observers: [NSObjectProtocol]?
	
	deinit {
		observers?.forEach {
			NotificationCenter.default.removeObserver($0)
		}
		observers = nil
	}
	
    init(presenter: SettingsInteractorOutputProtocol,
		 notificationsService: NotificationsServiceProtocol,
		 dietService: DietServiceProtocol,
		 remindersService: RemindersServiceProtocol,
         mealDatesService: MealDatesStorageProtocol) {
        self.presenter = presenter
		self.notificationsService = notificationsService
		self.dietService = dietService
		self.remindersService = remindersService
        self.mealDatesService = mealDatesService
		addObservers()
    }
	
	private func addObservers() {
        let notificationsObserver = NotificationCenter.default.addObserver(
            forName: .didChangeNotificationsAllowedNotification,
            object: nil,
            queue: .main
        ) { [weak self] _ in
            self?.presenter?.updateNotificationsView()
		}
        let remindersObserver = NotificationCenter.default.addObserver(
            forName: UIApplication.willEnterForegroundNotification,
            object: nil,
            queue: .main
        ) { [weak self] _ in
            self?.presenter?.updateRemindersView()
		}
		observers = [notificationsObserver, remindersObserver]
	}
}

// MARK: - SettingsInteractorInputProtocol
extension SettingsInteractor: SettingsInteractorInputProtocol {
	var notificationsEnabled: Bool {
		get {
			notificationsService.notificationsEnabled
		}
		set {
			notificationsService.notificationsEnabled = newValue
			presenter?.updateNotificationsView()
		}
	}
	
	var notificationsAllowed: Bool {
		notificationsService.notificationsAllowed
	}
	
	func exportMealsToReminders() {
		dietService.requestCurrentDiet { [weak self] diet in
			guard let self = self,
				let diet = diet,
				let currentDayIndex = self.dietService.getCurrentDayIndex() else { return }
			let days = diet.makeDietDays(
                currentDayIndex: currentDayIndex,
                startingFromCurrentDay: true,
                mealDates: self.mealDatesService.currentDates
            )
			let allMeals = days.reduce(into: [DayMeal]()) { $0.append(contentsOf: $1.meals) }
            self.remindersService.addReminders(for: allMeals) { [weak self] result in
                DispatchQueue.main.async {
                    self?.presenter?.didFinishExportToReminders()
                }
            }
		}
	}
}
