//
//  SettingsInteractorInputProtocol.swift
//  ketiet
//
//  Created by Alex on 15/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol SettingsInteractorInputProtocol {
	
    var remindersService: RemindersServiceProtocol { get }
	var notificationsEnabled: Bool { get set }
	var notificationsAllowed: Bool { get }
	func exportMealsToReminders()
}
