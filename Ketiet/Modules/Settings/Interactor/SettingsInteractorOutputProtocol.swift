//
//  SettingsInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 15/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol SettingsInteractorOutputProtocol: AnyObject {
	
	func didFinishExportToReminders()
	func updateNotificationsView()
	func updateRemindersView()
}
