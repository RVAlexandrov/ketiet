//
//  SettingsPresenter.swift
//  ketiet
//
//  Created by Alex on 15/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class SettingsPresenter {

	private lazy var notificationsItem = NotificationsTableViewCellItem(
        description: "NOTIFICATIONS_DESCRIPTIONS".localized(),
        notificationService: NotificationsService.shared) { [weak self] in
            self?.view?.updateLayout()
        }
	private lazy var remindersItem = RemindersExportTableViewCellItem(
        description: "REMINDERS_DESCRIPTION".localized(),
        hasAccessToReminders: interactor?.remindersService.accessGranted ?? false,
        remindersService: interactor?.remindersService ?? RemindersService(eventStore: .shared),
        exportToRemindersHandler: { [weak self] in
            self?.view?.showLoading()
            self?.interactor?.exportMealsToReminders()
        }, layoutUpdate: { [weak self] in
            self?.view?.updateLayout()
        })
    weak var view: SettingsViewInputProtocol?
    var interactor: SettingsInteractorInputProtocol?
    
    private func makeSection(
        title: String,
        item: LittTableViewCellItemProtocol
    ) -> LittTableViewSectionProtocol {
        LittTableViewSection(
            header: SystemHeaderItem(
                text: title
            ),
            items: [item]
        )
    }
}

// MARK: - SettingsViewOutputProtocol
extension SettingsPresenter: SettingsViewOutputProtocol {
    func viewReadyToDisplayData() {
		let writeToUsItem = WriteToUsTableViewCellItem(
            descriptionString: "WRITE_TO_US_DESCRIPTION".localized(),
            email: "ketietdevelopers@gmail.com",
            controller: view
        )
        var sections = [
            makeSection(
                title: "NOTIFICATIONS".localized(),
                item: notificationsItem
            ),
            makeSection(
                title: "REMINDERS".localized(),
                item: remindersItem
            ),
            makeSection(
                title: "WRITE_TO_US".localized(),
                item: writeToUsItem
            )
        ]
        
        let joinTelegramGroupItem = LinkSettingsTableViewCellItem(
            link: URL(string: Locale.current.isRu() ? "https://t.me/+-kMh37Hp3rlmMzIy" : "https://t.me/+1hO8KB3SdoM2NGVi")!,
            descriptionString: "JOIN_OUR_TELEGRAM_COMMUNITY".localized()
        )
        let telegramSection = makeSection(
            title: "Telegram",
            item: joinTelegramGroupItem
        )
        sections.append(telegramSection)
        
        let aboutKetoSection = makeSection(
            title: "ABOUT_KETO_TITLE".localized(),
            item: ValueDisplayingTableViewCellItem(
                title: "HARVARD_SCHOOL_PUBLIC_HEALTH".localized(),
                titleColor: .systemBlue,
                value: "",
                selectionHandler: {
                    if let link = URL(string: "https://www.hsph.harvard.edu/nutritionsource/healthy-weight/diet-reviews/ketogenic-diet/") {
                        UIApplication.shared.open(link)
                    }
                    
                }
            )
        )
        sections.append(aboutKetoSection)
        view?.display(sections: sections)
    }
}

// MARK: - SettingsInteractorOutputProtocol
extension SettingsPresenter: SettingsInteractorOutputProtocol {
	func didFinishExportToReminders() {
		view?.hideLoading()
	}
	
	func updateNotificationsView() {
		notificationsItem.notificationsAllowed = interactor?.notificationsAllowed ?? false
	}
	
	func updateRemindersView() {
        remindersItem.hasAccessToReminders = interactor?.remindersService.accessGranted ?? false
	}
}
