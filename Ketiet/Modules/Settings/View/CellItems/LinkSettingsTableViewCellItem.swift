//
//  SimpleSettingsTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 25.01.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

final class LinkSettingsTableViewCellItem {
    
    private let link: URL
    private let descriptionString: String
    
    init(
        link: URL,
        descriptionString: String
    ) {
        self.link = link
        self.descriptionString = descriptionString
    }
}

extension LinkSettingsTableViewCellItem: LittTableViewCellItemProtocol {
    
    func viewClass() -> AnyClass {
        LinkSettingsTableViewCell.self
    }
    func configure(view: UITableViewCell) {
        guard let cell = view as? LinkSettingsTableViewCell else { return }
        cell.descriptionLabel.text = descriptionString
        cell.joinButton.setTapHandler { [weak self] in
            guard let self = self else { return }
            UIApplication.shared.open(self.link)
        }
    }
}

