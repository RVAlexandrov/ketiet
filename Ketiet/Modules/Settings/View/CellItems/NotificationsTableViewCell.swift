//
//  LocalNotificationsTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 17.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class NotificationsTableViewCell: UITableViewCell {
	
	let descriptionLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .body
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		label.textColor = .systemGray
        label.backgroundColor = .littSecondaryBackgroundColor
		return label
	}()
    
    let goToSettingsButton: LittBasicButton = {
        let button = LittBasicButton(
            title: "ENABLE_NOTIFICATIONS".localized(),
            config: .capsule
        )
        button.setContentCompressionResistancePriority(
            .required,
            for: .vertical
        )
        return button
    }()
	
	let switchView: UISwitch = {
		let view = UISwitch()
		view.translatesAutoresizingMaskIntoConstraints = false
        view.setContentCompressionResistancePriority(.required, for: .horizontal)
		view.onTintColor = .accentColor
        view.backgroundColor = .littSecondaryBackgroundColor
		return view
	}()
    
    private lazy var descriptionLabelToBottomConstraint = descriptionLabel.bottomAnchor.constraint(
        equalTo: contentView.bottomAnchor,
        constant: -.mediumLittMargin
    )
    
    private lazy var goToSettingsButtonToBottomConstraint = contentView.bottomAnchor.constraint(
        equalTo: goToSettingsButton.bottomAnchor,
        constant: .largeLittMargin
    )
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
    
    func setSettingsButtonHidden(_ hidden: Bool) {
        goToSettingsButton.isHidden = hidden
        goToSettingsButtonToBottomConstraint.isActive = !hidden
        descriptionLabelToBottomConstraint.isActive = hidden
    }
	
	private func setupSubviews() {
        selectionStyle = .none
        contentView.addSubviews([descriptionLabel, goToSettingsButton, switchView])
		NSLayoutConstraint.activate([
            descriptionLabel.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: .systemCellSpacing
            ),
            goToSettingsButton.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: .systemCellSpacing
            ),
            switchView.centerYAnchor.constraint(equalTo: descriptionLabel.centerYAnchor),
            contentView.trailingAnchor.constraint(
                equalTo: switchView.trailingAnchor,
                constant: .largeLittMargin
            ),
            descriptionLabel.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: .mediumLittMargin
            ),
            goToSettingsButtonToBottomConstraint,
            descriptionLabel.trailingAnchor.constraint(
                equalTo: switchView.leadingAnchor,
                constant: -.mediumLittMargin
            ),
            goToSettingsButton.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: -.systemCellSpacing
            ),
            descriptionLabel.bottomAnchor.constraint(
                equalTo: goToSettingsButton.topAnchor,
                constant: -.mediumLittMargin
            )
		])
	}
}
