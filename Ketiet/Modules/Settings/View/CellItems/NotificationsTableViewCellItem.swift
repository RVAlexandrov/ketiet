//
//  LocalNotificationsTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 17.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class NotificationsTableViewCellItem: NSObject {
	
	private weak var cell: NotificationsTableViewCell?
	private let descriptionString: String
	private let notificationService: NotificationsServiceProtocol
    private let layoutUpdate: () -> Void
	var notificationsAllowed: Bool {
        didSet {
            DispatchQueue.main.async {
                self.updateCell()
                self.layoutUpdate()
            }
        }
	}
	
	init(
        description: String,
        notificationService: NotificationsServiceProtocol,
        layoutUpdate: @escaping () -> Void
    ) {
		self.notificationsAllowed = notificationService.notificationsAllowed
		self.descriptionString = description
		self.notificationService = notificationService
        self.layoutUpdate = layoutUpdate
		super.init()
	}
	
	private func updateCell() {
		guard let cell = cell else { return }
		configure(view: cell)
	}
	
	private func didTapGoToSettings() {
		if notificationService.onlyPossibleToGetAccessInSettings {
			UIApplication.shared.goToSettings()
		} else {
			notificationService.requestAccess()
		}
	}
	
	@objc private func didChangeSwitch() {
		notificationService.notificationsEnabled.toggle()
	}
}

extension NotificationsTableViewCellItem: LittTableViewCellItemProtocol {
	func viewClass() -> AnyClass {
		NotificationsTableViewCell.self
	}
	
	func configure(view: UITableViewCell) {
		guard let cell = view as? NotificationsTableViewCell else {
			return
		}
		self.cell = cell
		cell.descriptionLabel.text = descriptionString
		cell.switchView.isEnabled = notificationsAllowed
		cell.switchView.alpha = notificationsAllowed ? 1 : 0.5
		cell.switchView.isOn = notificationService.notificationsEnabled
        cell.setSettingsButtonHidden(notificationsAllowed)
        cell.goToSettingsButton.setTapHandler { [weak self] in
            self?.didTapGoToSettings()
        }
		if cell.switchView.allTargets.isEmpty {
			cell.switchView.addTarget(self, action: #selector(didChangeSwitch), for: .valueChanged)
		}
	}
}
