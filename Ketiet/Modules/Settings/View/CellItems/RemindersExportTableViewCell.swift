//
//  RemindersExportTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 17.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class RemindersExportTableViewCell: UITableViewCell {
	
	let descriptionLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .body
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		label.textColor = .systemGray
        label.backgroundColor = .littSecondaryBackgroundColor
		return label
	}()
    
    
    let exportButton = LittBasicButton(
        title: "EXPORT_TO_REMINDERS_APP".localized(),
        config: .capsule
    )
    
    let goToSettingsButton = LittBasicButton(
        title: "ENABLE_REMINDERS".localized(),
        config: .capsule
    )

	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubViews() {
        selectionStyle = .none
		let stack = UIStackView(arrangedSubviews: [descriptionLabel, goToSettingsButton, exportButton])
		stack.spacing = .mediumLittMargin
		stack.axis = .vertical
		stack.translatesAutoresizingMaskIntoConstraints = false
		contentView.addSubview(stack)
		stack.pinToSuperview(insets: UIEdgeInsets(top: .mediumLittMargin,
												  left: .systemCellSpacing,
												  bottom: .largeLittMargin,
												  right: .systemCellSpacing))
	}
}
