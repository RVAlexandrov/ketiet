//
//  RemindersExportTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class RemindersExportTableViewCellItem {
	
	private let exportToRemindersHandler: () -> Void
	private let descriptionString: String
    private let layoutUpdate: () -> Void
    var hasAccessToReminders: Bool {
        didSet {
            DispatchQueue.main.async {
                self.updateCell()
                self.layoutUpdate()
            }
        }
	}
    private let remindersService: RemindersServiceProtocol
	private weak var cell: RemindersExportTableViewCell?
	
	init(
        description: String,
        hasAccessToReminders: Bool,
        remindersService: RemindersServiceProtocol,
        exportToRemindersHandler: @escaping () -> Void,
        layoutUpdate: @escaping () -> Void
    ) {
		self.descriptionString = description
		self.hasAccessToReminders = hasAccessToReminders
        self.remindersService = remindersService
		self.exportToRemindersHandler = exportToRemindersHandler
        self.layoutUpdate = layoutUpdate
	}
	
	private func updateCell() {
		guard let cell = cell else { return }
		configure(view: cell)
	}
	
	private func didTapGoToSettings() {
        if remindersService.shouldAskFirstTimeAccess {
            remindersService.requestAccess { [weak self] granted in
                self?.hasAccessToReminders = granted
            }
        } else {
            UIApplication.shared.goToSettings()
        }
	}
}

extension RemindersExportTableViewCellItem: LittTableViewCellItemProtocol {
	
	func viewClass() -> AnyClass {
		RemindersExportTableViewCell.self
	}
	func configure(view: UITableViewCell) {
		guard let cell = view as? RemindersExportTableViewCell else { return }
		self.cell = cell
		cell.descriptionLabel.text = descriptionString
		cell.exportButton.isHidden = !hasAccessToReminders
		cell.goToSettingsButton.isHidden = hasAccessToReminders
        cell.goToSettingsButton.setTapHandler { [weak self] in
            self?.didTapGoToSettings()
        }
        cell.exportButton.setTapHandler(handler: exportToRemindersHandler)
	}
}
