//
//  WriteToUsTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WriteToUsTableViewCell: UITableViewCell {
	
	let descriptionLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .body
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		label.textColor = .systemGray
        label.backgroundColor = .littSecondaryBackgroundColor
		return label
	}()
    
    let copyButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(.label, for: .normal)
        button.backgroundColor = .littBackgroundColor
        button.titleLabel?.font = .bodySemibold
        button.layer.cornerRadius = 12
        return button
    }()
    
    let writeUsButton = LittBasicButton(
        title: "WRITE_US_BUTTON".localized(),
        config: .capsule
    )
    
    let shareButton = LittBasicButton(
        title: "SHARE_LINK".localized(),
        config: .capsule
    )
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubViews() {
        selectionStyle = .none
		let stack = UIStackView(arrangedSubviews: [descriptionLabel, copyButton, shareButton, writeUsButton])
		stack.translatesAutoresizingMaskIntoConstraints = false
		stack.spacing = .mediumLittMargin
		stack.axis = .vertical
		contentView.addSubview(stack)
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .mediumLittMargin),
            stack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .systemCellSpacing),
            stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.largeLittMargin),
            stack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.systemCellSpacing),
            
            copyButton.heightAnchor.constraint(equalToConstant: 50.33)
        ])
	}
}
