//
//  WriteToUsTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 21.08.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import MessageUI

final class WriteToUsTableViewCellItem: NSObject {
	
	private let descriptionString: String
	private let email: String
	private weak var controller: UIViewController?
	
	init(
        descriptionString: String,
        email: String,
        controller: UIViewController?
    ) {
		self.descriptionString = descriptionString
		self.email = email
		self.controller = controller
		super.init()
	}
	
}

extension WriteToUsTableViewCellItem: LittTableViewCellItemProtocol {
	func viewClass() -> AnyClass {
		WriteToUsTableViewCell.self
	}
	
	func configure(view: UITableViewCell) {
		guard let cell = view as? WriteToUsTableViewCell else { return }
		cell.descriptionLabel.text = descriptionString
        
        if MFMailComposeViewController.canSendMail() {
            cell.writeUsButton.isHidden = false
            cell.writeUsButton.addTarget(self,
                                         action: #selector(didTapWriteUsButton),
                                         for: .touchUpInside)
            
            cell.copyButton.isHidden = true
            cell.shareButton.isHidden = true
        } else {
            cell.copyButton.setTitle(email, for: .normal)
            cell.copyButton.isHidden = false
            cell.copyButton.addTarget(self,
                                      action: #selector(didTapCopyButton),
                                      for: .touchUpInside)
            
            cell.writeUsButton.isHidden = true
            
            cell.shareButton.isHidden = false
            cell.shareButton.addTarget(self,
                                       action: #selector(didTapShareButton),
                                       for: .touchUpInside)
        }
	}
	
	func didSelect() {
		if !MFMailComposeViewController.canSendMail() {
            return
        }
		let mail = MFMailComposeViewController()
		mail.mailComposeDelegate = self
		mail.setToRecipients([email])
		controller?.present(mail, animated: true)
	}
}

extension WriteToUsTableViewCellItem: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController,
							   didFinishWith result: MFMailComposeResult,
							   error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension WriteToUsTableViewCellItem {
    @objc
    private func didTapWriteUsButton() {
        didSelect()
    }
    
    @objc
    private func didTapCopyButton() {
        let pasteboard = UIPasteboard.general
        pasteboard.string = email

        let view = NotificationView(title: "EMAIL_COPIED".localized(),
                                    image: UIImage(systemName: "doc.on.doc",
                                                   withConfiguration: UIImage.SymbolConfiguration(pointSize: 20,
                                                                                                  weight: .semibold)))
        view.showNotificationAnimate()
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
    
    @objc
    private func didTapShareButton() {
        let items = [email]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        controller?.present(ac, animated: true)
    }
}
