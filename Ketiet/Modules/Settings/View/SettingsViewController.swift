//
//  SettingsViewController.swift
//  ketiet
//
//  Created by Alex on 15/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class SettingsViewController: UIViewController {

    private let presenter: SettingsViewOutputProtocol
	
	private lazy var loadingView: LoadingIndicator = {
		let loadingView = LoadingIndicator(frame: UIScreen.main.bounds)
		view.addSubview(loadingView)
		loadingView.translatesAutoresizingMaskIntoConstraints = false
		NSLayoutConstraint.activate([
			loadingView.widthAnchor.constraint(equalTo: view.widthAnchor),
			loadingView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height),
			loadingView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			loadingView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])
		return loadingView
	}()
    
	private lazy var tableView: LittTableView = {
        let tableView = LittTableView(
            frame: UIScreen.main.bounds,
            style: .insetGrouped
        )
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.backgroundColor = .littBackgroundColor
		tableView.separatorStyle = .none
		tableView.setContentInsetForControllersInTabbar()
        tableView.animations = []
		return tableView
	}()

    init(presenter: SettingsViewOutputProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
		view.addSubview(tableView)
		tableView.pinToSuperview()
		navigationItem.title = "SETTINGS".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tableView.sections.isEmpty {
            presenter.viewReadyToDisplayData()
        }
    }
}

// MARK: - SettingsViewInputProtocol
extension SettingsViewController: SettingsViewInputProtocol {
	
	func showLoading() {
		loadingView.startAnimating()
	}
	
	func hideLoading() {
		loadingView.stopAnimating()
	}

	func display(sections: [LittTableViewSectionProtocol]) {
        tableView.sections = sections
	}
    
    func updateLayout() {
        tableView.performBatchUpdates(nil)
    }
}
