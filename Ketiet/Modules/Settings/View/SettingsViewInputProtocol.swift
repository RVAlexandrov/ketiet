//
//  SettingsViewInputProtocol.swift
//  ketiet
//
//  Created by Alex on 15/08/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol SettingsViewInputProtocol: UIViewController {
	func showLoading()
	func hideLoading()
	func display(sections: [LittTableViewSectionProtocol])
    func updateLayout()
}
