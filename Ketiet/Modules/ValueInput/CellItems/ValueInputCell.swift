//
//  ValueInputCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 29.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ValueInputCell: UITableViewCell {
	
	private struct Constants {
		static let imageViewSize: CGFloat = 40
	}
	
	let formatter = NumberFormatter.formatterForFloatingPointValues()
	
    private(set) lazy var textInput = LittContaineredTextFieldView(
        validator: Validator(
            withRules: [FloatValueValidationRule(
                isEmptyAllowed: true,
                isIntermediate: false
            )]),
        keyboardType: .decimalPad,
        delegate: self
    )
	
	weak var item: ValueInputCellItem?
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func showCompleteSign() {
        UINotificationFeedbackGenerator().notificationOccurred(.success)
		textInput.endEditing(true)
	}
	
	private func setupSubviews() {
		contentView.addSubview(textInput)
        textInput.pinToSuperview(insets: UIEdgeInsets(top: .smallLittMargin,
                                                      left: .zero,
													  bottom: .smallLittMargin,
													  right: .zero))
	}
}

extension ValueInputCell: LittContaineredTextFieldViewDelegate {
    
    func textFieldDidEndEditing(view: LittContaineredTextFieldView) {
        if let text = textInput.textView.text,
           !text.isEmpty,
           let preparedValue = formatter.number(from: text)?.floatValue {
            item?.value = preparedValue
        } else {
            item?.value = nil
        }
    }
	
	func didUpdateErrorState(view: LittContaineredTextFieldView) {
		guard let tableView = superview as? UITableView else { return }
		tableView.performBatchUpdates(nil)
	}
}

extension NumberFormatter: ValidationRule {
	func validationResult(_ string: String) -> ValidationResult {
		if numberFrom(string) != nil {
			return ValidationResult(isValid: true, invalidMessage: "")
		}
		return ValidationResult(isValid: false, invalidMessage: "TEXTFIELD_SHOULD_CONTAIN_NUMBER".localized())
	}
	
	func numberFrom(_ string: String) -> NSNumber? {
		let separator = Locale.current.decimalSeparator ?? ""
		let str = string
            .replacingOccurrences(of: ",", with: separator)
            .replacingOccurrences(of: ".", with: separator)
		return number(from: str)
	}
	
	var isIntermediate: Bool {
		false
	}
}
