//
//  ValueInputCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 30.07.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class ValueInputCellItem {
    
    private var _value: Float? = nil
	
	var value: Float? {
        set {
            if let value = newValue {
                let prepared = cell?.formatter.string(from: NSNumber(value: value))
                cell?.textInput.textView.text = prepared
            } else {
                cell?.textInput.textView.text = nil
            }
            _value = newValue
        }
        
        get {
            if let preparedValue = cell?.textInput.textView.text.commaSeparatedValue {
                return Float(preparedValue)
            } else {
                return nil
            }
        }
	}
	private let placeholder: String
	private weak var cell: ValueInputCell?
	
	init(placeholder: String) {
		self.placeholder = placeholder
	}
	
	func showCompleteSign() {
		cell?.showCompleteSign()
	}
    
    func becomeFirstResponder() {
        cell?.textInput.textView.becomeFirstResponder()
    }
}

extension ValueInputCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? ValueInputCell else { return }
		self.cell = cell
		cell.selectionStyle = .none
		cell.item = self
        cell.textInput.setPlaceholder(placeholder)
		if let value = _value, value != 0 {
			cell.textInput.textView.text = cell.formatter.string(from: NSNumber(value: value))
		} else {
			cell.textInput.textView.text = nil
		}
        cell.backgroundColor = .clear
	}
	
	func viewClass() -> AnyClass {
		ValueInputCell.self
	}
}
