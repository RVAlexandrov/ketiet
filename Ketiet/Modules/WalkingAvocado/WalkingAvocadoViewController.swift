//
//  WalkingAvocadoViewController.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 26.03.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit
import Lottie

final class WalkingAvocadoViewController: UIViewController {
    
    private enum Constants {
        static let avocadoVelocity = CGFloat(33)
        static let avocadoSize = CGSize(width: 150, height: 150)
    }
    
    private lazy var avocadoView: AnimationView = {
        let view = AnimationView()
        view.loopMode = .loop
        view.animation = Animation.named("walking_avocado")
        view.addGestureRecognizer(UITapGestureRecognizer(
            target: self,
            action: #selector(didTapAvocado)
        ))
        view.frame.size = Constants.avocadoSize
        return view
    }()
    private var animator: UIViewPropertyAnimator?
    private let completion: (Bool) -> Void
    
    init(completion: @escaping (Bool) -> Void) {
        self.completion = completion
        super.init(nibName: nil, bundle: nil)
        transitioningDelegate = self
        modalPresentationStyle = .custom
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        addWalkingAvocado()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        runAvocado()
    }
    
    private func addWalkingAvocado() {
        let maxAvailableHeight = view.frame.height - Constants.avocadoSize.height
        let y = CGFloat.random(in: (UIApplication.shared.getKeyWindow()?.safeAreaInsets.top ?? 0)...maxAvailableHeight)
        avocadoView.frame.origin = CGPoint(x: -Constants.avocadoSize.width, y: y)
        view.addSubview(avocadoView)
    }
    
    private func runAvocado() {
        avocadoView.play()
        animator = makeAnimator()
        animator?.startAnimation()
    }
    
    private func makeAnimator() -> UIViewPropertyAnimator {
        let distance = view.frame.width + Constants.avocadoSize.width
        let duration = distance / Constants.avocadoVelocity
        let animator = UIViewPropertyAnimator(
            duration: duration,
            curve: .linear
        ) { [weak self] in
            guard let self = self else { return }
            self.avocadoView.transform = CGAffineTransform(translationX: distance, y: 0)
        }
        animator.addCompletion { [weak self] position in
            switch position {
            case .end:
                self?.dismiss(animated: true) {
                    self?.completion(false)
                }
            case .start, .current:
                break
            @unknown default:
                assertionFailure("Unexpected case")
            }
        }
        return animator
    }
    
    @objc private func didTapAvocado() {
        animator?.stopAnimation(true)
        avocadoView.stop()
        UIView.animate(
            withDuration: CATransaction.animationDuration(),
            delay: 0,
            options: []
        ) {
            self.avocadoView.alpha = 0
        } completion: { [weak self] _ in
            self?.dismiss(animated: true) {
                self?.completion(true)
            }
        }
    }
}

extension WalkingAvocadoViewController : UIViewControllerTransitioningDelegate {
    
    func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
        DimmingBackgroundPresentationController(
            presentedViewController: presented,
            presenting: presenting
        )
    }
}
