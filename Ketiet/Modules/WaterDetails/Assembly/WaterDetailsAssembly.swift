//
//  WaterDetailsAssembly.swift
//  ketiet
//
//  Created by Alex on 13/09/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class WaterDetailsAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: WaterDetailsModuleConfiguratorProtocol) {
		let presenter = WaterDetailsPresenter(numberFormatter: NumberFormatter.formatterForIntegerValues(),
											  measurementService: MeasurementService(type: .volume))
        let progressService = EveryDayInputProgressService.generatePreviousProgressService(type: .water,
                                                                                           dietId: moduleConfigurator.diet.id)
		let interactor = WaterDetailsInteractor(progressService: progressService,
                                                profileService: ProfileService.instance,
                                                diet: moduleConfigurator.diet)
        let view = WaterDetailsViewController(presenter: presenter, showCloseButton: moduleConfigurator.showCloseButton)
        let router = WaterDetailsRouter(viewController: view)

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
