//
//  WaterDetailsConfigurator.swift
//  ketiet
//
//  Created by Alex on 13/09/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol WaterDetailsModuleConfiguratorProtocol {
    var showCloseButton: Bool { get }
    var diet: DietProtocol { get }
}

struct WaterDetailsModuleConfigurator: WaterDetailsModuleConfiguratorProtocol {
    let showCloseButton: Bool
    let diet: DietProtocol
}
