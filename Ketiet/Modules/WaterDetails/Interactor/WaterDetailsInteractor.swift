//
//  WaterDetailsInteractor.swift
//  ketiet
//
//  Created by Alex on 13/09/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

struct WaterDetailsInteractor {

	let progressService: ProgressServiceProtocol
    let profileService: ProfileServiceGetProtocol
    let diet: DietProtocol
	
	init(progressService: ProgressServiceProtocol,
         profileService: ProfileServiceGetProtocol,
         diet: DietProtocol) {
		self.progressService = progressService
        self.profileService = profileService
        self.diet = diet
    }
}

// MARK: - WaterDetailsInteractorInputProtocol
extension WaterDetailsInteractor: WaterDetailsInteractorInputProtocol {}
