//
//  WaterDetailsPresenter.swift
//  ketiet
//
//  Created by Alex on 13/09/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import Foundation

final class WaterDetailsPresenter {

	private let numberFormatter: NumberFormatter
	private let measurementService: MeasurementServiceProtocol
    weak var view: WaterDetailsViewInputProtocol?
    var interactor: WaterDetailsInteractorInputProtocol?
    var router: WaterDetailsRouterInputProtocol?

    init(numberFormatter: NumberFormatter,
		 measurementService: MeasurementServiceProtocol) {
		self.numberFormatter = numberFormatter
		self.measurementService = measurementService
    }
}

// MARK: - WaterDetailsViewOutputProtocol
extension WaterDetailsPresenter: WaterDetailsViewOutputProtocol {
	func viewAppeared() {
		guard let interactor = interactor else {
			assertionFailure("No interactor")
			return
		}
		let header = SingleLabelCellItem(text: "WATER".localized(), textAlignment: .left)
		
		let graphItem = ProgressGraphTableViewCellItem(
            service: interactor.progressService,
            accentColor: .waterAccentColor
        )
		
		let measurement = measurementService.measurement()
		
        interactor.profileService.getTargetWater { [weak self] targetWater in
            guard let self = self else { return }
            let targetWaterLevelString = self.numberFormatter.string(from: NSNumber(value: targetWater)) ?? ""
            let waterByDays = interactor.progressService.getAllValues() ?? []
            let averageWater = waterByDays.filter{ $0 != 0 }.average()
            let averageWaterString = self.numberFormatter.string(from: NSNumber(value: averageWater)) ?? ""
            
            let targetAndAverageItem = DetailTwoItemCellItem(
                firstViewModel: DetailViewModel(
                    title: "TARGET_WATER_LEVEL".localized(),
                    value: targetWaterLevelString + " " + measurement,
                    valueDiff: nil,
                    valueDiffTrend: nil
                ),
                secondViewModel: DetailViewModel(
                    title: "DAYLY_AVERAGE".localized(),
                    value: averageWaterString + " " + measurement,
                    valueDiff: nil,
                    valueDiffTrend: nil
                )
            )
            
            let sumWaterString = self.numberFormatter.string(from: NSNumber(value: waterByDays.sum())) ?? ""
            let sumWaterItem = DetailOneItemCellItem(model: DetailViewModel(title: "TOTAL".localized(),
                                                                            value: sumWaterString + " " + measurement,
                                                                            valueDiff: nil,
                                                                            valueDiffTrend: nil))
            
            let recommendationsItem = SingleLabelCellItem(text: "WATER_INTAKE_RECOMMENDATION".localized(),
                                                          font: .body,
                                                          textColor: .systemGray2)
            
            let waterByDaysItem = ButtonTableViewCellItem(title: "SHOW_BY_DAYS".localized(),
                                                          color: .waterAccentColor) { [weak self] in
                guard let self = self, let diet = self.interactor?.diet else { return }
                self.router?.presentDetailScreen(withDiet: diet)
            }
            
            self.view?.displaySections(sections: [LittTableViewSection(items: [header,
                                                                               graphItem,
                                                                               targetAndAverageItem,
                                                                               sumWaterItem,
                                                                               waterByDaysItem,
                                                                               recommendationsItem])])
        }
    }
}
