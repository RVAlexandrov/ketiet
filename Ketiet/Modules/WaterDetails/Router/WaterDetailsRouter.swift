//
//  WaterDetailsRouter.swift
//  ketiet
//
//  Created by Alex on 13/09/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WaterDetailsRouter {

    private weak var viewController: UIViewController?

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

// MARK: - WaterDetailsRouterInputProtocol
extension WaterDetailsRouter: WaterDetailsRouterInputProtocol {
    func presentDetailScreen(withDiet diet: DietProtocol) {
        let fabric = ValuesByDaysViewControllerFabric()
        let vc = fabric.defaultValuesByDaysViewController(type: .water,
                                                          diet: diet)
        viewController?.present(vc, animated: true)
    }
}
