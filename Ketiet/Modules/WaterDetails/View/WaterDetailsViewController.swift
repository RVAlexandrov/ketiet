//
//  WaterDetailsViewController.swift
//  ketiet
//
//  Created by Alex on 13/09/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class WaterDetailsViewController: DismissableDetailsViewController {

    private let presenter: WaterDetailsViewOutputProtocol

	init(presenter: WaterDetailsViewOutputProtocol, showCloseButton: Bool = true) {
        self.presenter = presenter
        super.init(showCloseButton: showCloseButton)
    }
	
	@available(*, unavailable)
	required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .littBackgroundColor
        tableView.backgroundColor = .littBackgroundColor
        presenter.viewAppeared()
        navigationController?.navigationBar.prefersLargeTitles = false
    }
}

// MARK: - WaterDetailsViewInputProtocol
extension WaterDetailsViewController: WaterDetailsViewInputProtocol {
	func displaySections(sections: [LittTableViewSectionProtocol]) {
		tableView.sections = sections
	}
}
