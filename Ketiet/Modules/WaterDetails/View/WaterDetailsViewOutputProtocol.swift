//
//  WaterDetailsViewOutputProtocol.swift
//  ketiet
//
//  Created by Alex on 13/09/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol WaterDetailsViewOutputProtocol {
    func viewAppeared()
}
