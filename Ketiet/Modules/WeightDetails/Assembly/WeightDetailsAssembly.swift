//
//  WeightDetailsAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 25/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class WeightDetailsAssembly {

    private let view: UIViewController
    
    init(showCloseButton: Bool = true, diet: DietProtocol) {
        let presenter = WeightDetailsPresenter(measurementService: MeasurementService(type: .weight))
        let progressService = EveryDayInputProgressService.generatePreviousProgressService(type: .weight,
                                                                                           dietId: diet.id)
        let interactor = WeightDetailsInteractor(presenter: presenter,
                                                 progressService: progressService,
                                                 diet: diet)
        let view = WeightDetailsViewController(presenter: presenter,
                                               showCloseButton: showCloseButton)
        let router = WeightDetailsRouter(viewController: view)

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
