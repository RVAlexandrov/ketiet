//
//  WeightDetailsInteractor.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 25/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//
import Foundation

final class WeightDetailsInteractor {
	
    private weak var presenter: WeightDetailsInteractorOutputProtocol?
    let progressService: ProgressServiceProtocol
    let diet: DietProtocol
	
    init(presenter: WeightDetailsInteractorOutputProtocol,
         progressService: ProgressServiceProtocol,
         diet: DietProtocol) {
        self.presenter = presenter
        self.progressService = progressService
        self.diet = diet
    }
}

// MARK: - WeightDetailsInteractorInputProtocol
extension WeightDetailsInteractor: WeightDetailsInteractorInputProtocol {
    func getWeightData() {
        var values = progressService.getAllValues()
        let startDate = diet.startedDate ?? Date()
        let daysBetween = Date.daysBetweenTwoDates(startDate,
                                                   secondDate: Date()) ?? 0
        values = Array(values?.prefix(daysBetween + 1) ?? [])
        if let values = values {
            presenter?.didGetWeightData(data: values,
                                        isCurrentDiet: diet.status == .inProgress)
        } else {
            presenter?.didFailGetWeightData()
        }
    }
}
