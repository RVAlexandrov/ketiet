//
//  WeightDetailsInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 25/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WeightDetailsInteractorOutputProtocol: AnyObject {
    func didGetWeightData(data: [Float], isCurrentDiet: Bool)
    func didFailGetWeightData()
}
