//
//  WeightDetailsPresenter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 25/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class WeightDetailsPresenter {

    weak var view: WeightDetailsViewInputProtocol?
    var interactor: WeightDetailsInteractorInputProtocol?
    var router: WeightDetailsRouterInputProtocol?
    private let measurementService: MeasurementServiceProtocol

    private var weightData: [Float]?
    private var isViewDidAppear = false
    private var isCurrentDiet = false

    init(measurementService: MeasurementServiceProtocol) {
        self.measurementService = measurementService
    }
}

// MARK: - WeightDetailsViewOutputProtocol
extension WeightDetailsPresenter: WeightDetailsViewOutputProtocol {
    func viewDidLoad() {
        interactor?.getWeightData()
    }
}

// MARK: - WeightDetailsInteractorOutputProtocol
extension WeightDetailsPresenter: WeightDetailsInteractorOutputProtocol {
    func didGetWeightData(data: [Float], isCurrentDiet: Bool) {
        self.isCurrentDiet = isCurrentDiet
        weightData = data
        configureSections()
    }
    
    func didFailGetWeightData() {
        weightData = []
        configureSections()
    }
}

// MARK: - Private methods
extension WeightDetailsPresenter {
    private func configureSections() {
        guard let weightByDays = weightData, let interactor = interactor else { return }
        let filteredWeightByDays = weightByDays.filter { $0 != 0 }
        
        let mdValueString = filteredWeightByDays.count > 1 ? (String((filteredWeightByDays.diffMdValue() ?? 0).round(to: 2))) + " " + measurementService.measurement() : "-"
        
        var generalDevidedString: String?
        var generalDevidedTrend: Trend?
        if filteredWeightByDays.count >= 2 && filteredWeightByDays.last != 0 && filteredWeightByDays.first != 0 {
            let generalDiffDevided = (Float(filteredWeightByDays.last ?? 1.0)) / (Float(filteredWeightByDays.first ?? 1.0))
            if generalDiffDevided <= 1 {
                generalDevidedString = "-" + ((1.0 - generalDiffDevided) * 100).round(to: 0).cleanValue + "%"
                generalDevidedTrend = .positive
            } else {
                generalDevidedString = "+" + ((generalDiffDevided - 1.0) * 100).round(to: 0).cleanValue + "%"
                generalDevidedTrend = .negative
            }
        } else {
            generalDevidedString = nil
            generalDevidedTrend = nil
        }
        
        var generalAbsoluteString: String
        if let last = filteredWeightByDays.last, let first = filteredWeightByDays.first, last != 0, first != 0 {
            
            // если был введён только один день
            if first == last && filteredWeightByDays.count == 1 {
                generalAbsoluteString = "WEIGHT_DETAIL_POOR_DATA".localized()
            } else {
                let generalDiffAbsolute = last - first
                generalAbsoluteString = String(generalDiffAbsolute.round(to: 2)) + " " + measurementService.measurement()
            }
        } else if filteredWeightByDays.first == 0 {
            generalAbsoluteString = "WEIGHT_DETAILS_UNKNOWED_START_WEIGHT".localized()
        } else if filteredWeightByDays.last == 0 {
            generalAbsoluteString = "WEIGHT_DETAILS_INPUT_START_WEIGHT".localized()
        } else {
            generalAbsoluteString = "WEIGHT_DETAIL_POOR_DATA".localized()
        }
        
        let header = SingleLabelCellItem(text: "WEIGHT".localized(),
                                         textAlignment: .left)
        let graphItem = ProgressGraphTableViewCellItem(
            service: interactor.progressService,
            accentColor: .weightAccentColor
        )
        
        let lastValue = weightByDays.last?.round(to: 1)
        let lastValueString = lastValue != 0 ? (lastValue?.cleanValue ?? "-") + " " + measurementService.measurement() : "-"
        
        // Сегодня и среднее за день
        let currentWeight = DetailTwoItemCellItem(firstViewModel: DetailViewModel(title: isCurrentDiet ? "WEIGHT_DETAIL_TODAY".localized() : "LAST_DAY".localized(),
                                                                                  value: lastValueString,
                                                                                  valueDiff: nil,
                                                                                  valueDiffTrend: nil),
                                                  secondViewModel: DetailViewModel(title: "DAYLY_AVERAGE".localized(),
                                                                                   value: mdValueString,
                                                                                   valueDiff: nil,
                                                                                   valueDiffTrend: nil))
        
        // Общее изменение
        let statsItem = DetailOneItemCellItem(model: DetailViewModel(title: "WEIGHT_DETAIL_ALL_DIFF".localized(),
                                                                     value: generalAbsoluteString,
                                                                     valueDiff: generalDevidedString,
                                                                     valueDiffTrend: generalDevidedTrend))
                
        let weightByDaysItem = ButtonTableViewCellItem(title: "SHOW_BY_DAYS".localized(),
                                                       color: .weightAccentColor) { [weak self] in
            guard let self = self,  let diet = self.interactor?.diet else { return }
            self.router?.showWeightDetailByDays(withDiet: diet)
        }
        
        view?.displaySections(sections: [LittTableViewSection(items: [header,
                                                                      graphItem,
                                                                      currentWeight,
                                                                      statsItem,
                                                                      weightByDaysItem])])
        
    }
}

extension Array {
    func diffMdValue() -> Float? {
        guard (first as? Float) != nil else { return nil }
        
        var tempValue: Float = 0
        var sum: Float = 0
        
        enumerated().forEach {
            if $0.offset % 2 == 0 {
                tempValue = $0.element as! Float
            } else {
                sum = sum + ($0.element as! Float) - tempValue
                tempValue = 0
            }
        }
        
        return sum / (Float(count) / 2.0)
    }
}
