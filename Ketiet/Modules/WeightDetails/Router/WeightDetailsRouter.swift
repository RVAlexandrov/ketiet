//
//  WeightDetailsRouter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 25/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class WeightDetailsRouter {

    private weak var viewController: UIViewController?

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

// MARK: - WeightDetailsRouterInputProtocol
extension WeightDetailsRouter: WeightDetailsRouterInputProtocol {
    func showWeightDetailByDays(withDiet diet: DietProtocol) {
        let fabric = ValuesByDaysViewControllerFabric()
        let vc = fabric.defaultValuesByDaysViewController(type: .weight, diet: diet)
        viewController?.present(vc, animated: true)
    }
}
