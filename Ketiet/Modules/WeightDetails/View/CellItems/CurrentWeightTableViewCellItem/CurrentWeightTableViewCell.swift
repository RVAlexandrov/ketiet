//
//  CurrentWeightTableViewCellI.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 28.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class CurrentWeightTableViewCell: UITableViewCell {
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var firstContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .littBackgroundColor
        view.layer.cornerRadius = 12.0
        return view
    }()
    
    private lazy var secondContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .littBackgroundColor
        view.layer.cornerRadius = 12.0
        return view
    }()
    
    private lazy var currentTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .subTitle
		label.text = "TODAY".localized()
        return label
    }()
    
    private lazy var diffTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .subTitle
		label.text = "DAY_AVERAGE".localized()
        return label
    }()
    
    lazy var diffValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        return label
    }()
    
    lazy var currentValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        return label
    }()
    
    lazy var percentDiffLabel = ChipLabelView(text: "+0.5%",textColor: .systemRed)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupView()
    }
}

// MARK: - Private methods
extension CurrentWeightTableViewCell {
    private func setupView() {
        contentView.addSubviews([firstContainerView, secondContainerView])
        firstContainerView.addSubviews([currentTitleLabel, currentValueLabel, percentDiffLabel])
        secondContainerView.addSubviews([diffTitleLabel, diffValueLabel])
        NSLayoutConstraint.activate([
            firstContainerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallLittMargin),
            firstContainerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            firstContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.smallLittMargin),
            firstContainerView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 2 - .largeLittMargin - .mediumLittMargin),
            
            currentTitleLabel.topAnchor.constraint(equalTo: firstContainerView.topAnchor, constant: .mediumLittMargin),
            currentTitleLabel.leadingAnchor.constraint(equalTo: firstContainerView.leadingAnchor, constant: .mediumLittMargin),
            
            currentValueLabel.topAnchor.constraint(equalTo: currentTitleLabel.bottomAnchor, constant: .mediumLittMargin),
            currentValueLabel.leadingAnchor.constraint(equalTo: firstContainerView.leadingAnchor, constant: .mediumLittMargin),
            currentValueLabel.bottomAnchor.constraint(equalTo: firstContainerView.bottomAnchor, constant: -.mediumLittMargin),
            
            percentDiffLabel.trailingAnchor.constraint(equalTo: firstContainerView.trailingAnchor, constant: -.mediumLittMargin),
            percentDiffLabel.centerYAnchor.constraint(equalTo: currentValueLabel.centerYAnchor),
            
            secondContainerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallLittMargin),
            secondContainerView.leadingAnchor.constraint(equalTo: firstContainerView.trailingAnchor, constant: .mediumLittMargin),
            secondContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.smallLittMargin),
            secondContainerView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 2 - .largeLittMargin - .mediumLittMargin),
            
            diffTitleLabel.topAnchor.constraint(equalTo: secondContainerView.topAnchor, constant: .mediumLittMargin),
            diffTitleLabel.leadingAnchor.constraint(equalTo: secondContainerView.leadingAnchor, constant: .mediumLittMargin),
            
            diffValueLabel.topAnchor.constraint(equalTo: diffTitleLabel.bottomAnchor, constant: .mediumLittMargin),
            diffValueLabel.leadingAnchor.constraint(equalTo: secondContainerView.leadingAnchor, constant: .mediumLittMargin),
            diffValueLabel.bottomAnchor.constraint(equalTo: secondContainerView.bottomAnchor, constant: -.mediumLittMargin),
        ])
    }
}
