//
//  WeightStatsTableViewCell.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 25.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WeightStatsTableViewCell: UITableViewCell {
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .littBackgroundColor
        view.layer.cornerRadius = 12.0
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .subTitle
		label.text = "WHOLE_DROP".localized()
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        return label
    }()
    
    lazy var percentDiffLabel = ChipLabelView(text: "-10%",textColor: .systemGreen)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupView()
    }
}

extension WeightStatsTableViewCell {
    private func setupView() {
        contentView.addSubview(containerView)
        containerView.addSubviews([titleLabel, valueLabel, percentDiffLabel])
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .smallLittMargin),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .largeLittMargin),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.smallLittMargin),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
            
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .mediumLittMargin),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .mediumLittMargin),
            
            valueLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .mediumLittMargin),
            valueLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .mediumLittMargin),
            valueLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -.mediumLittMargin),
            
            percentDiffLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.mediumLittMargin),
            percentDiffLabel.centerYAnchor.constraint(equalTo: valueLabel.centerYAnchor)
        ])
    }
}
