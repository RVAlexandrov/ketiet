//
//  WeightStatsTableViewCellItem.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 25.10.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WeightStatsTableViewCellItem {
    
    private let diffValue: String
    private let diffPercentValue: String?
    private let diffPercentColor: UIColor
    
    init(diffValue: String,
         diffPercentValue: String?,
         diffPercentColor: UIColor) {
        self.diffValue = diffValue
        self.diffPercentValue = diffPercentValue
        self.diffPercentColor = diffPercentColor
    }
    
}

// MARK: - LittTableViewCellItemProtocol
extension WeightStatsTableViewCellItem: LittTableViewCellItemProtocol {
    func configure(view: UITableViewCell) {
        guard let cell = view as? WeightStatsTableViewCell else { return }
        cell.valueLabel.text = diffValue
        cell.percentDiffLabel.setText(diffPercentValue ?? "-")
        cell.percentDiffLabel.isHidden = diffPercentValue == nil
        cell.percentDiffLabel.setColor(diffPercentColor)
    }
    
    func viewClass() -> AnyClass {
        WeightStatsTableViewCell.self
    }
}
