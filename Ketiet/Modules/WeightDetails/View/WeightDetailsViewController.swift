//
//  WeightDetailsViewController.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 25/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WeightDetailsViewController: DismissableDetailsViewController {

    private let presenter: WeightDetailsViewOutputProtocol
    
    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

	init(presenter: WeightDetailsViewOutputProtocol, showCloseButton: Bool = true) {
        self.presenter = presenter
		super.init(showCloseButton: showCloseButton)
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        tableView.backgroundColor = .littBackgroundColor
        view.backgroundColor = .littBackgroundColor
        navigationController?.navigationBar.prefersLargeTitles = false
    }
}

// MARK: - WeightDetailsViewInputProtocol
extension WeightDetailsViewController: WeightDetailsViewInputProtocol {
    func displaySections(sections: [LittTableViewSectionProtocol]) {
        tableView.sections = sections
    }
}
