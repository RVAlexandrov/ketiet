//
//  WeightDetailsViewOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 25/10/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol WeightDetailsViewOutputProtocol {
    func viewDidLoad()
}
