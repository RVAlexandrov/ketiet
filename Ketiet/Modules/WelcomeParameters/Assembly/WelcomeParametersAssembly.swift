//
//  WelcomeParametersAssembly.swift
//  ketiet
//
//  Created by Alex on 26/04/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class WelcomeParametersAssembly {

    private let view: UIViewController
    
    init(profile: ProfileProtocol? = nil,
        profileService: ProfileServiceProtocol,
        mealDatesStorage: MealDatesStorageProtocol) {
        var name: String?
        var age: String?
        var weight: String?
        var tall: String?
        
        if let profile = profile {
            name = profile.name
            age = profile.age != nil ? String(profile.age ?? 0) : nil
            tall = profile.tall != nil ? String(profile.tall ?? 0) : nil
        }
                
        if let weightProfile = profile?.weight {
            let floatWeight = Float(weightProfile).round(to: 1)
            weight = String(floatWeight)
        }
        
        let presenter = WelcomeParametersPresenter(profile: profile)
        let interactor = WelcomeParametersInteractor(profileService: profileService)
		let weightService = MeasurementService(type: .weight)
		let tallService = MeasurementService(type: .tall)
		let view = ParametersViewController(title: "ABOUT_YOU".localized(),
											validatorFactory: ValidatorFactory(),
											parameters: [ParameterItem(title: "INPUT_NAME".localized(),
																	   valueType: .text,
                                                                       defaultValue: name.map { LittValue.text($0) }),
														 ParameterItem(title: "INPUT_AGE".localized(),
																	   valueType: .int,
                                                                       defaultValue: age.map { LittValue.text($0) }),
														 ParameterItem(title: "INPUT_WEIGHT".localized() + ", " + weightService.measurement(),
																	   valueType: .float,
                                                                       defaultValue: weight.map { LittValue.text($0) }),
														 ParameterItem(title: "INPUT_TALL".localized() + ", " + tallService.measurement(),
																	   valueType: .float,
                                                                       defaultValue: tall.map { LittValue.text($0) })],
											buttonTitle: "CONTINUE".localized(),
                                            presenter: presenter)
        let router = WelcomeParametersRouter(
            presenter: presenter,
            viewController: view,
            profileService: profileService,
            mealDatesStorage: mealDatesStorage
        )
		
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
