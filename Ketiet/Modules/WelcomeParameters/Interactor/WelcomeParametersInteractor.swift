//
//  WelcomeParametersInteractor.swift
//  ketiet
//
//  Created by Alex on 26/04/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

final class WelcomeParametersInteractor {

    private let profileService: ProfileServiceProtocol
	
    init(profileService: ProfileServiceProtocol) {
        self.profileService = profileService
    }
}

// MARK: - WelcomeParametersInteractorInputProtocol
extension WelcomeParametersInteractor: WelcomeParametersInteractorInputProtocol {
	func save(
        weight: Double?,
        tall: Double?,
        name: String?,
        age: Int?
    ) {
        if let weight = weight {
            profileService.setInitialWeight(weight, completion: { _ in })
        }
        if let tall = tall {
            profileService.setInitialTall(tall, completion: { _ in })
        }
		if let name = name {
			profileService.setName(name, completion: { _ in })
		}
		if let age = age {
			profileService.setAge(age, completion: { _ in })
		}
    }
}
