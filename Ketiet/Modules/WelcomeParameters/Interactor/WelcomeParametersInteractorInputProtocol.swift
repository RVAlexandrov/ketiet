//
//  WelcomeParametersInteractorInputProtocol.swift
//  ketiet
//
//  Created by Alex on 26/04/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WelcomeParametersInteractorInputProtocol {
	func save(
        weight: Double?,
        tall: Double?,
        name: String?,
        age: Int?
    )
}
