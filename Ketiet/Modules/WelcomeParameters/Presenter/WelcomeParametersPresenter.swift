//
//  WelcomeParametersPresenter.swift
//  ketiet
//
//  Created by Alex on 26/04/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

final class WelcomeParametersPresenter {

    var interactor: WelcomeParametersInteractorInputProtocol?
    var router: WelcomeParametersRouterInputProtocol?
    let profile: ProfileProtocol?
    
    init(profile: ProfileProtocol?) {
        self.profile = profile
    }
}

// MARK: - GenderChooseModuleOutputProtocol
extension WelcomeParametersPresenter: GenderChooseModuleOutputProtocol {
    func genderChooseDidTapBackButton() {
        router?.closeGenderChooseScreen()
    }
}

extension WelcomeParametersPresenter: ParametersPresenterProtocol {
    func didCompleteValuesInput(values: [LittValue?]) {
        var weight: Double?
        var tall: Double?
        if let weightValue = values[safe: 2]??.float {
            weight = Double(weightValue)
        }
        if let tallValue = values.last??.float {
            tall = Double(tallValue)
        }
        interactor?.save(
            weight: weight,
            tall: tall,
            name: values.first??.text,
            age: values[safe: 1]??.int
        )
        router?.showGenderChooseScreen(
            gender: profile?.gender,
            goal: profile?.goal,
            physicalActivity: profile?.physicalActivity
        )
    }
}
