//
//  WelcomeParametersRouter.swift
//  ketiet
//
//  Created by Alex on 26/04/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore
import UIKit

final class WelcomeParametersRouter {
    
    private weak var presenter: GenderChooseModuleOutputProtocol?
    private(set) weak var viewController: UIViewController?
    private weak var genderChooseViewController: UIViewController?
    private let profileService: ProfileServiceProtocol
    private let mealDatesStorage: MealDatesStorageProtocol

    init(
        presenter: GenderChooseModuleOutputProtocol,
        viewController: UIViewController,
        profileService: ProfileServiceProtocol,
        mealDatesStorage: MealDatesStorageProtocol
    ) {
        self.presenter = presenter
        self.viewController = viewController
        self.profileService = profileService
        self.mealDatesStorage = mealDatesStorage
    }
}

// MARK: - WelcomeParametersRouterInputProtocol
extension WelcomeParametersRouter: WelcomeParametersRouterInputProtocol {
    func showGenderChooseScreen(gender: Gender?, goal: Goal?, physicalActivity: PhysicalActivity?) {
        let assembly: GenderChooseAssembly = AssemblyFactory()
            .generateAssembly(
                moduleConfigurator: GenderChooseModuleConfigurator(
                    parentModule: presenter,
                    gender: gender,
                    goal: goal,
                    physicalActivity: physicalActivity,
                    profileService: profileService,
                    mealDatesStorage: mealDatesStorage
                )
            )
        genderChooseViewController = assembly.initialViewController()
        viewController?.navigationController?.pushViewController(assembly.initialViewController(), animated: true)
    }
    
    func closeGenderChooseScreen() {
        guard viewController?.navigationController?.topViewController == genderChooseViewController else { return }
        viewController?.navigationController?.popViewController(animated: true)
    }
}
