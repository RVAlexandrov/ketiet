//
//  WelcomeParametersRouterInputProtocol.swift
//  ketiet
//
//  Created by Alex on 26/04/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import DietCore

protocol WelcomeParametersRouterInputProtocol {
    func showGenderChooseScreen(gender: Gender?, goal: Goal?, physicalActivity: PhysicalActivity?)
    func closeGenderChooseScreen()
}
