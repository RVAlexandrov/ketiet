//
//  WelcomeResumeAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WelcomeResumeAssembly: BaseAssembly {

    private let view: UIViewController

    init(moduleConfigurator: WelcomeResumeModuleConfiguratorProtocol) {
        let presenter = WelcomeResumePresenter()
        let interactor = WelcomeResumeInteractor(
            presenter: presenter,
            dietService: DietService.shared,
            profileService: moduleConfigurator.profileService,
            mealsDateService: moduleConfigurator.mealsDateService
        )
        let view = WelcomeResumeViewController(presenter: presenter)
        let router = WelcomeResumeRouter(viewController: view)
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
