//
//  WelcomeResumeConfigurator.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WelcomeResumeModuleConfiguratorProtocol {
    var mealsDateService: MealDatesStorageProtocol { get }
    var profileService: ProfileServiceProtocol { get }
    var customTargetCalories: Float? { get }
}

struct WelcomeResumeModuleConfigurator: WelcomeResumeModuleConfiguratorProtocol {
    let mealsDateService: MealDatesStorageProtocol
    let profileService: ProfileServiceProtocol
    let customTargetCalories: Float?
}
