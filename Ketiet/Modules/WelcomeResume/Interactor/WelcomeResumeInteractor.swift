//
//  WelcomeResumeInteractor.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class WelcomeResumeInteractor {
	
    private weak var presenter: WelcomeResumeInteractorOutputProtocol?
	private let dietService: DietServiceProtocol
	private let profileService: ProfileServiceProtocol
    private let mealsDateService: MealDatesStorageProtocol
    private var diet: Diet?
	
	init(presenter: WelcomeResumeInteractorOutputProtocol,
		 dietService: DietServiceProtocol,
		 profileService: ProfileServiceProtocol,
         mealsDateService: MealDatesStorageProtocol) {
        self.presenter = presenter
		self.dietService = dietService
		self.profileService = profileService
		self.mealsDateService = mealsDateService
    }
}

// MARK: - WelcomeResumeInteractorInputProtocol
extension WelcomeResumeInteractor: WelcomeResumeInteractorInputProtocol {
    
    func prepareDiet() {
        profileService.getProfile { [weak self] profile in
            guard let self = self else { return }
            guard let profile = profile else {
                self.presenter?.didFailToCreateDiet(
                    error: NSError(
                        domain: "",
                        code: 1,
                        userInfo: [NSLocalizedDescriptionKey: "Unable to read your profile data"]
                    )
                )
                return
            }
            
            var dailyCalories: Float
            
            let caloriesCalculatorFactory = CaloriesCalculatorFactory()
            let caloriesCalculator = caloriesCalculatorFactory.buildCalculator(for: profile)
            dailyCalories = caloriesCalculator.calculateDailyCalories()
            self.profileService.setDailyCalories(dailyCalories, completion: { _ in })
            
            let ketoFoodNutrientsCalculator = KetoFoodNutrientsCalculator()
            let dailyNutrients = ketoFoodNutrientsCalculator.calculateFoodNutrients(
                withCalories: dailyCalories
            )
            
            let mealsProvider = KetoDietMealProvider(
                dailyNutritiens: dailyNutrients,
                dateProvider: CustomMealDatesProvider(
                    components: self.mealsDateService.currentDates
                )
            )
            let dietComposer = KetoDietComposer(
                mealsProvider: mealsProvider,
                numberOfDays: 28
            )
            DietService.shared.requestAllDiets(compeltion: { [weak self] diets in
                guard let self = self else { return }

                let lastId = diets?.last?.id ?? 0
                
                let diet = dietComposer.makeDiet(withId: lastId + 1)
                self.diet = diet
                self.presenter?.didCreatedDiet(diet: diet, withFoodNutrients: dailyNutrients)
            })
        }
    }
	
	func registerProfile() {
		profileService.profileRegistered = true
	}
    
    func saveDiet(completion: @escaping() -> Void) {
        guard let diet = diet else { return }
        dietService.setCurrentDiet(diet) { result in
            switch result {
            case .success:
                break
            case let .failure(error):
                assertionFailure(error.localizedDescription)
            }
            completion()
        }
    }
}
