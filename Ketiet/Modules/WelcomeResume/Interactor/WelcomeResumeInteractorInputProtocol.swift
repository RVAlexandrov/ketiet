//
//  WelcomeResumeInteractorInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WelcomeResumeInteractorInputProtocol {
	func registerProfile()
    func saveDiet(completion: @escaping() -> Void)
    func prepareDiet()
}
