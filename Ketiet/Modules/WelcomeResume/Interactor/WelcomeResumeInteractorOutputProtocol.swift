//
//  WelcomeResumeInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WelcomeResumeInteractorOutputProtocol: AnyObject {
    func didCreatedDiet(diet: DietProtocol,
                        withFoodNutrients foodNutrinets: FoodNutrients)
	func didFailToCreateDiet(error: Error)
}
