//
//  WelcomeResumePresenter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import Foundation

final class WelcomeResumePresenter {

    weak var view: WelcomeResumeViewInputProtocol?
    var interactor: WelcomeResumeInteractorInputProtocol?
    var router: WelcomeResumeRouterInputProtocol?
        
    private(set) var diet: DietProtocol?
    private(set) var foodNutrients: FoodNutrients?
}

// MARK: - WelcomeResumeViewOutputProtocol
extension WelcomeResumePresenter: WelcomeResumeViewOutputProtocol {
	
	func requestDiet() {
		view?.showLoading()
		interactor?.prepareDiet()
	}
	
	func didTapContinueButton() {
        interactor?.registerProfile()
        interactor?.saveDiet { [weak self] in
            DispatchQueue.main.async {
                self?.router?.showMainScreen()
            }
        }
	}
	
	func didSelectMeal(_ meal: DietMealProtocol) {
		router?.showDishes(for: meal)
	}
}

// MARK: - WelcomeResumeInteractorOutputProtocol
extension WelcomeResumePresenter: WelcomeResumeInteractorOutputProtocol {
	
    func didCreatedDiet(diet: DietProtocol,
                        withFoodNutrients foodNutrinets: FoodNutrients) {
        self.diet = diet
        self.foodNutrients = foodNutrinets
		DispatchQueue.main.async {
			self.view?.hideLoading()
		}
	}

	func didFailToCreateDiet(error: Error) {
		DispatchQueue.main.async {
			self.view?.hideLoading()
		}
	}
}
