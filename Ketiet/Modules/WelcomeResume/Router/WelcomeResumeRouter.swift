//
//  WelcomeResumeRouter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WelcomeResumeRouter {

    private weak var viewController: UIViewController?

    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

// MARK: - WelcomeResumeRouterInputProtocol
extension WelcomeResumeRouter: WelcomeResumeRouterInputProtocol {
	
	func showMainScreen() {
        guard let window = UIApplication.shared.getKeyWindow() else {
            assertionFailure("No key window")
            return
        }
		window.rootViewController = LittTabBarController()
        UIView.transition(
            with: window,
            duration: CATransaction.animationDuration(),
            options: .transitionCrossDissolve,
            animations: nil
        )
	}
	
	func showDishes(for meal: DietMealProtocol) {
		let assembly: MealDetailAssembly = AssemblyFactory().generateAssembly(
            moduleConfigurator: MealDetailModuleConfigurator(
                mealIndex: 0,
                meals: [meal],
                showCloseButton: false
            )
        )
		viewController?.navigationController?.pushViewController(assembly.initialViewController(), animated: true)
	}
}
