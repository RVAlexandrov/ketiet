//
//  WelcomeResumeRouterInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WelcomeResumeRouterInputProtocol {
	func showMainScreen()
	func showDishes(for meal: DietMealProtocol)
}
