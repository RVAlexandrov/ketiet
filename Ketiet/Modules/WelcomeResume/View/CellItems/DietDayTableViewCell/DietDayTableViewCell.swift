//
//  DietDayTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietDayTableViewCell: BaseWithoutTitleUndelayedTableViewCell {
	
	private struct Constants {
		static let chevronSize = CGSize(width: 11, height: 17)
	}
	
	private(set) lazy var titleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .subTitle
		label.numberOfLines = 0
		label.lineBreakMode = .byWordWrapping
		return label
	}()
	
	private lazy var chevron: UIImageView = {
		let view = UIImageView(image: UIImage(systemName: "chevron.right"))
		view.translatesAutoresizingMaskIntoConstraints = false
		view.tintColor = .systemGray
		return view
	}()
	
	var chevronExpanded = false {
		didSet {
			UIView.animate(withDuration: CATransaction.animationDuration(),
						   delay: 0,
						   options: [.curveEaseOut],
						   animations: {
				self.chevron.transform = self.chevronExpanded ? CGAffineTransform(rotationAngle: CGFloat.pi/2) : .identity
                self.separatorView.isHidden = !self.chevronExpanded
			})
		}
	}

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setupSubviews() {
        selectionStyle = .default
        
        separatorView.isHidden = true
        
        contentView.addSubviews([titleLabel, chevron])
		NSLayoutConstraint.activate([
			titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .littScreenEdgeMargin),
			titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .largeLittMargin),
			titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.largeLittMargin),
			titleLabel.trailingAnchor.constraint(equalTo: chevron.leadingAnchor, constant: -.largeLittMargin),
			chevron.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
			chevron.widthAnchor.constraint(equalToConstant: Constants.chevronSize.width),
			chevron.heightAnchor.constraint(equalToConstant: Constants.chevronSize.height),
			chevron.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin)
		])
	}
}
