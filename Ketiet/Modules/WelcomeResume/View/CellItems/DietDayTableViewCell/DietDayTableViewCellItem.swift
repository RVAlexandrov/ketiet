//
//  DietDayTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietDayTableViewCellItem {
	
	private let dayNumber: Int
	private var didSelectHandler: (() -> Void)?
	private weak var cell: DietDayTableViewCell?
	private var expanded = false {
		didSet {
			if oldValue == expanded {
				return
			}
			cell?.chevronExpanded = expanded
		}
	}
	
	init(dayNumber: Int) {
		self.dayNumber = dayNumber
	}
}

extension DietDayTableViewCellItem: LittTableViewFirstExpandableSectionItem {
	func configure(view: UITableViewCell) {
		guard let cell = view as? DietDayTableViewCell else { return }
		cell.titleLabel.text = "DAY".localized() + " \(dayNumber)"
		cell.chevronExpanded = expanded
		self.cell = cell
	}
	
	func viewClass() -> AnyClass {
		DietDayTableViewCell.self
	}
	
	func didSelect() {
		expanded.toggle()
		didSelectHandler?()
	}
	
	func setTapHandler(_ handler: @escaping () -> Void) {
		didSelectHandler = handler
	}
}
