//
//  DietMealTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietMealTableViewCell: BaseWithoutTitleUndelayedTableViewCell {
	
	private struct Constants {
		static let chevronSize = CGSize(width: 11, height: 17)
	}
	
	private(set) lazy var titleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
		label.lineBreakMode = .byWordWrapping
		label.numberOfLines = 0
		return label
	}()
	
	private lazy var caloriesTitleLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .body
        label.textColor = .systemGray
		label.text = "CALORIES".localized() + ": "
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
		return label
	}()
    
    private(set) lazy var dishLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .body
        return label
    }()
	
	private(set) lazy var caloriesValueLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false
		label.font = .bodySemibold
        label.textColor = .accentColor
		return label
	}()
    
    private(set) lazy var eatenImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(systemName: "checkmark.circle.fill"))
        imageView.tintColor = .systemGreen
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
        selectionStyle = .default
        separatorView.isHidden = true
        
		let chevron = UIImageView(image: UIImage(systemName: "chevron.right"))
		chevron.translatesAutoresizingMaskIntoConstraints = false
		chevron.tintColor = .accentColor
        
        contentView.addSubviews([titleLabel, eatenImageView, dishLabel, caloriesValueLabel, caloriesTitleLabel, chevron])
		NSLayoutConstraint.activate([
			titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .mediumLittMargin),
			titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .littScreenEdgeMargin),
			titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: chevron.leadingAnchor, constant: -.littScreenEdgeMargin),
            titleLabel.bottomAnchor.constraint(equalTo: dishLabel.topAnchor, constant: -.mediumLittMargin),
            
            eatenImageView.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: .mediumLittMargin),
            eatenImageView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            eatenImageView.heightAnchor.constraint(equalToConstant: 15),
            eatenImageView.widthAnchor.constraint(equalToConstant: 15),
            
            dishLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .littScreenEdgeMargin),
            dishLabel.trailingAnchor.constraint(equalTo: chevron.leadingAnchor, constant: -.logicBlockLittMargin),
            
			caloriesTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .littScreenEdgeMargin),
            caloriesTitleLabel.topAnchor.constraint(equalTo: dishLabel.bottomAnchor, constant: .mediumLittMargin),
            
			caloriesValueLabel.leadingAnchor.constraint(equalTo: caloriesTitleLabel.trailingAnchor),
			caloriesValueLabel.trailingAnchor.constraint(equalTo: chevron.leadingAnchor, constant: -.smallLittMargin),
			caloriesValueLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.largeLittMargin),
            caloriesValueLabel.topAnchor.constraint(equalTo: caloriesTitleLabel.topAnchor),
            
			chevron.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.largeLittMargin),
			chevron.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
			chevron.widthAnchor.constraint(equalToConstant: Constants.chevronSize.width),
			chevron.heightAnchor.constraint(equalToConstant: Constants.chevronSize.height)
		])
	}
}
