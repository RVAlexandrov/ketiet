//
//  DietMealTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 13.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietMealTableViewCellItem {
	private let meal: DietMealProtocol
	private let didSelectHandler: (DietMealProtocol) -> Void
	
	init(meal: DietMealProtocol,
		 didSelectHandler: @escaping(DietMealProtocol) -> Void) {
		self.meal = meal
		self.didSelectHandler = didSelectHandler
	}
}

extension DietMealTableViewCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? DietMealTableViewCell else { return }
		cell.titleLabel.text = meal.name
		cell.caloriesValueLabel.text = Nutrient(type: .calories, value: meal.calories).stringValue
        cell.dishLabel.text = meal.dishes.first?.name ?? ""
        cell.eatenImageView.isHidden = !meal.isEaten
	}
	
	func viewClass() -> AnyClass {
		DietMealTableViewCell.self
	}
	
	func didSelect() {
		didSelectHandler(meal)
	}
}
