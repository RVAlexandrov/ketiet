//
//  DietNutriensTableViewCell.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 12.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietNutriensTableViewCell: UITableViewCell {
	
	private struct Constants {
		static let containerCornerRadius: CGFloat = 15
	}
	
    private(set) lazy var fatVStack = BasicNutrientVStack(
        nutrient: Nutrient(
            type: .fat,
            value: 0
        ),
        color: .littFatColor
    )
    private(set) lazy var proteinVStack = BasicNutrientVStack(
        nutrient: Nutrient(
            type: .protein,
            value: 0
        ),
        color: .littProteinColor
    )
    private(set) lazy var carbohydrateVStack = BasicNutrientVStack(
        nutrient: Nutrient(
            type: .carbohydrate,
            
            value: 0
        ),
        color: .littCarbohydratesColor
    )
    private(set) lazy var calorieVStack = BasicNutrientVStack(
        nutrient: Nutrient(
            type: .calories,
            value: 0
        ),
        color: .nutrientAccentColor
    )
    
    private let separatorView: UIView = {
        let separatorView = UIView()
        separatorView.backgroundColor = .systemGray2
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        return separatorView
    }()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
		contentView.backgroundColor = .clear
		backgroundColor = .clear
		
		let hStack = UIStackView(arrangedSubviews: [fatVStack, proteinVStack, carbohydrateVStack])
		hStack.axis = .horizontal
		hStack.alignment = .fill
		hStack.distribution = .fillEqually
		hStack.translatesAutoresizingMaskIntoConstraints = false
        hStack.spacing = 16
		
		contentView.addSubviews([calorieVStack, separatorView, hStack])
		
		NSLayoutConstraint.activate([
			calorieVStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .largeLittMargin),
			calorieVStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			calorieVStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			
			hStack.topAnchor.constraint(equalTo: calorieVStack.bottomAnchor, constant: .largeLittMargin),
			hStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			hStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
			hStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
		])
	}
}
