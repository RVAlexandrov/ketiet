//
//  DietNutriensTableViewCellItem.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 12.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class DietNutriensTableViewCellItem {
	
	private let proteins: Float
	private let carbohydrates: Float
	private let fats: Float
	private let calories: Float
	
	init(proteins: Float,
		 carbohydrates: Float,
		 fats: Float,
		 calories: Float) {
		self.proteins = proteins
		self.carbohydrates = carbohydrates
		self.fats = fats
		self.calories = calories
	}
}

extension DietNutriensTableViewCellItem: LittTableViewCellItemProtocol {
	func configure(view: UITableViewCell) {
		guard let cell = view as? DietNutriensTableViewCell else { return }
		cell.selectionStyle = .none
		cell.proteinVStack.nutrient = Nutrient(type: .protein, value: proteins.rounded())
		cell.fatVStack.nutrient = Nutrient(type: .fat, value: fats.rounded())
		cell.carbohydrateVStack.nutrient = Nutrient(type: .carbohydrate, value: carbohydrates.rounded())
        cell.calorieVStack.nutrient = Nutrient(type: .calories, value: calories.rounded())
	}
	
	func viewClass() -> AnyClass {
		DietNutriensTableViewCell.self
	}
}
