//
//  WelcomeResumeViewController.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit
import Lottie

final class WelcomeResumeViewController: UIViewController {

    private let presenter: WelcomeResumeViewOutputProtocol
    private weak var alertVc: AnimationAlertViewController?
	
	private lazy var loadingIndicator: LoadingIndicator = {
		let indicator = LoadingIndicator()
		indicator.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(indicator)
		indicator.pinToSuperview()
		return indicator
	}()
	
	private lazy var tableView: LittTableView = {
        let tableView = LittTableView(frame: UIScreen.main.bounds, style: .insetGrouped)
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.separatorStyle = .singleLine
		tableView.backgroundColor = .littBackgroundColor
		return tableView
	}()
	
	init(presenter: WelcomeResumeViewOutputProtocol) {
		self.presenter = presenter
		super.init(nibName: nil, bundle: nil)
	}

	@available(*, unavailable)
	required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

    // MARK: - Life cycle
	override func loadView() {
		view = UIView()
		view.backgroundColor = .littBackgroundColor
		view.addSubview(tableView)
		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
		])
		let buttonAndOffset = addBottomButtonWithBlur(with: "CONTINUE".localized())
		buttonAndOffset.0.setTapHandler { [weak self] in self?.presenter.didTapContinueButton() }
		tableView.contentInset.bottom = buttonAndOffset.1
		tableView.verticalScrollIndicatorInsets.bottom = buttonAndOffset.1
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		presenter.requestDiet()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.cellForRow(at: selectedIndexPath)?.setSelected(false, animated: true)
        }
        littTabBarController?.setTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        littTabBarController?.setTabBarHidden(false)
    }
}

// MARK: - WelcomeResumeViewInputProtocol
extension WelcomeResumeViewController: WelcomeResumeViewInputProtocol {

    func showLoading() {
        let alert = AnimationAlertViewController(
            animationPath: "loading_sphere",
            title: "PREPARE_DIET_MESSAGE".localized(),
            didDismissAction: { [weak self] in
                self?.showDiet(withCustomNutrients: self?.presenter.foodNutrients)
                self?.title = "WELCOME_RESUME_TITLE".localized()
            })
        alertVc = alert
        present(alert, animated: true)
    }
	
	func hideLoading() {
        alertVc?.dismissAfterAnimationEnds()
	}
	
    func showDiet(withCustomNutrients customFoodNutrients: FoodNutrients?) {
        guard let diet = presenter.diet else { return }
        
        let customCalories = customFoodNutrients?.calories
        let dayCalories: Float = customCalories != nil ? customCalories ?? 0 : diet.dayCalories()
        let nutriensSection = LittTableViewSection(
            header: PlainHeaderItem(text: "NUTRIENTS_PER_DAY".localized()),
            items: [
                DietNutriensTableViewCellItem(
                    proteins: customFoodNutrients?.proteins ?? 0,
                    carbohydrates: customFoodNutrients?.carbohydrates ?? 0,
                    fats: customFoodNutrients?.fat ?? 0,
                    calories: dayCalories
                )
            ]
        )
        
        let menuTitle = SingleLabelCellItem(text: "FIRST_28_MENU".localized(),
                                            textAlignment: .left)
		let daysSections = diet.days.enumerated().map { pair -> LittTableViewSectionProtocol in
			let firstItem = DietDayTableViewCellItem(dayNumber: pair.offset + 1)
			let mealsItems = pair.element.meals.map { [weak self] meal in
				DietMealTableViewCellItem(meal: meal) { dietMeal in
					self?.presenter.didSelectMeal(dietMeal)
				}
			}
			return LittTableViewExpandableSection(firstSectionItem: firstItem,
												  sectionIndex: pair.offset + 2,
												  items: mealsItems,
												  tableView: tableView)
		}
        
        let daysTitleSection = LittTableViewSection(items: [menuTitle])
        
		tableView.sections = [nutriensSection, daysTitleSection] + daysSections
	}
}
