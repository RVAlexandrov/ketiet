//
//  WelcomeResumeViewInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol WelcomeResumeViewInputProtocol: AnyObject {	
    func showDiet(withCustomNutrients customFoodNutrients: FoodNutrients?)
	func showLoading()
	func hideLoading()
}
