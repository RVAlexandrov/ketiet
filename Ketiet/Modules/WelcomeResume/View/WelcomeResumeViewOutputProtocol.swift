//
//  WelcomeResumeViewOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 14/05/2020.
//  Copyright © 2020 littDev. All rights reserved.
//


protocol WelcomeResumeViewOutputProtocol {
	func requestDiet()
	func didTapContinueButton()
	func didSelectMeal(_ meal: DietMealProtocol)
    
    var foodNutrients: FoodNutrients? { get }
    var diet: DietProtocol? { get }
}
