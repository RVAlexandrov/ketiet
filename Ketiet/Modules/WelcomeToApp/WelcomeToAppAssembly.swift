//
//  WelcomeToAppAssembly.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 11.12.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WelcomeToAppAssembly {
	
	func makeViewController() -> UIViewController {
		let configureDietItem = InfoViewControllerItem(icon: UIImage(systemName: "checkmark.circle") ?? UIImage(),
													   title: "FLEXIBLE".localized(),
													   description: "CONFIGURE_DIET_FOR_YOURSELF".localized())
		let convenientDietItem = InfoViewControllerItem(icon: UIImage(systemName: "checkmark.circle") ?? UIImage(),
														title: "CONVENIENT".localized(),
														description: "CONVENIENT_DIET_DESCRIPTION".localized())
		let attentiveDietItem = InfoViewControllerItem(icon: UIImage(systemName: "checkmark.circle") ?? UIImage(),
													   title: "ATTENTIVE".localized(),
													   description: "ATTENTIVE_DIET_DESCRIPTION".localized())
		let sociableDietItem = InfoViewControllerItem(icon: UIImage(systemName: "checkmark.circle") ?? UIImage(),
													  title: "SOCIABLE".localized(),
													  description: "SOCIABLE_DIET_DESCRIPTION".localized())
		let noAddDietItem = InfoViewControllerItem(icon: UIImage(systemName: "xmark.circle") ?? UIImage(),
												   title: "NO_ADDS".localized(),
												   description: "NO_ADDS_DESCRIPTION".localized(),
												   tintColor: .littFailureColor)
        return InfoViewController(headerTitle: "WELCOME_TO_LITT_DIET".localized() + "\n😌 ☺️ 😎",
                                  infoItems: [configureDietItem,
                                              convenientDietItem,
                                              attentiveDietItem,
                                              sociableDietItem,
                                              noAddDietItem],
                                  buttonTitle: "CONTINUE".localized()) { controller in
            let alertFactory = AlertControllerFactory()
            let actions = [
                
                AlertAction(
                    title: "CHECK_IT_OUT".localized(),
                    handler: {
                        if let link = URL(string: "https://www.hsph.harvard.edu/nutritionsource/healthy-weight/diet-reviews/ketogenic-diet/") {
                            UIApplication.shared.open(link)
                        }
                    }
                ),
                
                AlertAction(
                    title: "I_DID_IT".localized(),
                    handler: {
                        let actions = [
                            AlertAction(
                                title: "OK".localized(),
                                handler: {
                                    let assembly = WelcomeParametersAssembly(
                                        profileService: ProfileService.instance,
                                        mealDatesStorage: MealDatesService.shared
                                    )
                                    controller.navigationController?.setNavigationBarHidden(false, animated: false)
                                    controller.navigationController?.pushViewController(assembly.initialViewController(), animated: true)
                                }
                            )
                        ]
                        
                        let alert = alertFactory.makeAlert(title: "PROVIDE_US_SOME_INFORMATION".localized(),
                                                           description: "WE_NEED_INFO_ABOUT_YOU".localized(),
                                                           actions: actions)
                        controller.present(alert, animated: true)
                    }
                )
            ]
            
            let alert = alertFactory.makeAlert(title: "IMPORTANT".localized(),
                                               description: "IMPORTANT_DESCRIPTION".localized(),
                                               actions: actions)
            controller.present(alert, animated: true)
        }
    }
}
