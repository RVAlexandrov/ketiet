//
//  WidgetsConfigurationAssembly.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WidgetsConfigurationAssembly: BaseAssembly {

    private let view: UIViewController
    
    init(moduleConfigurator: WidgetsConfigurationModuleConfiguratorProtocol) {
        let presenter = WidgetsConfigurationPresenter(parentModule: moduleConfigurator.parentModule, currentWidgets: moduleConfigurator.currentWidgets)
        let interactor = WidgetsConfigurationInteractor(presenter: presenter)
        let view = WidgetsConfigurationViewController(presenter: presenter)

        presenter.view = view
        presenter.interactor = interactor
        self.view = view
    }

    func initialViewController() -> UIViewController {
        return view
    }
}
