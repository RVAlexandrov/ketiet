//
//  WidgetsConfigurationConfigurator.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol WidgetsConfigurationModuleConfiguratorProtocol {
    var parentModule: WidgetsConfigurationModuleOutputProtocol? { get set }
    var currentWidgets: [WidgetType] { get set }
}

final class WidgetsConfigurationModuleConfigurator: WidgetsConfigurationModuleConfiguratorProtocol {
    var parentModule: WidgetsConfigurationModuleOutputProtocol?
    var currentWidgets: [WidgetType]

    init(parentModule: WidgetsConfigurationModuleOutputProtocol?, currentWidgets: [WidgetType]) {
        self.parentModule = parentModule
        self.currentWidgets = currentWidgets
    }
}
