//
//  WidgetsConfigurationInteractor.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

final class WidgetsConfigurationInteractor {
	
    private weak var presenter: WidgetsConfigurationInteractorOutputProtocol?
	
    init(presenter: WidgetsConfigurationInteractorOutputProtocol) {
        self.presenter = presenter
    }
}

// MARK: - WidgetsConfigurationInteractorInputProtocol
extension WidgetsConfigurationInteractor: WidgetsConfigurationInteractorInputProtocol {
    func getAllUnusedWidgets(currentWidgets: [WidgetType]) {
        var allWidgetTypes = WidgetType.allCases
        for widget in currentWidgets {
            allWidgetTypes.remove(at: allWidgetTypes.firstIndex(of: widget) ?? 0)
        }
        presenter?.didGetUnusedWidgets(allWidgetTypes)
    }
}
