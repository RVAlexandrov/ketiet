//
//  WidgetsConfigurationInteractorInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WidgetsConfigurationInteractorInputProtocol {
    func getAllUnusedWidgets(currentWidgets: [WidgetType])
}
