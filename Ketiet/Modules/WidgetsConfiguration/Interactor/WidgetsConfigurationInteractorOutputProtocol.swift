//
//  WidgetsConfigurationInteractorOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WidgetsConfigurationInteractorOutputProtocol: AnyObject {
    func didGetUnusedWidgets(_ widgets: [WidgetType])
}
