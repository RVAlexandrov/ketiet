//
//  WidgetsConfigurationModuleOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

protocol WidgetsConfigurationModuleOutputProtocol: AnyObject {
    func didTapCloseButton()
    func didChangeWidgetsOrder(with widgets: [WidgetType])
}
