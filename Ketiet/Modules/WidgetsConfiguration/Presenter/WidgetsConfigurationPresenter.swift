//
//  WidgetsConfigurationPresenter.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WidgetsConfigurationPresenter {

    private weak var parentModule: WidgetsConfigurationModuleOutputProtocol?
    private(set) var currentWidgets: [WidgetType]
    private(set) var unusedWidgets: [WidgetType] = WidgetType.allCases
    weak var view: WidgetsConfigurationViewInputProtocol?
    var interactor: WidgetsConfigurationInteractorInputProtocol?

    init(parentModule: WidgetsConfigurationModuleOutputProtocol?, currentWidgets: [WidgetType]) {
        self.parentModule = parentModule
        self.currentWidgets = currentWidgets
    }
}

// MARK: - WidgetsConfigurationViewOutputProtocol
extension WidgetsConfigurationPresenter: WidgetsConfigurationViewOutputProtocol {    
    func didMoveRow(from startIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if destinationIndexPath.section == startIndexPath.section {
            if destinationIndexPath.section == 0 {
                // в текущих виджетах переместили на другую позицию
                let movedWidget = currentWidgets[startIndexPath.row]
                currentWidgets.remove(at: startIndexPath.row)
                currentWidgets.insert(movedWidget, at: destinationIndexPath.row)
            } else {
                // во всех виджетах переместили на другую позицию
                let movedWidget = unusedWidgets[startIndexPath.row]
                unusedWidgets.remove(at: startIndexPath.row)
                unusedWidgets.insert(movedWidget, at: destinationIndexPath.row)
            }
        } else {
            if destinationIndexPath.section == 0 {
                // из всех виджетов в текущие переместили
                let movedWidget = unusedWidgets[startIndexPath.row]
                unusedWidgets.remove(at: startIndexPath.row)
                currentWidgets.insert(movedWidget, at: destinationIndexPath.row)
            } else {
                // из текущих виджетов переместили во все
                let movedWidget = currentWidgets[startIndexPath.row]
                currentWidgets.remove(at: startIndexPath.row)
                unusedWidgets.insert(movedWidget, at: destinationIndexPath.row)
            }
        }
        parentModule?.didChangeWidgetsOrder(with: currentWidgets)
    }
    
    func didTapInsertButton(_ index: Int) {
        currentWidgets.append(unusedWidgets[index])
        unusedWidgets.remove(at: index)
        view?.updateTableView(insertIndexPath: IndexPath(row: currentWidgets.count - 1, section: 0), deleteIndexPath: IndexPath(row: index, section: 1))
        parentModule?.didChangeWidgetsOrder(with: currentWidgets)
    }
    
    func didTapDeleteButton(_ index: Int) {
        unusedWidgets.append(currentWidgets[index])
        currentWidgets.remove(at: index)
        view?.updateTableView(insertIndexPath: IndexPath(row: unusedWidgets.count - 1, section: 1), deleteIndexPath: IndexPath(row: index, section: 0))
        parentModule?.didChangeWidgetsOrder(with: currentWidgets)
    }

    func didTapDoneButton() {
        parentModule?.didTapCloseButton()
    }
    
    func viewDidLoad() {
        interactor?.getAllUnusedWidgets(currentWidgets: currentWidgets)
    }
}

// MARK: - WidgetsConfigurationInteractorOutputProtocol
extension WidgetsConfigurationPresenter: WidgetsConfigurationInteractorOutputProtocol {
    func didGetUnusedWidgets(_ widgets: [WidgetType]) {
        unusedWidgets = widgets
    }
}
