//
//  WidgetsConfigurationViewController.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class WidgetsConfigurationViewController: UIViewController {

    private let presenter: WidgetsConfigurationViewOutputProtocol

    init(presenter: WidgetsConfigurationViewOutputProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private let tableViewCellId = "tableViewCellId"
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView.init(frame: .zero, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: tableViewCellId)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.isScrollEnabled = false
        tableView.isEditing = true
        tableView.separatorInset = .zero
        return tableView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.alwaysBounceHorizontal = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .largeTitle
        label.numberOfLines = 0
		label.text = "WIDGETS".localized()
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .smallBody
        label.textColor = .systemGray
        label.numberOfLines = 0
		label.text = "WIDGETS_CONFIGURATION_DESCRIPTION".localized()
        return label
    }()

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        configureView()
    }
}

// MARK: - WidgetsConfigurationViewInputProtocol
extension WidgetsConfigurationViewController: WidgetsConfigurationViewInputProtocol {
    func updateTableView(insertIndexPath: IndexPath, deleteIndexPath: IndexPath) {
        tableView.beginUpdates()
        tableView.deleteRows(at: [deleteIndexPath], with: .fade)
        tableView.insertRows(at: [insertIndexPath], with: .fade)
        tableView.endUpdates()
    }
}

// MARK: - UITableViewDelegate
extension WidgetsConfigurationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        presenter.didMoveRow(from: sourceIndexPath, to: destinationIndexPath)
        if sourceIndexPath.section != destinationIndexPath.section {
            CATransaction.setCompletionBlock({
                tableView.reloadRows(at: [destinationIndexPath], with: .none)
            })
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        indexPath.section == 0 ? UITableViewCell.EditingStyle.delete : UITableViewCell.EditingStyle.insert
    }
}

// MARK: - UITableViewDataSource
extension WidgetsConfigurationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .insert {
            presenter.didTapInsertButton(indexPath.row)
        }
        
        if editingStyle == .delete {
            presenter.didTapDeleteButton(indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return presenter.currentWidgets.count
        } else {
            return presenter.unusedWidgets.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellId)!
        cell.selectionStyle = .default
        let model = indexPath.section == 0 ? presenter.currentWidgets[indexPath.row] : presenter.unusedWidgets[indexPath.row]
        var conf = cell.defaultContentConfiguration()
        conf.text = model.generalTitle
        conf.image = UIImage(systemName: model.imageString)
        conf.imageProperties.tintColor = model.imageStringColor
        cell.contentConfiguration = conf
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        (section == 0 ? "CURRENT_WIDGETS" : "MORE_WIDGETS_EXTENDED").localized()
    }
}

// MARK: - Private methods
extension WidgetsConfigurationViewController {
    private func configureView() {
        view.backgroundColor = .littBackgroundColor
        tableView.isScrollEnabled = false
        
        let rightButton = LittCapsuleButton(title: "DONE".localized())
        rightButton.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
        
        view.addSubviews([titleLabel, tableView, rightButton])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: .largeLittMargin),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .logicBlockLittMargin),
            titleLabel.trailingAnchor.constraint(equalTo: rightButton.leadingAnchor, constant: .largeLittMargin),
            
            rightButton.topAnchor.constraint(equalTo: view.topAnchor, constant: .largeLittMargin),
            rightButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.logicBlockLittMargin),
            
            tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
    @objc
    private func didTapDoneButton() {
        presenter.didTapDoneButton()
    }
}
