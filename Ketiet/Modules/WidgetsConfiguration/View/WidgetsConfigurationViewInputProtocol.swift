//
//  WidgetsConfigurationViewInputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

protocol WidgetsConfigurationViewInputProtocol: AnyObject {
    func updateTableView(insertIndexPath: IndexPath, deleteIndexPath: IndexPath)
}
