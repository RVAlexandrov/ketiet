//
//  WidgetsConfigurationViewOutputProtocol.swift
//  ketiet
//
//  Created by Roman Aleksandrov on 06/07/2020.
//  Copyright © 2020 littDev. All rights reserved.
//
import UIKit

protocol WidgetsConfigurationViewOutputProtocol {
    func viewDidLoad()
    func didTapDoneButton()
    func didTapInsertButton(_ index: Int)
    func didTapDeleteButton(_ index: Int)
    func didMoveRow(from startIndexPath: IndexPath, to destinationIndexPath: IndexPath)

    
    var currentWidgets: [WidgetType] { get }
    var unusedWidgets: [WidgetType] { get }
}
