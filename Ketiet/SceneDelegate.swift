//
//  SceneDelegate.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 26.04.2020.
//  Copyright © 2020 littDev. All rights reserved.
//

import UIKit

final class SceneDelegate: UIResponder {
	
	var window: UIWindow?
}

extension SceneDelegate: UIWindowSceneDelegate {

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let appearance = UINavigationBarAppearance()
        appearance.shadowColor = nil
        
        let secondAppearance = UINavigationBarAppearance()
        secondAppearance.configureWithTransparentBackground()
        
        UINavigationBar.appearance().scrollEdgeAppearance = secondAppearance
        UINavigationBar.appearance().standardAppearance = appearance
        
        window = UIWindow(windowScene: windowScene)
		if ProfileService.instance.profileRegistered {
			window?.rootViewController = LittTabBarController()
		} else {
			UIApplication.shared.showWelcomeViewController(window: window)
		}
        window?.makeKeyAndVisible()
        DietService.shared.notificationCenter.addObserver(forName: .didFinishCurrentDietNotification,
														  object: nil,
														  queue: .main) { _ in
			ProfileService.instance.getGoal { [weak self] goal in
				guard let goal = goal else { return }
				let controller = DietFinishAssembly().makeFinishViewController(goal: goal)
				self?.window?.rootViewController?.present(controller, animated: true)
			}
		}
    }
}

