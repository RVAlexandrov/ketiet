//
//  main.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 28.03.2022.
//  Copyright © 2022 littDev. All rights reserved.
//

import UIKit

UIApplicationMain(
    CommandLine.argc, CommandLine.unsafeArgv,
    NSStringFromClass(DietApplication.self), NSStringFromClass(AppDelegate.self)
)
