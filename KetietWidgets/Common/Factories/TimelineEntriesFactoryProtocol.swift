//
//  TimelineEntriesFactory.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import WidgetKit

protocol TimelineEntriesFactoryProtocol {
    associatedtype Entry: TimelineEntry
    
    func makePlaceholderEntry(family: WidgetFamily) -> Entry
    func makePreviewEntry(family: WidgetFamily) -> Entry
    func makeEntryForSnapshot(family: WidgetFamily) -> Entry
    func makeEntryForStartOfDate(date: Date, family: WidgetFamily) -> Entry
}
