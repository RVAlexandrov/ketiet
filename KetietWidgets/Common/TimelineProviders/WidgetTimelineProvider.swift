//
//  WidgetTimelineProvider.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import WidgetKit

struct WidgetTimelineProvider<Factory: TimelineEntriesFactoryProtocol> {
    
    private let factory: Factory
    
    init(factory: Factory) {
        self.factory = factory
    }
}
 
extension WidgetTimelineProvider: TimelineProvider {

    func placeholder(in context: Context) -> Factory.Entry {
        factory.makePlaceholderEntry(family: context.family)
    }

    func getSnapshot(
        in context: Context,
        completion: @escaping (Factory.Entry) -> ()
    ) {
        let entry = context.isPreview ?
        factory.makePreviewEntry(family: context.family) :
        factory.makeEntryForSnapshot(family: context.family)
        completion(entry)
    }

    func getTimeline(
        in context: Context,
        completion: @escaping (Timeline<Factory.Entry>) -> ()
    ) {
        let calendar = Calendar.current
        guard let daysRange = calendar.maximumRange(of: .day) else {
            let timeline = Timeline(
                entries: [Factory.Entry](),
                policy: .never
            )
            completion(timeline)
            return
        }
        var entries: [Factory.Entry] = daysRange.compactMap {
            guard let newDate = calendar.date(
                byAdding: .day,
                value: $0,
                to: Date()
            ) else { return nil }
            return factory.makeEntryForStartOfDate(
                date: calendar.startOfDay(for: newDate),
                family: context.family
            )
        }
        entries.insert(
            factory.makeEntryForSnapshot(family: context.family),
            at: 0
        )
        let timeline = Timeline(
            entries: entries,
            policy: entries.isEmpty ? .never : .atEnd
        )
        completion(timeline)
    }
}

