//
//  DailyNutrients.swift
//  DailyNutrients
//
//  Created by Kovalev Alexander on 15.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import WidgetKit
import SwiftUI
import DietCore

struct DailyNutrientsWidget: Widget {

    var body: some WidgetConfiguration {
        StaticConfiguration(
            kind: WidgetType.dailyNutrients.kind,
            provider: WidgetTimelineProvider(factory: DailyNutrientsEntriesFactory())
        ) { entry in
            DailyNutrientsEntryView(entry: entry)
                .unredacted()
        }
        .configurationDisplayName("DAILY_NUTRIENTS")
        .description("THIS_IS_AN_EXAMPLE_WIDGET")
        .supportedFamilies([.systemSmall, .systemMedium])
    }
}
