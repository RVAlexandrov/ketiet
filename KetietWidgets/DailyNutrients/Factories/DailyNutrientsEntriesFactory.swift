//
//  DaylyNutrientsFactory.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import WidgetKit

struct DailyNutrientsEntriesFactory {
    
    private func makePreviewDailyNutrients() -> NutritionMetrics {
        NutritionMetrics(
            currentCalories: 10,
            targetCalories: 100,
            currentFats: 30,
            targetFats: 100,
            currentProteins: 60,
            targetProteins: 100,
            currentCarbohydrates: 90,
            targetCarbohydrates: 100
        )
    }
    
    private func makePlaceholderDailyNutrients() -> NutritionMetrics {
        NutritionMetrics(
            currentCalories: 0,
            targetCalories: 100,
            currentFats: 0,
            targetFats: 100,
            currentProteins: 0,
            targetProteins: 100,
            currentCarbohydrates: 0,
            targetCarbohydrates: 100
        )
    }
    
}

extension DailyNutrientsEntriesFactory: TimelineEntriesFactoryProtocol {
    
    func makePreviewEntry(family: WidgetFamily) -> DailyNutrientsEntry {
        DailyNutrientsEntry(
            date: Date(),
            dailyNutrients: makePreviewDailyNutrients(),
            family: family
        )
    }
    
    func makePlaceholderEntry(family: WidgetFamily) -> DailyNutrientsEntry {
        DailyNutrientsEntry(
            date: Date(),
            dailyNutrients: makePlaceholderDailyNutrients(),
            family: family
        )
    }
    
    func makeEntryForSnapshot(family: WidgetFamily) -> DailyNutrientsEntry {
        DailyNutrientsEntry(
            date: Date(),
            dailyNutrients: DailyNutrientsStorage().widgetData,
            family: family
        )
    }
    
    func makeEntryForStartOfDate(date: Date, family: WidgetFamily) -> DailyNutrientsEntry {
        DailyNutrientsEntry(
            date: date,
            dailyNutrients: .zero(),
            family: family
        )
    }
}
