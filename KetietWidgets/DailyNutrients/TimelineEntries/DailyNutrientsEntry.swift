//
//  NearestMealEntry.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import WidgetKit
import DietCore

struct DailyNutrientsEntry: TimelineEntry {
    let date: Date
    let dailyNutrients: NutritionMetrics
    let family: WidgetFamily
}
