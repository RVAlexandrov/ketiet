//
//  CaloriesStack.swift
//  DailyNutrientsExtension
//
//  Created by Kovalev Alexander on 15.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

struct CaloriesStack: View {
    
    private enum Constants {
        static let verticalSpacing: CGFloat = 8
    }
    
    let currentValue: CGFloat
    let targetValue: CGFloat
    let measurement: String
    
    init(
        currentValue: Float,
        targetValue: Float,
        measurement: String
    ) {
        self.currentValue = CGFloat(currentValue)
        self.targetValue = CGFloat(targetValue)
        self.measurement = measurement
    }
    
    var body: some View {
        VStack(
            alignment: .center,
            spacing: Constants.verticalSpacing
        ) {
            Text(formatted(value: currentValue))
                .font(.littFont(font: .subTitle))
                .foregroundColor(.littNutrientAccentColor)
            Text("FROM")
            Text(formatted(value: targetValue))
                .font(.littFont(font: .subTitle))
                .foregroundColor(.littNutrientAccentColor)
            Text(measurement)
                .font(.littFont(font: .subTitle))
                .foregroundColor(.littNutrientAccentColor)
        }
    }
    
    private func formatted(value: CGFloat) -> String {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        return formatter.string(from: NSNumber(value: value)) ?? ""
    }
}
