//
//  DailyNutrientsEntryView.swift
//  DailyNutrientsExtension
//
//  Created by Kovalev Alexander on 15.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import SwiftUI
import WidgetKit

struct DailyNutrientsEntryView: View {
    var entry: DailyNutrientsEntry

    var body: some View {
        switch entry.family {
        case .systemSmall:
            DailyNutrientsViewSmall(dailyNutrients: entry.dailyNutrients)
                .padding()
                .background(Color(UIColor.tertiarySystemBackground))
        case .systemMedium:
            DailyNutrientsViewMedium(
                dailyNutrients: entry.dailyNutrients,
                nutrientsMeasurement: MeasurementService(type: .smallWeight).measurement(),
                energyMeasurement: MeasurementService(type: .energy).measurement()
            )
                .padding()
                .background(Color(UIColor.tertiarySystemBackground))
        case .systemExtraLarge, .systemLarge:
            EmptyView()
        @unknown default:
            EmptyView()
        }
    }
}

struct DailyNutrients_Previews: PreviewProvider {
    static var previews: some View {
        DailyNutrientsEntryView(
            entry: DailyNutrientsEntry(
                date: Date(),
                dailyNutrients: DailyNutrientsEntriesFactory().makePreviewEntry(family: .systemSmall).dailyNutrients,
                family: .systemSmall
            )
        )
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
