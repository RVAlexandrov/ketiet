//
//  DailyNutrientsViewMedium.swift
//  DailyNutrientsExtension
//
//  Created by Kovalev Alexander on 15.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import DietCore

struct DailyNutrientsViewMedium: View {
    
    private enum Constants {
        static let horizontalSpacing: CGFloat = 16
        static let verticalNutrientsSpacing: CGFloat = 8
    }
    
    let dailyNutrients: NutritionMetrics
    let nutrientsMeasurement: String
    let energyMeasurement: String

    var body: some View {
        HStack(
            alignment: .center,
            spacing: Constants.horizontalSpacing
        ) {
            VStack(
                alignment: .center,
                spacing: Constants.verticalNutrientsSpacing
            ) {
                NutrientStack(
                    name: Text("PROTEIN"),
                    color: .littProteinColor,
                    currentValue: dailyNutrients.currentProteins,
                    targetValue: dailyNutrients.targetProteins,
                    measurement: nutrientsMeasurement
                )
                NutrientStack(
                    name: Text("CARBOHYDRATE"),
                    color: .littCarbohydratesColor,
                    currentValue: dailyNutrients.currentCarbohydrates,
                    targetValue: dailyNutrients.targetCarbohydrates,
                    measurement: nutrientsMeasurement
                )
                NutrientStack(
                    name: Text("FAT"),
                    color: .littFatColor,
                    currentValue: dailyNutrients.currentFats,
                    targetValue: dailyNutrients.targetFats,
                    measurement: nutrientsMeasurement
                )
            }
            CaloriesStack(
                currentValue: dailyNutrients.currentCalories,
                targetValue: dailyNutrients.targetCalories,
                measurement: energyMeasurement
            )
        }
    }
}
