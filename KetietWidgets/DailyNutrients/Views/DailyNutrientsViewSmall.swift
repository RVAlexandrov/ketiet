//
//  NearestDishViewSmall.swift
//  Ketiet
//
//  Created by Kovalev Alexander on 05.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import DietCore

struct DailyNutrientsViewSmall: View {
    
    private enum Constants {
        static let verticalItemsSpacing: CGFloat = 8
    }
    
    let dailyNutrients: NutritionMetrics
    
    var body: some View {
        VStack(
            alignment: .center,
            spacing: Constants.verticalItemsSpacing
        ) {
            NutrientStack(
                name: Text("PROTEIN"),
                color: .littProteinColor,
                currentValue: dailyNutrients.currentProteins,
                targetValue: dailyNutrients.targetProteins
            )
            NutrientStack(
                name: Text("CARBOHYDRATE"),
                color: .littCarbohydratesColor,
                currentValue: dailyNutrients.currentCarbohydrates,
                targetValue: dailyNutrients.targetCarbohydrates
            )
            NutrientStack(
                name: Text("FAT"),
                color: .littFatColor,
                currentValue: dailyNutrients.currentFats,
                targetValue: dailyNutrients.targetFats
            )
        }
    }
}
