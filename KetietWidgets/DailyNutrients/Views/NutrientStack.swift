//
//  NutrientStack.swift
//  DailyNutrientsExtension
//
//  Created by Kovalev Alexander on 15.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

struct NutrientStack: View {
    
    private enum Constants {
        static let innerStackSpacing: CGFloat = 4
    }
    
    let name: Text
    let color: Color
    let currentValue: Float
    let targetValue: Float
    let measurement: String?
    
    init(
        name: Text,
        color: Color,
        currentValue: Float,
        targetValue: Float,
        measurement: String? = nil
    ) {
        self.name = name
        self.color = color
        self.currentValue = currentValue
        self.targetValue = targetValue
        self.measurement = measurement
    }
    
    var body: some View {
        VStack(
            alignment: .leading,
            spacing: Constants.innerStackSpacing
        ) {
            HStack(alignment: .center) {
                name
                Spacer()
                Text(progressString()).font(.caption)
            }
            ProgressBar(
                percent: currentValue.isZero ? 0 : currentValue / targetValue,
                color: color
            )
        }
    }

    private func progressString() -> String {
        guard let measurement = measurement else { return "" }
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        let current = formatter.string(from: NSNumber(value: currentValue)) ?? ""
        let target = formatter.string(from: NSNumber(value: targetValue)) ?? ""
        return "\(current)/\(target) \(measurement)"
    }
}
