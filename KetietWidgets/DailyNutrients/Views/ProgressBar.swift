//
//  ProgressBar.swift
//  DailyNutrientsExtension
//
//  Created by Kovalev Alexander on 15.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

struct ProgressBar: View {
    
    private let percent: CGFloat
    private let color: Color
    private let backgroundColor: Color
    private let height: CGFloat
    
    init(
        percent: Float = 0,
        color: Color = .blue,
        backgroundColor: Color = .black.opacity(0.1),
        height: CGFloat = 8
    ) {
        self.percent = CGFloat(percent)
        self.color = color
        self.backgroundColor = backgroundColor
        self.height = height
    }
    
    var body: some View {
        ZStack {
            GeometryReader { geometry in
                Capsule()
                    .fill(backgroundColor)
                    .frame(height: height)
                Capsule()
                    .fill(color)
                    .frame(
                        width: geometry.size.width * percent,
                        height: height
                    )
            }
        }
    }
}
