//
//  KetietWidgetsBundle.swift
//  DailyNutrientsExtension
//
//  Created by Kovalev Alexander on 17.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

@main
struct KetietWidgetsBundle: WidgetBundle {
    var body: some Widget {
        DailyNutrientsWidget()
        WaterConsumptionWidget()
    }
}
