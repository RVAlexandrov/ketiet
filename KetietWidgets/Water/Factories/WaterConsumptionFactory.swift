//
//  WaterConsumptionFactory.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 31.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import DietCore
import WidgetKit

struct WaterConsumptionFactory {
    
    private func makePreviewConsumption() -> WaterConsumptionMetrics {
        WaterConsumptionMetrics(
            currentWater: 60,
            targetWater: 100
        )
    }
    
    private func makePlaceholderConsumption() -> WaterConsumptionMetrics {
        WaterConsumptionMetrics(
            currentWater: 0,
            targetWater: 100
        )
    }
}

extension WaterConsumptionFactory: TimelineEntriesFactoryProtocol {
    
    func makePreviewEntry(family: WidgetFamily) -> WaterEntry {
        WaterEntry(
            date: Date(),
            water: makePreviewConsumption(),
            family: family
        )
    }
    
    func makePlaceholderEntry(family: WidgetFamily) -> WaterEntry {
        WaterEntry(
            date: Date(),
            water: makePlaceholderConsumption(),
            family: family
        )
    }
    
    func makeEntryForSnapshot(family: WidgetFamily) -> WaterEntry {
        WaterEntry(
            date: Date(),
            water: WaterConsumptionStorage().widgetData,
            family: family
        )
    }
    
    func makeEntryForStartOfDate(date: Date, family: WidgetFamily) -> WaterEntry {
        WaterEntry(
            date: date,
            water: .zero(),
            family: family
        )
    }
}
