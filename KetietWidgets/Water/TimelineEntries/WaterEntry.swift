//
//  WaterEntry.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 30.10.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import WidgetKit
import DietCore

struct WaterEntry: TimelineEntry {
    let date: Date
    let water: WaterConsumptionMetrics
    let family: WidgetFamily
}
