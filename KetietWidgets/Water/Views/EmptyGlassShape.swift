//
//  EmptyGlassView.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 01.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

struct EmptyGlassShape: Shape {
    
    private let lineWidth: CGFloat
    
    init(lineWidth: CGFloat = 5) {
        self.lineWidth = lineWidth
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        let topOvalRect = GlassShapeConstants.ovalRect(
            in: rect,
            progress: 1,
            lineWidth: lineWidth
        )
        path.addPath(Path(ellipseIn: topOvalRect))
        
        let bottomOvalRect = GlassShapeConstants.ovalRect(
            in: rect,
            progress: 0,
            lineWidth: lineWidth
        )
        let bottomOval = Path(ellipseIn: bottomOvalRect)
        let trimmedBottomOval = bottomOval.trimmedPath(from: 0, to: 0.5)
        path.addPath(trimmedBottomOval)
        
        path.move(to: CGPoint(x: topOvalRect.minX, y: topOvalRect.midY))
        path.addLine(to: CGPoint(x: bottomOvalRect.minX, y: bottomOvalRect.midY))
        
        path.move(to: CGPoint(x: topOvalRect.maxX, y: topOvalRect.midY))
        path.addLine(to: CGPoint(x: bottomOvalRect.maxX, y: bottomOvalRect.midY))
        
        return path
    }
 }
