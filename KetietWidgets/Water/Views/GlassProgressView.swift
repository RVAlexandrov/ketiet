//
//  GlassProgressView.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 01.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import WidgetKit

struct GlassProgressView: View {
    
    private enum Constants {
        static let lineWidth = 2.0
    }
    
    private let progress: CGFloat
    private let waterColor: Color
    private let overlayColor: Color
    
    init(
        progress: Double = 0,
        waterColor: Color = .blue,
        overlayColor: Color = .cyan
    ) {
        self.progress = CGFloat(min(max(progress, 0), 1))
        self.waterColor = waterColor
        self.overlayColor = overlayColor
    }
    
    var body: some View {
        ZStack {
            GlassWater(progress: progress)
                .fill(waterColor)
            GlassWaterOverlay(progress: progress)
                .stroke(lineWidth: Constants.lineWidth)
                .foregroundColor(overlayColor)
            EmptyGlassShape(lineWidth: Constants.lineWidth)
                .stroke(lineWidth: Constants.lineWidth)
                .foregroundColor(.gray)
        }
    }
}

struct GlassProgressView_Previews: PreviewProvider {
    static var previews: some View {
        GlassProgressView(progress: 0.5)
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
