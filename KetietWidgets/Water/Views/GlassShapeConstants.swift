//
//  GlassShapeConstants.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 01.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import CoreGraphics

enum GlassShapeConstants {
    static let progressMultiplier = 0.95
    private static let widthToHeightRatio = 2.4
    private static let widthMaxMultiplier = 0.625
    private static let widthMinMultiplier = 0.475
    
    static func ovalRect(
        in rect: CGRect,
        progress: CGFloat,
        lineWidth: CGFloat
    ) -> CGRect {
        let maxWidth = rect.width * widthMaxMultiplier
        let minWidth = rect.width * widthMinMultiplier
        let widthDelta = (maxWidth - minWidth) * (1 - progress)
        let width = maxWidth - widthDelta
        
        let minHeight = minWidth / widthToHeightRatio
        let height = width / widthToHeightRatio
        
        let minOriginX = (rect.width - maxWidth) / 2
        let maxOriginX = (rect.width - minWidth) / 2
        let originXDelta = (maxOriginX - minOriginX) * (1 - progress)
        let originX = minOriginX + originXDelta
        
        let minOriginY = lineWidth / 2
        let maxOriginY = rect.height - minHeight - lineWidth / 2
        let originYDelta = (maxOriginY - minOriginY) * (1 - progress)
        let originY = minOriginY + originYDelta
        
        return CGRect(
            x: originX,
            y: originY,
            width: width,
            height: height
        )
    }
}
