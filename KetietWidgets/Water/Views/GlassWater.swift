//
//  GlassWater.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 01.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

struct GlassWater: Shape {
    
    private enum Constants {
        static let lineWidth = 1.0
    }
    
    private let progress: CGFloat
    
    init(progress: CGFloat) {
        self.progress = progress * GlassShapeConstants.progressMultiplier
    }

    func path(in rect: CGRect) -> Path {
        guard progress > 0 else { return Path() }
        var path = Path()
        let topOvalRect = GlassShapeConstants.ovalRect(
            in: rect,
            progress: progress,
            lineWidth: Constants.lineWidth
        )
        let topOval = Path(ellipseIn: topOvalRect)
        let topOvalArc = topOval.trimmedPath(from: 0.5, to: 1)
        path.move(to: CGPoint(x: topOvalRect.minX, y: topOvalRect.midY))
        path.addPath(topOvalArc)
        
        let bottomOvalRect = GlassShapeConstants.ovalRect(
            in: rect,
            progress: 0,
            lineWidth: Constants.lineWidth
        )
        let bottomOval = Path(ellipseIn: bottomOvalRect)
        let bottomOvalArc = bottomOval.trimmedPath(from: 0, to: 0.5)
        
        path.addLine(to: CGPoint(x: bottomOvalRect.maxX, y: bottomOvalRect.midY))
        path.addPath(bottomOvalArc)
        path.addLine(to: CGPoint(x: topOvalRect.minX, y: topOvalRect.midY))
        return path
    }
 }
