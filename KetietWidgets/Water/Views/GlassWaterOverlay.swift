//
//  GlassWaterOverlay.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 02.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI

struct GlassWaterOverlay: Shape {
    
    private enum Constants {
        static let lineWidth = 1.0
    }
    
    private let progress: CGFloat
    
    init(progress: CGFloat) {
        self.progress = progress * GlassShapeConstants.progressMultiplier
    }
    
    func path(in rect: CGRect) -> Path {
        let topOvalRect = GlassShapeConstants.ovalRect(
            in: rect,
            progress: progress,
            lineWidth: Constants.lineWidth
        )
        let topOval = Path(ellipseIn: topOvalRect)
        return topOval.trimmedPath(from: 0, to: 0.5)
    }
}
