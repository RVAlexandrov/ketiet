//
//  WaterConsumptionEntryView.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 01.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import WidgetKit

struct WaterConsumptionEntryView: View {
    var entry: WaterEntry

    var body: some View {
        switch entry.family {
        case .systemSmall:
            WaterConsumptionViewSmall(water: entry.water)
                .padding()
                .background(Color(UIColor.tertiarySystemBackground))
        case .systemMedium:
            WaterConsumptionViewMedium(water: entry.water)
                .padding()
                .background(Color(UIColor.tertiarySystemBackground))
        case .systemExtraLarge, .systemLarge:
            EmptyView()
        @unknown default:
            EmptyView()
        }
    }
}

struct WaterConsumptionEntryView_Previews: PreviewProvider {
    static var previews: some View {
        WaterConsumptionEntryView(
            entry: WaterEntry(
                date: Date(),
                water: WaterConsumptionFactory().makePreviewEntry(family: .systemMedium).water,
                family: .systemMedium
            )
        )
            .preferredColorScheme(.light)
            .previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}
