//
//  WaterConsumptionViewMedium.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 02.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import DietCore

struct WaterConsumptionViewMedium: View {
    
    private enum Constants {
        static let textSpacing: CGFloat = 15
    }
    
    let water: WaterConsumptionMetrics
    
    var body: some View {
        HStack(alignment: .center) {
            VStack {
                Spacer()
                GlassProgressView(progress: water.percentComplete() >= 0.5 ? 1 : water.percentComplete() * 2)
                Spacer()
            }
            VStack {
                Spacer()
                GlassProgressView(progress: water.percentComplete() <= 0.5 ? 0 : (water.percentComplete() - 0.5) * 2)
                Spacer()
            }
            VStack(
                alignment: .center,
                spacing: Constants.textSpacing
            ) {
                WaterPercentView(
                    font: .title,
                    percent: water.percentComplete()
                )
                WaterProgressView(
                    water: water,
                    measurement: MeasurementService(type: .volume).measurement()
                )
            }
        }
    }
}
