//
//  WaterConsumptionViewSmall.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 02.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import DietCore

struct WaterConsumptionViewSmall: View {
    let water: WaterConsumptionMetrics
    
    var body: some View {
        VStack(alignment: .center) {
            GlassProgressView(progress: water.percentComplete())
            WaterPercentView(
                font: .body,
                percent: water.percentComplete()
            )
        }
    }
}
