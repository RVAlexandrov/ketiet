//
//  WaterPercentView.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 02.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import DietCore

struct WaterPercentView: View {
    let font: Font
    let percent: Double
    
    var body: some View {
        Text(NumberFormatter.formatPercent(value: percent))
            .font(font)
            .bold()
            .foregroundColor(.blue)
    }
}
