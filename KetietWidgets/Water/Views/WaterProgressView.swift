//
//  WaterProgressView.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 02.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import SwiftUI
import DietCore

struct WaterProgressView: View {
    
    let water: WaterConsumptionMetrics
    let measurement: String

    var body: some View {
        Text("\(Float(water.currentWater).cleanValue) / \(Float(water.targetWater).cleanValue) \(measurement)")
            .font(.body)
            .foregroundColor(.gray)
    }
}
