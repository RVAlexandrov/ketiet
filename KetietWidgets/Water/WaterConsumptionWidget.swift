//
//  WaterConsumptionWidget.swift
//  KetietWidgets
//
//  Created by Kovalev Alexander on 01.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//

import WidgetKit
import SwiftUI
import DietCore

struct WaterConsumptionWidget: Widget {

    var body: some WidgetConfiguration {
        StaticConfiguration(
            kind: WidgetType.waterConsumption.kind,
            provider: WidgetTimelineProvider(factory: WaterConsumptionFactory())
        ) { entry in
            WaterConsumptionEntryView(entry: entry)
                .unredacted()
        }
        .configurationDisplayName("WATER")
        .description("THIS_IS_AN_EXAMPLE_WIDGET")
        .supportedFamilies([.systemSmall, .systemMedium])
    }
}
