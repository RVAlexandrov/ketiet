//
//  ProgressModel+CoreDataProperties.swift
//  Ketiet
//
//  Created by Roman Aleksandrov on 02.11.2021.
//  Copyright © 2021 littDev. All rights reserved.
//
//

import Foundation
import CoreData


extension ProgressModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProgressModel> {
        let request = NSFetchRequest<ProgressModel>(entityName: "ProgressModel")
		request.returnsObjectsAsFaults = false
		request.relationshipKeyPathsForPrefetching = ["days"]
		return request
    }

    @NSManaged public var type: Int16
    @NSManaged public var dietId: Int16
    @NSManaged public var days: NSOrderedSet?

}

// MARK: Generated accessors for days
extension ProgressModel {

    @objc(insertObject:inDaysAtIndex:)
    @NSManaged public func insertIntoDays(_ value: DayProgressModel, at idx: Int)

    @objc(removeObjectFromDaysAtIndex:)
    @NSManaged public func removeFromDays(at idx: Int)

    @objc(insertDays:atIndexes:)
    @NSManaged public func insertIntoDays(_ values: [DayProgressModel], at indexes: NSIndexSet)

    @objc(removeDaysAtIndexes:)
    @NSManaged public func removeFromDays(at indexes: NSIndexSet)

    @objc(replaceObjectInDaysAtIndex:withObject:)
    @NSManaged public func replaceDays(at idx: Int, with value: DayProgressModel)

    @objc(replaceDaysAtIndexes:withDays:)
    @NSManaged public func replaceDays(at indexes: NSIndexSet, with values: [DayProgressModel])

    @objc(addDaysObject:)
    @NSManaged public func addToDays(_ value: DayProgressModel)

    @objc(removeDaysObject:)
    @NSManaged public func removeFromDays(_ value: DayProgressModel)

    @objc(addDays:)
    @NSManaged public func addToDays(_ values: NSOrderedSet)

    @objc(removeDays:)
    @NSManaged public func removeFromDays(_ values: NSOrderedSet)

}

extension ProgressModel : Identifiable {

}
